package com.truckz.shipper.notification;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.activity.PaymentPaytmActivity;
import com.truckz.shipper.activity.Splash_Activity;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.truckz.shipper.other.InternetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {

    private SharedPreferences pref;
    private Intent intent;
    private Random random;

    private String object="",BookingId="",cusId="",GrandTotal="",BookingType="",refId="",PaymentType="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("call", "onMessageReceived dsflkjsdalfjldsajflsda;jfl;dsajflsdjafsj");


        if (remoteMessage != null) {
            Map<String, String> data = remoteMessage.getData();

            if (data != null) {
                final String body = data.get("body");
                String title = data.get("title");
                String type = data.get("type");

                Log.e("call", "onMessageReceived message : "+remoteMessage.getData());
                if(type.equalsIgnoreCase("TripComplete"))
                {
                    object = data.get("result_data");



                    Log.e("MyAdroidFirebaseMs", "onMessageReceived : "+ object.toString());
                    try {
                        JSONObject jsonObject = new JSONObject(object);

                        if(jsonObject.has("PaymentType"))
                        {PaymentType = jsonObject.getString("PaymentType");}

                        if(jsonObject.has("PassengerId"))
                        {cusId = jsonObject.getString("PassengerId");}

                        if(jsonObject.has("ReferenceId"))
                        {refId=jsonObject.getString("ReferenceId");}

                        if(jsonObject.has("BookingId"))
                        {BookingId=jsonObject.getString("BookingId");}

                        if(jsonObject.has("BookingType"))
                        {BookingType = jsonObject.getString("BookingType");}

                        if(jsonObject.has("GrandTotal"))
                        {GrandTotal = jsonObject.getString("GrandTotal");}

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(PaymentType != null && PaymentType.equalsIgnoreCase("paytm")) {
                        //SessionSave.saveUserSession("paymentddata", object.toString(), this);
                    }

                }

                Log.e("call", "onMessageReceived body = " + body);
                Log.e("call", "onMessageReceived title = " + title);
                Log.e("call", "onMessageReceived type = " + type);
                Log.e("call", "onMessageReceived object = " + object);
                random = new Random();
                int m = random.nextInt(9999 - 1000) + 1000;

                if (type != null &&
                        type.equalsIgnoreCase("AccountBlocked") ) {
                    SessionSave.clearUserSession(this);
                    SessionSave.saveUserSession(Common.USER_ACCOUNT_BLOCK, body, this);

                    TruckzShipperApplication.getCurrentActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            callLogout(body);
                        }
                    });

                }else if(type!=null && type.equalsIgnoreCase("PassengerLogout")){

                    SessionSave.clearUserSession(this);


                    TruckzShipperApplication.getCurrentActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            callLogout(body);
                        }
                    });
                }
                else {
                    createNotificationAccountVerify(body, title, m, type);
                }
            } else {
                Log.e("call", "onMessageReceived data null");
            }
        } else {
            Log.e("call", "onMessageReceived remoteMessage null");
        }
    }

    private void callLogout(String message) {
        final Dialog dialog = new Dialog(TruckzShipperApplication.getCurrentActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_my_class);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_Message = dialog.findViewById(R.id.message);
        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        LinearLayout ll_Ok = dialog.findViewById(R.id.ok_layout);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        iv_close.setVisibility(View.GONE);

        tv_Message.setText(message);
        tv_title.setText(getString(R.string.info_message));

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                logout();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                logout();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                logout();
            }
        });

        dialog.show();
    }

    public void logout() {
        SessionSave.clearUserSession(this);

        Intent intent = new Intent(this, LoginActivity1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void createNotificationAccountVerify(String message, String title, int m, String type) {

        Log.e("call", "createNotification");

        if (type != null && type.equalsIgnoreCase("AcceptBooking")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "AcceptBooking", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else if (type != null && type.equalsIgnoreCase("OnTheWay")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "OnTheWay", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else if (type != null && type.equalsIgnoreCase("RejectBooking")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "RejectBooking", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else if (type != null && type.equalsIgnoreCase("Booking")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "Booking", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else if (type != null && type.equalsIgnoreCase("AdvanceBooking")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "AdvanceBooking", this);
            intent = new Intent(this, Splash_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        } else if (type != null && type.equalsIgnoreCase("AddMoney")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "AddMoney", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else if (type != null && type.equalsIgnoreCase("TransferMoney")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "TransferMoney", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        else if (type != null && type.equalsIgnoreCase("TripComplete")) {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "TripComplete", this);
            if(TruckzShipperApplication.getCurrentActivity() == null)
            {
                if(PaymentType != null && PaymentType.equalsIgnoreCase("paytm"))
                {
                    Log.e("call", "paymentPatm type : "+PaymentType );
                    generateNotification1(m,title,message);
                }
                else
                {
                    intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
                return;
            }
            else
            {
                /*----------------23-3-2020-------------------------*/
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                /*----------------23-3-2020-------------------------*/
                Log.e("call", "paymentPatm else : ");
                //generateNotification(m,title,message);
            }
            //return;
        }
        else {
            SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", this);
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorBlack);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setContentIntent(contentIntent)
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notificationManager.notify(m, notification);
        } else {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    // .setSmallIcon(R.drawable.ic_push_noti)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentIntent(contentIntent)
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//            ShortcutBadger.applyCount(getApplicationContext(), badgeCount); //for 1.1.4+
//            ShortcutBadger.applyNotification(getApplicationContext(), mNotificationBuilder.build(), badgeCount);

            notificationManager.notify(m, mNotificationBuilder.build());
        }

//        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_push_icon)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon))
//                .setContentTitle(title)
//                .setContentText(message)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setAutoCancel(true)
//                .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
//                .setSound(notificationSoundURI)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setContentIntent(contentIntent)
//                .setGroupSummary(true)
//                .setGroup("KEY_NOTIFICATION_GROUP");
//
//
//        ///.setGroup(KEY_NOTIFICATION_GROUP)
////        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//
////        ShortcutBadger.applyCount(getApplicationContext(), badgeCount); //for 1.1.4+
////        ShortcutBadger.applyNotification(getApplicationContext(), mNotificationBuilder.build(), badgeCount);
//
//        notificationManager.notify(m, mNotificationBuilder.build());
    }

    private void generateNotification(int m, String title, String message)
    {

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorBlack);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(m, notification);
        } else {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    // .setSmallIcon(R.drawable.ic_push_noti)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            mNotificationBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//            ShortcutBadger.applyCount(getApplicationContext(), badgeCount); //for 1.1.4+
//            ShortcutBadger.applyNotification(getApplicationContext(), mNotificationBuilder.build(), badgeCount);

            notificationManager.notify(m, mNotificationBuilder.build());
        }
    }

    private void generateNotification1(int m, String title, String message)
    {
        SessionSave.saveUserSession("paymentddata", object.toString(), this);

        intent = new Intent(this, MainActivity.class);

        intent.putExtra("BookingId",BookingId);
        intent.putExtra("cusId",cusId);
        intent.putExtra("GrandTotal",GrandTotal);
        intent.putExtra("BookingType",BookingType);
        intent.putExtra("refId",refId);
        intent.putExtra("PaymentType", PaymentType);



        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorBlack);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setContentIntent(contentIntent)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(m, notification);
        } else {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    // .setSmallIcon(R.drawable.ic_push_noti)
                    .setSmallIcon(R.drawable.ic_trucks_white)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable./*notification_icon_2*/notification_icon/*ic_launcher*/))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorTheme))
                    .setSound(notificationSoundURI)
                    .setContentIntent(contentIntent)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            mNotificationBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//            ShortcutBadger.applyCount(getApplicationContext(), badgeCount); //for 1.1.4+
//            ShortcutBadger.applyNotification(getApplicationContext(), mNotificationBuilder.build(), badgeCount);

            notificationManager.notify(m, mNotificationBuilder.build());
        }
    }


}