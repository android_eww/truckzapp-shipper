package com.truckz.shipper.notification;

import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import java.util.HashMap;

public class MyAndroidFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyAndroidFCMIIDService";
    private HashMap<String,String> divicePref = new HashMap<String, String>();

    @Override
    public void onTokenRefresh() {
        //Get hold of the registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log the token
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server

        SharedPreferences pref = getSharedPreferences("pref",MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token",token);
        editor.commit();

        FCMUtil.saveToken(this, token);
    }
}
