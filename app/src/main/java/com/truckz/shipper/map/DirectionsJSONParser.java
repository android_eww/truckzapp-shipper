package com.truckz.shipper.map;

/**
 * Created by ADMIN on 11/2/2017.
 */

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import com.google.android.gms.maps.model.LatLng;

public class DirectionsJSONParser {

    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try
        {

            Log.e("call","parse()");
            Log.e("call","jObject = "+jObject.toString());
            jRoutes = jObject.getJSONArray("routes");
            Log.e("call","parse() jRoutes.lenth = "+jRoutes.length());
            /** Traversing all routes */
            for(int i=0;i<jRoutes.length();i++)
            {

                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
//                Log.e("call","parse() jLegs.lenth = "+jLegs.length());
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for(int j=0;j<jLegs.length();j++)
                {
                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");
//                    Log.e("call","parse() jSteps.lenth = "+jSteps.length());
                    /** Traversing all steps */
                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

//                        Log.e("call","decodePoly list size = "+list.size());
                        /** Traversing all points */
                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                            hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        }
        catch (Exception e)
        {
            Log.e("call","Exception in DirectionsJSONParser = "+e.getMessage());
            routes = null;
        }

        return routes;
    }
    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        if (poly.size()>0)
        {
            for (int i=0; i<poly.size(); i++)
            {
//                Log.e("call","poly.lat = "+poly.get(i).latitude);
//                Log.e("call","poly.lng = "+poly.get(i).longitude);
            }
        }
        else
        {
//            Log.e("call","poly.size() is zero");
        }
        return poly;
    }
}