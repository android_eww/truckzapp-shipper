package com.truckz.shipper.been;

import java.io.Serializable;

public class NotificationBeen implements Serializable {

    private String Id;
    private String UserId;
    private String UserType;
    private String PushMessage;
    private String PushTitle;
    private String MessageType;
    private String CreatedDate;

    public NotificationBeen(String id, String userId, String userType, String pushTitle, String pushMessage, String messageType, String createdDate) {
        Id = id;
        UserId = userId;
        UserType = userType;
        PushTitle = pushTitle;
        PushMessage = pushMessage;
        MessageType = messageType;
        CreatedDate = createdDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getPushTitle() {
        return PushTitle;
    }

    public void setPushTitle(String pushTitle) {
        PushTitle = pushTitle;
    }

    public String getPushMessage() {
        return PushMessage;
    }

    public void setPushMessage(String pushMessage) {
        PushMessage = pushMessage;
    }

    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String messageType) {
        MessageType = messageType;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
