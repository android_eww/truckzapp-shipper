package com.truckz.shipper.been;


import java.util.List;

public class Model_Been {

    private String ModelId;
    private String ModelName;
    private String ModelImage;
    private boolean IsSelected;
    private List<Package_Been> Package;

    public Model_Been(String modelId, String modelName, String modelImage, boolean isSelected, List<Package_Been> aPackage) {
        ModelId = modelId;
        ModelName = modelName;
        ModelImage = modelImage;
        IsSelected = isSelected;
        Package = aPackage;
    }

    public boolean getIsSelected() {
        return IsSelected;
    }

    public void setIsSelected(boolean IsSelected) {
        IsSelected = IsSelected;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    public String getModelImage() {
        return ModelImage;
    }

    public void setModelImage(String modelImage) {
        ModelImage = modelImage;
    }

    public List<Package_Been> getPackage() {
        return Package;
    }

    public void setPackage(List<Package_Been> aPackage) {
        Package = aPackage;
    }

}
