package com.truckz.shipper.been;


public class TransportService_Been {

    private String Id,Name,Image,Status,CreatedDate;
    private boolean isSelected;

    public TransportService_Been(String Id, String Name, String Image, String Status, String CreatedDate, boolean isSelected)
    {
        this.Id=Id;
        this.Name=Name;
        this.Image=Image;
        this.Status=Status;
        this.CreatedDate=CreatedDate;
        this.isSelected=isSelected;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
