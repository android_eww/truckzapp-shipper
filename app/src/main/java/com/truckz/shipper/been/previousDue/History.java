package com.truckz.shipper.been.previousDue;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class History {

@SerializedName("Id")
@Expose
private String id;
@SerializedName("PickupLocation")
@Expose
private String pickupLocation;
@SerializedName("DropoffLocation")
@Expose
private String dropoffLocation;
@SerializedName("BookingDate")
@Expose
private String bookingDate;
@SerializedName("BookingId")
@Expose
private String bookingId;
@SerializedName("BookingType")
@Expose
private String bookingType;
@SerializedName("Amount")
@Expose
private String amount;
@SerializedName("UserType")
@Expose
private String userType;
@SerializedName("UserId")
@Expose
private String userId;
@SerializedName("CreatedDate")
@Expose
private String createdDate;
@SerializedName("ChargeType")
@Expose
private String chargeType;
@SerializedName("Status")
@Expose
private String status;
@SerializedName("Timestamp")
@Expose
private Integer timestamp;

    @SerializedName("ReferenceId")
    @Expose
    private String ReferenceId;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getPickupLocation() {
return pickupLocation;
}

public void setPickupLocation(String pickupLocation) {
this.pickupLocation = pickupLocation;
}

public String getDropoffLocation() {
return dropoffLocation;
}

public void setDropoffLocation(String dropoffLocation) {
this.dropoffLocation = dropoffLocation;
}

public String getBookingDate() {
return bookingDate;
}

public void setBookingDate(String bookingDate) {
this.bookingDate = bookingDate;
}

public String getBookingId() {
return bookingId;
}

public void setBookingId(String bookingId) {
this.bookingId = bookingId;
}

public String getBookingType() {
return bookingType;
}

public void setBookingType(String bookingType) {
this.bookingType = bookingType;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getUserType() {
return userType;
}

public void setUserType(String userType) {
this.userType = userType;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getCreatedDate() {
return createdDate;
}

public void setCreatedDate(String createdDate) {
this.createdDate = createdDate;
}

public String getChargeType() {
return chargeType;
}

public void setChargeType(String chargeType) {
this.chargeType = chargeType;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Integer getTimestamp() {
return timestamp;
}

public void setTimestamp(Integer timestamp) {
this.timestamp = timestamp;
}

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }
}


