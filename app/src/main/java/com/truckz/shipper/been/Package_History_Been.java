package com.truckz.shipper.been;

public class Package_History_Been {

    private String Id, PassengerId, PackageId, PickupLocation, PickupDate, PickupTime, TransactionId, Status, Date, PaymentType,
            PaymentStatus, details;
    private String detailsId, detailsModelId, detailsType, detailsName, detailsDescription, detailsTime, detailsKM, detailsAmount,
            detailsNotes, detailsDate;

    public Package_History_Been(String id, String passengerId, String packageId, String pickupLocation, String pickupDate, String pickupTime, String transactionId, String status, String date, String paymentType, String paymentStatus, String details, String detailsId, String detailsModelId, String detailsType, String detailsName, String detailsDescription, String detailsTime, String detailsKM, String detailsAmount, String detailsNotes, String detailsDate) {
        Id = id;
        PassengerId = passengerId;
        PackageId = packageId;
        PickupLocation = pickupLocation;
        PickupDate = pickupDate;
        PickupTime = pickupTime;
        TransactionId = transactionId;
        Status = status;
        Date = date;
        PaymentType = paymentType;
        PaymentStatus = paymentStatus;
        this.details = details;
        this.detailsId = detailsId;
        this.detailsModelId = detailsModelId;
        this.detailsType = detailsType;
        this.detailsName = detailsName;
        this.detailsDescription = detailsDescription;
        this.detailsTime = detailsTime;
        this.detailsKM = detailsKM;
        this.detailsAmount = detailsAmount;
        this.detailsNotes = detailsNotes;
        this.detailsDate = detailsDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getPackageId() {
        return PackageId;
    }

    public void setPackageId(String packageId) {
        PackageId = packageId;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getPickupDate() {
        return PickupDate;
    }

    public void setPickupDate(String pickupDate) {
        PickupDate = pickupDate;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetailsId() {
        return detailsId;
    }

    public void setDetailsId(String detailsId) {
        this.detailsId = detailsId;
    }

    public String getDetailsModelId() {
        return detailsModelId;
    }

    public void setDetailsModelId(String detailsModelId) {
        this.detailsModelId = detailsModelId;
    }

    public String getDetailsType() {
        return detailsType;
    }

    public void setDetailsType(String detailsType) {
        this.detailsType = detailsType;
    }

    public String getDetailsName() {
        return detailsName;
    }

    public void setDetailsName(String detailsName) {
        this.detailsName = detailsName;
    }

    public String getDetailsDescription() {
        return detailsDescription;
    }

    public void setDetailsDescription(String detailsDescription) {
        this.detailsDescription = detailsDescription;
    }

    public String getDetailsTime() {
        return detailsTime;
    }

    public void setDetailsTime(String detailsTime) {
        this.detailsTime = detailsTime;
    }

    public String getDetailsKM() {
        return detailsKM;
    }

    public void setDetailsKM(String detailsKM) {
        this.detailsKM = detailsKM;
    }

    public String getDetailsAmount() {
        return detailsAmount;
    }

    public void setDetailsAmount(String detailsAmount) {
        this.detailsAmount = detailsAmount;
    }

    public String getDetailsNotes() {
        return detailsNotes;
    }

    public void setDetailsNotes(String detailsNotes) {
        this.detailsNotes = detailsNotes;
    }

    public String getDetailsDate() {
        return detailsDate;
    }

    public void setDetailsDate(String detailsDate) {
        this.detailsDate = detailsDate;
    }
}
