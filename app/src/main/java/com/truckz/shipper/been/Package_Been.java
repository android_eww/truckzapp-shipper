package com.truckz.shipper.been;


public class Package_Been {

    private String Id;
    private String ModelId;
    private String Type;
    private String Name;
    private String Description;
    private String Time;
    private String KM;
    private String Amount;
    private String Notes;
    private String Date;

    public Package_Been(String id, String modelId, String type, String name, String description, String time, String KM, String amount, String notes, String date) {
        Id = id;
        ModelId = modelId;
        Type = type;
        Name = name;
        Description = description;
        Time = time;
        this.KM = KM;
        Amount = amount;
        Notes = notes;
        Date = date;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getKM() {
        return KM;
    }

    public void setKM(String KM) {
        this.KM = KM;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

}
