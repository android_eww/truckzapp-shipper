package com.truckz.shipper.been;

import java.io.Serializable;

public class Parcel_Been implements Serializable
{
    private String  Id,Name,Height,Width,Weight,Image,Status,CreatedDate;

    public Parcel_Been(String id, String name, String height, String width, String weight, String image, String status, String createdDate) {
        Id = id;
        Name = name;
        Height = height;
        Width = width;
        Weight = weight;
        Image = image;
        Status = status;
        CreatedDate = createdDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
