package com.truckz.shipper.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class MenuList_Been {

    private String Title,DescriptionOne,DescritpionTwo;
    private int Icon;

    public MenuList_Been(String Title, String DescriptionOne, String DescritpionTwo, int Icon)
    {
        this.Title=Title;
        this.DescriptionOne=DescriptionOne;
        this.DescritpionTwo=DescritpionTwo;
        this.Icon=Icon;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        Icon = icon;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescriptionOne() {
        return DescriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        DescriptionOne = descriptionOne;
    }

    public String getDescritpionTwo() {
        return DescritpionTwo;
    }

    public void setDescritpionTwo(String descritpionTwo) {
        DescritpionTwo = descritpionTwo;
    }
}
