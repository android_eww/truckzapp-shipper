package com.truckz.shipper.been.previousDue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreviousDue {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("history")
@Expose
private List<History> history = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<History> getHistory() {
return history;
}

public void setHistory(List<History> history) {
this.history = history;
}

}
