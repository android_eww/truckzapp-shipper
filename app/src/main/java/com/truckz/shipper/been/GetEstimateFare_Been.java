package com.truckz.shipper.been;


public class GetEstimateFare_Been {

    private String per_km_charge,id,name,base_fare,km,trip_fare,booking_fee,total,duration,sort, capacity;

    public GetEstimateFare_Been(
            String per_km_charge,
            String id,
            String name,
            String base_fare,
            String km,
            String trip_fare,
            String booking_fee,
            String total,
            String duration,
            String sort,
            String capacity
    )
    {
        this.per_km_charge=per_km_charge;
        this.id=id;
        this.name=name;
        this.base_fare=base_fare;
        this.km=km;
        this.trip_fare=trip_fare;
        this.booking_fee=booking_fee;
        this.total=total;
        this.duration=duration;
        this.sort=sort;
        this.capacity = capacity;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase_fare() {
        return base_fare;
    }

    public void setBase_fare(String base_fare) {
        this.base_fare = base_fare;
    }

    public String getBooking_fee() {
        return booking_fee;
    }

    public void setBooking_fee(String booking_fee) {
        this.booking_fee = booking_fee;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getPer_km_charge() {
        return per_km_charge;
    }

    public void setPer_km_charge(String per_km_charge) {
        this.per_km_charge = per_km_charge;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTrip_fare() {
        return trip_fare;
    }

    public void setTrip_fare(String trip_fare) {
        this.trip_fare = trip_fare;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
