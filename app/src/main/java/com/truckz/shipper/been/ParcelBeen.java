package com.truckz.shipper.been;

import java.io.Serializable;

public class ParcelBeen implements Serializable
{
    private String parcelName;
    private boolean isChecked;

    public ParcelBeen(String parcelName, boolean isChecked)
    {
        this.parcelName = parcelName;
        this.isChecked = isChecked;
    }

    public String getParcelName() {
        return parcelName;
    }

    public void setParcelName(String parcelName) {
        this.parcelName = parcelName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
