package com.truckz.shipper.been;


import java.io.Serializable;

public class CreditCard_List_Been implements Serializable {
    private String Id;
    private String CardNum;
    private String CardNum_;
    private String cardType;
    private String cardName;
    private String cardExpiry;


    public CreditCard_List_Been(String Id, String CardNum, String CardNum_, String cardType, String cardName, String cardExpiry) {
        this.Id = Id;
        this.CardNum = CardNum;
        this.CardNum_ = CardNum_;
        this.cardType = cardType;
        this.cardName = cardName;
        this.cardExpiry = cardExpiry;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCardNum() {
        return CardNum;
    }

    public void setCardNum(String cardNum) {
        CardNum = cardNum;
    }

    public String getCardNum_() {
        return CardNum_;
    }

    public void setCardNum_(String cardNum_) {
        CardNum_ = cardNum_;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }



    public String getCardExpiry() {
        return cardExpiry;
    }

    public void setCardExpiry(String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }
}
