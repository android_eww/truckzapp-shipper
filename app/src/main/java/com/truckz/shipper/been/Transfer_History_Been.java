package com.truckz.shipper.been;

/**
 * Created by EXCELLENT-14 on 31-Oct-17.
 */

public class Transfer_History_Been {

    private String Id,UserId,WalletId,UpdatedDate,Amount,Type,Description,Status;

    public Transfer_History_Been(String Id, String UserId, String WalletId,
                                 String UpdatedDate, String Amount, String Type, String Description, String Status)
    {
        this.Id = Id;
        this.UserId = UserId;
        this.WalletId = WalletId;
        this.UpdatedDate = UpdatedDate;
        this.Amount = Amount;
        this.Type = Type;
        this.Description = Description;
        this.Status = Status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getWalletId() {
        return WalletId;
    }

    public void setWalletId(String walletId) {
        WalletId = walletId;
    }
}
