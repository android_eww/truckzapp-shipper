package com.truckz.shipper.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class CarType_Been {

    private String Id,CategoryId,Name,Sort,BaseFare,MinKm,BelowAndAboveKmLimit,BelowPerKmCharge,AbovePerKmCharge;
    private String CancellationFee,NightChargeApplicable,NightCharge,NightTimeFrom,NightTimeTo,SpecialEventSurcharge;
    private String SpecialEventTimeFrom,SpecialEventTimeTo,WaitingTimePerMinuteCharge,MinuteFare,BookingFee,SpecialExtraCharge;
    private String Capacity,Image,Description,Status,Commission,Height,Width,ModelSizeImage;
    private int avai,selectedCar;

    public CarType_Been(String id, String categoryId, String name, String sort, String baseFare, String minKm, String belowAndAboveKmLimit, String belowPerKmCharge, String abovePerKmCharge, String cancellationFee, String nightChargeApplicable, String nightCharge, String nightTimeFrom, String nightTimeTo, String specialEventSurcharge, String specialEventTimeFrom, String specialEventTimeTo, String waitingTimePerMinuteCharge, String minuteFare, String bookingFee, String specialExtraCharge, String capacity, String image, String description, String status, String commission, String height, String width, String modelSizeImage, int avai, int selectedCar) {
        Id = id;
        CategoryId = categoryId;
        Name = name;
        Sort = sort;
        BaseFare = baseFare;
        MinKm = minKm;
        BelowAndAboveKmLimit = belowAndAboveKmLimit;
        BelowPerKmCharge = belowPerKmCharge;
        AbovePerKmCharge = abovePerKmCharge;
        CancellationFee = cancellationFee;
        NightChargeApplicable = nightChargeApplicable;
        NightCharge = nightCharge;
        NightTimeFrom = nightTimeFrom;
        NightTimeTo = nightTimeTo;
        SpecialEventSurcharge = specialEventSurcharge;
        SpecialEventTimeFrom = specialEventTimeFrom;
        SpecialEventTimeTo = specialEventTimeTo;
        WaitingTimePerMinuteCharge = waitingTimePerMinuteCharge;
        MinuteFare = minuteFare;
        BookingFee = bookingFee;
        SpecialExtraCharge = specialExtraCharge;
        Capacity = capacity;
        Image = image;
        Description = description;
        Status = status;
        Commission = commission;
        Height = height;
        Width = width;
        ModelSizeImage = modelSizeImage;
        this.avai = avai;
        this.selectedCar=selectedCar;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSort() {
        return Sort;
    }

    public void setSort(String sort) {
        Sort = sort;
    }

    public String getBaseFare() {
        return BaseFare;
    }

    public void setBaseFare(String baseFare) {
        BaseFare = baseFare;
    }

    public String getMinKm() {
        return MinKm;
    }

    public void setMinKm(String minKm) {
        MinKm = minKm;
    }

    public String getBelowAndAboveKmLimit() {
        return BelowAndAboveKmLimit;
    }

    public void setBelowAndAboveKmLimit(String belowAndAboveKmLimit) {
        BelowAndAboveKmLimit = belowAndAboveKmLimit;
    }

    public String getBelowPerKmCharge() {
        return BelowPerKmCharge;
    }

    public void setBelowPerKmCharge(String belowPerKmCharge) {
        BelowPerKmCharge = belowPerKmCharge;
    }

    public String getAbovePerKmCharge() {
        return AbovePerKmCharge;
    }

    public void setAbovePerKmCharge(String abovePerKmCharge) {
        AbovePerKmCharge = abovePerKmCharge;
    }

    public String getCancellationFee() {
        return CancellationFee;
    }

    public void setCancellationFee(String cancellationFee) {
        CancellationFee = cancellationFee;
    }

    public String getNightChargeApplicable() {
        return NightChargeApplicable;
    }

    public void setNightChargeApplicable(String nightChargeApplicable) {
        NightChargeApplicable = nightChargeApplicable;
    }

    public String getNightCharge() {
        return NightCharge;
    }

    public void setNightCharge(String nightCharge) {
        NightCharge = nightCharge;
    }

    public String getNightTimeFrom() {
        return NightTimeFrom;
    }

    public void setNightTimeFrom(String nightTimeFrom) {
        NightTimeFrom = nightTimeFrom;
    }

    public String getNightTimeTo() {
        return NightTimeTo;
    }

    public void setNightTimeTo(String nightTimeTo) {
        NightTimeTo = nightTimeTo;
    }

    public String getSpecialEventSurcharge() {
        return SpecialEventSurcharge;
    }

    public void setSpecialEventSurcharge(String specialEventSurcharge) {
        SpecialEventSurcharge = specialEventSurcharge;
    }

    public String getSpecialEventTimeFrom() {
        return SpecialEventTimeFrom;
    }

    public void setSpecialEventTimeFrom(String specialEventTimeFrom) {
        SpecialEventTimeFrom = specialEventTimeFrom;
    }

    public String getSpecialEventTimeTo() {
        return SpecialEventTimeTo;
    }

    public void setSpecialEventTimeTo(String specialEventTimeTo) {
        SpecialEventTimeTo = specialEventTimeTo;
    }

    public String getWaitingTimePerMinuteCharge() {
        return WaitingTimePerMinuteCharge;
    }

    public void setWaitingTimePerMinuteCharge(String waitingTimePerMinuteCharge) {
        WaitingTimePerMinuteCharge = waitingTimePerMinuteCharge;
    }

    public String getMinuteFare() {
        return MinuteFare;
    }

    public void setMinuteFare(String minuteFare) {
        MinuteFare = minuteFare;
    }

    public String getBookingFee() {
        return BookingFee;
    }

    public void setBookingFee(String bookingFee) {
        BookingFee = bookingFee;
    }

    public String getSpecialExtraCharge() {
        return SpecialExtraCharge;
    }

    public void setSpecialExtraCharge(String specialExtraCharge) {
        SpecialExtraCharge = specialExtraCharge;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCommission() {
        return Commission;
    }

    public void setCommission(String commission) {
        Commission = commission;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getModelSizeImage() {
        return ModelSizeImage;
    }

    public void setModelSizeImage(String modelSizeImage) {
        ModelSizeImage = modelSizeImage;
    }

    public int getAvai() {
        return avai;
    }

    public void setAvai(int avai) {
        this.avai = avai;
    }

    public int getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(int selectedCar) {
        this.selectedCar = selectedCar;
    }
}
