package com.truckz.shipper.been;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class LatLong_Been {

    private double Lat,Long;
    private int carType = -1;

    public LatLong_Been(double Lat, double Long)
    {
        this.Lat=Lat;
        this.Long=Long;
    }

    public LatLong_Been(double lat, double aLong, int carType) {
        Lat = lat;
        Long = aLong;
        this.carType = carType;
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double aLong) {
        Long = aLong;
    }
}
