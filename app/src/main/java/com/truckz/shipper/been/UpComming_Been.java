package com.truckz.shipper.been;

import java.util.List;

/**
 * Created by ADMIN on 10/21/2016.
 */
public class UpComming_Been {

    private String Id,CompanyId,PassengerId,ModelId,DriverId,CreatedDate,TransactionId,PaymentStatus,PickupTime;
    private String PickupDateTime,DropTime,TripDuration,TripDistance,PickupLocation,DropoffLocation,NightFareApplicable;
    private String NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,PromoCode,CompanyTax;
    private String SubTotal,GrandTotal,Status,Reason,PaymentType,AdminAmount,CompanyAmount,PassengerType,PassengerName;
    private String PassengerContact,FlightNumber,Model,DriverName,CarDetails,HistoryType,BookingType;
    private List<Parcel_Been> parcel_beens;

    public UpComming_Been(
            String Id,
            String CompanyId,
            String PassengerId,
            String ModelId,
            String DriverId,
            String CreatedDate,
            String TransactionId,
            String PaymentStatus,
            String PickupTime,
            String PickupDateTime,
            String DropTime,
            String TripDuration,
            String TripDistance,
            String PickupLocation,
            String DropoffLocation,
            String NightFareApplicable,
            String NightFare,
            String TripFare,
            String WaitingTime,
            String WaitingTimeCost,
            String TollFee,
            String BookingCharge,
            String Tax,
            String PromoCode,
            String CompanyTax,
            String SubTotal,
            String GrandTotal,
            String Status,
            String Reason,
            String PaymentType,
            String AdminAmount,
            String CompanyAmount,
            String PassengerType,
            String PassengerName,
            String PassengerContact,
            String FlightNumber,
            String Model,
            String DriverName,
            String CarDetails,
            String HistoryType,
            String BookingType,
            List<Parcel_Been> parcel_beens
    )
    {
        this.Id=Id;
        this.CompanyId=CompanyId;
        this.PassengerId=PassengerId;
        this.ModelId=ModelId;
        this.DriverId=DriverId;
        this.CreatedDate=CreatedDate;
        this.TransactionId=TransactionId;
        this.PaymentStatus=PaymentStatus;
        this.PickupTime=PickupTime;
        this.PickupDateTime=PickupDateTime;
        this.DropTime=DropTime;
        this.TripDuration=TripDuration;
        this.TripDistance=TripDistance;
        this.PickupLocation=PickupLocation;
        this.DropoffLocation=DropoffLocation;
        this.NightFareApplicable=NightFareApplicable;
        this.NightFare=NightFare;
        this.TripFare=TripFare;
        this.WaitingTime=WaitingTime;
        this.WaitingTimeCost=WaitingTimeCost;
        this.TollFee=TollFee;
        this.BookingCharge=BookingCharge;
        this.Tax=Tax;
        this.PromoCode=PromoCode;
        this.CompanyTax=CompanyTax;
        this.SubTotal=SubTotal;
        this.GrandTotal=GrandTotal;
        this.Status=Status;
        this.Reason=Reason;
        this.PaymentType=PaymentType;
        this.AdminAmount=AdminAmount;
        this.CompanyAmount=CompanyAmount;
        this.PassengerType=PassengerType;
        this.PassengerName=PassengerName;
        this.PassengerContact=PassengerContact;
        this.FlightNumber=FlightNumber;
        this.Model=Model;
        this.DriverName=DriverName;
        this.CarDetails=CarDetails;
        this.HistoryType=HistoryType;
        this.BookingType=BookingType;
        this.parcel_beens=parcel_beens;
    }

    public String getWaitingTime() {
        return WaitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        WaitingTime = waitingTime;
    }

    public String getTripFare() {
        return TripFare;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }

    public String getTripDuration() {
        return TripDuration;
    }

    public void setTripDuration(String tripDuration) {
        TripDuration = tripDuration;
    }

    public String getTripDistance() {
        return TripDistance;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getTollFee() {
        return TollFee;
    }

    public void setTollFee(String tollFee) {
        TollFee = tollFee;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getNightFareApplicable() {
        return NightFareApplicable;
    }

    public void setNightFareApplicable(String nightFareApplicable) {
        NightFareApplicable = nightFareApplicable;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getNightFare() {
        return NightFare;
    }

    public void setNightFare(String nightFare) {
        NightFare = nightFare;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getDropTime() {
        return DropTime;
    }

    public void setDropTime(String dropTime) {
        DropTime = dropTime;
    }

    public String getDriverId() {
        return DriverId;
    }

    public void setDriverId(String driverId) {
        DriverId = driverId;
    }

    public String getBookingCharge() {
        return BookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        BookingCharge = bookingCharge;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getAdminAmount() {
        return AdminAmount;
    }

    public void setAdminAmount(String adminAmount) {
        AdminAmount = adminAmount;
    }

    public String getCompanyAmount() {
        return CompanyAmount;
    }

    public void setCompanyAmount(String companyAmount) {
        CompanyAmount = companyAmount;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getCompanyTax() {
        return CompanyTax;
    }

    public void setCompanyTax(String companyTax) {
        CompanyTax = companyTax;
    }

    public void setCarDetails(String carDetails) {
        CarDetails = carDetails;
    }

    public String getCarDetails() {
        return CarDetails;
    }


    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }

    public String getHistoryType() {
        return HistoryType;
    }

    public void setHistoryType(String historyType) {
        HistoryType = historyType;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getPassengerContact() {
        return PassengerContact;
    }

    public void setPassengerContact(String passengerContact) {
        PassengerContact = passengerContact;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getPassengerType() {
        return PassengerType;
    }

    public void setPassengerType(String passengerType) {
        PassengerType = passengerType;
    }

    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        PickupDateTime = pickupDateTime;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }

    public List<Parcel_Been> getParcel_beens() {
        return parcel_beens;
    }

    public void setParcel_beens(List<Parcel_Been> parcel_beens) {
        this.parcel_beens = parcel_beens;
    }
}
