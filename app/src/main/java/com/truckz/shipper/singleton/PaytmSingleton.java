package com.truckz.shipper.singleton;

import android.content.Context;

import com.truckz.shipper.interfaces.CallbackPaytm;

public class PaytmSingleton
{
    private static CallbackPaytm callbackPaytm;
    private static PaytmSingleton instance;
    Context context;

    public PaytmSingleton(Context context,CallbackPaytm callbackPaytm)
    {
        this.context = context;
        this.callbackPaytm = callbackPaytm;
    }

    public static PaytmSingleton setCallBack(Context context,CallbackPaytm callbackPaytm)
    {
        instance = new PaytmSingleton(context,callbackPaytm);
        return instance;
    }

    public static CallbackPaytm getCallBack()
    {
        return callbackPaytm;
    }

}
