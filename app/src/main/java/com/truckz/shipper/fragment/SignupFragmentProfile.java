package com.truckz.shipper.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.other.GPSTracker;
import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import de.hdodenhof.circleimageview.CircleImageView;
import static android.app.Activity.RESULT_OK;

public class SignupFragmentProfile extends Fragment {

    public static String TAG = "SignupFragmentProfile";
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    public Context context;
    public LinearLayout layoutSignup;
    public CustomEditText editFirstName,editLastName,editReferralCode,editCity;
    static CircleImageView imageProfile;
    public Uri mImageUri;
    private Uri imageUri;
    public String picturePath;
    private static int RESULT_LOAD_IMAGE = 1;
    static Uri uriGallery;

    private RadioButton radioButtonMale,radioButtonFemale;
    private LinearLayout ll_Male,ll_Female;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;

    private CTextViewLight tv_date_of_birth;
    private int mYear, mMonth, mDay;
    private Dialog dialog;
    public SignupFragment parentFrag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_signup_profile, container,false);

        context = getContext();

        parentFrag = ((SignupFragment)SignupFragmentProfile.this.getParentFragment());

        mImageUri=null;
        imageUri=null;
        picturePath=null;
        uriGallery=null;

        LoginActivity1.gender = "male";
        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());
        openRun();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ll_RootLayout = rootView.findViewById(R.id.main_content);

        layoutSignup = rootView.findViewById(R.id.layout_signup);
        editFirstName = rootView.findViewById(R.id.input_first_name);
        editLastName = rootView.findViewById(R.id.input_last_name);
        editReferralCode = rootView.findViewById(R.id.input_referal_code);
        tv_date_of_birth = rootView.findViewById(R.id.tv_date_of_birth);
        imageProfile = rootView.findViewById(R.id.image_profile);
        editCity = rootView.findViewById(R.id.input_city);

        ll_Male = rootView.findViewById(R.id.radio_button_male_layout);
        ll_Female = rootView.findViewById(R.id.radio_button_female_layout);

        radioButtonMale = rootView.findViewById(R.id.radio_button_male);
        radioButtonFemale = rootView.findViewById(R.id.radio_button_female);

        mySnackBar = new MySnackBar(getActivity());

        ll_Male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonMale.setChecked(true);
                LoginActivity1.gender = "male";
                radioButtonFemale.setChecked(false);
            }
        });

        ll_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonMale.setChecked(false);
                LoginActivity1.gender = "female";
                radioButtonFemale.setChecked(true);
            }
        });

        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePickure();
            }
        });

        layoutSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Global.isNetworkconn(getActivity()))
                {
                    gotoSignup();
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        tv_date_of_birth.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectDateAndTime();
            }
        });

        setUserData();

        return rootView;
    }

    private void setUserData()
    {
        Log.e(TAG,"setUserData() LoginActivity.socialProURL:- " + LoginActivity1.socialProURL);
        Log.e(TAG,"setUserData() LoginActivity.FirstName:- " + LoginActivity1.FirstName);
        Log.e(TAG,"setUserData() LoginActivity.LastName:- " + LoginActivity1.LastName);

        if (LoginActivity1.FirstName != null &&
                !LoginActivity1.FirstName.equalsIgnoreCase("") &&
                !LoginActivity1.FirstName.equalsIgnoreCase("null"))
        {
            editFirstName.setText(LoginActivity1.FirstName);
        }

        if (LoginActivity1.LastName != null &&
                !LoginActivity1.LastName.equalsIgnoreCase("") &&
                !LoginActivity1.LastName.equalsIgnoreCase("null"))
        {
            editLastName.setText(LoginActivity1.LastName);
        }

    }

    public void takePickure()
    {
        dialog = new Dialog(getActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_camera);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);
        TextView dialog_camera_message = (TextView) dialog.findViewById(R.id.dialog_camera_message);
        TextView dialog_gallery_message = (TextView) dialog.findViewById(R.id.dialog_gallery_message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog_camera_message.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                if (parentFrag.checkAndRequestPermissions())
                {
                    camaraImage();
                }
            }
        });

        dialog_gallery_message.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                if (parentFrag.checkAndRequestPermissions())
                {
                    pickFromGallery();
                }
            }
        });
        dialog.show();
    }

    public void gotoSignup()
    {
        if (TextUtils.isEmpty(editFirstName.getText().toString().trim()) &&
                TextUtils.isEmpty(editLastName.getText().toString().trim()) && TextUtils.isEmpty(editCity.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_fill_all_details));
            editFirstName.setFocusableInTouchMode(true);
            editFirstName.requestFocus();
        }
        else
        {
            if (editFirstName.getText().toString()!=null &&
                    !editFirstName.getText().toString().equals(""))
            {
                if (editLastName.getText().toString() != null &&
                        !editLastName.getText().toString().equals(""))
                {
//                if (tv_date_of_birth.toString() != null &&
//                        !tv_date_of_birth.getText().toString().equalsIgnoreCase(""))
//                {
                    if(editCity.getText().toString() != null && !editCity.getText().toString().equalsIgnoreCase("")) {
                        LoginActivity1.FirstName = editFirstName.getText().toString().trim();
                        LoginActivity1.LastName = editLastName.getText().toString().trim();
                        LoginActivity1.referralCode = editReferralCode.getText().toString().trim();

                        if (LoginActivity1.token != null &&
                                !LoginActivity1.token.equalsIgnoreCase("")) {
                            registerUser();
                        } else {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));

                        }

                    }
                    else
                    {
                        mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_city));
                        editCity.setFocusableInTouchMode(true);
                        editCity.requestFocus();
                    }
//                }
//                else
//                {
//                    mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_date_of_birth));
//                    editLastName.setFocusableInTouchMode(true);
//                    editLastName.requestFocus();
//                }

                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_last_name));
                    editLastName.setFocusableInTouchMode(true);
                    editLastName.requestFocus();
                }
            }
            else
            {
                mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_first_name));
                editFirstName.setFocusableInTouchMode(true);
                editFirstName.requestFocus();
            }
        }


    }

    //Image Pick From Camera
    private void camaraImage()
    {
        if (SignupFragment.deviceName != null && SignupFragment.deviceName.toLowerCase().contains("samsung"))
        {
            Log.e(TAG,"camaraImage() Device Name1:- " + SignupFragment.deviceName);
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 0);
        }
        else
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, 0);
        }
    }

    //Image Pick From Gallary
    private void pickFromGallery()
    {
        Intent intent_gallery = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try
        {
            if ( resultCode == RESULT_OK )
            {

                if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGallery = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    SignupFragment.bitmapImage = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    SignupFragment.bitmapImage = getResizedBitmap(SignupFragment.bitmapImage,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    SignupFragment.bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, stream);
                    byte[] byteArray = stream.toByteArray();

                    LoginActivity1.userImage = byteArray;

                    imageProfile.setImageBitmap(SignupFragment.bitmapImage);

                }
                else if(requestCode==0)
                {

                    if (SignupFragment.deviceName != null && SignupFragment.deviceName.toLowerCase().contains("samsung"))
                    {
                        Log.e(TAG,"onActivityResult() Device Name2 :- " + SignupFragment.deviceName);

                        SignupFragment.bitmapImage = (Bitmap) data.getExtras().get("data");
                        Log.e(TAG,"onActivityResult() Height:- " +  SignupFragment.bitmapImage.getHeight() + "\nWidth:- " + SignupFragment.bitmapImage.getWidth());

                        Log.e(TAG,"onActivityResult() bmp height = "+ SignupFragment.bitmapImage.getHeight());
                        Log.e(TAG,"onActivityResult() bmp width = "+ SignupFragment.bitmapImage.getWidth());

                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        SignupFragment.bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                        LoginActivity1.userImage = bytes.toByteArray();
                        SignupFragment.cameraClick = true;
                        imageProfile.setImageBitmap(SignupFragment.bitmapImage);
                    }
                    else
                    {
                        Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                        Log.e(TAG,"onActivityResult() ######### bmp height = "+thumbnail.getHeight());
                        Log.e(TAG,"onActivityResult() ######### bmp width = "+thumbnail.getWidth());
                        thumbnail = getResizedBitmap(thumbnail,400);
                        SignupFragment.bitmapImage = thumbnail;
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                        LoginActivity1.userImage = bytes.toByteArray();
                        imageProfile.setImageBitmap(SignupFragment.bitmapImage);
                    }

                }
                else
                {
                    LoginActivity1.userImage = null;
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"onActivityResult() Exception in selecting image = "+e.getMessage());
        }
    }

    public static void signupResume(Bitmap bitmap)
    {
        Log.e(TAG,"signupResume() Device Name2 :- " + SignupFragment.deviceName);

        Log.e(TAG,"signupResume() Height:- " +  bitmap.getHeight() + "\n Width:- " + bitmap.getWidth());

        Log.e(TAG,"signupResume() bmp height = "+bitmap.getHeight());
        Log.e(TAG,"signupResume() bmp width = "+bitmap.getWidth());

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        SignupFragment.bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, bytes);
        LoginActivity1.userImage = bytes.toByteArray();
        imageProfile.setImageBitmap(SignupFragment.bitmapImage);

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static boolean validateFirstName(String firstName) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", firstName)) {

            check =true;

        } else {
            check=false;
        }
        return check;

    }
    // end method validateFirstName

    // validate last name
    public static boolean validateLastName( String lastName ){
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", lastName)) {

            check =true;

        } else {
            check=false;
        }
        return check;
    }

    private void openRun() {

        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyContactWrapper();
            // Marshmallow+
        } else {
            // Pre-Marshmallow
        }
    }

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Storage");
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
//                showMessageOKCancel(message,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
//                            }
//                        });
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
//        insertDummyContact();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
//                    insertDummyContact();
                } else {
                    // Permission Denied
//                    Toast.makeText(SplashActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
//                            .show();
                    openRun();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void registerUser()
    {
        Log.e(TAG,"registerUser()");

        GPSTracker gpsTracker = new GPSTracker(getActivity());

        LoginActivity1.latitude = gpsTracker.getLatitude()+"";
        LoginActivity1.longitude = gpsTracker.getLongitude()+"";

        String url = WebServiceAPI.API_REGISTER;

        //Email,MobileNo,Password,Gender,Firstname,Lastname,DeviceType(IOS : 1 , Android : 2),Token,Lat,Lng,Image

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL, LoginActivity1.Email);
        params.put(WebServiceAPI.PARAM_MOBILE_NUMBER, LoginActivity1.phonNumber);
        params.put(WebServiceAPI.PARAM_PASSWORD, LoginActivity1.Password);
        params.put(WebServiceAPI.PARAM_GENDER, LoginActivity1.gender);
        params.put(WebServiceAPI.PARAM_FIRST_NAME, LoginActivity1.FirstName);
        params.put(WebServiceAPI.PARAM_LAST_NAME, LoginActivity1.LastName);
        params.put(WebServiceAPI.PARAM_DEVICE_TYPE, LoginActivity1.deviceType);
        params.put(WebServiceAPI.PARAM_TOKEN, LoginActivity1.token);
        params.put(WebServiceAPI.PARAM_LAT, LoginActivity1.latitude);
        params.put(WebServiceAPI.PARAM_LONG, LoginActivity1.longitude);
        if(editCity.getText().toString() != null && !editCity.getText().toString().equalsIgnoreCase(""))
        {params.put(WebServiceAPI.PARAM_CITY, editCity.getText().toString());}
        if (LoginActivity1.userImage != null)
        {
            params.put(WebServiceAPI.PARAM_IMAGE, LoginActivity1.userImage);

        }
        params.put(WebServiceAPI.PARAM_REFERRAL_CODE, LoginActivity1.referralCode);
        params.put(WebServiceAPI.PARAM_DATE_OF_BIRTH, LoginActivity1.selectedDate);

        if (LoginActivity1.SocialId != null &&
                !LoginActivity1.SocialId.trim().equalsIgnoreCase(""))
        {
            Log.e(TAG,"registerUser() LoginActivity.SocialId:-  " + LoginActivity1.SocialId);
            Log.e(TAG,"registerUser() LoginActivity.SOCIAL_TYPE:-  " + LoginActivity1.SOCIAL_TYPE);

            params.put(WebServiceAPI.PARAM_SOCIAL_TYPE,LoginActivity1.SOCIAL_TYPE);
            params.put(WebServiceAPI.PARAM_SOCIAL_ID,LoginActivity1.SocialId);
        }

        Log.e(TAG, "registerUser() url = " + url);
        Log.e(TAG, "registerUser() param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"registerUser() responseCode  = " + responseCode);
                    Log.e(TAG,"registerUser() Response  = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walletBallence = json.getString("walletBalance");

                                    if (walletBallence!=null && !walletBallence.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBallence,getActivity());
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                }

                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",DOB="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",referalTotal="";

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = profile.getString("MobileNo");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("DOB"))
                                        {
                                            DOB = profile.getString("DOB");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }
                                        if (profile.has("City"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CITY,profile.getString("City"),getActivity());
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("ReferralAmount"))
                                        {
                                            referalTotal = profile.getString("ReferralAmount");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,DOB,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL,referalTotal,getActivity());
                                        SessionSave.saveUserSession(Common.CREATED_PASSCODE,"",getActivity());
                                        SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"0",getActivity());

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,"",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,"",getActivity());
                                    }
                                    else
                                    {
                                        Log.e(TAG,"registerUser() profile null");
                                    }
                                }
                                else
                                {
                                    Log.e(TAG,"registerUser() profile not available");
                                }

                                if (json.has("car_class"))
                                {
                                    JSONArray car_class = json.getJSONArray("car_class");

                                    if (car_class!=null && car_class.length()>0)
                                    {
                                        LoginActivity1.FirstName="";
                                        LoginActivity1.LastName="";
                                        LoginActivity1.Email="";
                                        LoginActivity1.phonNumber="";
                                        LoginActivity1.Password="";
                                        LoginActivity1.ConfirmPassword="";
                                        LoginActivity1.gender="male";
                                        LoginActivity1.latitude="";
                                        LoginActivity1.longitude="";
                                        LoginActivity1.deviceType="2";
                                        LoginActivity1.userImage = null;
                                        LoginActivity1.token="";
                                        LoginActivity1.referralCode="";
                                        LoginActivity1.SOCIAL_TYPE="";
                                        LoginActivity1.selectedDate="";

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),getActivity());
                                        dialogClass.hideDialog();
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                        LoginActivity1.activity.finish();
                                        ((LoginActivity1)getActivity()).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                    }
                                    else
                                    {
                                        Log.e(TAG,"registerUser() car_class null or lenth available");
                                        dialogClass.hideDialog();
                                    }
                                }
                                else
                                {
                                    Log.e(TAG,"registerUser() car_class not available");
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"registerUser() status false");
                                dialogClass.hideDialog();

                                String message = getResources().getString(R.string.something_went_wrong);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            Log.e(TAG,"registerUser() status not available");
                            dialogClass.hideDialog();

                            String message = getResources().getString(R.string.something_went_wrong);

                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }

                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        Log.e(TAG,"registerUser() json null ");
                        dialogClass.hideDialog();

                        String message = getResources().getString(R.string.something_went_wrong);

                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                        errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"registerUser() Exception "+e.toString());
                    dialogClass.hideDialog();

                    String message = getResources().getString(R.string.something_went_wrong);

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                    errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void selectDateAndTime()
    {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),R.style.datepicker,
                new DatePickerDialog.OnDateSetListener()
                {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth)
                    {
                        LoginActivity1.selectedDate = "";

                        if ((monthOfYear +1) < 10 )
                        {

                            if (dayOfMonth < 10)
                            {
                                LoginActivity1.selectedDate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
//                                tv_date_of_birth.setText(year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth);
                                tv_date_of_birth.setText("0" + dayOfMonth + "-0" + (monthOfYear + 1) + "-"  + year);
                            }
                            else
                            {
                                LoginActivity1.selectedDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
//                                tv_date_of_birth.setText(year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth);
                                tv_date_of_birth.setText(dayOfMonth + "-0" + (monthOfYear + 1) + "-"  + year);
                            }
                        }
                        else
                        {
                            if (dayOfMonth < 10)
                            {
                                LoginActivity1.selectedDate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
//                                tv_date_of_birth.setText(year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth);
                                tv_date_of_birth.setText("0" + dayOfMonth + "-" + (monthOfYear + 1) + "-"  + year);
                            }
                            else
                            {
                                LoginActivity1.selectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                tv_date_of_birth.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                tv_date_of_birth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }

                        Log.e(TAG,"selectDateAndTime() selectedDate Date:- "+ LoginActivity1.selectedDate);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }
}
