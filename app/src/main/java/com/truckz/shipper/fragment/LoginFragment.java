package com.truckz.shipper.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.view.CTextViewMedium;
import com.truckz.shipper.view.MySnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    public String TAG = "LoginFragment";

    public EditText et_Email, et_Password;
    public LinearLayout ll_ForgotPassword, ll_RootLayout;
    public CTextViewMedium textSignup, textviewLogin;
    public ImageView ivFacebook, ivGoogle;

    public DialogClass dialogClass;
    public AQuery aQuery;
    public GPSTracker gpsTracker;
    public MySnackBar mySnackBar;

    //google login
    private SignInButton btn_sign_in;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;

    //facebook login
    private CallbackManager callbackManager;
    private LoginButton login_button;
    public static boolean socialLogin = false;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        dialogClass = new DialogClass(LoginActivity1.activity,0);
        aQuery = new AQuery(LoginActivity1.activity);
        gpsTracker = new GPSTracker(LoginActivity1.activity);
        mySnackBar = new MySnackBar(LoginActivity1.activity);

        FacebookSdk.sdkInitialize(getActivity());
        AppEventsLogger.activateApp(getActivity());

        btn_sign_in = (SignInButton) view.findViewById(R.id.btn_sign_in);
        login_button = (LoginButton) view.findViewById(R.id.login_button);

        ll_RootLayout = view.findViewById(R.id.ll_rootView);
        et_Email = view.findViewById(R.id.input_email);
        et_Password = view.findViewById(R.id.input_pass);
      //  et_Email.setText("vishal@gmail.com");
       // et_Password.setText("12345678");
        ll_ForgotPassword = view.findViewById(R.id.forgot_password_layout);
        textviewLogin = view.findViewById(R.id.textview_login);
        textSignup = view.findViewById(R.id.textSignup);
        ivFacebook = view.findViewById(R.id.ivFacebook);
        ivGoogle = view.findViewById(R.id.ivGoogle);

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(),
                        this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        btn_sign_in.setSize(SignInButton.SIZE_STANDARD);
        btn_sign_in.setScopes(gso.getScopeArray());

        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("email");

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "init() login_button.registerCallback() onSuccess()");

                getUserDetail(loginResult);
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "init() login_button.registerCallback() onCancel()");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "init() login_button.registerCallback() onError() getMessage:- " + error.getMessage());
                Log.e(TAG, "init() login_button.registerCallback() onError() getLocalizedMessage:- " + error.getLocalizedMessage());
            }
        });

        ll_ForgotPassword.setOnClickListener(this);
        textviewLogin.setOnClickListener(this);
        textSignup.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivGoogle.setOnClickListener(this);

        finsHashKey();

        return view;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.forgot_password_layout:
                openForgotPasswordPopup();
                break;

            case R.id.textview_login:
                checkEmailPassword();
                break;

            case R.id.textSignup:

                LoginActivity1.FirstName = "";
                LoginActivity1.LastName = "";
                LoginActivity1.Email = "";
                LoginActivity1.SocialId = "";
                LoginActivity1.SOCIAL_TYPE = "";
                ((LoginActivity1)getActivity()).callSignup();
                break;

            case R.id.ivFacebook:
                if (Global.isNetworkconn(getActivity()))
                {
                    callFaceBook();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(getActivity().getResources().getString(R.string.please_check_your_internet_connection),
                            getActivity().getResources().getString(R.string.no_internet_connection));
                }
                break;

            case R.id.ivGoogle:
                if (Global.isNetworkconn(getActivity()))
                {
                    callGoogle();
//                    Intent intent = new Intent(getActivity(), SocialActivity.class);
//                    startActivity(intent);
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(getActivity().getResources().getString(R.string.please_check_your_internet_connection),
                            getActivity().getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }

    public void openForgotPasswordPopup()
    {
        final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_forgot_password);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Submit = dialog.findViewById(R.id.submit_textview);
        ImageView tv_Cancle = dialog.findViewById(R.id.dialog_close);
        final EditText input_email = dialog.findViewById(R.id.input_email);
        LinearLayout ll_Submin = dialog.findViewById(R.id.submit_layout);


        input_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().trim().length()>0)
                {
                    input_email.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ll_Submin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (input_email.getText()!=null && !input_email.getText().toString().trim().equals("") )
                {
                    if (isValidEmail(input_email.getText().toString().trim()))
                    {
                        dialog.dismiss();
                        if (Global.isNetworkconn(getActivity()))
                        {
                            callApiForgotPassword(input_email.getText().toString().trim());
                        }
                        else
                        {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),
                                    getResources().getString(R.string.no_internet_connection));
                        }
                    }
                    else
                    {
                        input_email.setError(getResources().getString(R.string.invalid_email));

                    }
                }
                else
                {
                    input_email.setError(getResources().getString(R.string.please_enter_email));
                }
            }
        });

        tv_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input_email.getText()!=null && !input_email.getText().toString().trim().equals("") )
                {
                    if (isValidEmail(input_email.getText().toString().trim()))
                    {
                        dialog.dismiss();
                        if (Global.isNetworkconn(getActivity()))
                        {
                            callApiForgotPassword(input_email.getText().toString().trim());
                        }
                        else
                        {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                        }
                    }
                    else
                    {
                        input_email.setError(getResources().getString(R.string.invalid_email));

                    }
                }
                else
                {
                    input_email.setError(getResources().getString(R.string.please_enter_email));
                }
            }
        });


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void callApiForgotPassword(String strEmail)
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_FORGOT_PASSWORD;

        //Username,Password,DeviceType,Lat,Lng,Token

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL,strEmail);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = "Please check your Mobile for forgot password";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                errorDialogClass.showDialog(message,getResources().getString(R.string.success_message));
                            }
                            else
                            {
                                if (json.has("message"))
                                {
                                    dialogClass.hideDialog();
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void checkEmailPassword()
    {
        Log.e(TAG,"checkEmailPassword()");

        if (Global.isNetworkconn(getActivity()))
        {
            ((LoginActivity1)getActivity()).getToken();

            if (et_Email != null &&
                    et_Email.getText().toString().trim().equalsIgnoreCase("") &&
                    et_Password != null &&
                    et_Password.getText().toString().trim().equalsIgnoreCase(""))
            {
                mySnackBar.showSnackBar(ll_RootLayout,getActivity().getResources().getString(R.string.please_fill_all_details));
                et_Email.setFocusableInTouchMode(true);
                et_Email.requestFocus();
            }
            else if (et_Email.getText().toString().trim().equalsIgnoreCase(""))
            {
                mySnackBar.showSnackBar(ll_RootLayout,getActivity().getResources().getString(R.string.please_enter_mobile_email));
                et_Email.setFocusableInTouchMode(true);
                et_Email.requestFocus();
            }
//            else if (!isValidEmail(et_Email.getText().toString().trim()))
//            {
//                mySnackBar.showSnackBar(ll_RootLayout,activity.getResources().getString(R.string.email_validation));
//                et_Email.setFocusableInTouchMode(true);
//                et_Email.requestFocus();
//            }
            else if (et_Password.getText().toString().trim().equals(""))
            {
                mySnackBar.showSnackBar(ll_RootLayout,getActivity().getResources().getString(R.string.please_enter_pass));
                et_Password.setFocusableInTouchMode(true);
                et_Password.requestFocus();
            }
            else if (et_Password.getText().toString().trim().length()<=5)
            {
                mySnackBar.showSnackBar(ll_RootLayout,getActivity().getResources().getString(R.string.pass_validation));
                et_Password.setFocusableInTouchMode(true);
                et_Password.requestFocus();
            }
            else
            {
                Log.e(TAG,"checkEmailPassword() locationFound = "+((LoginActivity1)getActivity()).locationFound);
                Log.e(TAG,"checkEmailPassword() permissionGranted = "+((LoginActivity1)getActivity()).permissionGranted);
                if (((LoginActivity1)getActivity()).token!=null && !((LoginActivity1)getActivity()).token.equalsIgnoreCase(""))
                {
                    login();
                }
                else
                {
                    ((LoginActivity1)getActivity()).getToken();
                    if (((LoginActivity1)getActivity()).token!=null && !((LoginActivity1)getActivity()).token.equalsIgnoreCase(""))
                    {
                        login();
                    }
                    else
                    {
                        ((LoginActivity1)getActivity()).getToken();
                        if (((LoginActivity1)getActivity()).token!=null && !((LoginActivity1)getActivity()).token.equalsIgnoreCase(""))
                        {
                            login();
                        }
                    }
                }
            }
        }
        else
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
            errorDialogClass.showDialog(getActivity().getResources().getString(R.string.please_check_internet_connection),
                    getActivity().getResources().getString(R.string.no_internet_connection));
        }
    }

    public void login()
    {
        Log.e(TAG,"login()");

        dialogClass.showDialog();
        String url = WebServiceAPI.API_LOGIN;

        ((LoginActivity1)getActivity()).latitude = gpsTracker.getLatitude()+"";
        ((LoginActivity1)getActivity()).longitude = gpsTracker.getLongitude()+"";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_USER_NAME, et_Email.getText().toString());
        params.put(WebServiceAPI.PARAM_PASSWORD, et_Password.getText().toString());
        params.put(WebServiceAPI.PARAM_DEVICE_TYPE,"2");
        params.put(WebServiceAPI.PARAM_LAT, LoginActivity1.latitude);
        params.put(WebServiceAPI.PARAM_LONG, LoginActivity1.longitude);
        params.put(WebServiceAPI.PARAM_TOKEN, LoginActivity1.token);

        Log.e(TAG,"login() url :- " + url);
        Log.e(TAG,"login() param :- " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"login() responseCode:- " + responseCode);
                    Log.e(TAG,"login() Response:- " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walletBallence = json.getString("walletBalance");

                                    if (walletBallence!=null && !walletBallence.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBallence,getActivity());
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",getActivity());
                                }

                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",DOB="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",Address="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",referalTotal="",BankName="",BSB="",BankAccountNo="",Description="";

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = profile.getString("MobileNo");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("BankName"))
                                        {
                                            BankName = profile.getString("BankName");
                                        }

                                        if (profile.has("BSB"))
                                        {
                                            BSB = profile.getString("BSB");
                                        }

                                        if (profile.has("BankAccountNo"))
                                        {
                                            BankAccountNo = profile.getString("BankAccountNo");
                                        }

                                        if (profile.has("Description"))
                                        {
                                            Description = profile.getString("Description");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("DOB"))
                                        {
                                            DOB = profile.getString("DOB");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("Address"))
                                        {
                                            Address = profile.getString("Address");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        if (profile.has("ReferralAmount"))
                                        {
                                            referalTotal = profile.getString("ReferralAmount");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,DOB,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL,referalTotal,getActivity());
                                        SessionSave.saveUserSession(Common.CREATED_PASSCODE,"",getActivity());
                                        SessionSave.saveUserSession(Common.IS_PASSCODE_REQUIRED,"0",getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,BankName,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,BSB,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,BankAccountNo,getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,Description,getActivity());

                                    }
                                    else
                                    {
                                        Log.e(TAG,"login() errorrr profile null");
                                    }

                                    if (json.has("car_class"))
                                    {
                                        JSONArray car_class = json.getJSONArray("car_class");

                                        for (int i=0; i<car_class.length(); i++)
                                        {
                                            Log.e(TAG,"login() car object = "+car_class.getJSONObject(i).toString());
                                        }

                                        if (car_class!=null && car_class.length()>0)
                                        {
                                            Log.e(TAG,"login() car_class.size = "+car_class.length());
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(),getActivity());
                                            dialogClass.hideDialog();
                                        }
                                        else
                                        {
                                            Log.e(TAG,"login() car_class null or lenth 0");
                                            dialogClass.hideDialog();
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                    }

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    ((LoginActivity1)getActivity()).finish();
                                    ((LoginActivity1)getActivity()).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                }
                                else
                                {
                                    Log.e(TAG,"login() profile not available");
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"login() status false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    mySnackBar.showSnackBar(ll_RootLayout,json.getString("message"));
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_try_agai_later));
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG,"login() status not available");
                            dialogClass.hideDialog();
                            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_try_agai_later));
                        }
                    }
                    else
                    {
                        Log.e(TAG,"login() json null ");
                        dialogClass.hideDialog();
                        mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_try_agai_later));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"login() Exception "+e.toString());
                    dialogClass.hideDialog();
                    mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_try_agai_later));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    //email validation
    private boolean isValidEmail(CharSequence target)
    {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callFaceBook()
    {
        Log.e(TAG,"callFaceBook()");

        ((LoginActivity1)getActivity()).getToken();
        LoginActivity1.SOCIAL_TYPE = LoginActivity1.FACEBOOK;
        login_button.performClick();
    }

    private void callGoogle()
    {
        Log.e(TAG,"callGoogle()");

        ((LoginActivity1)getActivity()).getToken();
        LoginActivity1.SOCIAL_TYPE = LoginActivity1.GOOGLE;
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result)
    {
        Log.e(TAG, "handleSignInResult():" + result.isSuccess());

        if (result.isSuccess())
        {
            Log.e(TAG, "handleSignInResult(): result.isSuccess()" );

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct.getDisplayName() != null &&
                    !acct.getDisplayName().equals(""))
            {
                LoginActivity1.FullName = acct.getDisplayName();

                String[] separated = LoginActivity1.FullName.split(" ");
                LoginActivity1.FirstName = separated[0];
                LoginActivity1.LastName = separated[1];
            }

            if (acct.getPhotoUrl() != null &&
                    !acct.getPhotoUrl().toString().equals(""))
            {
                LoginActivity1.socialProURL = acct.getPhotoUrl().toString();
            }

            if (acct.getEmail() != null &&
                    !acct.getEmail().toString().equals(""))
            {
                LoginActivity1.Email = acct.getEmail();
            }




            Log.e(TAG, "handleSignInResult() personName:- "  + LoginActivity1.FullName);
            Log.e(TAG, "handleSignInResult() socialMailId:- "  + LoginActivity1.Email);
            Log.e(TAG, "handleSignInResult() socialProURL:- "  + LoginActivity1.socialProURL);

            String getFamilyName = acct.getFamilyName();
            String getGivenName = acct.getGivenName();
            String getIdToken = acct.getIdToken();
            LoginActivity1.SocialId = acct.getId();
            String getServerAuthCode = acct.getServerAuthCode();
            String getAccount = acct.getAccount().toString();
            String getGrantedScopes = acct.getGrantedScopes().toString();

            Log.e(TAG, "handleSignInResult() getFamilyName:- "  + getFamilyName);
            Log.e(TAG, "handleSignInResult() getGivenName:- "  + getGivenName);
            Log.e(TAG, "handleSignInResult() getIdToken:- "  + getIdToken);
            Log.e(TAG, "handleSignInResult() SocialId:- "  + LoginActivity1.SocialId);
            Log.e(TAG, "handleSignInResult() getServerAuthCode:- "  + getServerAuthCode);
            Log.e(TAG, "handleSignInResult() getAccount:- "  + getAccount);
            Log.e(TAG, "handleSignInResult() getGrantedScopes:- "  + getGrantedScopes);

            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            socialLogin();
        }
        else
        {
            Log.e(TAG, "handleSignInResult(): result == false" );
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void getUserDetail(LoginResult loginResult)
    {
        Log.e(TAG, "getUserDetail()");

        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response)
                    {
                        LoginManager.getInstance().logOut();

                        JSONObject profile_pic_url, profile_pic_data;
                        String name = "", email, pictuer, pictuerURL, id;

                        try
                        {
                            Log.e(TAG, "getUserDetail() onCompleted() Try");

                            if(json_object.get("name") != null &&
                                    !json_object.get("name").toString().equals(""))
                            {
                                name = json_object.get("name").toString();

                                String[] separated = name.split(" ");
                                LoginActivity1.FirstName = separated[0];
                                LoginActivity1.LastName = separated[1];
                            }

                            if (json_object.has("email") &&
                                    json_object.get("email") != null &&
                                    !json_object.get("email").toString().equals(""))
                            {
                                LoginActivity1.Email = json_object.get("email").toString();
                            }

                            if (json_object.get("id") != null &&
                                    !json_object.get("id").toString().equals(""))
                            {
                                LoginActivity1.SocialId = json_object.get("id").toString();
                            }



                            Log.e(TAG, "getUserDetail() onCompleted() Try name:- " + name);
                            Log.e(TAG, "getUserDetail() onCompleted() Try FirstName:- " + LoginActivity1.FirstName);
                            Log.e(TAG, "getUserDetail() onCompleted() Try LastName:- " + LoginActivity1.LastName);
                            Log.e(TAG, "getUserDetail() onCompleted() Try socialMailId:-" + LoginActivity1.Email);
                            Log.e(TAG, "getUserDetail() onCompleted() Try SocialId:-" + LoginActivity1.SocialId);

                            pictuer = json_object.get("picture").toString();
                            Log.e(TAG, "getUserDetail() onCompleted() Try pictuer:-" + pictuer);

                            profile_pic_data = new JSONObject(json_object.getString("picture"));
                            profile_pic_url = new JSONObject(profile_pic_data.getString("data"));

                            LoginActivity1.socialProURL = profile_pic_url.getString("url");
                            Log.e(TAG , "getUserDetail() onCompleted() Try url socialProURL:- "+ LoginActivity1.socialProURL);

                            socialLogin();

                        }
                        catch (Exception e)
                        {
                            Log.e(TAG, "getUserDetail() onCompleted() catch:- " + e.getMessage());
                        }
                    }

                });

        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id, name, email, picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    private void finsHashKey()
    {
        Log.e(TAG, "finsHashKey()");

        try
        {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e(TAG, "finsHashKey() KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, "finsHashKey() Exception When generate key hash " + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG,"onActivityResult()");

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else
        {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void socialLogin()
    {
        Log.e(TAG,"socialLogin()");

        dialogClass.showDialog();
        String url = WebServiceAPI.API_SOCIAL_LOGIN;

        gpsTracker = new GPSTracker(getActivity());
        LoginActivity1.latitude = gpsTracker.getLatitude()+"";
        LoginActivity1.longitude = gpsTracker.getLongitude()+"";

        Map<String, Object> params = new HashMap<String, Object>();

        params.put(WebServiceAPI.PARAM_SOCIAL_ID,LoginActivity1.SocialId.trim());
        params.put(WebServiceAPI.PARAM_SOCIAL_TYPE,LoginActivity1.SOCIAL_TYPE.trim());
        params.put(WebServiceAPI.PARAM_DEVICE_TYPE,"2");
        params.put(WebServiceAPI.PARAM_TOKEN,LoginActivity1.token);
        params.put(WebServiceAPI.PARAM_FIRST_NAME,LoginActivity1.FirstName.trim());
        params.put(WebServiceAPI.PARAM_LAST_NAME,LoginActivity1.LastName.trim());
        params.put(WebServiceAPI.PARAM_LAT,LoginActivity1.latitude);
        params.put(WebServiceAPI.PARAM_LONG,LoginActivity1.longitude);
        params.put(WebServiceAPI.PARAM_EMAIL,LoginActivity1.Email);

        Log.e(TAG,"socialLogin() url = " + url);
        Log.e(TAG,"socialLogin() param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"socialLogin() responseCode= " + responseCode);
                    Log.e(TAG,"socialLogin() Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");

                                    if (walletBalance!=null && !walletBalance.toString().trim().equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBalance, getActivity());
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0", getActivity());
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0", getActivity());
                                }

                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Image="",Fullname="",Email="",Password="",MobileNo="",Gender="",AgeGroup="";
                                        String ZipCode="",Address="",DeviceType="",Token="",Lat="",Lng="",Status="";
                                        String CreatedDate="",QRCode="",Id="",ReferralCode="",ReferralAmount="",CompanyName="";
                                        String ABN="",BankName="",BSB="",BankAccountNo="",HomeNumber="";

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = profile.getString("MobileNo");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("AgeGroup"))
                                        {
                                            AgeGroup = profile.getString("AgeGroup");
                                        }

                                        if (profile.has("ZipCode"))
                                        {
                                            ZipCode = profile.getString("ZipCode");
                                        }

                                        if (profile.has("Address"))
                                        {
                                            Address = profile.getString("Address");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            ReferralCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = profile.getString("ReferralAmount");
                                        }

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("BankName"))
                                        {
                                            BankName = profile.getString("BankName");
                                        }

                                        if (profile.has("BSB"))
                                        {
                                            BSB = profile.getString("BSB");
                                        }

                                        if (profile.has("BankAccountNo"))
                                        {
                                            BankAccountNo = profile.getString("BankAccountNo");
                                        }

                                        if (profile.has("HomeNumber"))
                                        {
                                            HomeNumber = profile.getString("HomeNumber");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,AgeGroup, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,ReferralCode, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL,ReferralAmount, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,BankName, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,BSB, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,BankAccountNo, getActivity());
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address, getActivity());
//                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ZIP_CODE,ZipCode, getActivity());
//                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_HOME_NUMBER,ZipCode, getActivity());
                                    }
                                    else
                                    {
                                        Log.e(TAG,"socialLogin() profile null");
                                    }

                                    if (json.has("car_class"))
                                    {
                                        JSONArray car_class = json.getJSONArray("car_class");

                                        for (int i=0; i<car_class.length(); i++)
                                        {
                                            Log.e(TAG,"socialLogin() car object = "+car_class.getJSONObject(i).toString());
                                        }

                                        if (car_class!=null && car_class.length()>0)
                                        {
                                            Log.e(TAG,"socialLogin() car_class.size = "+car_class.length());
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS,car_class.toString(), getActivity());
                                            dialogClass.hideDialog();
                                        }
                                        else
                                        {
                                            Log.e(TAG,"socialLogin() car_class null or lenth 0");
                                            dialogClass.hideDialog();
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                    }

                                    Intent intent = new Intent( getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    ((LoginActivity1)getActivity()).finish();
                                    ((LoginActivity1)getActivity()).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                }
                                else
                                {
                                    Log.e(TAG,"socialLogin() profile not available");

                                    dialogClass.hideDialog();
                                    mySnackBar.showSnackBar(ll_RootLayout,"Please try again later!");
                                }
                            }
                            else
                            {
                                Log.e(TAG,"socialLogin() status false");
                                Log.e(TAG,"socialLogin() status false message" + json.getString("message"));
                                dialogClass.hideDialog();
                                socialLogin = true;
                                ((LoginActivity1)getActivity()).callSignup();
                            }
                        }
                        else
                        {
                            Log.e(TAG,"socialLogin() status not available");
                            dialogClass.hideDialog();
                            mySnackBar.showSnackBar(ll_RootLayout, getActivity().getResources().getString(R.string.please_try_agian_later));
                        }
                    }
                    else
                    {
                        Log.e(TAG,"socialLogin() json null ");
                        dialogClass.hideDialog();
                        mySnackBar.showSnackBar(ll_RootLayout, getActivity().getResources().getString(R.string.please_try_agian_later));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"socialLogin() Exception "+e.toString());
                    dialogClass.hideDialog();
                    mySnackBar.showSnackBar(ll_RootLayout, getActivity().getResources().getString(R.string.please_try_agian_later));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onResume()
    {
        Log.e(TAG,"onResume()");
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop()
    {
        Log.e(TAG,"onPause()");
        super.onStop();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }
}
