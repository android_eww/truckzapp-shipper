package com.truckz.shipper.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.R;
import com.truckz.shipper.adapter.UpComming_Adapter;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.been.PastBooking_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.view.CTextViewLight;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UpComming_Fragment extends Fragment {

    private String TAG = "UpComming_Fragment";
    public ExpandableRecyclerView recyclerView;
    public ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public CTextViewLight tv_NoDataFound;
    private SwipeRefreshLayout swipeRefreshLayout;

    private DialogClass dialogClass;
    private AQuery aQuery;

    public UpComming_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_up_comming, container, false);

        aQuery = new AQuery(getActivity());
        dialogClass = new DialogClass(getActivity(),0);

        recyclerView = (ExpandableRecyclerView) view.findViewById(R.id.rv_dispatched_job);
        tv_NoDataFound = view.findViewById(R.id.tv_NoDataFound);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        if (MyBookingActivity.upComming_beens!=null && MyBookingActivity.upComming_beens.size()>0)
        {
            recyclerView.setVisibility(View.VISIBLE);
            tv_NoDataFound.setVisibility(View.GONE);
        }
        else
        {
            recyclerView.setVisibility(View.GONE);
            tv_NoDataFound.setVisibility(View.VISIBLE);
        }

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new UpComming_Adapter((LinearLayoutManager) layoutManager, getActivity());
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                if (Global.isNetworkconn(getActivity()))
                {
                    callUpComingApi();
                }
                else
                {
                    swipeRefreshLayout.setRefreshing(false);
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(getActivity().getResources().getString(R.string.please_check_internet_connection),
                            getActivity().getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        return view;
    }

    private void callUpComingApi()
    {
        Log.e(TAG,"callUpComingApi()");

        String url = WebServiceAPI.API_UPCOMING_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,getActivity());

        Log.e(TAG,"callUpComingApi() url = " + url);

//        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callUpComingApi() responseCode= " + responseCode);
                    Log.e(TAG,"callUpComingApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                MyBookingActivity.upComming_beens.clear();
                                MyBookingActivity.MainList_Upcoming_Booking.clear();

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);
                                            List<Parcel_Been> parcel_beens = new ArrayList<Parcel_Been>();

                                            if (jsonObject!=null)
                                            {
                                                /////DistanceFare, Discount, Trash, CardId, ByDriverAmount, Notes
                                                String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="",PickupTime="", DropTime=""
                                                        ,TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="", NightFareApplicable="",NightFare="",TripFare="", DistanceFare=""
                                                        ,WaitingTime="", WaitingTimeCost="",TollFee="",BookingCharge="", Tax="", PromoCode="", Discount="", SubTotal="",GrandTotal=""
                                                        ,Status="", Trash="", Reason="",PaymentType="", CardId="", ByDriverAmount="", AdminAmount="", CompanyAmount="", PickupLat="",PickupLng=""
                                                        ,DropOffLat="",DropOffLon="", BookingType="", PassengerName="",PassengerContact="", Notes="", FlightNumber="",  NoOfPassenger=""
                                                        , NoOfLuggage="", Model="", DriverName="", CarDetails="",RequestFor="taxi";

                                                String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="", CarDetails_Company="",CarDetails_Color=""
                                                        ,CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="", CarDetails_VehicleInsuranceCertificate=""
                                                        ,CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="", CarDetails_VehicleImage=""
                                                        ,CarDetails_Description="", HistoryType="",
                                                        LoadingUnloadingTime="", LoadingUnloadingCharge="",ShareUrl="",
                                                        ParcelWeight="",ArrivedTime="",DriverMobileNo="",SpecialExtraCharge="",PreviousDue="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("CompanyId"))
                                                {
                                                    CompanyId = jsonObject.getString("CompanyId");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("ModelId"))
                                                {
                                                    ModelId = jsonObject.getString("ModelId");
                                                }

                                                if (jsonObject.has("DriverId"))
                                                {
                                                    DriverId = jsonObject.getString("DriverId");
                                                }

                                                if (jsonObject.has("CreatedDate"))
                                                {
                                                    CreatedDate = jsonObject.getString("CreatedDate");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("PickupDateTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupDateTime");
                                                }

                                                if (jsonObject.has("DropTime"))
                                                {
                                                    DropTime = jsonObject.getString("DropTime");
                                                }

                                                if (jsonObject.has("TripDuration"))
                                                {
                                                    TripDuration = jsonObject.getString("TripDuration");
                                                }

                                                if (jsonObject.has("TripDistance"))
                                                {
                                                    TripDistance = jsonObject.getString("TripDistance");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = jsonObject.getString("DropoffLocation");
                                                }

                                                if (jsonObject.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                }

                                                if (jsonObject.has("NightFare"))
                                                {
                                                    NightFare = jsonObject.getString("NightFare");
                                                }

                                                if (jsonObject.has("TripFare"))
                                                {
                                                    TripFare = jsonObject.getString("TripFare");
                                                }

                                                if (jsonObject.has("DistanceFare"))
                                                {
                                                    DistanceFare = jsonObject.getString("DistanceFare");
                                                }

                                                if (jsonObject.has("WaitingTime"))
                                                {
                                                    WaitingTime = jsonObject.getString("WaitingTime");
                                                }

                                                if (jsonObject.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                }

                                                if (jsonObject.has("TollFee"))
                                                {
                                                    TollFee = jsonObject.getString("TollFee");
                                                }

                                                if (jsonObject.has("BookingCharge"))
                                                {
                                                    BookingCharge = jsonObject.getString("BookingCharge");
                                                }

                                                if (jsonObject.has("Tax"))
                                                {
                                                    Tax = jsonObject.getString("Tax");
                                                }

                                                if (jsonObject.has("PromoCode"))
                                                {
                                                    PromoCode = jsonObject.getString("PromoCode");
                                                }

                                                if (jsonObject.has("Discount"))
                                                {
                                                    Discount = jsonObject.getString("Discount");
                                                }

                                                if (jsonObject.has("SubTotal"))
                                                {
                                                    SubTotal = jsonObject.getString("SubTotal");
                                                }

                                                if (jsonObject.has("GrandTotal"))
                                                {
                                                    GrandTotal = jsonObject.getString("GrandTotal");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Trash"))
                                                {
                                                    Trash = jsonObject.getString("Trash");
                                                }

                                                if (jsonObject.has("Reason"))
                                                {
                                                    Reason = jsonObject.getString("Reason");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("CardId"))
                                                {
                                                    CardId = jsonObject.getString("CardId");
                                                }

                                                if (jsonObject.has("ByDriverAmount"))
                                                {
                                                    ByDriverAmount = jsonObject.getString("ByDriverAmount");
                                                }

                                                if (jsonObject.has("AdminAmount"))
                                                {
                                                    AdminAmount = jsonObject.getString("AdminAmount");
                                                }

                                                if (jsonObject.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = jsonObject.getString("CompanyAmount");
                                                }

                                                if (jsonObject.has("PickupLat"))
                                                {
                                                    PickupLat = jsonObject.getString("PickupLat");
                                                }

                                                if (jsonObject.has("PickupLng"))
                                                {
                                                    PickupLng = jsonObject.getString("PickupLng");
                                                }

                                                if (jsonObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = jsonObject.getString("DropOffLat");
                                                }

                                                if (jsonObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = jsonObject.getString("DropOffLon");
                                                }

                                                if (jsonObject.has("BookingType"))
                                                {
                                                    BookingType = jsonObject.getString("BookingType");
                                                }

                                                if (jsonObject.has("PassengerName"))
                                                {
                                                    PassengerName = jsonObject.getString("PassengerName");
                                                }

                                                if (jsonObject.has("PassengerContact"))
                                                {
                                                    PassengerContact = jsonObject.getString("PassengerContact");
                                                }

                                                if (jsonObject.has("Notes"))
                                                {
                                                    Notes = jsonObject.getString("Notes");
                                                }

                                                if (jsonObject.has("FlightNumber"))
                                                {
                                                    FlightNumber = jsonObject.getString("FlightNumber");
                                                }

                                                if (jsonObject.has("NoOfPassenger"))
                                                {
                                                    NoOfPassenger = jsonObject.getString("NoOfPassenger");
                                                }

                                                if (jsonObject.has("NoOfLuggage"))
                                                {
                                                    NoOfLuggage = jsonObject.getString("NoOfLuggage");
                                                }

                                                if (jsonObject.has("Model"))
                                                {
                                                    Model = jsonObject.getString("Model");
                                                }

                                                if (jsonObject.has("DriverName"))
                                                {
                                                    DriverName = jsonObject.getString("DriverName");
                                                }

                                                if (jsonObject.has("RequestFor"))
                                                {
                                                    RequestFor = jsonObject.getString("RequestFor");
                                                }
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    HistoryType = jsonObject.getString("HistoryType");
                                                }
                                                if (jsonObject.has("LoadingUnloadingTime"))
                                                {
                                                    LoadingUnloadingTime = jsonObject.getString("LoadingUnloadingTime");
                                                }
                                                if (jsonObject.has("LoadingUnloadingCharge"))
                                                {
                                                    LoadingUnloadingCharge = jsonObject.getString("LoadingUnloadingCharge");
                                                }
                                                if (jsonObject.has("ShareUrl"))
                                                {
                                                    ShareUrl = jsonObject.getString("ShareUrl");
                                                }
                                                if (jsonObject.has("ParcelWeight"))
                                                {
                                                    ParcelWeight = jsonObject.getString("ParcelWeight");
                                                }
                                                if (jsonObject.has("ArrivedTime"))
                                                {
                                                    ArrivedTime = jsonObject.getString("ArrivedTime");
                                                }
                                                if (jsonObject.has("DriverMobileNo"))
                                                {
                                                    DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                }
                                                if (jsonObject.has("SpecialExtraCharge"))
                                                {
                                                    SpecialExtraCharge = jsonObject.getString("SpecialExtraCharge");
                                                }
                                                if (jsonObject.has("PreviousDue"))
                                                {
                                                    PreviousDue = jsonObject.getString("PreviousDue");
                                                }

                                                if (jsonObject.has("Parcel"))
                                                {
                                                    JSONObject Parcel = jsonObject.getJSONObject("Parcel");

                                                    if (Parcel!=null)
                                                    {
                                                        String Parcel_Id = "",Parcel_Name = "",Parcel_Height = "",Parcel_Width = "";
                                                        String Parcel_Weight= "",Parcel_Image= "",Parcel_Status= "",Parcel_CreatedDate= "";

                                                        if (Parcel.has("Id"))
                                                        {
                                                            Parcel_Id = Parcel.getString("Id");
                                                        }

                                                        if (Parcel.has("Name"))
                                                        {
                                                            Parcel_Name = Parcel.getString("Name");
                                                        }

                                                        if (Parcel.has("Height"))
                                                        {
                                                            Parcel_Height = Parcel.getString("Height");
                                                        }

                                                        if (Parcel.has("Width"))
                                                        {
                                                            Parcel_Width = Parcel.getString("Width");
                                                        }

                                                        if (Parcel.has("Weight"))
                                                        {
                                                            Parcel_Weight = Parcel.getString("Weight");
                                                        }

                                                        if (Parcel.has("Image"))
                                                        {
                                                            Parcel_Image = Parcel.getString("Image");
                                                        }

                                                        if (Parcel.has("Status"))
                                                        {
                                                            Parcel_Status = Parcel.getString("Status");
                                                        }

                                                        if (Parcel.has("CreatedDate"))
                                                        {
                                                            Parcel_CreatedDate = Parcel.getString("CreatedDate");
                                                        }

                                                        parcel_beens.add(new Parcel_Been(Parcel_Id,Parcel_Name,Parcel_Height,Parcel_Width,Parcel_Weight,Parcel_Image,Parcel_Status,Parcel_CreatedDate));
                                                    }
                                                }

                                                if (jsonObject.has("CarDetails"))
                                                {
                                                    CarDetails = jsonObject.getString("CarDetails");

                                                    JSONObject CarDetails1 = jsonObject.getJSONObject("CarDetails");

                                                    if (CarDetails1!=null)
                                                    {
                                                        if (CarDetails1.has("Id"))
                                                        {
                                                            CarDetails_Id = CarDetails1.getString("Id");
                                                        }

                                                        if (CarDetails1.has("CompanyId"))
                                                        {
                                                            CarDetails_CompanyId = CarDetails1.getString("CompanyId");
                                                        }

                                                        if (CarDetails1.has("VehicleModel"))
                                                        {
                                                            CarDetails_VehicleModel = CarDetails1.getString("VehicleModel");
                                                        }

                                                        if (CarDetails1.has("Company"))
                                                        {
                                                            CarDetails_Company = CarDetails1.getString("Company");
                                                        }

                                                        if (CarDetails1.has("Color"))
                                                        {
                                                            CarDetails_Color = CarDetails1.getString("Color");
                                                        }

                                                        if (CarDetails1.has("VehicleRegistrationNo"))
                                                        {
                                                            CarDetails_VehicleRegistrationNo = CarDetails1.getString("VehicleRegistrationNo");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificate"))
                                                        {
                                                            CarDetails_RegistrationCertificate = CarDetails1.getString("RegistrationCertificate");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificate"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificate = CarDetails1.getString("VehicleInsuranceCertificate");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificateExpire"))
                                                        {
                                                            CarDetails_RegistrationCertificateExpire = CarDetails1.getString("RegistrationCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificateExpire = CarDetails1.getString("VehicleInsuranceCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleImage"))
                                                        {
                                                            CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails1.getString("VehicleImage");
                                                        }

                                                        if (CarDetails1.has("Description"))
                                                        {
                                                            CarDetails_Description = CarDetails1.getString("Description");
                                                        }
                                                    }
                                                }

                                                MyBookingActivity.MainList_Upcoming_Booking.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime, LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,"",""));

                                                MyBookingActivity.upComming_beens.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime, LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,"",""));
                                            }
                                            else
                                            {
                                                Log.e(TAG,"callUpComingApi() jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"callUpComingApi() history null or lenth 0");
                                    }

                                    Log.e(TAG,"callUpComingApi() upComming_beens.size() = "+MyBookingActivity.upComming_beens.size());

                                    if (MyBookingActivity.upComming_beens.size()>0)
                                    {
                                        recyclerView.setVisibility(View.VISIBLE);
                                        tv_NoDataFound.setVisibility(View.GONE);
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        recyclerView.setVisibility(View.GONE);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                }
                                else
                                {
                                    Log.e(TAG,"callUpComingApi() history not found");
                                    recyclerView.setVisibility(View.GONE);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callUpComingApi() status false");
                                recyclerView.setVisibility(View.GONE);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callUpComingApi() status not found");
                            recyclerView.setVisibility(View.GONE);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callUpComingApi() json null");
                        recyclerView.setVisibility(View.GONE);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callUpComingApi() Exception "+e.toString());
                    recyclerView.setVisibility(View.GONE);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}