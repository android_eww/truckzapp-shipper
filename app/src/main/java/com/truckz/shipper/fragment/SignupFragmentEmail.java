package com.truckz.shipper.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.LoginActivity;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupFragmentEmail extends Fragment {

    private LinearLayout layout_next;
    private CustomEditText editPhone,editEmail,editPass,editConfirmPass;
    private LinearLayout ll_RootView;
    private MySnackBar mySnackBar;

    private DialogClass dialogClass;
    private AQuery aQuery;
    public SignupFragment parentFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_signup_email, container,false);

        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());

//        parentFragment = ((signupFragment)SignupFragment.this.getParentFragment());

        layout_next = rootView.findViewById(R.id.layout_next);
        editPhone = rootView.findViewById(R.id.input_phone);
        editEmail = rootView.findViewById(R.id.input_email);
        editPass = rootView.findViewById(R.id.input_pass);
        editConfirmPass = rootView.findViewById(R.id.input_confirm_pass);

        ll_RootView = rootView.findViewById(R.id.main_content);
        layout_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                goToNext();
//                gotoOtp();
            }
        });

        mySnackBar = new MySnackBar(getActivity());

        if (LoginActivity1.Email != null &&
                !LoginActivity1.Email.equalsIgnoreCase("") &&
                !LoginActivity1.Email.equalsIgnoreCase("null"))
        {
            editEmail.setText(LoginActivity1.Email);
            editEmail.setEnabled(false);
        }
        else
        {
            editEmail.setText("");
            editEmail.setEnabled(true);
        }

        return rootView;
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;

        if(!Pattern.matches("[0-9]+", phone)) {

            check =true;
        } else {
            check=false;
        }
        return check;
    }

    public void goToNext()
    {

        if (TextUtils.isEmpty(editPhone.getText().toString().trim()) &&
                TextUtils.isEmpty(editEmail.getText().toString().trim()) &&
                TextUtils.isEmpty(editPass.getText().toString().trim()) &&
                TextUtils.isEmpty(editConfirmPass.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.please_fill_all_details));
            editPhone.setFocusableInTouchMode(true);
            editPhone.requestFocus();
        }
        else
        {
            if (editPhone.getText().toString() != null&&
                    !editPhone.getText().toString().equals(""))
            {
                if (editEmail.getText().toString() != null &&
                        !editEmail.getText().toString().equals(""))
                {
                    if (editPass.getText().toString() != null &&
                            !editPass.getText().toString().equals(""))
                    {
                        if (editConfirmPass.getText().toString() != null &&
                                !editConfirmPass.getText().toString().equals(""))
                        {
                            if (editPhone.getText().toString().length() == 10)
                            {
                                if (isValidEmail(editEmail.getText().toString()))
                                {
                                    if (editPass.getText().toString().length()>=6)
                                    {
                                        if (editPass.getText().toString().equals(editConfirmPass.getText().toString()))
                                        {
                                            LoginActivity1.phonNumber = editPhone.getText().toString().trim();
                                            LoginActivity1.Email = editEmail.getText().toString().trim();
                                            LoginActivity1.Password = editPass.getText().toString().trim();
                                            LoginActivity1.ConfirmPassword = editConfirmPass.getText().toString().trim();

                                            if (Global.isNetworkconn(getActivity()))
                                            {
                                                verifyOtp();
                                            }
                                            else
                                            {
                                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                                errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                                            }

                                        }
                                        else
                                        {
                                            mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.password_and_confim_password_not_match));
                                            editConfirmPass.setFocusableInTouchMode(true);
                                            editConfirmPass.requestFocus();
                                        }
                                    }
                                    else
                                    {
                                        mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.password_lenth_must_be_greater_than_five));
                                        editPass.setFocusableInTouchMode(true);
                                        editPass.requestFocus();
                                    }
                                }
                                else
                                {
                                    mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.invalid_email));
                                    editEmail.setFocusableInTouchMode(true);
                                    editEmail.requestFocus();
                                }
                            }
                            else
                            {
                                mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.invalid_phone_number));
                                editPhone.setFocusableInTouchMode(true);
                                editPhone.requestFocus();
                            }
                        }
                        else
                        {
                            mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.please_enter_confirm_password));
                            editConfirmPass.setFocusableInTouchMode(true);
                            editConfirmPass.requestFocus();
                        }
                    }
                    else
                    {
                        mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.please_enter_password));
                        editPass.setFocusableInTouchMode(true);
                        editPass.requestFocus();
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.please_enter_email));
                    editEmail.setFocusableInTouchMode(true);
                    editEmail.requestFocus();
                }
            }
            else
            {
                mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.please_enter_phone_number));
                editPhone.setFocusableInTouchMode(true);
                editPhone.requestFocus();
            }
        }
    }

    private void verifyOtp()
    {
        GPSTracker gpsTracker = new GPSTracker(getActivity());

        LoginActivity1.latitude = gpsTracker.getLatitude()+"";
        LoginActivity1.longitude = gpsTracker.getLongitude()+"";

        String url = WebServiceAPI.API_REGISTER_OTP;

        //Email,MobileNo,Password,Gender,Firstname,Lastname,DeviceType(IOS : 1 , Android : 2),Token,Lat,Lng,Image

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_EMAIL, LoginActivity1.Email);
        params.put(WebServiceAPI.PARAM_MOBILE_NUMBER, LoginActivity1.phonNumber);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if ((json.has("message")))
                                {
                                    String message = json.getString("message");

                                    if (json.has("otp"))
                                    {
                                        Log.e("call","otp = ");
                                        LoginActivity1.otp = json.getString("otp");
                                        dialogClass.hideDialog();
                                        Log.e("call","otp = "+ LoginActivity1.otp);
                                        showSuccessMessage(message,getResources().getString(R.string.success_message));
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                        errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                            }
                            else
                            {
                                Log.e("errorrr","status false");
                                dialogClass.hideDialog();

                                String message = getResources().getString(R.string.something_went_wrong);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                                errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            Log.e("errorrr","status not available");
                            dialogClass.hideDialog();

                            String message = getResources().getString(R.string.something_went_wrong);

                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }

                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                            errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        Log.e("errorrr","json null ");
                        dialogClass.hideDialog();

                        String message = getResources().getString(R.string.something_went_wrong);

                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                        errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    String message = getResources().getString(R.string.something_went_wrong);

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                    errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessMessage(String message,String title)
    {
        final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_my_class);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        tv_Message.setText(message);
        tv_title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                gotoOtp();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                gotoOtp();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                gotoOtp();
            }
        });

        dialog.show();
    }

    public void gotoOtp()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SignupFragment.mViewPager.setCurrentItem(1);
            }
        },800);
    }
}

