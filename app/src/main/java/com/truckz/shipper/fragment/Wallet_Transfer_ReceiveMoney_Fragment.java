package com.truckz.shipper.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.squareup.picasso.Picasso;


public class Wallet_Transfer_ReceiveMoney_Fragment extends Fragment {

    private EditText et_setAmount;
    private ImageView iv_QrCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_receive_transfer_wallet, container, false);

        iv_QrCode = (ImageView) rootView.findViewById(R.id.qr_code_image);
        et_setAmount= (EditText) rootView.findViewById(R.id.et_setAmount);

        et_setAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){

                }
                return false;
            }
        });

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,getActivity())!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,getActivity()).equalsIgnoreCase(""))
        {
            iv_QrCode.setVisibility(View.VISIBLE);
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,getActivity()).contains(WebServiceAPI.BASE_URL_IMAGE))
            {
                Picasso.with(getActivity()).load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,getActivity())).into(iv_QrCode);
            }
            else
            {
                Picasso.with(getActivity()).load(WebServiceAPI.BASE_URL_IMAGE+"/"+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,getActivity())).into(iv_QrCode);
            }
        }
        else
        {
            iv_QrCode.setVisibility(View.INVISIBLE);
        }

        return rootView;
    }
}