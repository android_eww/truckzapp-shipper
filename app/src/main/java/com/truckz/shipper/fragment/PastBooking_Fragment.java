package com.truckz.shipper.fragment;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.activity.PaymentPaytmActivity;
import com.truckz.shipper.activity.Wallet_Add_Cards_Activity;
import com.truckz.shipper.adapter.PastBooking_Adapter;
import com.truckz.shipper.R;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.been.PastBooking_Been;
import com.truckz.shipper.been.previousDue.History;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.interfaces.CallbackPaytm;
import com.truckz.shipper.listner.OnLoadMoreListener;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.singleton.PaytmSingleton;
import com.truckz.shipper.view.CTextViewLight;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PastBooking_Fragment extends Fragment {

    public static String TAG = "PastBooking_Fragment";
    public static ExpandableRecyclerView recyclerView;
    public static PastBooking_Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static CTextViewLight tv_NoDataFound;
    public static SwipeRefreshLayout swipeRefreshLayout;

    DialogClass dialogClass;
    AQuery aQuery;
    CreditCard_List_Been selectedCard;
    TextView tv_type,tv_cardNo;//dialog
    ImageView iv_card;//dialog

//    public static boolean isReffreshing = false;
//    private static AQuery aQuery;
//    private DialogClass dialogClass;

    public PastBooking_Fragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_past_booking, container, false);

//        isReffreshing = false;

//        aQuery = new AQuery(getContext());
//        dialogClass = new DialogClass(getActivity(),0);

        recyclerView = (ExpandableRecyclerView) view.findViewById(R.id.rv_dispatched_job);
        tv_NoDataFound = view.findViewById(R.id.tv_NoDataFound);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        if (MyBookingActivity.pastBooking_beens!=null && MyBookingActivity.pastBooking_beens.size()>0)
        {
            recyclerView.setVisibility(View.VISIBLE);
            tv_NoDataFound.setVisibility(View.GONE);
        }
        else
        {
            recyclerView.setVisibility(View.GONE);
            tv_NoDataFound.setVisibility(View.VISIBLE);
        }

        dialogClass = new DialogClass(getActivity(),0);
        aQuery = new AQuery(getActivity());

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PastBooking_Adapter((LinearLayoutManager) layoutManager, recyclerView, getActivity(), new PastBooking_Adapter.onpayCliick() {
            @Override
            public void onPayButton(final PastBooking_Been history, final int position) {
                //openDialog(history,position);

                PaytmSingleton.setCallBack(getActivity(), new CallbackPaytm() {
                    @Override
                    public void onSuccess(String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }

                    @Override
                    public void onFailed(String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }

                    @Override
                    public void onNetworkError() {
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(getActivity());
                        errorDialogClass.showDialog("Network Problem","Error");
                    }

                    @Override
                    public void onClientAuthenticationFailed(String error, String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl, String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }

                    @Override
                    public void onBackPressedCancelTransaction(String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse, String status, String bookingType, String bookingId,String txId,String refId) {
                        callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getGrandTotal());
                    }
                });

                Intent intent = new Intent(getActivity(), PaymentPaytmActivity.class);
                intent.putExtra("BookingId",history.getId());
                intent.putExtra("cusId",history.getPassengerId());
                intent.putExtra("GrandTotal",history.getGrandTotal());
                intent.putExtra("BookingType",history.getBookingType());
                intent.putExtra("refId",history.getReferenceId());
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener()
        {
            @Override
            public void onLoadMore()
            {
                Log.e(TAG,"MyBookingActivity.priviousListSize1111 = "+MyBookingActivity.priviousListSize);
                Log.e(TAG,"storeBeensList.size()1111 = "+ MyBookingActivity.pastBooking_beens.size());

                if (MyBookingActivity.priviousListSize < MyBookingActivity.pastBooking_beens.size())
                {
                    {
                        Log.e(TAG,"MyBookingActivity.pastBooking_beens.add(null); ");
                        MyBookingActivity.pastBooking_beens.add(null);
                        mAdapter.notifyItemInserted(MyBookingActivity.pastBooking_beens.size());

                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                //add items one by one
                                Log.e(TAG,"MyBookingActivity.priviousListSize2222 = "+MyBookingActivity.priviousListSize);
                                Log.e(TAG,"MyBookingActivity.pastBooking_beens.size()2222 = "+MyBookingActivity.pastBooking_beens.size());

                                if (MyBookingActivity.priviousListSize < MyBookingActivity.pastBooking_beens.size())
                                {
                                    MyBookingActivity.priviousListSize = MyBookingActivity.pastBooking_beens.size();
                                    MyBookingActivity.pastPageIndex = MyBookingActivity.pastPageIndex + 1;
//                                    isReffreshing =true;
                                    Log.e(TAG,"MyBookingActivity.pastPageIndex " + MyBookingActivity.pastPageIndex);

                                    callPastApi();
                                }
                                else
                                {
                                    if (MyBookingActivity.pastBooking_beens != null && MyBookingActivity.pastBooking_beens.size() > 0)
                                    {
                                        MyBookingActivity.pastBooking_beens.remove(MyBookingActivity.pastBooking_beens.size() - 1);
                                        mAdapter.notifyItemRemoved(MyBookingActivity.pastBooking_beens.size());
                                        Log.e(TAG,"call loadmore = in else");
                                        mAdapter.setLoaded();
                                    }
                                }
                            }
                        }, 0);
                    }
                }
                else
                {
                    Log.e(TAG, "StoreFragment Load More Loading data completed");
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                if (Global.isNetworkconn(getActivity()))
                {
                    MyBookingActivity.priviousListSize = 0;
                    MyBookingActivity.pastPageIndex = 1;
                    callPastApi();
                }
                else
                {
                    swipeRefreshLayout.setRefreshing(false);
                    InternetDialog internetDialog = new InternetDialog(getActivity());
                    internetDialog.showDialog(getActivity().getResources().getString(R.string.please_check_internet_connection),
                            getActivity().getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        return view;
    }


    public static void callPastApi()
    {
        Log.e(TAG,"callPastApi()");

        String url = WebServiceAPI.API_PAST_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,MyBookingActivity.activity) + "/" +
                MyBookingActivity.pastPageIndex;// + "/" + MyBookingActivity.selectService;

        Log.e(TAG,"callPastApi() url = " + url);

//        dialogClass.showDialog();
        MyBookingActivity.aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callPastApi() responseCode= " + responseCode);
                    Log.e(TAG,"callPastApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (MyBookingActivity.pastPageIndex==1)
                                {
                                    if (MyBookingActivity.pastBooking_beens != null)
                                    {
                                        MyBookingActivity.pastBooking_beens.clear();
                                    }
                                }
                                else
                                {
                                    if (MyBookingActivity.pastBooking_beens != null && MyBookingActivity.pastBooking_beens.size() >0 )
                                    {
                                        MyBookingActivity.pastBooking_beens.remove(MyBookingActivity.pastBooking_beens.size() - 1);
                                        mAdapter.notifyItemRemoved(MyBookingActivity.pastBooking_beens.size() - 1);
                                    }
                                }

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);
                                            List<Parcel_Been> parcel_beens = new ArrayList<Parcel_Been>();

                                            if (jsonObject!=null)
                                            {
                                                /////DistanceFare, Discount, Trash, CardId, ByDriverAmount, Notes
                                                String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="",PickupTime="", DropTime=""
                                                        ,TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="", NightFareApplicable="",NightFare="",TripFare="", DistanceFare=""
                                                        ,WaitingTime="", WaitingTimeCost="",TollFee="",BookingCharge="", Tax="", PromoCode="", Discount="", SubTotal="",GrandTotal=""
                                                        ,Status="", Trash="", Reason="",PaymentType="", CardId="", ByDriverAmount="", AdminAmount="", CompanyAmount="", PickupLat="",PickupLng=""
                                                        ,DropOffLat="",DropOffLon="", BookingType="", PassengerName="",PassengerContact="", Notes="", FlightNumber="",  NoOfPassenger=""
                                                        , NoOfLuggage="", Model="", DriverName="", CarDetails="",RequestFor="taxi";

                                                String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="", CarDetails_Company="",CarDetails_Color=""
                                                        ,CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="", CarDetails_VehicleInsuranceCertificate=""
                                                        ,CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="", CarDetails_VehicleImage=""
                                                        ,CarDetails_Description="", HistoryType="",
                                                        LoadingUnloadingTime="", LoadingUnloadingCharge="", vehicalNo = "",ShareUrl="",
                                                        ParcelWeight="",ArrivedTime="",DriverMobileNo="",SpecialExtraCharge="",PreviousDue="",CancellationFee="",ReferenceId="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("CompanyId"))
                                                {
                                                    CompanyId = jsonObject.getString("CompanyId");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("ModelId"))
                                                {
                                                    ModelId = jsonObject.getString("ModelId");
                                                }

                                                if (jsonObject.has("DriverId"))
                                                {
                                                    DriverId = jsonObject.getString("DriverId");
                                                }

                                                if (jsonObject.has("CreatedDate"))
                                                {
                                                    CreatedDate = jsonObject.getString("CreatedDate");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("PickupTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupTime");
                                                }

                                                if (jsonObject.has("DropTime"))
                                                {
                                                    DropTime = jsonObject.getString("DropTime");
                                                }

                                                if (jsonObject.has("TripDuration"))
                                                {
                                                    TripDuration = jsonObject.getString("TripDuration");
                                                }

                                                if (jsonObject.has("TripDistance"))
                                                {
                                                    TripDistance = jsonObject.getString("TripDistance");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = jsonObject.getString("DropoffLocation");
                                                }

                                                if (jsonObject.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                }

                                                if (jsonObject.has("NightFare"))
                                                {
                                                    NightFare = jsonObject.getString("NightFare");
                                                }

                                                if (jsonObject.has("TripFare"))
                                                {
                                                    TripFare = jsonObject.getString("TripFare");
                                                }

                                                if (jsonObject.has("DistanceFare"))
                                                {
                                                    DistanceFare = jsonObject.getString("DistanceFare");
                                                }

                                                if (jsonObject.has("WaitingTime"))
                                                {
                                                    WaitingTime = jsonObject.getString("WaitingTime");
                                                }

                                                if (jsonObject.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                }

                                                if (jsonObject.has("TollFee"))
                                                {
                                                    TollFee = jsonObject.getString("TollFee");
                                                }

                                                if (jsonObject.has("BookingCharge"))
                                                {
                                                    BookingCharge = jsonObject.getString("BookingCharge");
                                                }

                                                if (jsonObject.has("Tax"))
                                                {
                                                    Tax = jsonObject.getString("Tax");
                                                }

                                                if (jsonObject.has("PromoCode"))
                                                {
                                                    PromoCode = jsonObject.getString("PromoCode");
                                                }

                                                if (jsonObject.has("Discount"))
                                                {
                                                    Discount = jsonObject.getString("Discount");
                                                }

                                                if (jsonObject.has("SubTotal"))
                                                {
                                                    SubTotal = jsonObject.getString("SubTotal");
                                                }

                                                if (jsonObject.has("GrandTotal"))
                                                {
                                                    GrandTotal = jsonObject.getString("GrandTotal");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Trash"))
                                                {
                                                    Trash = jsonObject.getString("Trash");
                                                }

                                                if (jsonObject.has("Reason"))
                                                {
                                                    Reason = jsonObject.getString("Reason");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("CardId"))
                                                {
                                                    CardId = jsonObject.getString("CardId");
                                                }

                                                if (jsonObject.has("ByDriverAmount"))
                                                {
                                                    ByDriverAmount = jsonObject.getString("ByDriverAmount");
                                                }

                                                if (jsonObject.has("AdminAmount"))
                                                {
                                                    AdminAmount = jsonObject.getString("AdminAmount");
                                                }

                                                if (jsonObject.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = jsonObject.getString("CompanyAmount");
                                                }

                                                if (jsonObject.has("PickupLat"))
                                                {
                                                    PickupLat = jsonObject.getString("PickupLat");
                                                }

                                                if (jsonObject.has("PickupLng"))
                                                {
                                                    PickupLng = jsonObject.getString("PickupLng");
                                                }

                                                if (jsonObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = jsonObject.getString("DropOffLat");
                                                }

                                                if (jsonObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = jsonObject.getString("DropOffLon");
                                                }

                                                if (jsonObject.has("BookingType"))
                                                {
                                                    BookingType = jsonObject.getString("BookingType");
                                                }

                                                if (jsonObject.has("PassengerName"))
                                                {
                                                    PassengerName = jsonObject.getString("PassengerName");
                                                }

                                                if (jsonObject.has("PassengerContact"))
                                                {
                                                    PassengerContact = jsonObject.getString("PassengerContact");
                                                }

                                                if (jsonObject.has("Notes"))
                                                {
                                                    Notes = jsonObject.getString("Notes");
                                                }

                                                if (jsonObject.has("FlightNumber"))
                                                {
                                                    FlightNumber = jsonObject.getString("FlightNumber");
                                                }

                                                if (jsonObject.has("NoOfPassenger"))
                                                {
                                                    NoOfPassenger = jsonObject.getString("NoOfPassenger");
                                                }

                                                if (jsonObject.has("NoOfLuggage"))
                                                {
                                                    NoOfLuggage = jsonObject.getString("NoOfLuggage");
                                                }

                                                if (jsonObject.has("Model"))
                                                {
                                                    Model = jsonObject.getString("Model");
                                                }

                                                if (jsonObject.has("DriverName"))
                                                {
                                                    DriverName = jsonObject.getString("DriverName");
                                                }

                                                if (jsonObject.has("RequestFor"))
                                                {
                                                    RequestFor = jsonObject.getString("RequestFor");
                                                }
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    HistoryType = jsonObject.getString("HistoryType");
                                                }
                                                if (jsonObject.has("LoadingUnloadingTime"))
                                                {
                                                    LoadingUnloadingTime = jsonObject.getString("LoadingUnloadingTime");
                                                }
                                                if (jsonObject.has("LoadingUnloadingCharge"))
                                                {
                                                    LoadingUnloadingCharge = jsonObject.getString("LoadingUnloadingCharge");
                                                }
                                                if (jsonObject.has("ShareUrl"))
                                                {
                                                    ShareUrl = jsonObject.getString("ShareUrl");
                                                }
                                                if (jsonObject.has("ParcelWeight"))
                                                {
                                                    ParcelWeight = jsonObject.getString("ParcelWeight");
                                                }
                                                if (jsonObject.has("ArrivedTime"))
                                                {
                                                    ArrivedTime = jsonObject.getString("ArrivedTime");
                                                }
                                                if (jsonObject.has("DriverMobileNo"))
                                                {
                                                    DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                }
                                                if (jsonObject.has("SpecialExtraCharge"))
                                                {
                                                    SpecialExtraCharge = jsonObject.getString("SpecialExtraCharge");
                                                }
                                                if (jsonObject.has("PreviousDue"))
                                                {
                                                    PreviousDue = jsonObject.getString("PreviousDue");
                                                }
                                                if (jsonObject.has("CancellationFee"))
                                                {
                                                    CancellationFee = jsonObject.getString("CancellationFee");
                                                }
                                                if (jsonObject.has("ReferenceId"))
                                                {
                                                    ReferenceId = jsonObject.getString("ReferenceId");
                                                }

                                                if (jsonObject.has("Parcel"))
                                                {
                                                    JSONObject Parcel = jsonObject.getJSONObject("Parcel");

                                                    if (Parcel!=null)
                                                    {
                                                        String Parcel_Id = "",Parcel_Name = "",Parcel_Height = "",Parcel_Width = "";
                                                        String Parcel_Weight= "",Parcel_Image= "",Parcel_Status= "",Parcel_CreatedDate= "";

                                                        if (Parcel.has("Id"))
                                                        {
                                                            Parcel_Id = Parcel.getString("Id");
                                                        }

                                                        if (Parcel.has("Name"))
                                                        {
                                                            Parcel_Name = Parcel.getString("Name");
                                                        }

                                                        if (Parcel.has("Height"))
                                                        {
                                                            Parcel_Height = Parcel.getString("Height");
                                                        }

                                                        if (Parcel.has("Width"))
                                                        {
                                                            Parcel_Width = Parcel.getString("Width");
                                                        }

                                                        if (Parcel.has("Weight"))
                                                        {
                                                            Parcel_Weight = Parcel.getString("Weight");
                                                        }

                                                        if (Parcel.has("Image"))
                                                        {
                                                            Parcel_Image = Parcel.getString("Image");
                                                        }

                                                        if (Parcel.has("Status"))
                                                        {
                                                            Parcel_Status = Parcel.getString("Status");
                                                        }

                                                        if (Parcel.has("CreatedDate"))
                                                        {
                                                            Parcel_CreatedDate = Parcel.getString("CreatedDate");
                                                        }

                                                        parcel_beens.add(new Parcel_Been(Parcel_Id,Parcel_Name,Parcel_Height,Parcel_Width,Parcel_Weight,Parcel_Image,Parcel_Status,Parcel_CreatedDate));
                                                    }
                                                }

                                                if (jsonObject.has("CarDetails"))
                                                {
                                                    CarDetails = jsonObject.getString("CarDetails");

                                                    JSONObject CarDetails1 = jsonObject.getJSONObject("CarDetails");

                                                    if (CarDetails1!=null)
                                                    {
                                                        if (CarDetails1.has("Id"))
                                                        {
                                                            CarDetails_Id = CarDetails1.getString("Id");
                                                        }

                                                        if (CarDetails1.has("CompanyId"))
                                                        {
                                                            CarDetails_CompanyId = CarDetails1.getString("CompanyId");
                                                        }

                                                        if (CarDetails1.has("VehicleModel"))
                                                        {
                                                            CarDetails_VehicleModel = CarDetails1.getString("VehicleModel");
                                                        }

                                                        if (CarDetails1.has("Company"))
                                                        {
                                                            CarDetails_Company = CarDetails1.getString("Company");
                                                        }

                                                        if (CarDetails1.has("Color"))
                                                        {
                                                            CarDetails_Color = CarDetails1.getString("Color");
                                                        }

                                                        if (CarDetails1.has("VehicleRegistrationNo"))
                                                        {
                                                            CarDetails_VehicleRegistrationNo = CarDetails1.getString("VehicleRegistrationNo");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificate"))
                                                        {
                                                            CarDetails_RegistrationCertificate = CarDetails1.getString("RegistrationCertificate");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificate"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificate = CarDetails1.getString("VehicleInsuranceCertificate");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificateExpire"))
                                                        {
                                                            CarDetails_RegistrationCertificateExpire = CarDetails1.getString("RegistrationCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificateExpire = CarDetails1.getString("VehicleInsuranceCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleImage"))
                                                        {
                                                            CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails1.getString("VehicleImage");
                                                        }

                                                        if (CarDetails1.has("Description"))
                                                        {
                                                            CarDetails_Description = CarDetails1.getString("Description");
                                                        }
                                                    }
                                                }

//                                                MyBookingActivity.MainList_Past_Booking.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
//                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
//                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
//                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
//                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
//                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
//                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
//                                                        CarDetails_Description, HistoryType, BookingType,RequestFor));

                                                MyBookingActivity.pastBooking_beens.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime, LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));
                                            }
                                            else
                                            {
                                                Log.e(TAG,"callPastApi() jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"callPastApi() history null or lenth 0");
                                    }

                                    Log.e(TAG,"callPastApi() MyBookingActivity.pastBooking_beens.size() = "+MyBookingActivity.pastBooking_beens.size());

//                                    dialogClass.hideDialog();
                                    if (MyBookingActivity.pastBooking_beens.size()>0)
                                    {
                                        recyclerView.setVisibility(View.VISIBLE);
                                        tv_NoDataFound.setVisibility(View.GONE);
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                        mAdapter.setLoaded();
                                    }
                                    else
                                    {
                                        recyclerView.setVisibility(View.GONE);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.setLoaded();
                                    }
                                }
                                else
                                {
                                    Log.e(TAG,"callPastApi() history not found");
//                                    dialogClass.hideDialog();
                                    recyclerView.setVisibility(View.GONE);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.setLoaded();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callPastApi() status false");
//                                dialogClass.hideDialog();
                                recyclerView.setVisibility(View.GONE);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                mAdapter.setLoaded();
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callPastApi() status not found");
//                            dialogClass.hideDialog();
                            recyclerView.setVisibility(View.GONE);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                            mAdapter.setLoaded();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callPastApi() json null");
//                        dialogClass.hideDialog();
                        recyclerView.setVisibility(View.GONE);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);
                        mAdapter.setLoaded();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callPastApi() Exception "+e.toString());
//                    dialogClass.hideDialog();
                    recyclerView.setVisibility(View.GONE);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    mAdapter.setLoaded();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void openDialog(final PastBooking_Been history, final int position)
    {
        final Dialog dialog = new Dialog(getContext(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_previous_due);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        ImageView dialog_close;
        LinearLayout submit_layout;
        LinearLayout select_card_layout;

        dialog_close = dialog.findViewById(R.id.dialog_close);
        submit_layout = dialog.findViewById(R.id.submit_layout);
        select_card_layout = dialog.findViewById(R.id.select_card_layout);
        iv_card = dialog.findViewById(R.id.iv_card);
        tv_type = dialog.findViewById(R.id.tv_type);
        tv_cardNo = dialog.findViewById(R.id.tv_cardNo);

        select_card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), Wallet_Add_Cards_Activity.class);
                intent.putExtra("from","due");
                startActivityForResult(intent,5);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPayApi(dialog,history,position);
            }
        });

        dialog.show();
    }

    private void callPayApi(final Dialog dialog, PastBooking_Been history, final int position)
    {
        dialogClass.showDialog();
        String id = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,getActivity());
        String url = WebServiceAPI.API_PAY_PREVIOUSDUE;

        HashMap<String,Object> param = new HashMap<>();
        param.put(WebServiceAPI.PARAM_AMOUNT,history.getPreviousDue());
        param.put(WebServiceAPI.PARAM_CARD_ID,selectedCard.getId());
        param.put(WebServiceAPI.PARAM_BOOKING_ID,history.getId());

        if(history.getBookingType().equalsIgnoreCase("Book Now"))
        {param.put(WebServiceAPI.PARAM_BOOKING_TYPE,"BookNow");}
        else {param.put(WebServiceAPI.PARAM_BOOKING_TYPE,"BookLater");}

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + param);

        aQuery.ajax(url.trim(), param, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                dialogClass.hideDialog();
                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    if(json.has("status"))
                    {
                        if(json.getBoolean("status"))
                        {
                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(json.getString("payment_message"),"");

                            MyBookingActivity.pastBooking_beens.get(position).setPaymentType("card");
                            MyBookingActivity.pastBooking_beens.get(position).setPaymentStatus("success");
                            MyBookingActivity.pastBooking_beens.get(position).setPreviousDue("");
                            mAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(json.getString("payment_message"),"");
                        }
                    }

                    dialog.dismiss();
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5)
        {
            selectedCard = (CreditCard_List_Been)data.getSerializableExtra("Card");
            if (selectedCard.getCardType().equalsIgnoreCase("visa"))
            {
                iv_card.setImageResource(R.drawable.icon_creditcard_visa);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("discover"))
            {
                iv_card.setImageResource(R.drawable.card_discover);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("amex"))
            {
                iv_card.setImageResource(R.drawable.icon_creditcard_maestro);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("diners"))
            {
                iv_card.setImageResource(R.drawable.card_dinner);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("jcb"))
            {
                iv_card.setImageResource(R.drawable.card_jcbs);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("mastercard"))
            {
                iv_card.setImageResource(R.drawable.card_master);
            }
            else if (selectedCard.getCardType().equalsIgnoreCase("maestro"))
            {
                iv_card.setImageResource(R.drawable.icon_creditcard_maestro);
            }
            else
            {
                iv_card.setImageResource(R.drawable.icon_creditcard_visa);
            }

            tv_cardNo.setVisibility(View.VISIBLE);
            tv_cardNo.setText(selectedCard.getCardNum_());
            tv_type.setText(selectedCard.getCardType());
        }
    }

    private void callPaytmPaymentApi(final String status_, String bookingType, String bookingId,final int position,String txId,String refId,String amt)
    {
        final DialogClass dialogClass = new DialogClass(getActivity(), 0);
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PAYMENT_PAYTM;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PAYMENT_PAYTM_BID, bookingId);
        params.put(WebServiceAPI.PAYMENT_PAYTM_BTYPE, bookingType);
        params.put(WebServiceAPI.PAYMENT_PAYTM_STATUS, status_);
        params.put(WebServiceAPI.PAYMENT_PAYTM_PAYMENTFOR, "PreviousDue");
        params.put(WebServiceAPI.PAYMENT_PAYTM_AMOUNT, amt);

        if(txId != null && !txId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_TXID, txId);
        if(refId != null && !refId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_REFID,refId);

        Log.e("TAG", "giveRate() url = " + url);
        Log.e("TAG", "giveRate() params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("TAG", "giveRate() responseCode = " + responseCode);
                    Log.e("TAG", "giveRate() Response = " + json);
                    dialogClass.hideDialog();

                    if(json.has("status"))
                    {
                        if(json.getBoolean("status") && status_.equalsIgnoreCase("success"))
                        {
                            MyBookingActivity.pastBooking_beens.get(position).setPaymentType("Paytm");
                            MyBookingActivity.pastBooking_beens.get(position).setPaymentStatus("success");
                            MyBookingActivity.pastBooking_beens.get(position).setPreviousDue("");
                            mAdapter.notifyDataSetChanged();

                            InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(json.getString("message"),"");
                        }
                        else
                        {
                            /*InternetDialog internetDialog = new InternetDialog(getActivity());
                            internetDialog.showDialog(json.getString("message"),"Payment Status");*/

                            InternetDialog internetDialog1 = new InternetDialog(getActivity());
                            internetDialog1.setListener(new InternetDialog.dialogPositiveClick() {
                                @Override
                                public void onOkClick() {
                                    if (Global.isNetworkconn(getActivity()))
                                    {
                                        MyBookingActivity.priviousListSize = 0;
                                        MyBookingActivity.pastPageIndex = 1;
                                        callPastApi();
                                    }
                                    else
                                    {
                                        swipeRefreshLayout.setRefreshing(false);
                                        InternetDialog internetDialog = new InternetDialog(getActivity());
                                        internetDialog.showDialog(getActivity().getResources().getString(R.string.please_check_internet_connection),
                                                getActivity().getResources().getString(R.string.no_internet_connection));
                                    }
                                }
                            });
                            internetDialog1.showDialog(json.getString("message"),"Payment Status",false,false);
                        }
                    }

                    if(json.getBoolean("payment_status"))
                    {playNotificationSound(true);}
                    else
                    {playNotificationSound(false);}

                } catch (Exception e) {
                    Log.e("TAG", "giveRate()  Exception " + e.toString());
                    dialogClass.hideDialog();

                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void playNotificationSound(boolean payment_status)
    {
        try {
            Log.e("TAG","33333333");
            Uri alarmSound;
            if(payment_status)
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getActivity().getPackageName() + "/raw/payment_1");}
            else
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getActivity().getPackageName() + "/raw/paytm_failed");}

            Log.e("TAG","2222222");
            Ringtone r = RingtoneManager.getRingtone(getActivity(), alarmSound);
            Log.e("TAG","11111111111");
            r.play();
        } catch (Exception e)
        {
            Log.e("TAG","Error = "+e.getMessage());
            Log.e("TAG","Error = "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
}