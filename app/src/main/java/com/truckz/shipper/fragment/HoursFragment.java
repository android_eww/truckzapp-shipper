package com.truckz.shipper.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.truckz.shipper.activity.CustomPackageActivity;
import com.truckz.shipper.adapter.Hours_Adapter;
import com.truckz.shipper.R;


public class HoursFragment extends Fragment {

    private RecyclerView recyclerView;
    private Hours_Adapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hours, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.rv_hours);
        adapter = new Hours_Adapter(getActivity(), CustomPackageActivity.hour_beens);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        return v;
    }
}
