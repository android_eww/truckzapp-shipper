package com.truckz.shipper.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.notification.FCMUtil;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.view.CustomViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignupFragment extends Fragment {

    public String TAG = "SignupFragment()";
    public static CustomViewPager mViewPager;
    private GPSTracker gpsTracker;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static String deviceName = "";
    public static Bitmap bitmapImage;
    public static boolean cameraClick = false;

    public SignupFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        deviceName = Build.MANUFACTURER;

        if (cameraClick = true)
        {
            Log.e("Call SignUp Again","Sign Up Activity Agian");

            Log.e("Call SignUp Again", "Camera Click True Again");
            if (deviceName != null && deviceName.toLowerCase().contains("samsung"))
            {
                Log.e("Call SignUp Again", "Company Name Again:- " + deviceName);
//                if (bitmapImage != null)
//                {
//                    Log.e("Call SignUp Again", "Bitmap Not Null Again ");
//
//                }
//                else
//                {
//                    Log.e("Call SignUp Reset1","Sign Up Activity Reset1");
//                    resetData();
//                }
            }
            else
            {
                Log.e("Call SignUp Reset2","Sign Up Activity Reset2");
                resetData();
            }

        }
        else
        {
            Log.e("Call SignUp Reset3","Sign Up Activity Reset3");
            resetData();
        }

        gpsTracker = new GPSTracker(getActivity());
        mViewPager = view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new SamplePagerAdapter(getChildFragmentManager()));

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                String strToken = FCMUtil.getFcmToken(LoginActivity1.activity);
                LoginActivity1.token=strToken;
            }
        },1500);

        if(checkAndRequestPermissions()) {
            Log.e("call","permissionGranted");
        }


        return view;
    }

    public boolean checkAndRequestPermissions() {
        Log.e("","check permission call");
        int permissionWriteExternalStorage= ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionReadExternalStorage= ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionCamera= ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        Log.e("","listPermissionsNeeded.size() = "+listPermissionsNeeded.size());
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public class SamplePagerAdapter extends FragmentPagerAdapter {

        public SamplePagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            /** Show a Fragment based on the position of the current screen */
            if (position == 0)
            {
                return new SignupFragmentEmail();
            }
            if (position == 1)
            {
                return new SignupFragmentOtp();
            }
            else
                return new SignupFragmentProfile();
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 3;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.SEND_SMS) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK(getResources().getString(R.string.sms_and_location_service_permission_required_for_this_app),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.goto_settings_and_enable_permission),
                                    Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.ok), okListener)
                .setNegativeButton(getResources().getString(R.string.cancel_cap), okListener)
                .create()
                .show();
    }

    public void resetData()
    {
        Log.e(TAG,"resetData()");

        LoginActivity1.FirstName="";
        LoginActivity1.LastName="";
        LoginActivity1.otp="";
        LoginActivity1.Email="";
        LoginActivity1.phonNumber="";
        LoginActivity1.Password="";
        LoginActivity1.ConfirmPassword="";
        LoginActivity1.gender="male";
        LoginActivity1.latitude="";
        LoginActivity1.longitude="";
        LoginActivity1.deviceType="2";
        LoginActivity1.userImage = null;
        LoginActivity1.token="";
        LoginActivity1.referralCode="";
        LoginActivity1.selectedDate="";
    }
}
