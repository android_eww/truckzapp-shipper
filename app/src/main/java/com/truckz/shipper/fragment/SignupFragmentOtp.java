package com.truckz.shipper.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.LoginActivity1;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;

public class SignupFragmentOtp extends Fragment {

    private LinearLayout layout_next;
    private CustomEditText editOtp;
    private LinearLayout ll_RootView;
    private MySnackBar mySnackBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_signup_otp, container,false);

        layout_next = rootView.findViewById(R.id.layout_next);
        editOtp = rootView.findViewById(R.id.input_otp);

        ll_RootView = rootView.findViewById(R.id.main_content);
        layout_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                goToNext();
            }
        });

        mySnackBar = new MySnackBar(getActivity());


        return rootView;
    }

    public void goToNext()
    {
//        signupFragment.mViewPager.setCurrentItem(2);
        if (LoginActivity1.otp.equals(editOtp.getText().toString()))
        {
            editOtp.setText("");
            LoginActivity1.otp="";
            SignupFragment.mViewPager.setCurrentItem(2);
        }
        else
        {
            mySnackBar.showSnackBar(ll_RootView,getResources().getString(R.string.invalid_otp));
        }
    }
}

