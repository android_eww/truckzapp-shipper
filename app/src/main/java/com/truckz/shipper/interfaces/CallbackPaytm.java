package com.truckz.shipper.interfaces;

import android.os.Bundle;

public interface CallbackPaytm
{
    void onSuccess(String status,String bookingType,String bookingId,String txId,String refId);
    void onFailed(String status,String bookingType,String bookingId,String txId,String refId);
    void onNetworkError();
    void onClientAuthenticationFailed(String error,String status,String bookingType,String bookingId,String txId,String refId);
    void onErrorLoadingWebPage(int iniErrorCode,
                               String inErrorMessage, String inFailingUrl,String status,String bookingType,String bookingId,String txId,String refId);
    void onBackPressedCancelTransaction(String status,String bookingType,String bookingId,String txId,String refId);
    void onTransactionCancel(String inErrorMessage, Bundle inResponse,String status,String bookingType,String bookingId,String txId,String refId);
}
