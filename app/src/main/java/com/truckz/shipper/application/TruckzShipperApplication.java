package com.truckz.shipper.application;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TruckzShipperApplication extends MultiDexApplication {

    private static TruckzShipperApplication mInstance;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
    public static Activity activity;


    /**
     * Custom typeface
     */
    public static Typeface sTypeface;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        mInstance = this;
        sTypeface = Typeface.createFromAsset(getAssets(), "OCR-A.ttf");
        try
        {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(),PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:",  Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (Exception e)
        {
            Log.e("Exception","When generate key hash"+e.getMessage());
        }
    }

    public static Activity getCurrentActivity()
    {
        //Log.e("call","getCurrentActivity = "+activity.getLocalClassName());
        return activity;
    }

    public static void setCurrentActivity(Activity activity1)
    {
        Log.e("call","getCurrentActivity = "+activity1.getLocalClassName());
        activity = activity1;
    }

    public static boolean isEmailValid(CharSequence input) {
//        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
//        Matcher matcher = pattern.matcher(input);
//        return (!matcher.matches());
        return android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd, EEE");
        String str = dateFormat.format(new Date());
        Log.e("strDate","date = "+str);
        return dateFormat.format(new Date());
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }
}
