package com.truckz.shipper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.adapter.CreditCardList_Adapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.truckz.shipper.view.CTextViewBold;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;



public class Wallet_Add_Cards_Activity extends AppCompatActivity implements View.OnClickListener{

    private String TAG = "Wallet_Add_Cards_Activity";
    public static Wallet_Add_Cards_Activity activity;

    private LinearLayout ll_Back, ll_Add;
    private ImageView iv_Back, iv_call;
    private CTextViewBold tv_Title;

    private RecyclerView recyclerView;
    private CreditCardList_Adapter adapter;
    private List<CreditCard_List_Been> cardList = new ArrayList<>();
    private AQuery aQuery;
    private DialogClass dialogClass;
    public static boolean callApi = false;
    public static String from = "";
    private String strCardList = "";
    public static int resumeFlag = 1;
    private CardView cv_add_card;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card);

        activity = Wallet_Add_Cards_Activity.this;
        callApi = false;
        from = "";
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);
        strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("from")!=null)
            {
                from = this.getIntent().getStringExtra("from");
            }
            else
            {
                from = "";
            }
        }
        else
        {
            from = "";
        }

        initUI();
    }

    private void initUI()
    {

        Log.e(TAG,"initUI()");

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        ll_Add = findViewById(R.id.ll_right);
        ll_Add.setVisibility(View.VISIBLE);

        tv_Title = findViewById(R.id.title_textview);

        tv_Title.setText(activity.getResources().getString(R.string.card_list));

        recyclerView = findViewById(R.id.rv_CreditCard);
        cv_add_card = findViewById(R.id.cv_add_card);

        adapter = new CreditCardList_Adapter(activity, cardList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        cv_add_card.setOnClickListener(activity);

        if (checkCardList())
        {
            saveCardList();
        }
        else
        {
            if (Global.isNetworkconn(activity))
            {
                getAllCards();
            }
            else
            {
                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
            }
        }
    }

    public boolean checkCardList()
    {
        Log.e(TAG,"checkCardList()");

        try
        {
            Log.e(TAG,"checkCardList() 111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e(TAG,"checkCardList() 222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e(TAG,"checkCardList() 33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e(TAG,"checkCardList() 6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e(TAG,"checkCardList() no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e(TAG,"checkCardList() no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e(TAG,"checkCardList() json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"checkCardList() Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    public void saveCardList()
    {
        Log.e(TAG,"saveCardList()");

        try
        {
            Log.e(TAG,"saveCardList() 111111111111111");
            cardList.clear();
            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e(TAG,"saveCardList() 222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e(TAG,"saveCardList() 33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e(TAG,"saveCardList() 6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            Log.e(TAG,"saveCardList() 77777777777777");
                            for (int i=0; i<cards.length(); i++)
                            {
                                Log.e(TAG,"saveCardList() 8888888888888");
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData!=null)
                                {
                                    String Id="",CardNum="",CardNum_="",Type="",Alias="",Expiry="";

                                    if (cardsData.has("Id"))
                                    {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum"))
                                    {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2"))
                                    {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type"))
                                    {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias"))
                                    {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    if (cardsData.has("Expiry"))
                                    {
                                        Expiry = cardsData.getString("Expiry");
                                    }

                                    cardList.add(new CreditCard_List_Been(Id,CardNum,CardNum_,Type,Alias,Expiry));
                                }
                            }
                            Log.e(TAG,"saveCardList() 9999999999999 size = "+cardList.size());
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            Log.e(TAG,"saveCardList() no cards available");
                        }
                    }
                    else
                    {
                        Log.e(TAG,"saveCardList() no cards found");
                    }
                }
                else
                {
                    Log.e(TAG,"saveCardList() json null");
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"saveCardList()  Exception in getting card list = "+e.getMessage());
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.cv_add_card:
                resumeFlag = 0;
                gotoAddCardScreen();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;
        }
    }

    private void gotoAddCardScreen()
    {
        Intent i =  new Intent(activity, Add_Card_In_List_Activity.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        if (callApi)
        {
            strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                saveCardList();
            }
            else
            {
                if (Global.isNetworkconn(activity))
                {
                    getAllCards();
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
            }
        }
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }

                if (Wallet_Balance_TopUp_Activity.activity!=null)
                {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }

                if (Add_Card_In_List_Activity.activity!=null)
                {
                    Add_Card_In_List_Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public void getAllCards()
    {
        callApi = false;
        dialogClass.showDialog();
        cardList.clear();
        String url = WebServiceAPI.API_GET_ALL_CARDS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("cards"))
                                {
                                    JSONArray cards = json.getJSONArray("cards");

                                    if (cards!=null && cards.length()>0)
                                    {
                                        for (int i=0; i<cards.length(); i++)
                                        {
                                            JSONObject cardsData = cards.getJSONObject(i);

                                            if (cardsData!=null)
                                            {
                                                String Id="",CardNum="",CardNum_="",Type="",Alias="",Expiry="";

                                                if (cardsData.has("Id"))
                                                {
                                                    Id = cardsData.getString("Id");
                                                }

                                                if (cardsData.has("CardNum"))
                                                {
                                                    CardNum = cardsData.getString("CardNum");
                                                }

                                                if (cardsData.has("CardNum2"))
                                                {
                                                    CardNum_ = cardsData.getString("CardNum2");
                                                }

                                                if (cardsData.has("Type"))
                                                {
                                                    Type = cardsData.getString("Type");
                                                }

                                                if (cardsData.has("Alias"))
                                                {
                                                    Alias = cardsData.getString("Alias");
                                                }

                                                if (cardsData.has("Expiry"))
                                                {
                                                    Expiry = cardsData.getString("Expiry");
                                                }

                                                cardList.add(new CreditCard_List_Been(Id,CardNum,CardNum_,Type,Alias,Expiry));
                                            }
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,json.toString(),activity);

                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        Log.e("call","no cards available");
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                else
                                {
                                    Log.e("call","no cards found");
                                    dialogClass.hideDialog();
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    adapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}