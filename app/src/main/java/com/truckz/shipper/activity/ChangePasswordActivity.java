package com.truckz.shipper.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private String TAG = "ChangePasswordActivity";
    public static ChangePasswordActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    private CTextViewBold tv_Title;
    private CardView cv_submit;
    private CustomEditText et_NewPassword, et_ConfirmPassword,input_old_password;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private MySnackBar mySnackBar;
    private LinearLayout ll_RootLayour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        activity = ChangePasswordActivity.this;
        ll_RootLayour = (LinearLayout) findViewById(R.id.rootView);
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        mySnackBar = new MySnackBar(activity);
        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        tv_Title = findViewById(R.id.title_textview);
        tv_Title.setText(activity.getResources().getString(R.string.change_pass));

        et_NewPassword =  findViewById(R.id.input_new_password);
        et_ConfirmPassword = findViewById(R.id.input_confirm_password);
        input_old_password = findViewById(R.id.input_old_password);

        cv_submit = (CardView) findViewById(R.id.cv_submit);


        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);

        cv_submit.setOnClickListener(activity);

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.cv_submit:
                if (Global.isNetworkconn(activity))
                {
                    changePassword();
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }

    private void changePassword()
    {
        Log.e(TAG,"changePassword()");

        Log.e(TAG,"changePassword() new password "+et_NewPassword.getText().toString().trim());
        Log.e(TAG,"changePassword() confirm password "+et_ConfirmPassword.getText().toString().trim());

        if(input_old_password.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.please_enter_old_password));
            input_old_password.setFocusableInTouchMode(true);
            input_old_password.requestFocus();
        }
        else if (et_NewPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.please_enter_new_password));
            et_NewPassword.setFocusableInTouchMode(true);
            et_NewPassword.requestFocus();
        }
        else if (et_ConfirmPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.please_enter_confirm_password));
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else if (et_NewPassword.getText().toString().trim().length()<=5)
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.password_must_be_greater_than_five_character));
            et_NewPassword.setFocusableInTouchMode(true);
            et_NewPassword.requestFocus();
        }
        else if (et_ConfirmPassword.getText().toString().trim().length()<=5)
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.password_must_be_greater_than_five_character));
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else if (!et_NewPassword.getText().toString().trim().equals(et_ConfirmPassword.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(ll_RootLayour,getResources().getString(R.string.your_new_password_and_confirm_password_not_match));
            et_ConfirmPassword.setFocusableInTouchMode(true);
            et_ConfirmPassword.requestFocus();
        }
        else
        {
            dialogClass.showDialog();
            String url = WebServiceAPI.API_CHANGE_PASSWORD;

            //http://54.206.55.185/web/Passenger_Api/ChangePassword
            Log.e("call","url = "+url);

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(WebServiceAPI.PARAM_PASSENGER_ID,SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
            params.put(WebServiceAPI.PARAM_PASSWORD,et_NewPassword.getText().toString().trim());
            params.put(WebServiceAPI.PARAM_OLD_PASSWORD,input_old_password.getText().toString().trim());

            Log.e("call", "param = " + params);

            aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("responseCode", " = " + responseCode);
                        Log.e("Response", " = " + json);

                        if (json!=null)
                        {
                            if (json.has("status"))
                            {
                                if (json.has("message"))
                                {
                                    dialogClass.hideDialog();
                                    showSuccessMessage(getResources().getString(R.string.success_message),json.getString("message"));
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    showSuccessMessage(getResources().getString(R.string.error_message),getResources().getString(R.string.please_try_agai_later));
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                showSuccessMessage(getResources().getString(R.string.error_message),getResources().getString(R.string.please_try_agai_later));
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            showSuccessMessage(getResources().getString(R.string.error_message),getResources().getString(R.string.please_try_agai_later));
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("Exception","Exception "+e.toString());
                        dialogClass.hideDialog();
                    }
                }

            }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
    }

    public void showSuccessMessage(String title, final String message)
    {
        final Dialog dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        LinearLayout dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        ImageView dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        dialog_title.setText(title);

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(!message.equalsIgnoreCase("Incorrect old password"))
                        {
                            finish();
                            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
                        }
                    }
                },800);

            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(!message.equalsIgnoreCase("Incorrect old password"))
                        {
                            finish();
                            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
                        }
                    }
                },800);
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(!message.equalsIgnoreCase("Incorrect old password"))
                        {
                            finish();
                            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
                        }
                    }
                },800);
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
        {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
