package com.truckz.shipper.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Wallet_Balance_TransferToBank_Activity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "Wallet_Balance_TransferToBank_Activity";
    public static Wallet_Balance_TransferToBank_Activity activity;

    private LinearLayout ll_Back, ll_right;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title,tv_Cards;
    private TextView tv_WalletBallence;
    public static String cardType="",cardNumber="",cardId="";
    private EditText et_Amount,et_BankHolderName,et_BankName,et_BankAccounNo,et_BSB; //et_ABN,
    private TextView tv_Transfer;
    private String cardHolderName="";
//    private String abn="";
    private String bankName="";
    private String bankAccountNumber="";
    private String bsb="";
    private String amount="";
    private String passengerId="";
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static double wallet_ballence = 0;
    public static int resumeFlag = 1;
    private MySnackBar mySnackBar;
    private LinearLayout ll_rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_withdraw);

        activity = Wallet_Balance_TransferToBank_Activity.this;
        wallet_ballence = 0;
        resumeFlag = 1;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        ll_rootView = (LinearLayout) findViewById(R.id.ll_rootView);
        mySnackBar = new MySnackBar(activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity).equalsIgnoreCase(""))
        {
            passengerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        }
        else
        {
            passengerId = "0";
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity).equalsIgnoreCase(""))
        {
            cardHolderName = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity);
        }
        else
        {
            cardHolderName = "";
        }

        /*if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity).equalsIgnoreCase(""))
        {
            abn = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ABN,activity);
        }
        else
        {
            abn = "";
        }*/

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity).equalsIgnoreCase(""))
        {
            bankName = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity);
        }
        else
        {
            bankName = "";
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity).equalsIgnoreCase(""))
        {
            bankAccountNumber = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity);
        }
        else
        {
            bankAccountNumber = "";
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity).equalsIgnoreCase(""))
        {
            bsb = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity);
        }
        else
        {
            bsb = "";
        }


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity).equalsIgnoreCase(""))
        {
            wallet_ballence = Double.parseDouble(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity));
        }
        else
        {
            wallet_ballence = 0;
        }

        initUI();
    }

    private void initUI()
    {
        Log.e(TAG,"initUI()");

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_right = (LinearLayout) findViewById(R.id.ll_right);
        ll_right.setVisibility(View.INVISIBLE);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_WalletBallence = (TextView) findViewById(R.id.wallet_ballence);

        tv_Cards = (TextView) findViewById(R.id.select_card_textview);
        tv_Transfer = (TextView) findViewById(R.id.tv_Transfer);

        et_Amount = (EditText) findViewById(R.id.et_Amount);
        et_BankHolderName = (EditText) findViewById(R.id.et_BankHolderName);
//        et_ABN = (EditText) findViewById(R.id.et_ABN);
        et_BankName = (EditText) findViewById(R.id.et_BankName);
        et_BankAccounNo = (EditText) findViewById(R.id.et_BankAccounNo);
        et_BSB = (EditText) findViewById(R.id.et_BSB);

        tv_Title.setText(activity.getResources().getString(R.string.activity_transfer_to_bank));
        tv_WalletBallence.setText(getResources().getString(R.string.currency)+String.format("%.2f",wallet_ballence));

        et_BankHolderName.setText(cardHolderName);
//        et_ABN.setText(abn);
        et_BankName.setText(bankName);
        et_BankAccounNo.setText(bankAccountNumber);
        et_BSB.setText(bsb);

//        et_BankHolderName.setEnabled(false);
//        et_ABN.setEnabled(false);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        tv_Cards.setOnClickListener(activity);
        tv_Transfer.setOnClickListener(activity);

        et_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length()==1 && charSequence.toString().trim().equalsIgnoreCase("."))
                {
                    et_Amount.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.select_card_textview:
                gotoCardScreen();
                break;

            case R.id.tv_Transfer:
                if (Global.isNetworkconn(activity))
                {
                    transferToBank();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }

    private void transferToBank()
    {
        if (et_BankHolderName.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.please_enter_bank_holder_name));
        }
//        else if (et_ABN.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            mySnackBar.showSnackBar(ll_rootView,"Please enter ABN!");
//        }
        else if (et_BankName.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.please_enter_bank_name));
        }
        else if (et_BankAccounNo.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.please_enter_bank_account_number));
        }
        else if (et_BSB.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.please_enter_bsb));
        }
        else if (et_Amount.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.please_enter_amount));
        }
        else
        {
            if (wallet_ballence>=Double.parseDouble(et_Amount.getText().toString().trim()))
            {
                cardHolderName = et_BankHolderName.getText().toString().trim();
//                abn = et_ABN.getText().toString().trim();
                bankName = et_BankName.getText().toString().trim();
                bankAccountNumber = et_BankAccounNo.getText().toString().trim();
                bsb = et_BSB.getText().toString().trim();
                amount = et_Amount.getText().toString().trim();

                callApiTransferToBank();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(getResources().getString(R.string.your_wallet_balance_is_low),getResources().getString(R.string.info_message));
            }
        }
    }

    private void callApiTransferToBank()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_TRANSFER_TO_BANK;
        //PassengerId,Amount,HolderName,ABN,BankName,BSB,AccountNo

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, passengerId);
        params.put(WebServiceAPI.PARAM_HOLDER_NAME, cardHolderName);
//        params.put(WebServiceAPI.PARAM_ABN, abn);
        params.put(WebServiceAPI.PARAM_BANK_NAME, bankName);
        params.put(WebServiceAPI.PARAM_ACCOUNT_NO, bankAccountNumber);
        params.put(WebServiceAPI.PARAM_BSB, bsb);
        params.put(WebServiceAPI.PARAM_AMOUNT, amount);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = getResources().getString(R.string.transfer_success_check_your_history);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                Wallet_Balance_Activity.callGetMonny = true;
                                showSuccessDialog(message);
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                String message = getResources().getString(R.string.something_went_wrong);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message,getResources().getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessDialog(String message)
    {
        final Dialog dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);
        tv_title.setText(getResources().getString(R.string.success_message));

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(activity,Wallet_Transfer_History_Activity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(activity,Wallet_Transfer_History_Activity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void gotoCardScreen()
    {
        if (checkCardList()==false)
        {
            Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
            intent.putExtra("from","Wallet_Balance_TransferToBank_Activity");
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(activity,Wallet_Add_Cards_Activity.class);
            intent.putExtra("from","Wallet_Balance_TransferToBank_Activity");
            startActivity(intent);
        }
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        if (!cardType.equalsIgnoreCase("") && !cardId.equalsIgnoreCase(""))
        {
            tv_Cards.setText(cardType+"  "+cardNumber);
        }

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }

                if (Wallet_Transfer_Activity.activity!=null)
                {
                    Wallet_Transfer_Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }
}