package com.truckz.shipper.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.truckz.shipper.interfaces.CallbackPaytm;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.singleton.PaytmSingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PaymentPaytmActivity extends AppCompatActivity
{
    PaytmSingleton paytmSingleton;
    CallbackPaytm callbackPaytm;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private String CHECKSUMHASH="",CALLBACK_URL="";

    private String cusId="",bookingId="",grandTotal="",bookingType="",txId="",refId="",refId1="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        dialogClass = new DialogClass(this,0);
        aQuery = new AQuery(this);

        callbackPaytm = PaytmSingleton.getCallBack();


        bookingId = getIntent().getStringExtra("BookingId");
        cusId = getIntent().getStringExtra("cusId");
        grandTotal = getIntent().getStringExtra("GrandTotal");
        bookingType = getIntent().getStringExtra("BookingType");
        refId = getIntent().getStringExtra("refId");
        refId1 = getIntent().getStringExtra("refId");

        call_GetCheckSum();
    }

    public void call_GetCheckSum()
    {
        Map<String, Object> params = new HashMap<String, Object>();

        String url = "https://truckzapp.in/web/Drvier_Api/GenerateCheckSumHash";

        HashMap<String,String> hashMap = new HashMap<>();

        hashMap.put("MID","kKkQoF92720926396613");
        //hashMap.put("ORDER_ID","REF"+refId);
        if(bookingType.toLowerCase().contains("now"))
        {hashMap.put("ORDER_ID","N"+bookingId+"-"+refId);}
        else if(bookingType.toLowerCase().contains("later"))
        {hashMap.put("ORDER_ID","L"+bookingId+"-"+refId);}
        hashMap.put("CUST_ID",cusId);
        hashMap.put("INDUSTRY_TYPE_ID","Retail");
        hashMap.put("CHANNEL_ID","WAP");
        hashMap.put("TXN_AMOUNT",grandTotal);
        hashMap.put("WEBSITE","WEBSTAGING");
//		hashMap.put("MOBILE_NO","9999999999");
//		hashMap.put("EMAIL","abc@gmail.com");

        Log.e("call", "url = " + url);
        Log.e("call", "hashMap = " + hashMap);


        dialogClass.showDialog();
        aQuery.ajax(url.trim(), hashMap, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("data"))
                                {
                                    JSONObject jsonObject = json.getJSONObject("data");

                                    if (jsonObject!=null)
                                    {
                                        if (jsonObject.has("CHECKSUMHASH"))
                                        {
                                            if (jsonObject.has("CALLBACK_URL"))
                                            {
                                                CALLBACK_URL = jsonObject.getString("CALLBACK_URL");
                                            }

                                            if (jsonObject.has("ORDER_ID"))
                                            {
                                                refId = jsonObject.getString("ORDER_ID");
                                            }

                                            Log.e("call","call back url : "+CALLBACK_URL);
                                            CHECKSUMHASH = jsonObject.getString("CHECKSUMHASH");

                                            if (CHECKSUMHASH!=null && !CHECKSUMHASH.equalsIgnoreCase(""))
                                            {
                                                dialogClass.hideDialog();
                                                onStartTransaction(CHECKSUMHASH);
                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                                Toast.makeText(getApplicationContext(),"Check sum not found!",Toast.LENGTH_LONG).show();
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            Toast.makeText(getApplicationContext(),"Check sum not found!",Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e("errorrr","json null ");
                        dialogClass.hideDialog();
                        Toast.makeText(getApplicationContext(),"Json null",Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header("key","TruckZapp123*"));
    }

    public void onStartTransaction(String checksum)
    {
        PaytmPGService Service = PaytmPGService.getStagingService();

        HashMap<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("MID","kKkQoF92720926396613");
        //paramMap.put("ORDER_ID",refId);
        paramMap.put("ORDER_ID",refId);
        paramMap.put("CUST_ID",cusId);
        paramMap.put("INDUSTRY_TYPE_ID","Retail");
        paramMap.put("CHANNEL_ID","WAP");
        paramMap.put("TXN_AMOUNT",grandTotal);
        paramMap.put("WEBSITE","WEBSTAGING");
        paramMap.put("CALLBACK_URL",CALLBACK_URL);
        paramMap.put("CHECKSUMHASH",checksum);

        //paramMap.put("MOBILE_NO","9999999999");
        //paramMap.put("EMAIL","abc@gmail.com");

        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order, null);

        Log.e("call","paramMap = "+paramMap.toString());

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {


                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        // Some UI Error Occurred in Payment Gateway Activity.
                        // // This may be due to initialization of views in
                        // Payment Gateway Activity or may be due to //
                        // initialization of webview. // Error Message details
                        // the error occurred.
                        Log.e("call","someUIErrorOccurred() = "+inErrorMessage);
                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        //Log.e("call","onTransactionResponse() = "+inResponse.toString());

                        String x ="0";
                        txId = inResponse.getString("TXNID");
                        if(inResponse.getString("STATUS").equalsIgnoreCase("TXN_SUCCESS"))
                        {
                            callbackPaytm.onSuccess("success",bookingType,bookingId,txId,refId1);
                            finish();
                        }
                        else
                        {
                            callbackPaytm.onFailed("failed",bookingType,bookingId,txId,refId1);
                            finish();
                        }
                        if(x.equalsIgnoreCase("0"))
                        {return;}
                        try
                        {
                            JSONArray jsonArray = new JSONArray(inResponse.toString());

                            if (jsonArray!=null && jsonArray.length()>0)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);

                                if (jsonObject!=null) {
                                    String STATUS = "", CHECKSUMHASH = "", BANKNAME = "", ORDERID = "", TXNAMOUNT = "", TXNDATE = "", MID = "", TXNID = "", RESPCODE = "", PAYMENTMODE = "", ANKTXNID = "", CURRENCY = "", GATEWAYNAME = "", RESPMSG = "";

                                    if (jsonObject.has("STATUS")) {
                                        STATUS = jsonObject.getString("STATUS");
                                        txId = jsonObject.getString("TXNID");
                                        if(STATUS.equalsIgnoreCase("TXN_FAILURE"))
                                        {
                                            callbackPaytm.onFailed("failed",bookingType,bookingId,txId,refId1);
                                            finish();
                                        }
                                        if(STATUS.equalsIgnoreCase("TXN_SUCCESS"))
                                        {
                                            callbackPaytm.onSuccess("success",bookingType,bookingId,txId,refId1);
                                            finish();
                                        }
                                    }

                                    if (jsonObject.has("CHECKSUMHASH")) {
                                        CHECKSUMHASH = jsonObject.getString("CHECKSUMHASH");
                                    }

                                    if (jsonObject.has("BANKNAME")) {
                                        BANKNAME = jsonObject.getString("BANKNAME");
                                    }

                                    if (jsonObject.has("ORDERID")) {
                                        ORDERID = jsonObject.getString("ORDERID");
                                    }

                                    if (jsonObject.has("TXNAMOUNT")) {
                                        TXNAMOUNT = jsonObject.getString("TXNAMOUNT");
                                    }

                                    if (jsonObject.has("TXNDATE")) {
                                        TXNDATE = jsonObject.getString("TXNDATE");
                                    }

                                    if (jsonObject.has("MID")) {
                                        MID = jsonObject.getString("MID");
                                    }

                                    if (jsonObject.has("TXNID")) {
                                        TXNID = jsonObject.getString("TXNID");
                                    }

                                    if (jsonObject.has("RESPCODE")) {
                                        RESPCODE = jsonObject.getString("RESPCODE");
                                    }

                                    if (jsonObject.has("PAYMENTMODE")) {
                                        PAYMENTMODE = jsonObject.getString("PAYMENTMODE");
                                    }

                                    if (jsonObject.has("ANKTXNID")) {
                                        ANKTXNID = jsonObject.getString("ANKTXNID");
                                    }

                                    if (jsonObject.has("CURRENCY")) {
                                        CURRENCY = jsonObject.getString("CURRENCY");
                                    }

                                    if (jsonObject.has("GATEWAYNAME")) {
                                        GATEWAYNAME = jsonObject.getString("GATEWAYNAME");
                                    }

                                    if (jsonObject.has("RESPMSG")) {
                                        RESPMSG = jsonObject.getString("RESPMSG");
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.e("call","json exception = "+e.getMessage());
                        }
                    }

                    @Override
                    public void networkNotAvailable() {
                        // If network is not
                        // available, then this
                        // method gets called.
                        callbackPaytm.onNetworkError();
                        finish();
                        Log.e("call","networkNotAvailable()");
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.
                        callbackPaytm.onClientAuthenticationFailed(inErrorMessage,"failed",bookingType,bookingId,"",refId1);
                        finish();
                        Log.e("call","clientAuthenticationFailed() = "+inErrorMessage);
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        Log.e("call","onErrorLoadingWebPage() code = "+iniErrorCode);
                        Log.e("call","onErrorLoadingWebPage() message = "+inErrorMessage);
                        Log.e("call","onErrorLoadingWebPage() url = "+inFailingUrl);
                        callbackPaytm.onErrorLoadingWebPage(iniErrorCode,inErrorMessage,inFailingUrl,"failed",bookingType,bookingId,"",refId1);
                        finish();
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        Log.e("call","onBackPressedCancelTransaction");
                        // TODO Auto-generated method stub
                        callbackPaytm.onBackPressedCancelTransaction("failed",bookingType,bookingId,"",refId1);
                        finish();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.e("call","onTransactionCancel() message = "+inErrorMessage);
                        //Log.e("call","onTransactionCancel() error response = "+inResponse.toString());
                        callbackPaytm.onTransactionCancel(inErrorMessage,inResponse,"failed",bookingType,bookingId,"",refId1);
                        finish();
                    }


                });
    }
}
