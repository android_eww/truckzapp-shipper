package com.truckz.shipper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.adapter.Transfer_History_Adapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.Transfer_History_Been;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class Wallet_Balance_Activity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "Wallet_Balance_Activity";
    public static Wallet_Balance_Activity activity;

    private LinearLayout ll_Back, ll_right;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title;

    private LinearLayout ll_dollar,ll_bank,ll_history;
    private RecyclerView rv_transferHistory;
    private List<Transfer_History_Been> list = new ArrayList<>();
    private Transfer_History_Adapter adapter;
    private AQuery aQuery;
    private DialogClass dialogClass;
    public static boolean callGetMonny = false;
    private TextView tv_WalleteBallence;
    public static int resumeFlag = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance);

        activity = Wallet_Balance_Activity.this;
        callGetMonny = false;
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);

        initUI();
    }

    private void initUI()
    {
        Log.e(TAG,"initUI()");

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_right = (LinearLayout) findViewById(R.id.ll_right);
        ll_right.setVisibility(View.INVISIBLE);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText(getResources().getString(R.string.balance));

        ll_dollar = (LinearLayout)findViewById(R.id.ll_dollar);
        ll_bank = (LinearLayout)findViewById(R.id.ll_bank);
        ll_history = (LinearLayout)findViewById(R.id.ll_history);

        tv_WalleteBallence = (TextView)findViewById(R.id.wallet_ballence);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity).equalsIgnoreCase(""))
        {
            tv_WalleteBallence.setText(activity.getResources().getString(R.string.currency)+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity));
        }
        else
        {
            tv_WalleteBallence.setText(activity.getResources().getString(R.string.currency)+"0.00");
        }

        rv_transferHistory = (RecyclerView) findViewById(R.id.rv_transferHistory);

        adapter = new Transfer_History_Adapter(activity, list);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rv_transferHistory.setLayoutManager(mLayoutManager);
        rv_transferHistory.setItemAnimator(new DefaultItemAnimator());
        rv_transferHistory.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        ll_dollar.setOnClickListener(activity);
        ll_bank.setOnClickListener(activity);
        ll_history.setOnClickListener(activity);

        if (Global.isNetworkconn(activity))
        {
            getMoneyHistory();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.ll_dollar:
                resumeFlag = 0;
                Goto_TopUp_dollar();
                break;

            case R.id.ll_bank:
                resumeFlag = 0;
                Goto_TransferToBank();
                break;

            case R.id.ll_history:
                resumeFlag = 0;
                Goto_TransferHistory();
                break;

        }
    }

    private void Goto_TopUp_dollar()
    {
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TopUp_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void Goto_TransferToBank()
    {
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void Goto_TransferHistory()
    {
        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity).equalsIgnoreCase(""))
        {
            tv_WalleteBallence.setText(getResources().getString(R.string.currency)+SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,activity));
        }
        else
        {
            tv_WalleteBallence.setText(getResources().getString(R.string.currency)+"0.00");
        }

        if (callGetMonny)
        {
            if (Global.isNetworkconn(activity))
            {
                getMoneyHistory();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.please_check_internet_connection),activity.getResources().getString(R.string.no_internet_connection));
            }
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e("call","resumeFlag = "+resumeFlag);
        Log.e("call","IS_PASSCODE_REQUIRED = "+SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity));

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public void getMoneyHistory()
    {
        callGetMonny = false;
        dialogClass.showDialog();
        list.clear();
        String url = WebServiceAPI.API_GET_TRANSACTION_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("walletBalance"))
                                {
                                    String walleteBallence = json.getString("walletBalance");

                                    if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
                                    {
                                        Wallet__Activity.walleteBallence = walleteBallence;
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,Wallet__Activity.walleteBallence,activity);
                                        tv_WalleteBallence.setText(getResources().getString(R.string.currency)+walleteBallence);
                                    }
                                    else
                                    {
                                        tv_WalleteBallence.setText(getResources().getString(R.string.currency)+"0");
                                    }
                                }
                                else
                                {
                                    tv_WalleteBallence.setText(getResources().getString(R.string.currency)+"0");
                                }

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        int lenth = history.length();

                                        if (history.length()>5)
                                        {
                                            lenth = 5;
                                        }
                                        else
                                        {
                                            lenth = history.length();
                                        }

                                        for (int i=0; i<lenth; i++)
                                        {
                                            JSONObject historyData = history.getJSONObject(i);

                                            if (historyData!=null)
                                            {
                                                String Id="",UserId="",WalletId="",UpdatedDate="",Amount="",Type="",Description="",Status="";

                                                if (historyData.has("Id"))
                                                {
                                                    Id = historyData.getString("Id");
                                                }

                                                if (historyData.has("UserId"))
                                                {
                                                    UserId = historyData.getString("UserId");
                                                }

                                                if (historyData.has("WalletId"))
                                                {
                                                    WalletId = historyData.getString("WalletId");
                                                }

                                                if (historyData.has("UpdatedDate"))
                                                {
                                                    UpdatedDate = historyData.getString("UpdatedDate");
                                                }

                                                if (historyData.has("Amount"))
                                                {
                                                    Amount = historyData.getString("Amount");
                                                }

                                                if (historyData.has("Type"))
                                                {
                                                    Type = historyData.getString("Type");
                                                }

                                                if (historyData.has("Description"))
                                                {
                                                    Description = historyData.getString("Description");
                                                }

                                                if (historyData.has("Status"))
                                                {
                                                    Status = historyData.getString("Status");
                                                }

                                                list.add(new Transfer_History_Been(Id,UserId,WalletId,UpdatedDate,Amount,Type,Description,Status));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        Log.e("call","no cards available");
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                else
                                {
                                    Log.e("call","no cards found");
                                    dialogClass.hideDialog();
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            adapter.notifyDataSetChanged();
                        }

                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    adapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}