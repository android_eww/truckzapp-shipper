package com.truckz.shipper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.view.CTextViewBold;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener{

    public static UpdateProfileActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back,  iv_call;
    private CTextViewBold tv_Title;
    private CardView card_EditProfile,card_EditAccount;
//    private Animation animationProfile,animationAccount;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        activity = UpdateProfileActivity.this;
//        animationProfile = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.card_slide);
//        animationAccount = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.card_slide_left);
        handler = new Handler();
        init();
    }

    private void init() {

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        tv_Title = findViewById(R.id.title_textview);
        tv_Title.setText(getResources().getString(R.string.profile));

        card_EditProfile = findViewById(R.id. card_EditProfile);
        card_EditAccount = findViewById(R.id. card_EditAccount);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        card_EditProfile.setOnClickListener(activity);
        card_EditAccount.setOnClickListener(activity);


        handler.postDelayed(runnable,700);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run()
        {

//            card_EditProfile.startAnimation(animationProfile);
            card_EditProfile.setVisibility(View.VISIBLE);
//            card_EditAccount.startAnimation(animationAccount);
            card_EditAccount.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.card_EditProfile:
                gotoProfile();
                break;

            case R.id.card_EditAccount:
                gotoAccount();
                break;
        }
    }

    private void gotoAccount()
    {
        handler.removeCallbacks(runnable,null);
        Intent intent = new Intent(activity,Profile_AccountDetail_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    private void gotoProfile()
    {
        handler.removeCallbacks(runnable,null);
        Intent intent = new Intent(activity,ProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

    }
}
