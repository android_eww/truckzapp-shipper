package com.truckz.shipper.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.truckz.shipper.adapter.RecyclerViewAdapter;
import com.truckz.shipper.been.Model_Been;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.Package_Been;
import com.truckz.shipper.comman.TaxiUtil;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.fragment.DaysFragment;
import com.truckz.shipper.fragment.HoursFragment;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CustomPackageActivity extends AppCompatActivity implements View.OnClickListener {

    public static CustomPackageActivity activity;
    private LinearLayout ll_back, ll_rootView, ll_pickup_date, ll_pickup_time,ll_hours, ll_days;
    private FrameLayout fl_frame;
    private ImageView iv_back, iv_call;
    private TextView tv_title,tv_pickup_address, tv_pickup_date, tv_pickup_time, tv_hours, tv_days;
    public static String strSelectedDate = "", strCurrentDate="", strCurrentTime="", strSelectedTime="";
    public static int checkMinut = 0;
    public static int checkHour = 0;
    private RecyclerView recyclerView_cars;
    private RecyclerView.LayoutManager RecyclerViewLayoutManager;
    private RecyclerViewAdapter RecyclerViewHorizontalAdapter;
    private LinearLayoutManager HorizontalLayout ;

    private int mYear, mMonth, mDay, mHour, mMinute;

    public static List<Model_Been> model_beens = new ArrayList<Model_Been>();

    public static List<Package_Been> hour_beens = new ArrayList<Package_Been>();
    public static List<Package_Been> day_beens = new ArrayList<Package_Been>();
    public static int selectedCarPosition = 0;
    public static int selectedHourDayFlag = 0; //0 = hours, 1 = days;
    public static int itemPos = -1;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private MySnackBar mySnackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_package);

        activity = CustomPackageActivity.this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        mySnackBar = new MySnackBar(activity);
        model_beens.clear();
        hour_beens.clear();
        day_beens.clear();
        selectedCarPosition = 0;
        selectedHourDayFlag = 0;
        itemPos = -1;

        strSelectedDate = "";
        strCurrentDate="";
        strCurrentTime="";
        strSelectedTime="";

        checkMinut = 0;
        checkHour = 0;

        init();
    }

    private void init()
    {
        ll_back = (LinearLayout) findViewById(R.id.back_layout);
        ll_rootView = (LinearLayout) findViewById(R.id.ll_rootView);
        iv_back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        tv_title = (TextView) findViewById(R.id.title_textview);

        fl_frame = (FrameLayout) findViewById(R.id.fl_frame);

        ll_pickup_date = (LinearLayout)findViewById(R.id.ll_pickup_date);
        ll_pickup_time = (LinearLayout)findViewById(R.id.ll_pickup_time);
        ll_hours = (LinearLayout) findViewById(R.id.ll_hours);
        ll_days = (LinearLayout)findViewById(R.id.ll_days);

        tv_pickup_address = (TextView) findViewById(R.id.tv_pickup_location);
        tv_pickup_date = (TextView) findViewById(R.id.tv_pickup_date);
        tv_pickup_time = (TextView) findViewById(R.id.tv_pickup_time);
        tv_hours = (TextView) findViewById(R.id.tv_hours);
        tv_days = (TextView) findViewById(R.id.tv_days);

        recyclerView_cars = (RecyclerView) findViewById(R.id.recyclerView_cars);

        tv_title.setText(getResources().getString(R.string.custom_package));
        tv_pickup_address.setText(TaxiUtil.picup_Address);

        ll_back.setOnClickListener(activity);
        iv_back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);

        ll_pickup_date.setOnClickListener(activity);
        ll_pickup_time.setOnClickListener(activity);
        ll_hours.setOnClickListener(activity);
        ll_days.setOnClickListener(activity);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView_cars.setLayoutManager(RecyclerViewLayoutManager);

        RecyclerViewHorizontalAdapter = new RecyclerViewAdapter(activity,model_beens);

        HorizontalLayout = new LinearLayoutManager(CustomPackageActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView_cars.setLayoutManager(HorizontalLayout);
        recyclerView_cars.setAdapter(RecyclerViewHorizontalAdapter);

        todayDate();

        if (Global.isNetworkconn(activity))
        {
            call_Api();
        }
        else
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
        }
    }

    private void call_Api()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PACKAGE;

        Log.e("call","url = "+url);
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("package"))
                                {
                                    JSONArray modelList = json.getJSONArray("package");

                                    if (modelList!=null && modelList.length()>0)
                                    {
                                        for (int i=0; i<modelList.length(); i++)
                                        {
                                            JSONObject modelObject = modelList.getJSONObject(i);

                                            if (modelObject!=null)
                                            {
                                                List<Package_Been> package_beens = new ArrayList<Package_Been>();

                                                String ModelId = "", ModelName = "", ModelImage = "";
                                                boolean isSelected = false;

                                                if (i==0)
                                                {
                                                    isSelected = true;
                                                }

                                                if (modelObject.has("ModelId"))
                                                {
                                                    ModelId = modelObject.getString("ModelId");
                                                }

                                                if (modelObject.has("ModelName"))
                                                {
                                                    ModelName = modelObject.getString("ModelName");
                                                }

                                                if (modelObject.has("ModelImage"))
                                                {
                                                    ModelImage = modelObject.getString("ModelImage");
                                                }

                                                if (modelObject.has("Package"))
                                                {
                                                    JSONArray packageList = modelObject.getJSONArray("Package");

                                                    if (packageList!=null && packageList.length()>0)
                                                    {
                                                        for (int j=0; j<packageList.length(); j++)
                                                        {
                                                            JSONObject packageObject = packageList.getJSONObject(j);

                                                            if (packageObject!=null)
                                                            {
                                                                String package_Id="",package_ModelId="",package_Type="",package_Name="";
                                                                String package_Description="",package_Time="",package_KM="",package_Amount="";
                                                                String package_Notes="",package_Date="";

                                                                if (packageObject.has("Id"))
                                                                {
                                                                    package_Id = packageObject.getString("Id");
                                                                }

                                                                if (packageObject.has("ModelId"))
                                                                {
                                                                    package_ModelId = packageObject.getString("ModelId");
                                                                }

                                                                if (packageObject.has("Type"))
                                                                {
                                                                    package_Type = packageObject.getString("Type");
                                                                }

                                                                if (packageObject.has("Name"))
                                                                {
                                                                    package_Name = packageObject.getString("Name");
                                                                }

                                                                if (packageObject.has("Description"))
                                                                {
                                                                    package_Description = packageObject.getString("Description");
                                                                }

                                                                if (packageObject.has("Time"))
                                                                {
                                                                    package_Time = packageObject.getString("Time");
                                                                }

                                                                if (packageObject.has("KM"))
                                                                {
                                                                    package_KM = packageObject.getString("KM");
                                                                }

                                                                if (packageObject.has("Amount"))
                                                                {
                                                                    package_Amount = packageObject.getString("Amount");
                                                                }

                                                                if (packageObject.has("Notes"))
                                                                {
                                                                    package_Notes = packageObject.getString("Notes");
                                                                }

                                                                if (packageObject.has("Date"))
                                                                {
                                                                    package_Description = packageObject.getString("Date");
                                                                }

                                                                package_beens.add(new Package_Been(
                                                                        package_Id,
                                                                        package_ModelId,
                                                                        package_Type,
                                                                        package_Name,
                                                                        package_Description,
                                                                        package_Time,
                                                                        package_KM,
                                                                        package_Amount,
                                                                        package_Notes,
                                                                        package_Date
                                                                ));
                                                            }
                                                        }
                                                    }
                                                }
                                                model_beens.add(new Model_Been(ModelId,ModelName,ModelImage,isSelected,package_beens));
                                            }
                                        }

                                        dialogClass.hideDialog();

                                        if (model_beens.size()>0)
                                        {
                                            RecyclerViewHorizontalAdapter.notifyDataSetChanged();
                                            selectFragment(0);
                                        }

                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                }
                            }
                            else
                            {
                                Log.e("call","book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("call","book later status not found");
                            dialogClass.hideDialog();

                            if (json.has("message"))
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("call","book later json null");
                        dialogClass.hideDialog();

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.ll_pickup_date:
                selectDateAndTime();
                break;

            case R.id.ll_pickup_time:
                showTimePicker();
                break;

            case R.id.ll_hours:
                selectFragment(0);
                break;

            case R.id.ll_days:
                selectFragment(1);
                break;
        }

    }

    public void selectFragment(int num)
    {
        if (model_beens.size()>0)
        {

            if (model_beens.get(selectedCarPosition).getPackage()!=null && model_beens.get(selectedCarPosition).getPackage().size()>0)
            {
                hour_beens.clear();
                day_beens.clear();
                for (int i=0; i<model_beens.get(selectedCarPosition).getPackage().size(); i++)
                {
                    if (model_beens.get(selectedCarPosition).getPackage().get(i).getType()!=null && model_beens.get(selectedCarPosition).getPackage().get(i).getType().equalsIgnoreCase("hours"))
                    {
                        hour_beens.add(new Package_Been(
                                model_beens.get(selectedCarPosition).getPackage().get(i).getId(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getModelId(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getType(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getName(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getDescription(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getTime(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getKM(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getAmount(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getNotes(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getDate()
                        ));
                    }
                    else
                    {
                        day_beens.add(new Package_Been(
                                model_beens.get(selectedCarPosition).getPackage().get(i).getId(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getModelId(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getType(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getName(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getDescription(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getTime(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getKM(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getAmount(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getNotes(),
                                model_beens.get(selectedCarPosition).getPackage().get(i).getDate()
                        ));
                    }
                }
            }
        }

        selectedHourDayFlag = num;

        if (num == 0)
        {
            ll_hours.setBackgroundResource(R.drawable.pick_package_box_selected);
            ll_days.setBackgroundResource(R.drawable.pick_package_box);
            tv_hours.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_days.setTextColor(getResources().getColor(R.color.colorTextBlack));

            FragmentTransaction ftHours = getSupportFragmentManager().beginTransaction();
            ftHours.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            ftHours.replace(R.id.fl_frame,new HoursFragment());
            ftHours.commit();
        }
        else
        {
            ll_days.setBackgroundResource(R.drawable.pick_package_box_selected);
            ll_hours.setBackgroundResource(R.drawable.pick_package_box);
            tv_days.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_hours.setTextColor(getResources().getColor(R.color.colorTextBlack));

            FragmentTransaction ftDays = getSupportFragmentManager().beginTransaction();
            ftDays.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
            ftDays.replace(R.id.fl_frame,new DaysFragment());
            ftDays.commit();
        }

    }

    public void selectDateAndTime()
    {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        strSelectedDate = dayOfMonth+"/"+(monthOfYear + 1)+"/"+year;

                        if ((monthOfYear +1) < 10 )
                        {

                            if (dayOfMonth < 10)
                            {
                                tv_pickup_date.setText(year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth);
                            }
                            else
                            {
                                tv_pickup_date.setText(year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }
                        else
                        {
                            if (dayOfMonth < 10)
                            {
                                tv_pickup_date.setText(year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth);
                            }
                            else
                            {
                                tv_pickup_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }
//                        dateTime = "";
//                        showTimePicker(dayOfMonth,(monthOfYear + 1),year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void todayDate()
    {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        strCurrentDate = mDay+"/"+(mMonth + 1)+"/"+mYear;
        strSelectedDate = mDay+"/"+(mMonth + 1)+"/"+mYear;

        checkHour = mHour;
        checkMinut = mMinute;

        if ((mMonth +1) < 10 )
        {
            if (mDay < 10)
            {
                tv_pickup_date.setText(mYear + "-0" + (mMonth + 1) + "-0" + mDay);
            }
            else
            {
                tv_pickup_date.setText(mYear + "-0" + (mMonth + 1) + "-" + mDay);
            }
        }
        else
        {
            if (mDay < 10)
            {
                tv_pickup_date.setText(mYear + "-" + (mMonth + 1) + "-0" + mDay);
            }
            else
            {
                tv_pickup_date.setText(mYear + "-" + (mMonth + 1) + "-" + mDay);
            }
        }

        strCurrentTime = mHour + ":" + mMinute;
        strSelectedTime = mHour + ":" + mMinute;

        String AM_PM ;

        if(mHour < 12)
        {
            AM_PM = getResources().getString(R.string.am_cap);
        }
        else
        {
            AM_PM = getResources().getString(R.string.pm_cap);
        }

        int hour;

        if (mHour == 0 || mHour==12)
        {
            hour = 12;
        }
        else if (mHour < 12)
        {
            hour = mHour;
        }
        else
        {
            hour = mHour - 12;
        }

        Log.e("call","hour ========= "+hour);
        Log.e("call","minutes ========= " + mMinute);



        tv_pickup_time.setText(hour + ":" + mMinute + " "+AM_PM);

    }

    public void setCurrentAndSelectedTime()
    {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        strCurrentDate = mYear+"-"+(mMonth + 1)+"-"+mDay;
        strSelectedDate = tv_pickup_date.getText().toString();

        strCurrentTime = mHour + ":" + mMinute;

        Log.e("call","selectedTime = "+strSelectedTime);
        Log.e("call","selectedDate = "+strSelectedDate);
        Log.e("call","currentDate = "+strCurrentDate);
        Log.e("call","currentTime = "+strCurrentTime);

        float dayDifference = getCountOfDays(strCurrentDate,strSelectedDate);
        boolean isValidTime = checkValidTime(strSelectedDate+" "+checkHour+":"+checkMinut+":00");

        Log.e("call","diffenence = "+dayDifference);
        Log.e("call","isValidTime = "+isValidTime);

        if (selectedHourDayFlag == 0)
        {
            if (isValidTime)
            {
                Intent intent = new Intent(activity,CustomPackageDetailActivity.class);
                intent.putExtra("pickup_address",tv_pickup_address.getText().toString());
                intent.putExtra("pickup_date",tv_pickup_date.getText().toString());
                intent.putExtra("pickup_time",tv_pickup_time.getText().toString());
                intent.putExtra("carPos",selectedCarPosition);
                intent.putExtra("tabPos",selectedHourDayFlag);
                startActivity(intent);
            }
            else
            {
                Log.e("call","please select valid time");
                mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.pre_booking_validation));

            }
        }
        else
        {
            if (dayDifference > 30)
            {
                Intent intent = new Intent(activity,CustomPackageDetailActivity.class);
                intent.putExtra("pickup_address",tv_pickup_address.getText().toString());
                intent.putExtra("pickup_date",tv_pickup_date.getText().toString());
                intent.putExtra("pickup_time",tv_pickup_time.getText().toString());
                intent.putExtra("carPos",selectedCarPosition);
                intent.putExtra("tabPos",selectedHourDayFlag);
                startActivity(intent);
            }
            else
            {
                Log.e("call","please select valid date");
                mySnackBar.showSnackBar(ll_rootView,getResources().getString(R.string.pre_booking_validation));
            }
        }
    }


    public float getCountOfDays(String strCurrent, String strSelected) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        Date currentDate = null, selectedDate = null, todayWithZeroTime = null;
        try {
            currentDate = dateFormat.parse(strCurrent);
            selectedDate = dateFormat.parse(strSelected);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (currentDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(currentDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(selectedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return dayCount;
    }

    public boolean checkValidTime(String selectedDateTime)
    {


        try
        {

            Log.e("call","selected = "+selectedDateTime);
            Log.e("call","current = "+new Date().toString());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date selected = simpleDateFormat.parse(selectedDateTime);
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(cal.getTime());
            Date current =simpleDateFormat.parse(strDate);

            long diff = selected.getTime() - current.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;

            Log.e("call","selected = "+selected.toString());
            Log.e("call","current = "+current.toString());
            Log.e("call","difference in minutes = "+minutes);

            if (hours>=2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e("call","exception in date formate = "+e.getMessage());
            return false;
        }
    }

    public void showTimePicker()
    {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(CustomPackageActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute)
            {
//                tv_pickup_time.setText( hourOfDay + ":" + minute);

                strSelectedTime = hourOfDay + ":" + minute;

                checkHour = hourOfDay;
                checkMinut = minute;

                String AM_PM ;
                if(hourOfDay < 12)
                {
                    AM_PM = getResources().getString(R.string.am_cap);
                }
                else
                {
                    AM_PM = getResources().getString(R.string.pm_cap);
                }

                int hour;

                if (hourOfDay == 0 || hourOfDay==12)
                {
                    hour = 12;
                }
                else if (hourOfDay < 12)
                {
                    hour = hourOfDay;
                }
                else
                {
                    hour = hourOfDay - 12;
                }

                Log.e("call","hour ========= "+hour);
                Log.e("call","minutes ========= " + minute);



                tv_pickup_time.setText(hour + ":" + minute + " "+AM_PM);

            }
        }, mHour, mMinute, false);//Yes 24 hour time
        mTimePicker.setTitle(getResources().getString(R.string.select_time));
        mTimePicker.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TruckzShipperApplication.setCurrentActivity(activity);
    }
}
