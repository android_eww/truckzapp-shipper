package com.truckz.shipper.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Constants;
import com.truckz.shipper.fragment.LoginFragment;
import com.truckz.shipper.fragment.SignupFragment;
import com.truckz.shipper.notification.FCMUtil;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.other.MyAlertDialog;
import com.truckz.shipper.view.CTextViewMedium;

public class LoginActivity1 extends AppCompatActivity implements View.OnClickListener {

    public String TAG = "loginActivity1";
    public static LoginActivity1 activity;

    public LinearLayout llSignin, llSignup;
    public CTextViewMedium tvSignin, tvSignup;

    public LoginFragment loginFragment;
    public SignupFragment signupFragment;

    public final int PERMISSION_REQUEST_CODE = 1;
    public int selectedTab = 0;

    public static String Email="", phonNumber="", Password="", ConfirmPassword="",FullName="", FirstName="", LastName="",
             latitude="",longitude="",deviceType="2", token="", SOCIAL_TYPE = "", SocialId="", socialProURL="", gender="male",
            referralCode="", otp="", selectedDate,city="";

    public static String FACEBOOK="Facebook", GOOGLE="Google";

    public static byte[] userImage = null;
    public boolean permissionGranted = false;
    public boolean locationFound = false;

    public GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);

        activity = LoginActivity1.this;
        gpsTracker = new GPSTracker(activity);

        selectedTab = 0;
        permissionGranted = false;
        locationFound = false;
        Email="";
        phonNumber="";
        Password="";
        ConfirmPassword="";
        FullName="";
        FirstName="";
        LastName="";
        latitude="";
        longitude="";
        deviceType="2";
        token="";
        SOCIAL_TYPE = "";
        SocialId="";
        socialProURL="";
        gender="male";
        referralCode="";
        FACEBOOK="Facebook";
        GOOGLE="Google";
        otp="";
        selectedDate="";

        Log.e(TAG,"######################### = "+ FCMUtil.getFcmToken(activity));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                String strToken = FCMUtil.getFcmToken(activity);
                token=strToken;
                Log.e("call","token = " + token);
            }
        },1500);

        init();
    }

    public void init()
    {
        Log.e(TAG,"init()");

        llSignin = findViewById(R.id.llSignin);
        llSignup = findViewById(R.id.llSignup);
        tvSignin = findViewById(R.id.tvSignin);
        tvSignup = findViewById(R.id.tvSignup);

        llSignin.setOnClickListener(this);
        llSignup.setOnClickListener(this);

        checkGPS();
        callSignin();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llSignin:
                if (selectedTab == 1)
                {
                    callSignin();
                }
                break;

            case R.id.llSignup:

                FirstName = "";
                LastName = "";
                Email = "";
                SocialId = "";
                SOCIAL_TYPE = "";

                if (selectedTab == 0)
                {
                    callSignup();
                }
                break;
        }
    }

    public void checkGPS()
    {
        Log.e(TAG,"checkGPS() 1111111111111");
        if (checkPermission())
        {
            permissionGranted = true;
            Log.e(TAG,"checkGPS() 222222222222");
            gpsTracker = new GPSTracker(activity);
            if (gpsTracker.canGetLocation())
            {
                Log.e(TAG,"checkGPS() 3333333333333");
                latitude = gpsTracker.getLatitude()+"";
                longitude = gpsTracker.getLongitude()+"";
                if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") &&
                        !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") &&
                        !longitude.equalsIgnoreCase("0"))
                {
                    locationFound = true;
                    Constants.newgpsLatitude = latitude+"";
                    Constants.newgpsLongitude = longitude+"";

                    Log.e(TAG,"checkGPS() latitude:- " + Constants.newgpsLatitude);
                    Log.e(TAG,"checkGPS() longitude:- " + Constants.newgpsLongitude);
                }
                else
                {
                    locationFound = false;
                }
            }
            else
            {
                locationFound = false;
                Log.e(TAG,"checkGPS() 44444444444444");
                AlertMessageNoGps();
            }
        }
        else
        {
            Log.e(TAG,"checkGPS() 5555555555555");
            permissionGranted = false;
            requestPermission();
        }
    }

    private boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        } else
        {
            return false;
        }
    }

    private void requestPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionsResult() 11 = "+requestCode);

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    permissionGranted = true;
                    Log.e("call","onRequestPermissionsResult() 22 = ");

                    gpsTracker = new GPSTracker(activity);
                    if (gpsTracker.canGetLocation())
                    {

                        Log.e("call","onRequestPermissionsResult() 33 = ");
                        latitude = gpsTracker.getLatitude()+"";
                        longitude = gpsTracker.getLongitude()+"";
                        if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
                        {
                            Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                            Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                            locationFound = true;
                        }
                        else
                        {
                            locationFound = false;
                        }
                    }
                    else
                    {
                        locationFound = false;
                        Log.e("call","onRequestPermissionsResult() 44 = ");
                        AlertMessageNoGps();
                    }
                }
                else
                {
                    permissionGranted = false;
                    Log.e("call","onRequestPermissionsResult() 55 = ");

                    MyAlertDialog dialog = new MyAlertDialog(activity);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

    public void AlertMessageNoGps()
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>"+getResources().getString(R.string.new_gps_settings_message)+"</font>"));

        String positiveText = getResources().getString(R.string.new_gps_settings_positive_button);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        String negativeText = getResources().getString(R.string.new_gps_settings_nagative_button);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        final android.app.AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorThemeDark));
                dialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorThemeDark));
            }
        });

        dialog.show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            permissionGranted = true;
            gpsTracker = new GPSTracker(activity);

            if (gpsTracker.canGetLocation())
            {
                Log.e("onPostResum","call");
                gpsTracker = new GPSTracker(activity);
                latitude = gpsTracker.getLatitude()+"";
                longitude = gpsTracker.getLongitude()+"";

                if (latitude!=null && longitude!=null && !latitude.equalsIgnoreCase("") && !longitude.equalsIgnoreCase("") && !latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
                {
                    Constants.newgpsLatitude = latitude+"";
                    Constants.newgpsLongitude = longitude+"";
                    locationFound = true;
                }
                else
                {
                    locationFound = false;
                }
            }
            else
            {
                locationFound = false;
            }
        }
        else
        {
            permissionGranted = false;
        }
    }

    public void callSignin()
    {
        Log.e(TAG,"callSignin()");
        selectedTab = 0;

        llSignin.setBackgroundColor(getResources().getColor(R.color.colorBlack40));
        llSignup.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvSignin.setTextColor(getResources().getColor(R.color.colorTextWhite));
        tvSignup.setTextColor(getResources().getColor(R.color.colorTextWhite40));

        loadSignin();
    }

    public void callSignup()
    {
        Log.e(TAG,"callSignup()");
        selectedTab = 1;

        llSignin.setBackgroundColor(getResources().getColor(R.color.transparent));
        llSignup.setBackgroundColor(getResources().getColor(R.color.colorBlack40));
        tvSignin.setTextColor(getResources().getColor(R.color.colorTextWhite40));
        tvSignup.setTextColor(getResources().getColor(R.color.colorTextWhite));

        loadSignup();
    }

    public void loadSignin()
    {
        Log.e(TAG,"loadSigninFragment()");

        loginFragment = new LoginFragment();

        FragmentManager fragmentManager_items = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
        fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come,R.anim.gla_back_gone,R.anim.gla_there_come,R.anim.gla_there_gone);
        fragmentTransaction_items.replace(R.id.llContainer, loginFragment, "Login");
        fragmentTransaction_items.commitAllowingStateLoss();
    }

    public void loadSignup()
    {
        Log.e(TAG,"loadSigninFragment()");

        signupFragment = new SignupFragment();

        FragmentManager fragmentManager_items = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
        fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come,R.anim.gla_there_gone,R.anim.gla_back_come,R.anim.gla_back_gone);
        fragmentTransaction_items.replace(R.id.llContainer, signupFragment, "Signup");
        fragmentTransaction_items.commitAllowingStateLoss();
    }

    public void getToken()
    {
        /*new Thread(new Runnable()
        {
            public void run()
            {

            }
        }).start();*/

        try
        {
                    /*InstanceID instanceID = InstanceID.getInstance(activity);
                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.e("TAG", "GCM Registration Token: " + token);*/

            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task)
                        {
                            if (!task.isSuccessful())
                            {
                                Log.e(TAG, "getToken() getInstanceId failed:- ", task.getException());
                                return;
                            }

                            // Get new Instance ID token
                            token = task.getResult().getToken();
                            Log.e(TAG,"getToken() token:- " + token);
                        }
                    });
        }
        catch (Exception e)
        {
            Log.e(TAG, "getToken() Failed to complete token refresh", e);
        }
    }

    @Override
    protected void onResume()
    {
        Log.e(TAG,"onResume()");
        super.onResume();
        getToken();
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");

        if (selectedTab == 1 &&
                signupFragment != null )
        {
            callSignin();
            signupFragment.resetData();
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (loginFragment!=null && loginFragment.isAdded())
        {
            loginFragment.onActivityResult(requestCode,resultCode,data);
        }
    }
}
