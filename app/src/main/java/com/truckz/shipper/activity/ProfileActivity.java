package com.truckz.shipper.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.roundedimage.RoundedTransformationBuilder;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private String TAG = "ProfileActivity";
    public static ProfileActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    private CTextViewBold tv_Title;
    private CardView cv_change_pass, cv_save;
    private ImageView iv_ProfileImage;
    private CTextViewLight tv_Email, tv_PhoneNumber;
    private CustomEditText et_FirstName, tv_date_of_birth, et_Address, etCity;
    private LinearLayout ll_date_birth;
    private RadioButton radioButtonMale,radioButtonFemale;
    private String maleFemal = "";

    private DialogClass dialogClass;
    private AQuery aQuery;
    private Transformation mTransformation;
    private byte[] image;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;
    public static boolean changeImage = false;

    private String filename="";
    private SharedPreferences permissionStatus;
    public static String base64 = "";
    public static Bitmap bmp = null;
    private boolean sentToSettings = false;
    private Dialog dialog;
    private Uri selectedImageUri;
    private CircleImageView imageProfile;

    Uri mImageUri;
    private Uri imageUri;
    public static Bitmap bitmapImage;
    String picturePath;
    static Uri uriGallery;

    private String selectedDate;
    private int mYear, mMonth, mDay;
    public static String companyName = "";
    public static boolean cameraClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        activity = ProfileActivity.this;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        changeImage = false;
        image = null;
        maleFemal = "";
        bmp = null;
        base64 = "";
        mImageUri=null;
        imageUri=null;
        picturePath=null;
        uriGallery=null;
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);

        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        mySnackBar = new MySnackBar(activity);

        companyName = Build.MANUFACTURER;

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(getResources().getColor(R.color.colorThemeRed))
                .borderWidthDp(2)
                .oval(true)
                .build();

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        tv_Title = findViewById(R.id.title_textview);
        tv_Title.setText(getResources().getString(R.string.profile));

        tv_Email = findViewById(R.id.email_textview);
        tv_PhoneNumber = findViewById(R.id.phone_textview);
        et_FirstName = findViewById(R.id.input_first_name);
        tv_date_of_birth = findViewById(R.id.tv_date_of_birth);
        ll_date_birth = findViewById(R.id.ll_date_birth);
        et_Address = findViewById(R.id.input_address);
        etCity = findViewById(R.id.etCity);

        ll_RootLayout = findViewById(R.id.rootView);

        radioButtonMale = findViewById(R.id.radio_button_male);
        radioButtonFemale = findViewById(R.id.radio_button_female);

        iv_ProfileImage = findViewById(R.id.profile_activity_image);
        imageProfile = findViewById(R.id.image_profile);

        cv_change_pass = findViewById(R.id.cv_change_pass);
        cv_save = findViewById(R.id.cv_save);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        cv_change_pass.setOnClickListener(activity);
        cv_save.setOnClickListener(activity);
        iv_ProfileImage.setOnClickListener(activity);
        imageProfile.setOnClickListener(activity);
//        tv_date_of_birth.setOnClickListener(activity);
        ll_date_birth.setOnClickListener(activity);

        radioButtonMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maleFemal = "male";
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(true);
            }
        });

        radioButtonFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maleFemal = "female";
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
        });

        setUserDetail();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.cv_change_pass:
                gotoChangePassword();
                break;

//            case R.id.tv_date_of_birth:
//                selectDateAndTime();
//                break;

            case R.id.ll_date_birth:
                selectDateAndTime();
                break;

            case R.id.cv_save:
                Log.e("call","save clicked");
                checkValidation();
                break;

            case R.id.profile_activity_image:
                Log.e("call","image clicked");
                takePermission();
                break;

            case R.id.image_profile:
                Log.e("call","image clicked");
                takePermission();
                break;
        }
    }

    public void gotoChangePassword()
    {
        Intent intent = new Intent(ProfileActivity.this,ChangePasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    public void setUserDetail()
    {
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity).equalsIgnoreCase(""))
        {
            et_FirstName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,activity));

        }
        else
        {
            et_FirstName.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity).equalsIgnoreCase(""))
        {
            tv_Email.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL,activity));
        }
        else
        {
            tv_Email.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity).equalsIgnoreCase(""))
        {
            tv_PhoneNumber.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,activity));
        }
        else
        {
            tv_PhoneNumber.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity).equalsIgnoreCase(""))
        {
            et_Address.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,activity));
        }
        else
        {
            et_Address.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CITY,activity)!=null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CITY,activity).equalsIgnoreCase(""))
        {
            etCity.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CITY,activity));
        }
        else
        {
            etCity.setText("");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,activity).equalsIgnoreCase(""))
        {
            String date = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,activity);
            SimpleDateFormat simpleDateFormate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat conDate = new SimpleDateFormat("dd-MM-yyyy");

            String dateOfBirth = "";
            try {

                Date date1 = simpleDateFormate.parse(date);
                dateOfBirth = conDate.format(date1);

            } catch (Exception e) {
                Log.e(TAG, "ERROR : "+e.getMessage());
            }

            tv_date_of_birth.setText(dateOfBirth);
            selectedDate = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,activity);
        }
        else
        {
            tv_date_of_birth.setText("");
            selectedDate = "";
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase(""))
        {
            maleFemal = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity);

            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase("male"))
            {
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(true);
            }
            else if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_GENDER,activity).equalsIgnoreCase("female"))
            {
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
            else
            {
                radioButtonFemale.setChecked(false);
                radioButtonMale.setChecked(false);
            }
        }
        else
        {
            maleFemal = "";
            radioButtonFemale.setChecked(false);
            radioButtonMale.setChecked(false);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity)!=null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity).equals(""))
        {
            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE,activity))
                    .into(imageProfile);
        }
        else
        {
            iv_ProfileImage.setVisibility(View.VISIBLE);
            imageProfile.setVisibility(View.GONE);
            iv_ProfileImage.setImageResource(R.mipmap.man_gray);
        }
    }

    public void checkValidation()
    {
        if (et_FirstName.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_full_name));
            et_FirstName.setFocusableInTouchMode(true);
            et_FirstName.requestFocus();
        }
//        else if (tv_date_of_birth.toString() != null &&
//                tv_date_of_birth.getText().toString().equalsIgnoreCase(""))
//        {
//            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_date_of_birth));
//        }
        else if (et_Address.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_address));
            et_Address.setFocusableInTouchMode(true);
            et_Address.requestFocus();
        }
        else if (etCity.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_city));
            etCity.setFocusableInTouchMode(true);
            etCity.requestFocus();
        }
        else if (maleFemal.equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_select_gender));
        }
        else
        {
            if (Global.isNetworkconn(activity))
            {
                call_UpdateProfile();
            }
            else
            {
                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
            }
        }
    }

    private void call_UpdateProfile()
    {
        Log.e(TAG,"call_UpdateProfile()");

        String url = WebServiceAPI.API_UPDATE_PROFILE;

        //http://54.206.55.185/web/Passenger_Api/UpdateProfile
        //PassengerId,Fullname,Gender,Address,Image
        Log.e(TAG,"call_UpdateProfile() url = "+url);

        String passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        String fullName = et_FirstName.getText().toString().trim();
        String address = et_Address.getText().toString().trim();
        String city = etCity.getText().toString().trim();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID,passangerId);
        params.put(WebServiceAPI.PARAM_FULL_NAME,fullName);
        params.put(WebServiceAPI.PARAM_GENDER,maleFemal);
        params.put(WebServiceAPI.PARAM_ADDRESS,address);
        params.put(WebServiceAPI.PARAM_CITY,city);
        params.put(WebServiceAPI.PARAM_DATE_OF_BIRTH,selectedDate);

        Log.e(TAG,"call_UpdateProfile() changeImage flag = "+changeImage);

        if (changeImage)
        {
            if (image!=null)
            {
                Log.e(TAG,"call_UpdateProfile() image byte array = "+image.toString());
                params.put(WebServiceAPI.PARAM_IMAGE,image);
            }
            else
            {
                Log.e(TAG,"call_UpdateProfile() image byte array null");
            }
        }
        else
        {
            Log.e(TAG,"call_UpdateProfile() image not change");
        }

        Log.e(TAG,"call_UpdateProfile() param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"call_UpdateProfile() responseCode = " + responseCode);
                    Log.e(TAG,"call_UpdateProfile() Response = " + json);
                    dialogClass.hideDialog();
                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        if (profile.has("Fullname"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,profile.getString("Fullname"),activity);
                                        }

                                        if (profile.has("Image"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,profile.getString("Image"),activity);
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,profile.getString("Gender"),activity);
                                        }

                                        if (profile.has("Address"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,profile.getString("Address"),activity);
                                        }

                                        if (profile.has("City"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CITY,profile.getString("City"),activity);
                                        }

                                        if (profile.has("DOB"))
                                        {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DATE_OF_BIRTH,profile.getString("DOB"),activity);
                                        }
                                    }
                                }
                                dialogClass.hideDialog();
                                showSuccessMessage(getResources().getString(R.string.your_profile_update_successfully),getResources().getString(R.string.success_message));
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));

                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"call_UpdateProfile() Exception:- "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void showSuccessMessage(String message, String title)
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok, tv_Message, tv_Title;
        LinearLayout ll_Ok;
        ImageView iv_close;

        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,300);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,300);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,300);
            }
        });

        dialog.show();
    }

    //forselect image
    public void takePermission()
    {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getResources().getString(R.string.storage_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_storage_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();
        }
        else
        {
            seletcOption();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }

            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
            }
        }
    }

    public void seletcOption()
    {
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_camera, null);

        dialog = new Dialog(ProfileActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final LinearLayout dialog_ok_layout;
        final TextView dialog_gallery_message,dialog_camera_message,dialog_ok_textview;
        ImageView dialog_close;

        dialog_close = (ImageView) view.findViewById(R.id.dialog_close);

        dialog_ok_layout = (LinearLayout) view.findViewById(R.id.dialog_ok_layout);

        dialog_camera_message = (TextView) view.findViewById(R.id.dialog_camera_message);
        dialog_gallery_message = (TextView) view.findViewById(R.id.dialog_gallery_message);
        dialog_ok_textview = (TextView) view.findViewById(R.id.dialog_ok_textview);


        dialog_camera_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCamera();
            }
        });

        dialog_gallery_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                selectPhoto();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog_ok_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    //Image Pick From Camera
    private void openCamera()
    {
        if (companyName != null && companyName.toLowerCase().contains("samsung"))
        {
            Log.e("Comapny Name1 ", "Device Name1:- " + companyName);
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 0);
        }
        else
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, 0);
        }
    }

    //Image Pick From Gallary
    private void selectPhoto() {
        Intent intent_gallery = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, 1);
    }

//    public void selectPhoto()
//    {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("image/*");
//        startActivityForResult(intent,1);
//    }
//
//    public void openCamera()
//    {
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
//        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//        startActivityForResult(intent, 2);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            Log.e("resultCode"," = "+resultCode);

            if ( resultCode == RESULT_OK )
            {

                if (requestCode == 1 && resultCode == RESULT_OK && null != data)
                {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    uriGallery = data.getData();
                    Log.d("2222",""+data.getData());
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("filePathUri", "" + picturePath);
                    mImageUri = Uri.parse(picturePath);
                /*frameActivity( filePathUri );*/
                    bitmapImage = BitmapFactory.decodeFile(String.valueOf(mImageUri));
                    bitmapImage = getResizedBitmap(bitmapImage,400);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, stream);

                    byte[] byteArray = stream.toByteArray();
                    image = byteArray;
                    changeImage = true;
                    imageProfile.setVisibility(View.VISIBLE);
                    iv_ProfileImage.setVisibility(View.GONE);
                    imageProfile.setImageBitmap(bitmapImage);

                }
                else if(requestCode==0)
                {
                    if (companyName != null && companyName.toLowerCase().contains("samsung"))
                    {
                        Log.e("Comapny Name2 ", "Device Name2 :- " + companyName);

                        bitmapImage = (Bitmap) data.getExtras().get("data");
                        Log.e("Call Data","Height:- " +  bitmapImage.getHeight() + "\nWidth:- " + bitmapImage.getWidth());

                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, bytes);

                        byte[] byteArray = bytes.toByteArray();
                        image = byteArray;
                        changeImage = true;
                        cameraClick = true;
                        imageProfile.setVisibility(View.VISIBLE);
                        iv_ProfileImage.setVisibility(View.GONE);
                        imageProfile.setImageBitmap(bitmapImage);
                    }
                    else
                    {
                        Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        Log.e("#########","bmp height = "+thumbnail.getHeight());
                        Log.e("#########","bmp width = "+thumbnail.getWidth());
                        thumbnail = getResizedBitmap(thumbnail,400);
                        bitmapImage=thumbnail;
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        thumbnail.compress(Bitmap.CompressFormat.PNG, 10, bytes);
                        byte[] byteArray = bytes.toByteArray();
                        image = byteArray;
                        changeImage = true;
                        imageProfile.setVisibility(View.VISIBLE);
                        iv_ProfileImage.setVisibility(View.GONE);
                        imageProfile.setImageBitmap(bitmapImage);
                    }
                }
                else
                {
                    image = null;
                    changeImage = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        if (cameraClick = true)
        {
            Log.e("Call OnResume:- ", "Camera Click True");
            if (companyName !=null && companyName.contains("samsung"))
            {
                Log.e("Call OnResume:- ", "Company Name" + companyName);

                if (bitmapImage != null)
                {
                    Log.e("Call OnResume:- ", "Bitmap Not Null ");

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, bytes);

                    byte[] byteArray = bytes.toByteArray();
                    image = byteArray;
                    changeImage = true;
                    cameraClick = true;
                    imageProfile.setVisibility(View.VISIBLE);
                    iv_ProfileImage.setVisibility(View.GONE);
                    imageProfile.setImageBitmap(bitmapImage);
                }
            }

        }

    }

    public void selectDateAndTime()
    {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,R.style.datepicker,
                new DatePickerDialog.OnDateSetListener()
                {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth)
                    {
                        selectedDate = "";

                        if ((monthOfYear +1) < 10 )
                        {

                            if (dayOfMonth < 10)
                            {
                                selectedDate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                                tv_date_of_birth.setText(year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth);
                            }
                            else
                            {
                                selectedDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                                tv_date_of_birth.setText(year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }
                        else
                        {
                            if (dayOfMonth < 10)
                            {
                                selectedDate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                                tv_date_of_birth.setText(year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth);
                            }
                            else
                            {
                                selectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                tv_date_of_birth.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }

                        Log.e("selectedDate","Date:- "+ selectedDate);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
        {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
