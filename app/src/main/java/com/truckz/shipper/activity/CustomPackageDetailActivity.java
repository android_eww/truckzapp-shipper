package com.truckz.shipper.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.view.CustomSpinner;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomPackageDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static CustomPackageDetailActivity activity;
    private LinearLayout ll_rootView, ll_back, select_card_layout, ll_confirm;
    private ImageView iv_back, iv_call;
    private TextView tv_title, tv_pickup_location, tv_pickup_date, tv_pickup_time, tv_package_type, tv_days, tv_distance, tv_vehicle_type,
            tv_amount, tv_km_charge, tv_hours_charge, tv_confirm;

    private CustomSpinner Spinner_paymentMethod;
    private CustomerAdapter customerAdapter;
    public static String cardId="";
    public static String cardNumber="";
    public static String cardType="";
    public static String selectedPaymentType = "";
    public static int spinnerSelectedPosition = 0;
    public static boolean isCardSelected = false;

    private EditText et_promo_code;

    private DialogClass dialogClass;
    private AQuery aQuery;

    private int selectedCarPOS = 0;
    private int selectedHourDayFlag = 0; //0 = for hours, 1=days
    private String pickup_address="",pickup_date="",pickup_time="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_package_detail);

        activity = CustomPackageDetailActivity.this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        cardId="";
        cardNumber="";
        cardType="";
        spinnerSelectedPosition = 0;
        selectedPaymentType = "";

        if (this.getIntent()!=null)
        {
            selectedHourDayFlag = this.getIntent().getIntExtra("tabPos",-1);
            selectedCarPOS = this.getIntent().getIntExtra("carPos",-1);

            if (this.getIntent().getStringExtra("pickup_address")!=null)
            {
                pickup_address = this.getIntent().getStringExtra("pickup_address");
            }
            else
            {
                pickup_address = "";
            }

            if (this.getIntent().getStringExtra("pickup_date")!=null)
            {
                pickup_date = this.getIntent().getStringExtra("pickup_date");
            }
            else
            {
                pickup_date = "";
            }

            if (this.getIntent().getStringExtra("pickup_time")!=null)
            {
                pickup_time = this.getIntent().getStringExtra("pickup_time");
            }
            else
            {
                pickup_time = "";
            }
        }
        else
        {
            selectedCarPOS = 0;
            selectedHourDayFlag = 0;
            pickup_address="";
            pickup_date="";
            pickup_time="";
        }

        init();
    }

    @Override
    protected void onPause() {
        Log.i("Life Cycle", "onPause");
        if (Spinner_paymentMethod!=null)
        {
            Spinner_paymentMethod.onDetachedFromWindow();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);
        saveCardList();
        if (isCardSelected)
        {
            Spinner_paymentMethod.setSelection((2+spinnerSelectedPosition));
        }
    }

    private void init()
    {
        ll_rootView = (LinearLayout) findViewById(R.id.ll_rootView);
        ll_back = (LinearLayout) findViewById(R.id.back_layout);
        iv_back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        tv_title = (TextView) findViewById(R.id.title_textview);

        select_card_layout = (LinearLayout) findViewById(R.id.select_card_layout);
        ll_confirm = (LinearLayout) findViewById(R.id.ll_confirm);

        tv_pickup_location = (TextView) findViewById(R.id.tv_pickup_location);
        tv_pickup_date = (TextView) findViewById(R.id.tv_pickup_date);
        tv_pickup_time = (TextView) findViewById(R.id.tv_pickup_time);
        tv_package_type = (TextView) findViewById(R.id.tv_package_type);
        tv_days = (TextView) findViewById(R.id.tv_days);
        tv_distance = (TextView) findViewById(R.id.tv_distance);
        tv_vehicle_type = (TextView) findViewById(R.id.tv_vehicle_type);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_km_charge = (TextView) findViewById(R.id.tv_km_charge);
        tv_hours_charge = (TextView) findViewById(R.id.tv_hours_charge);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);

        Spinner_paymentMethod = (CustomSpinner) findViewById(R.id.Spinner_paymentMethod);
        et_promo_code = (EditText) findViewById(R.id.et_promo_code);

        tv_title.setText(getResources().getString(R.string.confirm_package));

        ll_back.setOnClickListener(activity);
        iv_back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        select_card_layout.setOnClickListener(activity);
        ll_confirm.setOnClickListener(activity);

        customerAdapter = new CustomerAdapter(activity, MainActivity.cardListBeens);
        Spinner_paymentMethod.setAdapter(customerAdapter);

        Spinner_paymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cardId = MainActivity.cardListBeens.get(position).getId();
                cardNumber = MainActivity.cardListBeens.get(position).getCardNum_();

                if (position == (MainActivity.cardListBeens.size() -1))
                {
                    Log.e("call","1111");
                    selectedPaymentType = "wallet";//PaymentType : cash,wallet,card

                }
                else if (position == (MainActivity.cardListBeens.size() -2))
                {
                    Log.e("call","2222");
                    selectedPaymentType = "cash";//PaymentType : cash,wallet,card
                }
                else
                {
                    Log.e("call","3333");
                    selectedPaymentType = "card";//PaymentType : cash,wallet,card
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

                Log.e("call","onNothing selected");
            }
        });

        tv_pickup_location.setText(pickup_address);
        tv_pickup_date.setText(pickup_date);
        tv_pickup_time.setText(pickup_time);

        if (selectedHourDayFlag==0)
        {
            tv_package_type.setText(getResources().getString(R.string.hours));
        }
        else if (selectedHourDayFlag==1)
        {
            tv_package_type.setText(getResources().getString(R.string.days));
        }
        else
        {
            tv_package_type.setText("");
        }

        if (selectedHourDayFlag==0)
        {
            if (CustomPackageActivity.hour_beens!=null && (CustomPackageActivity.hour_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_days.setText(CustomPackageActivity.hour_beens.get(CustomPackageActivity.itemPos).getTime());
            }
            else
            {
                tv_days.setText("");
            }

            if (CustomPackageActivity.hour_beens!=null && (CustomPackageActivity.hour_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_distance.setText(CustomPackageActivity.hour_beens.get(CustomPackageActivity.itemPos).getKM()+getResources().getString(R.string.km));
            }
            else
            {
                tv_distance.setText("");
            }

            if (CustomPackageActivity.model_beens!=null && (CustomPackageActivity.model_beens.size()-1)>=selectedCarPOS)
            {
                tv_vehicle_type.setText(CustomPackageActivity.model_beens.get(selectedCarPOS).getModelName());
            }
            else
            {
                tv_vehicle_type.setText("");
            }

            if (CustomPackageActivity.hour_beens!=null && (CustomPackageActivity.hour_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_amount.setText(getResources().getString(R.string.currency)+String.format("%.1f",Double.parseDouble(CustomPackageActivity.hour_beens.get(CustomPackageActivity.itemPos).getAmount())));
            }
            else
            {
                tv_amount.setText("");
            }

        }
        else if (selectedHourDayFlag==1)
        {
            if (CustomPackageActivity.day_beens!=null && (CustomPackageActivity.day_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_days.setText(CustomPackageActivity.day_beens.get(CustomPackageActivity.itemPos).getTime());
            }
            else
            {
                tv_days.setText("");
            }

            if (CustomPackageActivity.day_beens!=null && (CustomPackageActivity.day_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_distance.setText(CustomPackageActivity.day_beens.get(CustomPackageActivity.itemPos).getKM()+getResources().getString(R.string.km));
            }
            else
            {
                tv_distance.setText("");
            }

            if (CustomPackageActivity.model_beens!=null && (CustomPackageActivity.model_beens.size()-1)>=selectedCarPOS)
            {
                tv_vehicle_type.setText(CustomPackageActivity.model_beens.get(selectedCarPOS).getModelName());
            }
            else
            {
                tv_vehicle_type.setText("");
            }

            if (CustomPackageActivity.day_beens!=null && (CustomPackageActivity.day_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                tv_amount.setText(getResources().getString(R.string.currency)+String.format("%.1f",Double.parseDouble(CustomPackageActivity.day_beens.get(CustomPackageActivity.itemPos).getAmount())));
            }
            else
            {
                tv_amount.setText("");
            }
        }
        else
        {
            tv_days.setText("");
            tv_distance.setText("");
            tv_vehicle_type.setText("");
            tv_amount.setText("");
        }


    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.select_card_layout:
                Spinner_paymentMethod.performClick();
                break;

            case R.id.ll_confirm:
                call_Api();
                break;
        }
    }

    private void call_Api()
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID,SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));

        if (selectedHourDayFlag==0)
        {
            if (CustomPackageActivity.hour_beens!=null && (CustomPackageActivity.hour_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                params.put(WebServiceAPI.PARAM_PACKAGE_ID,CustomPackageActivity.hour_beens.get(CustomPackageActivity.itemPos).getId());
            }
        }
        else if (selectedHourDayFlag==1)
        {
            if (CustomPackageActivity.day_beens!=null && (CustomPackageActivity.day_beens.size()-1)>=CustomPackageActivity.itemPos)
            {
                params.put(WebServiceAPI.PARAM_PACKAGE_ID,CustomPackageActivity.day_beens.get(CustomPackageActivity.itemPos).getId());
            }
        }

        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION,tv_pickup_location.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PICKUP_DATE,tv_pickup_date.getText().toString());
        params.put(WebServiceAPI.PARAM_PICKUP_TIME,CustomPackageActivity.strSelectedTime + ":00");
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE,selectedPaymentType);

        if (selectedPaymentType!=null && selectedPaymentType.equalsIgnoreCase("card"))
        {
            params.put(WebServiceAPI.PARAM_CARD_ID,cardId);

        }


        String url = WebServiceAPI.API_BOOK_PACKAGE;

        Log.e("call","url = "+url);
        Log.e("call","params = "+params.toString());

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();

                                String message = getResources().getString(R.string.your_package_book_successfully);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                showSuccessDialog(message,getResources().getString(R.string.package_booking));
                            }
                            else
                            {
                                Log.e("call","book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("call","book later status not found");
                            dialogClass.hideDialog();

                            if (json.has("message"))
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("call","book later json null");
                        dialogClass.hideDialog();

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessDialog(String message,String title)
    {
        TextView tv_Ok, tv_Message, tv_Title;
        LinearLayout ll_Ok;
        ImageView iv_close;

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        /*CustomPackageActivity.activity.finish();
                        finish();
                        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);*/

                        if (CustomPackageActivity.activity != null)
                        {
                            CustomPackageActivity.activity.finish();
                        }

                        Intent intentHistory = new Intent(CustomPackageDetailActivity.this,CustomPackageHistoryActivity.class);
                        startActivity(intentHistory);
                        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
                        finish();

                    }
                },800);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       /* CustomPackageActivity.activity.finish();
                        finish();
                        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);*/

                        Intent intentHistory = new Intent(CustomPackageDetailActivity.this,CustomPackageHistoryActivity.class);
                        startActivity(intentHistory);
                        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
                    }
                },800);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if (message!=null && !message.equalsIgnoreCase(""))
        {
            tv_Message.setText(message);
        }
        else
        {
            tv_Message.setVisibility(View.GONE);
        }

        if (title!=null && !title.equalsIgnoreCase(""))
        {
            tv_Title.setText(title);
        }
        else
        {
            tv_Title.setVisibility(View.GONE);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void saveCardList()
    {
        try
        {
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            MainActivity.cardListBeens.clear();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    if (json.has("cards"))
                    {
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            for (int i=0; i<cards.length(); i++)
                            {
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData!=null)
                                {
                                    String Id="",CardNum="",CardNum_="",Type="",Alias="",Expiry="";

                                    if (cardsData.has("Id"))
                                    {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum"))
                                    {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2"))
                                    {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type"))
                                    {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias"))
                                    {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    if (cardsData.has("Expiry"))
                                    {
                                        Expiry = cardsData.getString("Expiry");
                                    }

                                    MainActivity.cardListBeens.add(new CreditCard_List_Been(Id,CardNum,CardNum_,Type,Alias,Expiry));
                                }
                            }
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","cash","","",""));
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                        }
                        else
                        {
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","cash","","",""));
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                        }
                    }
                    else
                    {
                        MainActivity.cardListBeens.add(new CreditCard_List_Been("","","cash","","",""));
                        MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                    }
                }
                else
                {
                    MainActivity.cardListBeens.add(new CreditCard_List_Been("","","cash","","",""));
                    MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                }
            }
            else
            {
                MainActivity.cardListBeens.add(new CreditCard_List_Been("","","cash","","",""));
                MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
            }
        }
        catch (Exception e)
        {

        }
    }

    public class CustomerAdapter extends ArrayAdapter<CreditCard_List_Been>
    {
        ArrayList<CreditCard_List_Been> customers, tempCustomer, suggestions;

        public CustomerAdapter(Context context, ArrayList<CreditCard_List_Been> objects) {
            super(context, R.layout.row_spinner_item, R.id.spinner_cardnumber, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<CreditCard_List_Been>(objects);
            this.suggestions = new ArrayList<CreditCard_List_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            CreditCard_List_Been customer = getItem(position);
            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, parent, false);
            }
            TextView txtCustomer = (TextView) convertView.findViewById(R.id.spinner_cardnumber);
            LinearLayout ll_Image = (LinearLayout) convertView.findViewById(R.id.spinner_image_layout);
            ImageView iv_Image = (ImageView) convertView.findViewById(R.id.image);
            LinearLayout ll_Add_Card = (LinearLayout) convertView.findViewById(R.id.ll_add_payment_method);

            if (txtCustomer != null)
                txtCustomer.setText(customer.getCardNum_());

            if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("visa"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("mastercard"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_master);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("amex"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_american);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("diners"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_dinner);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("discover"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_discover);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("jcb"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_jcb);
            }
            else if (customer.getCardType()!=null && customer.getCardType().equalsIgnoreCase("other"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            }
            else if (customer.getCardNum_()!=null && customer.getCardNum_().equalsIgnoreCase("cash"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_cash);
            }
            else if (customer.getCardNum_()!=null && customer.getCardNum_().equalsIgnoreCase("wallet"))
            {
                iv_Image.setImageResource(R.drawable.ic_card_icon_wallet);
            }

            if (position == (customers.size()-1))
            {
                if (checkCardList())
                {
                    ll_Add_Card.setVisibility(View.GONE);
                }
                else
                {
                    ll_Add_Card.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                ll_Add_Card.setVisibility(View.GONE);
            }

            ll_Add_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity,Add_Card_In_List_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                }
            });

            return convertView;
        }
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
