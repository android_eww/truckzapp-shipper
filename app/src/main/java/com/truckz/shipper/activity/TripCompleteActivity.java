package com.truckz.shipper.activity;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TripCompleteActivity extends AppCompatActivity implements View.OnClickListener {

    public static TripCompleteActivity activity;
    private LinearLayout ll_Back, ll_right;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title;

    private JSONObject tripCompleted = null;
    private TextView tv_PickupLocation, tv_DropOffLocation, tv_ParcelType, tv_NightFair, tv_TripFee, tv_WaitingCost;
    private TextView tv_BookingCharge, tv_Discount, tv_SubTotal, tv_GrandTotal, tv_Status, tv_TollFee, tv_Tax, tv_DistanceFare,
            tv_SpecialExtraCharge, txt_good_weight,tv_previousDue;
    LinearLayout ll_SpecialExtra_Charge, ll_good_weight;
    private Button btn_ok;
    private String tripFare = "", bookingCharge = "";

    TextView tv_vehicleNo,tv_DriverName,tv_DistanceTravelled,tv_WaitingTime,tv_PaymentType;
    TextView tv_bookinId,tv_startTime,tv_endTime,tv_baseFare,tv_Tripduration,tv_vehicleModel;
    LinearLayout ll_NightFare,ll_disCount,ll_previousDue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_complete);

        activity = TripCompleteActivity.this;
        tripFare = "";
        bookingCharge = "";

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {

            if (SessionSave.getUserSession("TripCompleted", activity) != null && !SessionSave.getUserSession("TripCompleted", activity).equalsIgnoreCase("")) {
                Log.e("TAG", "TripComplete : " + SessionSave.getUserSession("TripCompleted", activity));
                tripCompleted = new JSONObject(SessionSave.getUserSession("TripCompleted", activity));
            }
        } catch (Exception e) {
            Log.e("call", "exception = " + e.getMessage());
        }

        init();
    }

    private void init() {

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_right = (LinearLayout) findViewById(R.id.ll_right);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText(getResources().getString(R.string.trip_detail));
        btn_ok = (Button) findViewById(R.id.btn_ok);

        tv_PaymentType =  findViewById(R.id.tv_PaymentType);
        tv_WaitingTime =  findViewById(R.id.tv_WaitingTime);
        tv_DistanceTravelled =  findViewById(R.id.tv_DistanceTravelled);
        tv_DriverName =  findViewById(R.id.tv_DriverName);
        tv_vehicleNo =  findViewById(R.id.tv_vehicleNo);
        ll_previousDue =  findViewById(R.id.ll_previousDue);
        tv_previousDue =  findViewById(R.id.tv_previousDue);

        tv_PickupLocation = (TextView) findViewById(R.id.activity_trip_complete_textview_pickup_location);
        tv_DropOffLocation = (TextView) findViewById(R.id.activity_trip_complete_textview_dropoff_location);
        tv_ParcelType = (TextView) findViewById(R.id.activity_trip_complete_textview_parcel_type);
        tv_NightFair = (TextView) findViewById(R.id.activity_trip_complete_textview_night_fair);
        tv_TripFee = (TextView) findViewById(R.id.activity_trip_complete_textview_trip_fee);
        tv_WaitingCost = (TextView) findViewById(R.id.activity_trip_complete_textview_waiting_cost);
        //tv_WaitingCost.setVisibility(View.GONE);
        tv_BookingCharge = (TextView) findViewById(R.id.activity_trip_complete_textview_booking_charge);
        tv_Discount = (TextView) findViewById(R.id.activity_trip_complete_textview_discount);
        tv_SubTotal = (TextView) findViewById(R.id.activity_trip_complete_textview_sub_total);
        tv_GrandTotal = (TextView) findViewById(R.id.activity_trip_complete_textview_grand_total);
        tv_Status = (TextView) findViewById(R.id.activity_trip_complete_textview_status);
        tv_TollFee = (TextView) findViewById(R.id.activity_trip_complete_textview_toll_fee);
        tv_TollFee.setVisibility(View.GONE);
        tv_Tax = (TextView) findViewById(R.id.activity_trip_complete_textview_tax);
        tv_DistanceFare = (TextView) findViewById(R.id.activity_trip_complete_textview_distance_fare);
        tv_SpecialExtraCharge = (TextView) findViewById(R.id.activity_trip_complete_textview_special_extra_charge);
        ll_SpecialExtra_Charge = (LinearLayout) findViewById(R.id.ll_SpecialExtra_Charge);
        ll_good_weight = (LinearLayout) findViewById(R.id.ll_good_weight);
        txt_good_weight = (TextView) findViewById(R.id.txt_good_weight);
        tv_bookinId =  findViewById(R.id.tv_bookinId);
        tv_startTime =  findViewById(R.id.tv_startTime);
        tv_endTime =  findViewById(R.id.tv_endTime);
        tv_baseFare =  findViewById(R.id.tv_baseFare);
        tv_Tripduration =  findViewById(R.id.tv_Tripduration);
        tv_vehicleModel =  findViewById(R.id.tv_vehicleModel);
        ll_NightFare =  findViewById(R.id.ll_NightFare);
        ll_disCount =  findViewById(R.id.ll_disCount);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            if (tripCompleted != null) {
                JSONArray jsonArray = tripCompleted.getJSONArray("Info");

                if (jsonArray != null && jsonArray.length() > 0) {
                    JSONObject data = jsonArray.getJSONObject(0);

                    if (data != null) {

                        if(data.has("Id"))
                        {
                            char prifix = 0;
                            if(data.has("BookingType"))
                            {
                                prifix = data.getString("BookingType").charAt(4);
                            }
                            tv_bookinId.setText(prifix+data.getString("Id"));
                        }
                        if(data.has("ArrivedTime"))
                        {
                            tv_startTime.setText(getDate(Long.parseLong(data.getString("ArrivedTime") + "000"), "dd-MMM-yyyy hh:mm a"));
                        }
                        if(data.has("DropTime"))
                        {
                            tv_endTime.setText(getDate(Long.parseLong(data.getString("DropTime") + "000"), "dd-MMM-yyyy hh:mm a"));
                        }
                        if(data.has("TripDuration") && !data.getString("TripDuration").equalsIgnoreCase(""))
                        {
                            int minutes =  Integer.parseInt(data.getString("TripDuration"))  / 60;
                            tv_Tripduration.setText(minutes +" min");
                        }
                        if(data.has("ModelId") && !data.getString("ModelId").equalsIgnoreCase(""))
                        {
                            for(int i = 0;i<MainActivity.carType_beens.size();i++)
                            {
                                if(MainActivity.carType_beens.get(i).getId().equalsIgnoreCase(data.getString("ModelId")))
                                {
                                    tv_vehicleModel.setText(MainActivity.carType_beens.get(i).getName());
                                    break;
                                }
                            }
                        }
                        if (data.has("PickupLocation")) {
                            tv_PickupLocation.setText(data.getString("PickupLocation"));
                        } else {
                            tv_PickupLocation.setText("");
                        }

                        if (data.has("DropoffLocation")) {
                            tv_DropOffLocation.setText(data.getString("DropoffLocation"));
                        } else {
                            tv_DropOffLocation.setText("");
                        }

                        if (tripCompleted.has("Parcel")) {
                            JSONArray Parcel = tripCompleted.getJSONArray("Parcel");

                            if (Parcel != null && Parcel.length() > 0) {
                                tv_ParcelType.setText(Parcel.getJSONObject(0).getString("Name"));
                            } else {
                                tv_ParcelType.setText("");
                            }
                        } else {
                            tv_ParcelType.setText("");
                        }

                        if (data.has("Name")) {
                            tv_ParcelType.setText(data.getString("Name"));
                        }

                        if (data.has("NightFare") && !data.getString("NightFare").equalsIgnoreCase("")
                                && !data.getString("NightFare").equalsIgnoreCase("null")
                                && !data.getString("NightFare").equalsIgnoreCase("0")) {

                            tv_NightFair.setText(getString(R.string.currency) +
                                    data.getString("NightFare"));
                        } else {
                            tv_NightFair.setText(getString(R.string.currency) + "0.00");
                            ll_NightFare.setVisibility(View.GONE);
                        }

                        if (data.has("ParcelWeight")) {
                            txt_good_weight.setText(data.getString("ParcelWeight") + " Kg");
                            ll_good_weight.setVisibility(View.VISIBLE);
                        } else {
                            ll_good_weight.setVisibility(View.GONE);
                        }

                        String distance = "";
                        if (data.has("TripDistance")) {
                            distance = data.getString("TripDistance");
                        } else {
                            distance = "";
                        }

                        if (data.has("DistanceFare")) {
                            if (distance != null && !distance.equalsIgnoreCase("")) {

                                Double toBeTruncated = new Double(distance);
                                Double truncatedDouble=new BigDecimal(toBeTruncated ).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();

                                tv_DistanceFare.setText(getString(R.string.currency) +
                                        data.getString("DistanceFare") + " (" +truncatedDouble + " km)");
                            } else {
                                tv_DistanceFare.setText(getString(R.string.currency) +
                                        data.getString("DistanceFare"));
                            }
                        } else {
                            tv_DistanceFare.setText(getString(R.string.currency) + "0.00");
                        }

                        if (data.has("TripFare") &&
                                !data.getString("TripFare").equalsIgnoreCase("") &&
                                !data.getString("TripFare").equalsIgnoreCase("null")) {
                            tripFare = data.getString("TripFare");
                            tv_baseFare.setText(getString(R.string.currency)+tripFare);
                        } else {
                            tripFare = "0";
                        }

                        if (data.has("BookingCharge") &&
                                !data.getString("BookingCharge").equalsIgnoreCase("") &&
                                !data.getString("BookingCharge").equalsIgnoreCase("null")) {
                            bookingCharge = data.getString("BookingCharge");
                        } else {
                            bookingCharge = "0";
                        }

                        Double tripCharges = Double.parseDouble(tripFare) + Double.parseDouble(bookingCharge);

                        String surcharge="0";
                        if (data.has("NightFare") && !data.getString("NightFare").equalsIgnoreCase("")
                                && !data.getString("NightFare").equalsIgnoreCase("null")
                                && !data.getString("NightFare").equalsIgnoreCase("0")) {

                            surcharge = data.getString("NightFare");
                        }

                        tripCharges = tripCharges + Double.parseDouble(surcharge);

                        if(data.has("DistanceFare") && data.getString("DistanceFare") != null && !data.getString("DistanceFare").equalsIgnoreCase("")
                        && !data.getString("DistanceFare").equalsIgnoreCase("0"))
                        {
                            tripCharges = tripCharges + Double.parseDouble(data.getString("DistanceFare"));
                        }

                        Double toBeTruncated = new Double(distance);
                        Double truncatedDouble=new BigDecimal(toBeTruncated ).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
                        //tv_DistanceFare.setText(getString(R.string.currency) + String.format("%.1f", tripCharges) +"("+truncatedDouble +" km)");
                        tv_DistanceFare.setText(getString(R.string.currency) + commaFormat(tripCharges) +"("+truncatedDouble +" km)");

                        if (data.has("LoadingUnloadingCharge") && !data.getString("LoadingUnloadingCharge").equalsIgnoreCase("")
                                && !data.getString("LoadingUnloadingCharge").equalsIgnoreCase("null")) {
                            tv_WaitingCost.setText(getString(R.string.currency) +
                                    String.format("%.1f",Double.parseDouble(data.getString("LoadingUnloadingCharge"))) + "(" + data.getString("LoadingUnloadingTime") + " min)");
                        } else {
                            tv_WaitingCost.setText(getString(R.string.currency) + "0.00");
                        }

                        if (data.has("BookingCharge"))
                        {
                            tv_BookingCharge.setText(getString(R.string.currency) +
                                    data.getString("BookingCharge"));
                        }
                        else
                        {
                            tv_BookingCharge.setText(getString(R.string.currency) + "0.0");
                        }

                        if (data.has("Discount") && !data.getString("Discount").equalsIgnoreCase("0")
                                && !data.getString("Discount").equalsIgnoreCase("null")) {
                            tv_Discount.setText(getString(R.string.currency) +
                                    String.format("%.1f",Double.parseDouble(data.getString("Discount"))));
                        } else {
                            tv_Discount.setText(getString(R.string.currency) + "0.0");
                            ll_disCount.setVisibility(View.GONE);
                        }

                        if (data.has("SubTotal") && !data.getString("SubTotal").equalsIgnoreCase("")
                                && !data.getString("SubTotal").equalsIgnoreCase("null")) {
                            tv_SubTotal.setText(getString(R.string.currency) +
                                    commaFormat(Double.parseDouble(data.getString("SubTotal"))));
                        } else {
                            tv_SubTotal.setText(getString(R.string.currency) + "0.0");
                        }

                         double prviousDue = 0;
                        if(data.has("PreviousDue") && data.getString("PreviousDue") != null
                                && !data.getString("PreviousDue").equalsIgnoreCase("") && !data.getString("PreviousDue").equalsIgnoreCase("null")
                                && !data.getString("PreviousDue").equalsIgnoreCase("0")
                                && !data.getString("PreviousDue").equalsIgnoreCase("0.0"))
                        {
                            prviousDue = Double.parseDouble(data.getString("PreviousDue"));
                            ll_previousDue.setVisibility(View.VISIBLE);
                            tv_previousDue.setText(getString(R.string.currency)+String.format("%.1f",prviousDue));
                        }

                        if (data.has("GrandTotal")) {

                            String grandTotal = data.getString("GrandTotal");
                            double ttt = /*prviousDue +*/ Double.parseDouble(grandTotal);/////Change date 29/7/19

                            //tv_GrandTotal.setText(getString(R.string.currency) + String.format("%.1f",ttt));
                            tv_GrandTotal.setText(getString(R.string.currency) + commaFormat(ttt));
                        } else {
                            tv_GrandTotal.setText(getString(R.string.currency) + "0.00");
                        }

                        if(data.has("TripDistance") && !data.getString("TripDistance").equalsIgnoreCase(""))
                        {
                            tv_DistanceTravelled.setText(String.format("%.1f",Double.parseDouble(data.getString("TripDistance")))+" Km");
                        }
                        else
                        {
                            tv_DistanceTravelled.setText("0.00 Km");
                        }

                        if(data.has("LoadingUnloadingTime") && !data.getString("LoadingUnloadingTime").equalsIgnoreCase(""))
                        {
                            tv_WaitingTime.setText(data.getString("LoadingUnloadingTime") + " min");
                        }
                        else
                        {
                            tv_WaitingTime.setText("0 min");
                        }

                        if(data.has("PaymentType") && !data.getString("PaymentType").equalsIgnoreCase(""))
                        {
                            if(data.getString("PaymentType").equalsIgnoreCase("paytm"))
                            {
                                tv_PaymentType.setText("Paytm");
                            }
                            else
                            {
                                tv_PaymentType.setText(data.getString("PaymentType"));
                            }
                        }
                        if(data.has("DriverName") && !data.getString("DriverName").equalsIgnoreCase(""))
                        {
                            tv_DriverName.setText(data.getString("DriverName"));
                        }
                        if(data.has("VehicleRegistrationNo") && !data.getString("VehicleRegistrationNo").equalsIgnoreCase(""))
                        {
                            tv_vehicleNo.setText(data.getString("VehicleRegistrationNo"));
                        }


                        if (data.has("TollFee")) {
                            tv_TollFee.setText(getString(R.string.currency) +
                                    data.getString("TollFee"));
                            tv_TollFee.setVisibility(View.VISIBLE);
                        } else {
                            tv_TollFee.setText(getString(R.string.currency) + "0.00");
                        }

                        if (data.has("Tax") && !data.getString("Tax").equalsIgnoreCase("")
                                && !data.getString("Tax").equalsIgnoreCase("null")) {
                            tv_Tax.setText(getString(R.string.currency) +
                                    String.format("%.1f",Double.parseDouble(data.getString("Tax"))));
                        } else {
                            tv_Tax.setText(getString(R.string.currency) + "0.0");
                        }




                                if (data.has("SpecialExtraCharge") && data.getString("SpecialExtraCharge") != null &&
                                        !data.getString("SpecialExtraCharge").equalsIgnoreCase("") &&
                                        !data.getString("SpecialExtraCharge").equalsIgnoreCase("null") &&
                                        !data.getString("SpecialExtraCharge").equalsIgnoreCase("NULL") &&
                                        !data.getString("SpecialExtraCharge").equalsIgnoreCase("0") ) {
                                    ll_SpecialExtra_Charge.setVisibility(View.VISIBLE);
                                    tv_SpecialExtraCharge.setText(getString(R.string.currency) +
                                            commaFormat(Double.parseDouble(data.getString("SpecialExtraCharge"))));
                                }
                         else
                             {
                            ll_SpecialExtra_Charge.setVisibility(View.GONE);
                        }

                        if (data.has("Status")) {
                            String output = data.getString("Status").substring(0, 1).toUpperCase() + data.getString("Status").substring(1);
                            tv_Status.setText(output);
                        } else {
                            tv_Status.setText("");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        ll_right.setOnClickListener(activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.ll_right:
                MainActivity.call_sos(activity);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(TripCompleteActivity.this,Splash_Activity.class);
//        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

    }


    public String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    //String dateString = getDate(Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getDropTime() + "000"), "dd-MMM-yyyy hh:mm a");

    public String commaFormat(Double x)
    {
        return new DecimalFormat("#,###.0").format(x);
    }
}
