package com.truckz.shipper.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.view.CTextViewBold;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "WebViewActivity";
    public  WebViewActivity activity;

    private LinearLayout ll_Back, ll_right;
    private ImageView iv_Back, iv_call;
    private TextView txt_title;

    private WebView webview;
    private ProgressBar progress;
    private String webUrl = "https://truckzapp.in/web/contact-us", title="";

    private ViewPager adViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        activity = WebViewActivity.this;
        title = "";

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_right = (LinearLayout) findViewById(R.id.ll_right);
        ll_right.setVisibility(View.INVISIBLE);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        txt_title = (TextView) findViewById(R.id.title_textview);

        txt_title.setText(activity.getResources().getString(R.string.contact_us));

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        webview = (WebView) findViewById(R.id.webView);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");

        txt_title.setText(title);

//        if (!title.equalsIgnoreCase("") &&
//                title.equalsIgnoreCase("Purchase Feeds"))
//        {
//            txt_title.setText(title);
//            webUrl = "https://www.dairyworld.co.ke/";
////            webUrl = "https://www.excellentwebworld.com/";
//        }
//        else if (!title.equalsIgnoreCase("") &&
//                title.equalsIgnoreCase("About Us"))
//        {
//            txt_title.setText(title);
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorBlack));
//        }
//        else if (!title.equalsIgnoreCase("") &&
//                title.equalsIgnoreCase("Knowledge Base"))
//        {
//            txt_title.setText(title);
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorBlack));
//        }
//        else if (!title.equalsIgnoreCase("") &&
//                title.equalsIgnoreCase("Help"))
//        {
//            txt_title.setText(title);
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorBlack));
//        }

        if (Global.isNetworkconn(WebViewActivity.this))
        {
            call_web_view();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog(getString(R.string.please_check_your_internet_connection),
                    getString(R.string.no_internet_connection));
        }

        ll_Back.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        iv_call.setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;
        }
    }

    private void call_web_view()
    {
        Log.e(TAG,"call_web_view():- " + webUrl);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webview.loadUrl(webUrl);

        webview.setWebViewClient(new myWebClient());

        webview.setWebChromeClient(new WebChromeClient()
        {
            public void onProgressChanged(WebView view, int progressInt)
            {
                if (progressInt < 80 && progress.getVisibility() == ProgressBar.GONE)
                {
                    progress.setVisibility(ProgressBar.VISIBLE);
                }

                if (progressInt >= 80)
                {
                    progress.setVisibility(ProgressBar.GONE);
                }
            }
        });
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
// TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
// TODO Auto-generated method stub
            progress.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error)
        {
            //super.onReceivedSslError(view, handler, error);
            final AlertDialog.Builder builder = new AlertDialog.Builder(WebViewActivity.this);
            builder.setMessage("This site is insecure. Do you want to proceed?");
            builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                    finish();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
// TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progress.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if (webview.canGoBack())
                    {
                        webview.goBack();
                    }
                    else
                    {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        TruckzShipperApplication.setCurrentActivity(activity);
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");

        if (webview != null && webview.canGoBack())
        {
            webview.goBack();
        }
        else
        {
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
        }
    }
}
