package com.truckz.shipper.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.truckz.shipper.adapter.ParcelAdapter;
import com.truckz.shipper.adapter.TransportServiceAdapter;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.been.ParcelBeen;
import com.truckz.shipper.been.TransportService_Been;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.other.SpacesItemDecoration;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.TaxiUtil;
import com.truckz.shipper.comman.Utility;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.view.CustomSpinner;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class BookLaterActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "BookLaterActivity";
    public static BookLaterActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title;

    private CheckBox checkBox_Myself, checkBox_Others, checkBox_Flight, checkBox_Notes, cbReceiverInfo;
    public static int mySelfOtherFlag = 0; //0 = myself, 1=others
    public static boolean flight = false; //not applicable.
    public static boolean notes = false; //

    private LinearLayout ll_FlightNumber, ll_Notes;
    private EditText et_Name, et_Phone, et_Flight, et_Notes, etReceiverName, etreceiverPhone, etParcelWeight, etreceiverMail;
    private TextView tv_PickupTime, tv_Submit;
    private LinearLayout ll_Submit;

    private AutoCompleteTextView auto_Pickup, auto_Dropoff;
    private String jsonurl = "";
    private ArrayList<String> names, Place_id_type;
    private ParseData parse;
    public static JSONObject json;
    private JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    private String place_id = "";
    public static int addFlag = 1;

    public String pickupLocation = "";
    public String dropoffLocation = "";
    public double pickup_Lat, pickup_Long;
    public double dropoff_Lat, dropoff_Long;
    public String pickup_Date = "";
    public String pickup_Time = "";

    private ImageView iv_DateTime;
    private LinearLayout ll_DateTime;
    private TextView dateTimetxt, tv_ModelName, tv_Promocode;
    private ImageView iv_ModelImage, ivRemovePromo;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private DialogClass dialogClass;
    private AQuery aQuery;
    public static String dateTime = "";
    private LinearLayout ll_ClearPickup, ll_ClearDropoff, ll_Promocode;
    private LinearLayout ll_RootLayout;
    private MySnackBar mySnackBar;
    private boolean pickup_Touch = false;
    private boolean dropoff_Touch = false;
    private GPSTracker gpsTracker;
    private String promocodeStr = "", parcelID = "";
    private double latitude = 0;
    private double longintude = 0;
    private CustomSpinner Spinner_paymentMethod,Spinner_transportService;
    public static String cardId = "";
    public static String cardNumber = "";
    public static String cardType = "";
    public static boolean isCardSelected = false;
    public static String selectedPaymentType = "";
    private CustomerAdapter customerAdapter;
    private CustomerTransportServiceAdapter customerTransportServiceAdapter;
    public static String selectedTransportService = "";
    public static int spinnerSelectedPosition = 0;
    public static int pressNo = 0;
    private LinearLayout ll_Spinner,ll_TransportService;
    private int selectedCar = -1;
    private String categoryId = "1";
    private String RequestFor = "taxi";
    private ImageView iv_BigCar;
    private LinearLayout ll_CarInfoMain, ll_Info, ll_MySelfOtherMain, ll_NameNumber;
    private LinearLayout ll_Labour, ll_EstimateFare, ll_Discount;
    public TextView tv_Labour, tv_TransportService, tv_EstimateFare, tv_Discount;
    public static ArrayList<TransportService_Been> transportService_beens = new ArrayList<TransportService_Been>();
    private TransportServiceAdapter transportServiceAdapter;
    public static int selectedPos = -1;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerLabour;
    private String[] valueLabour;
    int selectedLabour;

    public ImageView ivAddParcel;
    public RecyclerView rvParcel;
    public ParcelAdapter parcelAdapter;
    public List<ParcelBeen> parcelList = new ArrayList<>();
    public int position = 1;

    public double estimationFare = 0;
    public double TruckCapacity = 0;

    public LinearLayout ll_EstimateFare_pay;
    public TextView tv_EstimateFare_pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_later);

        activity = BookLaterActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        pressNo = 0;
        cardId = "";
        cardNumber = "";
        cardType = "";
        mySelfOtherFlag = 0;
        spinnerSelectedPosition = 0;
        flight = false;
        selectedPos = -1;
        selectedPaymentType = "";
        selectedTransportService = "";
        isCardSelected = false;
        notes = false;
        addFlag = 1;
        selectedCar = -1;
        pickupLocation = "";
        dropoffLocation = "";
        pickup_Touch = false;
        dropoff_Touch = false;
        pickup_Lat = 0;
        pickup_Long = 0;
        dropoff_Lat = 0;
        dropoff_Long = 0;
        pickup_Date = "";
        pickup_Time = "";
        dateTime = "";
        promocodeStr = "";
        parcelID = "";
        transportService_beens.clear();
        dialogClass = new DialogClass(activity, 0);
        aQuery = new AQuery(activity);
        gpsTracker = new GPSTracker(activity);

        init();
    }

    @Override
    protected void onPause() {
        Log.i("Life Cycle", "onPause");
        if (Spinner_paymentMethod != null) {
            Spinner_paymentMethod.onDetachedFromWindow();
        }
        if (Spinner_transportService != null) {
            Spinner_transportService.onDetachedFromWindow();
        }
        super.onPause();
    }

    private void init() {
        Log.e(TAG, "init()");

        TruckCapacity = Double.parseDouble(getIntent().getStringExtra(Common.INTENT_CAPACITY));

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Title.setText(activity.getResources().getString(R.string.activity_book_later));

        tv_Promocode = (TextView) findViewById(R.id.promo_code_textview);
        ivRemovePromo = findViewById(R.id.ivRemovePromo);
        Spinner_paymentMethod = (CustomSpinner) findViewById(R.id.Spinner_paymentMethod);
        Spinner_transportService = (CustomSpinner) findViewById(R.id.Spinner_transportService);
        ll_ClearPickup = (LinearLayout) findViewById(R.id.clear_pickup_location);
        ll_ClearDropoff = (LinearLayout) findViewById(R.id.clear_dropoff_location);
        ll_Promocode = (LinearLayout) findViewById(R.id.promo_code_layout);
        ll_EstimateFare_pay = (LinearLayout) findViewById(R.id.ll_EstimateFare_pay);
        tv_EstimateFare_pay = (TextView) findViewById(R.id.tv_EstimateFare_pay);

        dateTimetxt = (TextView) findViewById(R.id.date_time_textview);
        ll_Spinner = (LinearLayout) findViewById(R.id.select_card_layout);

        ll_RootLayout = (LinearLayout) findViewById(R.id.rootView);
        tv_Submit = (TextView) findViewById(R.id.submit_textview);
        ll_Submit = (LinearLayout) findViewById(R.id.submit_layout);

        tv_ModelName = (TextView) findViewById(R.id.model_name);
        iv_ModelImage = (ImageView) findViewById(R.id.model_image);

        iv_BigCar = (ImageView) findViewById(R.id.iv_BigCar);
        ll_CarInfoMain = (LinearLayout) findViewById(R.id.ll_CarInfoMain);
        ll_Info = (LinearLayout) findViewById(R.id.ll_Info);
        ll_MySelfOtherMain = (LinearLayout) findViewById(R.id.ll_MySelfOtherMain);
        ll_NameNumber = (LinearLayout) findViewById(R.id.ll_NameNumber);
        ll_TransportService = (LinearLayout) findViewById(R.id.ll_TransportService);
        ll_Labour = (LinearLayout) findViewById(R.id.ll_Labour);
        tv_Labour = (TextView) findViewById(R.id.tv_Labour);
        tv_TransportService = (TextView) findViewById(R.id.tv_TransportService);
        tv_EstimateFare = (TextView) findViewById(R.id.tv_EstimateFare);
        tv_Discount = findViewById(R.id.tv_Discount);

        ll_EstimateFare = (LinearLayout) findViewById(R.id.ll_EstimateFare);
        ll_Discount = findViewById(R.id.ll_Discount);


        tv_PickupTime = (TextView) findViewById(R.id.title_textview);
        et_Flight = (EditText) findViewById(R.id.input_flight_number);
        et_Notes = (EditText) findViewById(R.id.input_note);
        et_Phone = (EditText) findViewById(R.id.input_phone);
        et_Name = (EditText) findViewById(R.id.input_name);
        etReceiverName =  findViewById(R.id.etReceiverName);
        etreceiverPhone =  findViewById(R.id.etreceiverPhone);
        etParcelWeight =  findViewById(R.id.etParcelWeight);
        etreceiverMail =  findViewById(R.id.etreceiverMail);

        checkBox_Flight = (CheckBox) findViewById(R.id.checkbox_flight);
        checkBox_Notes = (CheckBox) findViewById(R.id.checkbox_notes);
        checkBox_Myself = (CheckBox) findViewById(R.id.checkbox_myself);
        checkBox_Others = (CheckBox) findViewById(R.id.checkbox_others);
        cbReceiverInfo = findViewById(R.id.cbReceiverInfo);

        iv_DateTime = (ImageView) findViewById(R.id.date_time_image);
        ll_DateTime = (LinearLayout) findViewById(R.id.date_time_layout);

        ll_FlightNumber = (LinearLayout) findViewById(R.id.ll_FlightNumber);
        ll_Notes = (LinearLayout) findViewById(R.id.ll_Notes);

        auto_Pickup = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_pickup);
        auto_Dropoff = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_drop_off);

        ivAddParcel = findViewById(R.id.ivAddParcel);
        rvParcel = findViewById(R.id.rvParcel);

        mySnackBar = new MySnackBar(activity);

        parcelAdapter = new ParcelAdapter(activity, parcelList);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        rvParcel.addItemDecoration(decoration);
        rvParcel.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        rvParcel.setLayoutManager(layoutManager);
        rvParcel.setAdapter(parcelAdapter);

        checkBox_Others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBox_Others.setChecked(true);
                mySelfOtherFlag = 1;
                checkBox_Myself.setChecked(false);
                setNameNumber(mySelfOtherFlag);
            }
        });

        checkBox_Myself.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBox_Others.setChecked(false);
                mySelfOtherFlag = 0;
                checkBox_Myself.setChecked(true);
                setNameNumber(mySelfOtherFlag);
            }
        });

        checkBox_Flight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flight) {
                    ll_FlightNumber.setVisibility(View.GONE);
                    flight = false;
                } else {
                    ll_FlightNumber.setVisibility(View.VISIBLE);
                    flight = true;
                }
            }
        });

        checkBox_Notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (notes) {
                    ll_Notes.setVisibility(View.GONE);
                    notes = false;
                } else {
                    ll_Notes.setVisibility(View.VISIBLE);
                    notes = true;
                }
            }
        });

        cbReceiverInfo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isTrue)
            {
                if (isTrue)
                {
                    setNameNumber(0);
                }
                else
                {
                    setNameNumber(1);
                }
            }
        });

        customerAdapter = new CustomerAdapter(activity, MainActivity.cardListBeens);
        Spinner_paymentMethod.setAdapter(customerAdapter);

        Spinner_paymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (MainActivity.cardListBeens != null && MainActivity.cardListBeens.size() > 0) {
                    cardId = MainActivity.cardListBeens.get(position).getId();
                    cardNumber = MainActivity.cardListBeens.get(position).getCardNum_();

//                    if (position == (MainActivity.cardListBeens.size() -1))
//                    {
//                        Log.e(TAG,"setOnItemSelectedListener() 1111");
//                        selectedPaymentType = "wallet";//PaymentType : cash,wallet,card
//
//                    }
//                    else
                    if (position == (MainActivity.cardListBeens.size() - 1))
                    {
                        Log.e(TAG, "setOnItemSelectedListener() 2222");
                        selectedPaymentType = "paytm";//PaymentType : cash,wallet,card
                    }
                    else if(position == (MainActivity.cardListBeens.size() - 2))
                    {
                        selectedPaymentType = "cash";
                    }
                    else
                    {
                        Log.e(TAG, "setOnItemSelectedListener() 3333");
                        selectedPaymentType = "card";//PaymentType : cash,wallet,card
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

                Log.e(TAG, "setOnItemSelectedListener() onNothing selected");
            }
        });

        Spinner_transportService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0)
                {
                    selectedTransportService = "";
                }
                else
                {
                    selectedTransportService = transportService_beens.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

                Log.e(TAG, "setOnItemSelectedListener() onNothing selected");
            }
        });

        auto_Pickup.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                addFlag = 1;
                if (gpsTracker != null && gpsTracker.canGetLocation()) {
                    latitude = gpsTracker.getLatitude();
                    longintude = gpsTracker.getLongitude();
                }
                search_text = auto_Pickup.getText().toString().split(",");
                jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" + latitude + "," + longintude + "&radius=1000&sensor=true&key=" + getResources().getString(R.string.map_api_key) + "&components=&language=en";
                Log.e("url", "autocomplete = " + jsonurl);
                names = new ArrayList<String>();
                Place_id_type = new ArrayList<String>();
                if (parse != null) {
                    parse.cancel(true);
                    parse = null;
                }
                parse = new ParseData();
                parse.execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Pickup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Pickup.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Pickup.getText().toString());
                    } else {
                        pickup_Lat = 0;
                        pickup_Long = 0;
                        pickupLocation = "";
                    }
                    // finish();
                }
                return false;
            }
        });

        auto_Dropoff.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                addFlag = 2;
                if (gpsTracker != null && gpsTracker.canGetLocation()) {
                    latitude = gpsTracker.getLatitude();
                    longintude = gpsTracker.getLongitude();
                }
                search_text = auto_Dropoff.getText().toString().split(",");
                jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" + latitude + "," + longintude + "&radius=1000&sensor=true&key=" + getResources().getString(R.string.map_api_key) + "&components=&language=en";
                Log.e("url", "autocomplete = " + jsonurl);
                names = new ArrayList<String>();
                Place_id_type = new ArrayList<String>();
                if (parse != null) {
                    parse.cancel(true);
                    parse = null;
                }
                parse = new ParseData();
                parse.execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        auto_Dropoff.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Dropoff.getText().toString());
                    } else {
                        dropoff_Lat = 0;
                        dropoff_Long = 0;
                        dropoffLocation = "";
                    }
                    // finish();
                }
                return false;
            }
        });

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        tv_Submit.setOnClickListener(activity);
        ll_Submit.setOnClickListener(activity);
        iv_DateTime.setOnClickListener(activity);
        ll_DateTime.setOnClickListener(activity);
        ll_ClearDropoff.setOnClickListener(activity);
        ll_ClearPickup.setOnClickListener(activity);
        ll_Promocode.setOnClickListener(activity);
        tv_Promocode.setOnClickListener(activity);
        ivRemovePromo.setOnClickListener(activity);
        ll_Labour.setOnClickListener(activity);
        ll_Info.setOnClickListener(activity);
        ll_TransportService.setOnClickListener(activity);
        ivAddParcel.setOnClickListener(activity);

//        if (MainActivity.redirectBooking!=null && !MainActivity.redirectBooking.equalsIgnoreCase("")
//                && MainActivity.redirectBooking.equalsIgnoreCase("booklater"))
//        {
//            if (MainActivity.redirectBookingAdrs!=null && !MainActivity.redirectBookingAdrs.equalsIgnoreCase(""))
//            {
//                auto_Dropoff.setText(MainActivity.redirectBookingAdrs);
//                dropoffLocation = MainActivity.redirectBookingAdrs;
//                dropoff_Touch =true;
//                auto_Dropoff.dismissDropDown();
//
//                MainActivity.redirectBooking = "";
//                MainActivity.redirectBookingAdrs = "";
//            }
//            else {
//                dropoff_Touch =false;
//                auto_Dropoff.setText("");
//                MainActivity.redirectBooking = "";
//                MainActivity.redirectBookingAdrs = "";
//            }
//        }
//        else {
//            dropoff_Touch =false;
//            auto_Dropoff.setText("");
//            MainActivity.redirectBooking = "";
//            MainActivity.redirectBookingAdrs = "";
//        }

        if (MainActivity.carType_beens != null && MainActivity.carType_beens.size() > 0) {
            categoryId = MainActivity.carType_beens.get(0).getCategoryId();

            for (int i = 0; i < MainActivity.carType_beens.size(); i++) {
                if (MainActivity.carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = MainActivity.carType_beens.get(i).getSelectedCar();
                }
            }

            if (selectedCar != -1) {
                if (MainActivity.carType_beens.get(selectedCar).getImage() != null && !MainActivity.carType_beens.get(selectedCar).getImage().equalsIgnoreCase("")) {
                    iv_ModelImage.setVisibility(View.VISIBLE);
                    Picasso.with(activity).load(MainActivity.carType_beens.get(selectedCar).getImage()).into(iv_ModelImage);
                } else {
                    iv_ModelImage.setVisibility(View.GONE);
                }

                if (MainActivity.carType_beens.get(selectedCar).getModelSizeImage() != null && !MainActivity.carType_beens.get(selectedCar).getModelSizeImage().equalsIgnoreCase("")) {
                    iv_BigCar.setVisibility(View.VISIBLE);
                    Picasso.with(activity).load(MainActivity.carType_beens.get(selectedCar).getModelSizeImage()).into(iv_BigCar);
                } else {
                    iv_BigCar.setVisibility(View.INVISIBLE);
                }

                if (MainActivity.carType_beens.get(selectedCar).getName() != null && !MainActivity.carType_beens.get(selectedCar).getName().equalsIgnoreCase("")) {
                    tv_ModelName.setText(MainActivity.carType_beens.get(selectedCar).getName());
                } else {
                    tv_ModelName.setText("");
                }
            } else {
                iv_ModelImage.setVisibility(View.GONE);
                tv_ModelName.setText("");
            }
        } else {
            iv_ModelImage.setVisibility(View.GONE);
            tv_ModelName.setText("");
        }

        if (categoryId != null && categoryId.equals("1")) {
            ll_MySelfOtherMain.setVisibility(View.VISIBLE);
            ll_CarInfoMain.setVisibility(View.GONE);
            ll_NameNumber.setVisibility(View.VISIBLE);
            ll_TransportService.setVisibility(View.GONE);
            ll_Labour.setVisibility(View.GONE);
            ll_EstimateFare.setVisibility(View.GONE);
        }
        else
        {
            ll_MySelfOtherMain.setVisibility(View.GONE);
            ll_CarInfoMain.setVisibility(View.GONE);
            ll_NameNumber.setVisibility(View.VISIBLE);
            ll_TransportService.setVisibility(View.GONE);
            ll_Labour.setVisibility(View.GONE);
            ll_EstimateFare.setVisibility(View.VISIBLE);

            getTransportService();
        }

        setNameNumber(mySelfOtherFlag);
//        getAddressFromLatLong();

        pickup_Lat = TaxiUtil.picup_Lat;
        pickup_Long = TaxiUtil.picup_Long;
        pickupLocation = TaxiUtil.picup_Address;
        pickup_Touch = true;

        dropoff_Lat = TaxiUtil.dropoff_Lat;
        dropoff_Long = TaxiUtil.dropoff_Long;
        dropoffLocation = TaxiUtil.dropoff_Address;
        dropoff_Touch = true;

        auto_Pickup.setText(TaxiUtil.picup_Address);
        auto_Dropoff.setText(TaxiUtil.dropoff_Address);
    }

    private void openCarInfo() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_open_car_info, null);

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        LinearLayout ll_Ok;
        TextView tv_CarName, tv_CarHeight, tv_CarWidth, tv_CarCapacity, tv_CarDescription;
        ImageView iv_CarImage;

        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);

        tv_CarName = (TextView) dialog.findViewById(R.id.tv_CarName);
        tv_CarHeight = (TextView) dialog.findViewById(R.id.tv_CarHeight);
        tv_CarWidth = (TextView) dialog.findViewById(R.id.tv_CarWidth);
        tv_CarCapacity = (TextView) dialog.findViewById(R.id.tv_CarCapacity);

        iv_CarImage = (ImageView) dialog.findViewById(R.id.iv_CarImage);

        tv_CarDescription = (TextView) dialog.findViewById(R.id.tv_CarDescription);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        if (MainActivity.carType_beens != null && MainActivity.carType_beens.size() > 0) {
            tv_CarName.setText(MainActivity.carType_beens.get(selectedCar).getName() + "");
            tv_CarHeight.setText(MainActivity.carType_beens.get(selectedCar).getHeight() + "");
            tv_CarWidth.setText(MainActivity.carType_beens.get(selectedCar).getWidth() + "");
            tv_CarCapacity.setText(MainActivity.carType_beens.get(selectedCar).getCapacity() + "");
            tv_CarDescription.setText(MainActivity.carType_beens.get(selectedCar).getDescription() + "");
        } else {
            tv_CarName.setText("");
            tv_CarHeight.setText("");
            tv_CarWidth.setText("");
            tv_CarCapacity.setText("");
            tv_CarDescription.setText("");
        }

        if (MainActivity.carType_beens.get(selectedCar).getModelSizeImage() != null && !MainActivity.carType_beens.get(selectedCar).getModelSizeImage().equalsIgnoreCase("")) {
            iv_CarImage.setVisibility(View.VISIBLE);
            Picasso.with(activity).load(MainActivity.carType_beens.get(selectedCar).getModelSizeImage()).into(iv_CarImage);
        } else {
            iv_CarImage.setVisibility(View.INVISIBLE);
        }

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void getTransportService() {
        Log.e(TAG, "getTransportService()");

        transportService_beens.clear();
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GET_TRANSPORT_SERVICE;
//        transportService_beens.add(new TransportService_Been("", "Select Transport Service", "", "", "", false));
        Log.e(TAG, "getTransportService() url:- " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "getTransportService() Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("percel")) {
                                    JSONArray percel = json.getJSONArray("percel");

                                    if (percel != null && percel.length() > 0) {
                                        for (int i = 0; i < percel.length(); i++) {
                                            JSONObject percelData = percel.getJSONObject(i);

                                            if (percelData != null) {
                                                String Id = "", Name = "", Image = "", Status = "", CreatedDate = "";
                                                boolean isSelected = false;

                                                if (percelData.has("Id")) {
                                                    Id = percelData.getString("Id");
                                                }

                                                if (percelData.has("Name")) {
                                                    Name = percelData.getString("Name");
                                                }

                                                if (percelData.has("Image")) {
                                                    Image = percelData.getString("Image");
                                                }

                                                if (percelData.has("Status")) {
                                                    Status = percelData.getString("Status");
                                                }

                                                if (percelData.has("CreatedDate")) {
                                                    CreatedDate = percelData.getString("CreatedDate");
                                                }

                                                transportService_beens.add(new TransportService_Been(Id, Name, Image, Status, CreatedDate, isSelected));
                                            }
                                        }

                                        transportService_beens.add(0,new TransportService_Been("0", "Select Goods Type", "", "", "", true));
                                        dialogClass.hideDialog();
                                        getEstimatedFare();
                                    } else {
                                        Log.e(TAG, "getTransportService() no percel available");
                                        dialogClass.hideDialog();
                                    }
                                } else {
                                    Log.e(TAG, "getTransportService() no percel found");
                                    dialogClass.hideDialog();
                                }
                            } else {
                                Log.e(TAG, "getTransportService() status false");
                                dialogClass.hideDialog();
                            }
                        } else {
                            Log.e("call", "status not found");
                            dialogClass.hideDialog();
                        }

                    } else {
                        Log.e("call", "json null");
                        dialogClass.hideDialog();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callDiscountApi(String promocodeStr, String estimateFare) {

    }

    private void openTransportService() {

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_open_transport_service, null);

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        LinearLayout ll_Done, ll_Cancel;
        RecyclerView rv_TransportService;

        ll_Done = (LinearLayout) dialog.findViewById(R.id.ll_Done);
        ll_Cancel = (LinearLayout) dialog.findViewById(R.id.ll_Cancel);

        rv_TransportService = (RecyclerView) dialog.findViewById(R.id.rv_TransportService);
        rv_TransportService.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        rv_TransportService.addItemDecoration(new SpacesItemDecoration(2));
        transportServiceAdapter = new TransportServiceAdapter(activity);
        rv_TransportService.setAdapter(transportServiceAdapter);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        ll_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedPos = -1;
                if (transportService_beens != null && transportService_beens.size() > 0) {
                    first:
                    for (int i = 0; i < transportService_beens.size(); i++) {
                        if (transportService_beens.get(i).isSelected()) {
                            selectedPos = i;
                            break first;
                        }
                    }

                    if (selectedPos == -1) {
                        tv_TransportService.setText(getResources().getString(R.string.transport_service));
                        parcelID = "";
                    } else {
                        tv_TransportService.setText("" + transportService_beens.get(selectedPos).getName());
                        parcelID = transportService_beens.get(selectedPos).getId();
                    }
                } else {
                    tv_TransportService.setText(getResources().getString(R.string.transport_service));
                    parcelID = "";
                }

                Log.e("call", "selected transport service :- " + selectedPos);

                dialog.dismiss();
            }
        });

        ll_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("call", "selected transport service :- " + selectedPos);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showCreaditCardPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_yes_no_layout);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);

        TextView tv_Yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);

        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText(getResources().getString(R.string.do_you_want_to_add_card));

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                pressNo = 1;
                openPromocodePopup();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                pressNo = 1;
                openPromocodePopup();
            }
        });

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);

            }
        });

        tv_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);

            }
        });

        dialog.show();
    }

    public boolean checkCardList() {
        try {
            Log.e("call", "111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            return true;
                        } else {
                            Log.e("call", "no cards available");
                            return false;
                        }
                    } else {
                        Log.e("call", "no cards found");
                        return false;
                    }
                } else {
                    Log.e("call", "json null");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
            return false;
        }
    }

    public void getAddressFromLatLong() {
        try {
            gpsTracker = new GPSTracker(activity);

            if (gpsTracker != null && gpsTracker.canGetLocation()) {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();

                if (latitude != 0 && longitude != 0) {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("111111111111", "address = " + address);
                    Log.e("111111111111", "city = " + city);
                    Log.e("111111111111", "state = " + state);
                    Log.e("111111111111", "country = " + country);
                    Log.e("111111111111", "postalCode = " + postalCode);
                    Log.e("111111111111", "knownName = " + knownName);

                    auto_Pickup.setText(address);
                    pickupLocation = address;
                    pickup_Touch = true;
                    pickup_Lat = latitude;
                    pickup_Long = longitude;
                    auto_Pickup.dismissDropDown();

                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void setNameNumber(int flag)
    {
        if (flag == 0)
        {
            et_Phone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, activity));
            et_Name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, activity));
            etReceiverName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, activity));
            etreceiverPhone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, activity));
            etreceiverMail.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, activity));
            etreceiverPhone.setEnabled(false);
            etreceiverMail.setEnabled(false);
            etReceiverName.setEnabled(false);
        }
        else
        {
            etReceiverName.setText("");
            etreceiverPhone.setText("");
            etreceiverMail.setText("");
            etreceiverPhone.setEnabled(true);
            etreceiverMail.setEnabled(true);
            etReceiverName.setEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.submit_layout:
                call_CheckValidation();
                break;

            case R.id.submit_textview:
                call_CheckValidation();
                break;

            case R.id.date_time_image:
                selectDateAndTime();
                break;

            case R.id.date_time_layout:
                selectDateAndTime();
                break;

            case R.id.promo_code_layout:
                openPromocodePopup();
                break;

            case R.id.ll_Info:
                openCarInfo();
                break;

            case R.id.promo_code_textview:
                openPromocodePopup();
                break;

            case R.id.ivRemovePromo:
                callRemovePromoCode();
                break;

            case R.id.clear_pickup_location:
                pickupLocation = "";
                auto_Pickup.setText("");
                pickup_Touch = false;
                tv_EstimateFare.setText(activity.getResources().getString(R.string.currency) + "0.0");
                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();
                break;

            case R.id.clear_dropoff_location:
                dropoffLocation = "";
                auto_Dropoff.setText("");
                dropoff_Touch = false;
                tv_EstimateFare.setText(activity.getResources().getString(R.string.currency) + "0.0");
                if (auto_Pickup.getText().toString().trim().length() > 0) {
                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                } else {
                    auto_Pickup.setFocusableInTouchMode(true);
                    auto_Pickup.requestFocus();
                }
                break;

            case R.id.ll_Labour:
                showBottomSheetDailog_payment();
                break;

            case R.id.ll_TransportService:
                openTransportService();
                break;

            case R.id.ivAddParcel:

                position++;

                parcelList.add(new ParcelBeen("110.00", false));
                parcelAdapter.notifyDataSetChanged();
                Log.e(TAG, "btAdd.click() position:- " + position);
                Log.e(TAG, "btAdd.click() parcelList.size():- " + parcelList.size());
                break;
        }
    }

    private void callRemovePromoCode()
    {
        Log.e(TAG,"callRemovePromoCode()");

        tv_Promocode.setText("");
        promocodeStr = "";
        ivRemovePromo.setVisibility(View.GONE);
        ll_Discount.setVisibility(View.GONE);
        ll_EstimateFare_pay.setVisibility(View.GONE);
    }

    public void showBottomSheetDailog_payment() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_labour, null);
        mBottomSheetDialog = new BottomSheetDialog(activity);
        mBottomSheetDialog.setContentView(view);


        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) view.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) view.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerLabour = (NumberPicker) view.findViewById(R.id.numberPickerLabour);

        numberPickerLabour.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerLabour, ContextCompat.getColor(activity, R.color.colorBlack));

        numberPickerLabour.setMinValue(1);
        numberPickerLabour.setMaxValue(9);

        valueLabour = new String[9];

        for (int i = 0; i < 9; i++) {
            if (i == 0) {
                valueLabour[i] = getResources().getString(R.string.select_labour);
            } else {
                valueLabour[i] = i + "";
            }
        }

        numberPickerLabour.setValue(selectedLabour + 1);
        numberPickerLabour.setDisplayedValues(valueLabour);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog != null) {
                    selectedLabour = numberPickerLabour.getValue() - 1;
                    tv_Labour.setText(valueLabour[selectedLabour] + "");

                    mBottomSheetDialog.dismiss();
                }

                if (auto_Pickup.getText().toString().trim().equalsIgnoreCase("") || auto_Pickup.getText().toString().trim().equalsIgnoreCase("")) {
                    tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                } else {
                    getEstimatedFare();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }

    public void getEstimatedFare()
    {
        Log.e(TAG, "getEstimatedFare()");

        dialogClass.showDialog();
        String url = WebServiceAPI.API_GET_ESTIMATED_FARE_FOR_DELIVERY;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION, auto_Pickup.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, auto_Dropoff.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_MODEL_ID, MainActivity.carType_beens.get(selectedCar).getId());
        params.put(WebServiceAPI.PARAM_LABOUR, selectedLabour);

        Log.e(TAG, "getEstimatedFare() url :- " + url);
        Log.e(TAG, "getEstimatedFare() param :- " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "getEstimatedFare() responseCode:- " + responseCode);
                    Log.e(TAG, "getEstimatedFare() Response:- " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("estimate_fare")) {
                                    JSONObject estimate_fare = json.getJSONObject("estimate_fare");

                                    if (estimate_fare != null) {
                                        if (estimate_fare.has("total")) {
                                            double estimateFare = 0;

                                            if (estimate_fare.getString("total") != null && !estimate_fare.getString("total").equalsIgnoreCase("")) {
                                                estimateFare = Double.parseDouble(estimate_fare.getString("total"));
                                            }

                                            Log.e(TAG, "getEstimatedFare() estimate_fare:- " + estimate_fare);

                                            //tv_EstimateFare.setText(getResources().getString(R.string.currency) + String.format("%.2f", estimateFare));
                                            tv_EstimateFare.setText(getResources().getString(R.string.currency) +  commaFormat(estimateFare));
                                            parcelList.add(new ParcelBeen(String.format("%.2f", estimateFare), false));
                                            parcelAdapter.notifyDataSetChanged();
                                            estimationFare = estimateFare;
                                            customerTransportServiceAdapter = new CustomerTransportServiceAdapter(activity, transportService_beens);
                                            Spinner_transportService.setAdapter(customerTransportServiceAdapter);
                                            Spinner_transportService.setSelection(0);
                                            selectedTransportService = transportService_beens.get(0).getId();
                                            dialogClass.hideDialog();
                                        } else {
                                            tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                                            dialogClass.hideDialog();
                                        }
                                    } else {
                                        tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                                        dialogClass.hideDialog();
                                    }
                                } else {
                                    tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                                    dialogClass.hideDialog();
                                }
                            } else {
                                Log.e(TAG, "getEstimatedFare() status false");
                                tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    mySnackBar.showSnackBar(ll_RootLayout, json.getString("message"));
                                } else {
                                    mySnackBar.showSnackBar(ll_RootLayout, getResources().getString(R.string.please_try_agai_later));
                                }
                            }
                        } else {
                            Log.e(TAG, "getEstimatedFare() status not available");
                            tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                            dialogClass.hideDialog();
                            mySnackBar.showSnackBar(ll_RootLayout, getResources().getString(R.string.please_try_agai_later));
                        }
                    } else {
                        Log.e(TAG, "getEstimatedFare() json null ");
                        tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                        dialogClass.hideDialog();
                        mySnackBar.showSnackBar(ll_RootLayout, getResources().getString(R.string.please_try_agai_later));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getEstimatedFare() Exception " + e.toString());
                    tv_EstimateFare.setText(getResources().getString(R.string.currency) + "0.0");
                    dialogClass.hideDialog();
                    mySnackBar.showSnackBar(ll_RootLayout, getResources().getString(R.string.please_try_agai_later));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void openPromocodePopup()
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.promocode_layout);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        final LinearLayout layout_Dialog = (LinearLayout) dialog.findViewById(R.id.layout_Dialog);
        final EditText et_Promocode = (EditText) dialog.findViewById(R.id.promocode_edittext);
        final TextView tv_Apply = (TextView) dialog.findViewById(R.id.apply_textview);
        final TextView tv_Cancel = (TextView) dialog.findViewById(R.id.cancel_textview);

        et_Promocode.setHint(getResources().getString(R.string.enter_promocode_here));

        et_Promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().trim().length() > 0) {
                    et_Promocode.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        tv_Apply.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (et_Promocode.getText().toString().trim().length() > 0)
                {
                    tv_Promocode.setText(et_Promocode.getText().toString());
                    promocodeStr = et_Promocode.getText().toString();
                    ivRemovePromo.setVisibility(View.VISIBLE);
//                    dialog.dismiss();

                    callCheckPromoCode(promocodeStr,dialog, et_Promocode);

                }
                else
                {
                    et_Promocode.setError(getResources().getString(R.string.please_enter_promocode));
                    et_Promocode.setText("");
                    ivRemovePromo.setVisibility(View.GONE);
                    promocodeStr = "";
                }
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void callCheckPromoCode(final String promocode, final Dialog dialog, final EditText et_Promocode)
    {
        Log.e(TAG,"callCheckPromoCode() promocodeStr:- " + promocode);

        String url = WebServiceAPI.API_CHECK_PROMOCODE + "/"+ promocode + "/"+estimationFare;

        /*Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PROMO_CODE, promocode);*/

        Log.e(TAG, "callCheckPromoCode() url = " + url);
        //Log.e(TAG, "callCheckPromoCode() param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callCheckPromoCode() responseCode = " + responseCode);
                    Log.e(TAG,"callCheckPromoCode() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            String Message = "";

                            if (json.getBoolean("status"))
                            {
                                Log.e(TAG,"callCheckPromoCode() book later send request successfully");
                                dialogClass.hideDialog();
                                ivRemovePromo.setVisibility(View.VISIBLE);
                                dialog.dismiss();

                                String new_estimation_fare = "";

                                if(json.has("new_estimate_fare"))
                                {
                                    new_estimation_fare = json.getString("new_estimate_fare");
                                }

                                //tv_EstimateFare.setText(getResources().getString(R.string.currency) + new_estimation_fare);
                                tv_EstimateFare_pay.setText(getResources().getString(R.string.currency) + commaFormat(Double.parseDouble(new_estimation_fare)));

                                if(json.has("promocode"))
                                {
                                    JSONObject jsonObjectPromo = json.getJSONObject("promocode");

                                    String discount = "";

                                    if(jsonObjectPromo.has("DiscountValue"))
                                    {
                                        discount = jsonObjectPromo.getString("DiscountValue");
                                    }

                                    ll_Discount.setVisibility(View.VISIBLE);
                                    ll_EstimateFare_pay.setVisibility(View.VISIBLE);
                                    tv_Discount.setText(getResources().getString(R.string.currency) + commaFormat(Double.parseDouble(discount)));
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callCheckPromoCode() book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    Message = json.getString("message");
                                    et_Promocode.setError(Message);
                                    tv_Promocode.setText("");
                                    promocodeStr = "";
                                    ivRemovePromo.setVisibility(View.GONE);
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callCheckPromoCode() book later status not found");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callCheckPromoCode() book later json null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callCheckPromoCode() Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                json = GetAddress(jsonurl.toString());
                if (json != null) {
                    names.clear();
                    Place_id_type.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        Log.d("description", description);
                        names.add(description);
                        Place_id_type.add(plc_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (addFlag == 1) {
                Log.e("names", "" + names);
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }

                                    pickup_Touch = true;
                                    auto_Pickup.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Pickup.dismissDropDown();

                                    if (auto_Dropoff.getText().toString().trim().length() <= 0) {
                                        auto_Dropoff.setFocusableInTouchMode(true);
                                        auto_Dropoff.requestFocus();
                                    } else if (et_Name.getText().toString().trim().length() <= 0) {
                                        et_Name.setFocusableInTouchMode(true);
                                        et_Name.requestFocus();
                                    } else if (et_Phone.getText().toString().trim().length() <= 0) {
                                        et_Phone.setFocusableInTouchMode(true);
                                        et_Phone.requestFocus();
                                    } else if (flight == true) {
                                        if (et_Flight.getText().toString().trim().length() <= 0) {
                                            et_Flight.setFocusableInTouchMode(true);
                                            et_Flight.requestFocus();
                                        }
                                    }
                                    if (!auto_Dropoff.getText().toString().trim().equalsIgnoreCase("")) {
                                        getEstimatedFare();
                                    } else {
                                        tv_EstimateFare.setText(activity.getResources().getString(R.string.currency) + "0.0");
                                    }
                                }
                            });
                            return view;
                        }
                    };
                    auto_Pickup.setAdapter(adp);
                    adp.notifyDataSetChanged();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            } else if (addFlag == 2) {
                Log.e("names", "" + names);
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = (TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }

                                    dropoff_Touch = true;
                                    auto_Dropoff.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Dropoff.dismissDropDown();

                                    if (auto_Pickup.getText().toString().trim().length() <= 0) {
                                        auto_Pickup.setFocusableInTouchMode(true);
                                        auto_Pickup.requestFocus();
                                    } else if (et_Name.getText().toString().trim().length() <= 0) {
                                        et_Name.setFocusableInTouchMode(true);
                                        et_Name.requestFocus();
                                    } else if (et_Phone.getText().toString().trim().length() <= 0) {
                                        et_Phone.setFocusableInTouchMode(true);
                                        et_Phone.requestFocus();
                                    } else if (flight == true) {
                                        if (et_Flight.getText().toString().trim().length() <= 0) {
                                            et_Flight.setFocusableInTouchMode(true);
                                            et_Flight.requestFocus();
                                        }
                                    }
                                    if (!auto_Pickup.getText().toString().trim().equalsIgnoreCase("")) {
                                        getEstimatedFare();
                                    } else {
                                        tv_EstimateFare.setText(activity.getResources().getString(R.string.currency) + "0.0");
                                    }
                                }
                            });
                            return view;
                        }
                    };
                    auto_Dropoff.setAdapter(adp);
                    adp.notifyDataSetChanged();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }
    }

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress) {
        try {
            if (addFlag == 1) {
                if (auto_Pickup.getText().toString().trim().length() > 0) {
                    pickupLocation = auto_Pickup.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + getResources().getString(R.string.map_api_key);
                    Log.e("call", "pickupurl = " + encode_url);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                }
            } else if (addFlag == 2) {
                if (auto_Dropoff.getText().toString().trim().length() > 0) {
                    dropoffLocation = auto_Dropoff.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + getResources().getString(R.string.map_api_key);
                    Log.e("call", "dropoffurl = " + encode_url);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                }
            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("call", "exception = " + e.getMessage());
            if (addFlag == 1) {
                pickup_Lat = 0.0;
                pickup_Long = 0.0;
                pickupLocation = "";
            } else if (addFlag == 2) {
                dropoff_Lat = 0.0;
                dropoff_Long = 0.0;
                dropoffLocation = "";
            }
        }
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        private String strAddress;

        public GetGeoCoderAddress(String url, String strAddress) {
            this.Urlcoreconfig = url;
            this.strAddress = strAddress;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("sureshhhhhhhhh " + jsonResult);
                JSONObject json = new JSONObject(jsonResult);
                System.out.println("ssssssssssss " + json);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (lat != null && !lat.trim().equalsIgnoreCase("") && lng != null && !lng.equalsIgnoreCase("")) {
                    if (addFlag == 1) {
                        pickup_Lat = Double.valueOf(lat);
                        pickup_Long = Double.valueOf(lng);
                        System.out.println("sureshhhhhhhhh " + pickupLocation + pickup_Lat + " , " + pickup_Long);
                    } else if (addFlag == 2) {
                        dropoff_Lat = Double.valueOf(lat);
                        dropoff_Long = Double.valueOf(lng);
                        System.out.println("sureshhhhhhhhh " + dropoffLocation + dropoff_Lat + " , " + dropoff_Long);
                    }
                } else {
                    Geocoder coder = new Geocoder(activity);
                    List<Address> address;
                    Address location = null;

                    address = coder.getFromLocationName(strAddress, 5);
                    address = coder.getFromLocationName(strAddress, 5);
                    if (!address.isEmpty()) {
                        location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();
                    }

                    if (!address.isEmpty()) {
                        if (addFlag == 1) {
                            pickup_Lat = location.getLatitude();
                            pickup_Long = location.getLongitude();
                            if (auto_Pickup.getText().toString().trim().length() > 0) {
                                pickupLocation = auto_Pickup.getText().toString();
                            }
                        } else if (addFlag == 2) {
                            dropoff_Lat = location.getLatitude();
                            dropoff_Long = location.getLongitude();
                            if (auto_Dropoff.getText().toString().trim().length() > 0) {
                                dropoffLocation = auto_Dropoff.getText().toString();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("call", "exception = " + e.getMessage());
            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    public void selectDateAndTime()
    {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth)
                    {
                        dateTimetxt.setText("");
                        dateTime = "";
                        showTimePicker(dayOfMonth, (monthOfYear + 1), year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void showTimePicker(final int dayOfMonth, final int monthOfYear, final int year) {
        String selectedDate = "";
        if (monthOfYear < 10)
        {
            if (dayOfMonth < 10)
            {
                selectedDate = year + "-0" + monthOfYear + "-0" + dayOfMonth;
            }
            else
            {
                selectedDate = year + "-0" + monthOfYear + "-" + dayOfMonth;
            }
        }
        else
            {
            if (dayOfMonth < 10)
            {
                selectedDate = year + "-" + monthOfYear + "-0" + dayOfMonth;
            }
            else
            {
                selectedDate = year + "-" + monthOfYear + "-" + dayOfMonth;
            }
        }

        final String formattedDate = TruckzShipperApplication.getCurrentDate().split(",")[0].trim();
        Log.e("call", "sdlfjsldjfsldjflj = " + formattedDate);
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        Log.e("call", "sdfsdfsdfsfsf selectedDate = " + selectedDate);
        if (formattedDate.equalsIgnoreCase(selectedDate))
        {
            Log.e("call", "111111111111");

            //if (c.get(Calendar.MINUTE) + 31 >= 60)
            if (c.get(Calendar.MINUTE) + 61 >= 60)
            {
                mMinute = (c.get(Calendar.MINUTE) + 61 - 60);
                mHour = mHour + 1;
            }
            else
            {
                //mMinute = (c.get(Calendar.MINUTE) + 31);
                mMinute = (c.get(Calendar.MINUTE) + 61);
            }
        }
        else
        {
            Log.e("call", "22222222222222");
            mMinute = c.get(Calendar.MINUTE);
        }


        // Launch Time Picker Dialog
        final String finalSelectedDate = selectedDate;
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                try {
                    int checkMinut = minute;
                    int checkHour = hourOfDay;
                    int x = hourOfDay;
                    Calendar datetime1 = Calendar.getInstance();
                    Calendar c1 = Calendar.getInstance();
                    datetime1.set(Calendar.MINUTE, minute);
                    datetime1.set(Calendar.HOUR_OF_DAY, hourOfDay);

                    Calendar datetime = Calendar.getInstance();
                    Calendar c = Calendar.getInstance();
                    int flag = 0;
                    if (formattedDate.equalsIgnoreCase(finalSelectedDate))
                    {
                        Log.e("call", "333333333333333333");

                        //if (minute + 31 >= 60)
                        if (minute + 61 >= 60)
                        {
                            flag = 1;
                            hourOfDay = Math.abs(hourOfDay + 1);
                        }
                    }

                    Log.e("call", "hourOfDay ========= " + hourOfDay);
                    Log.e("call", "minutes ========= " + minute);

                    datetime.set(Calendar.MINUTE, minute);
                    datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);

                    String my_date = dayOfMonth + "/" + monthOfYear + "/" + year;

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date strDate = sdf.parse(my_date);

                    if (sdf.parse(my_date).before(new Date()))
                    {
                        if (checkValidTime(my_date + " " + checkHour + ":" + checkMinut + ":00")) {
                            String AM_PM;


                            int hour;
                            if (flag == 1)
                            {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay < 12)
                            {
                                AM_PM = getResources().getString(R.string.am_cap);
                            }
                            else
                            {
                                AM_PM = getResources().getString(R.string.pm_cap);
                            }

                            if (hourOfDay == 0 || hourOfDay == 12)
                            {
                                hour = 12;
                            }
                            else if (hourOfDay < 12)
                            {
                                hour = hourOfDay;
                            }
                            else
                            {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call", "hour ========= " + hour);
                            Log.e("call", "minutes ========= " + minute);
                            Log.e("call", "date and time = " + dateTime);

                            if (hour < 10 )
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + minute + " " + AM_PM);
                                }
                            }
                            else
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " " + AM_PM);
                                }
                            }

                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + ":" + minute + ":00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call", "date and time = " + dateTime);
                        } else {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(getResources().getString(R.string.book_later_thirty_minute_validation), getResources().getString(R.string.info_message));

                        }
                    }
                    else if (sdf.parse(my_date).equals(new Date()))
                    {
                        if (checkValidTime(my_date + " " + checkHour + ":" + checkMinut + ":00")) {
                            String AM_PM;


                            int hour;
                            if (flag == 1) {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay < 12) {
                                AM_PM = getResources().getString(R.string.am_cap);
                            } else {
                                AM_PM = getResources().getString(R.string.pm_cap);
                            }

                            if (hourOfDay == 0 || hourOfDay == 12) {
                                hour = 12;
                            } else if (hourOfDay < 12) {
                                hour = hourOfDay;
                            } else {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call", "hour ========= " + hour);
                            Log.e("call", "minutes ========= " + minute);
                            Log.e("call", "date and time = " + dateTime);

                            if (hour < 10 )
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + minute + " " + AM_PM);
                                }

                            }
                            else
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " " + AM_PM);
                                }
                            }

                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + ":" + minute + ":00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call", "date and time = " + dateTime);
                        } else {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(getResources().getString(R.string.book_later_thirty_minute_validation), getResources().getString(R.string.info_message));
                        }
                    } else {
                        if (checkValidTime(my_date + " " + checkHour + ":" + checkMinut + ":00")) {
                            String AM_PM;


                            int hour;
                            if (flag == 1) {
                                hourOfDay = hourOfDay - 1;
                            }

                            if (hourOfDay < 12) {
                                AM_PM = getResources().getString(R.string.am_cap);
                            } else {
                                AM_PM = getResources().getString(R.string.pm_cap);
                            }

                            if (hourOfDay == 0 || hourOfDay == 12) {
                                hour = 12;
                            } else if (hourOfDay < 12) {
                                hour = hourOfDay;
                            } else {
                                hour = hourOfDay - 12;
                            }

                            Log.e("call", "hour ========= " + hour);
                            Log.e("call", "minutes ========= " + minute);
                            Log.e("call", "date and time = " + dateTime);
                            if (hour < 10 )
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + "0" + hour + ":" + minute + " " + AM_PM);
                                }

                            }
                            else
                            {
                                if (minute < 10)
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + "0" + minute + " " + AM_PM);
                                }
                                else
                                {
                                    dateTimetxt.setText(dayOfMonth + "-" + monthOfYear + "-" + year + "  " + hour + ":" + minute + " " + AM_PM);
                                }
                            }
                            pickup_Date = year + "-" + monthOfYear + "-" + dayOfMonth;
                            pickup_Time = hourOfDay + ":" + minute + ":00";
                            dateTime = pickup_Date + " " + pickup_Time;
                            Log.e("call", "date and time = " + dateTime);
                        } else {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(getResources().getString(R.string.book_later_thirty_minute_validation), getResources().getString(R.string.info_message));
                        }
                    }
                } catch (Exception e) {
                    Log.e("Exception", " = " + e.getMessage());
                }

            }
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public boolean checkValidTime(String selectedDateTime) {


        try {

            Log.e("call", "selected = " + selectedDateTime);
            Log.e("call", "current = " + new Date().toString());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date selected = simpleDateFormat.parse(selectedDateTime);
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String strDate = sdf.format(cal.getTime());
            Date current = simpleDateFormat.parse(strDate);

            long diff = selected.getTime() - current.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;

            Log.e("call", "selected = " + selected.toString());
            Log.e("call", "current = " + current.toString());
            Log.e("call", "difference in minutes = " + minutes);

            //if (minutes >= 30) {
            if (minutes >= 60) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("call", "exception in date formate = " + e.getMessage());
            return false;
        }
    }

    public void call_CheckValidation()
    {
//        InternetDialog internetDialog = new InternetDialog(activity);
//        internetDialog.showDialog(getResources().getString(R.string.coming_soon),getResources().getString(R.string.info_message));

        if (pickupLocation == null || pickupLocation.equalsIgnoreCase("") || pickup_Touch == false)
        {
            mySnackBar.showSnackBar(ll_RootLayout, getString(R.string.please_enter_pickup_add));
            auto_Pickup.setFocusableInTouchMode(true);
            auto_Pickup.requestFocus();
        }
        else if (dropoffLocation == null || dropoffLocation.equalsIgnoreCase("") || dropoff_Touch == false)
        {
            mySnackBar.showSnackBar(ll_RootLayout, getString(R.string.please_enter_destination_add));
            auto_Dropoff.setFocusableInTouchMode(true);
            auto_Dropoff.requestFocus();
        }
        else if (et_Name.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please enter name!");
            et_Name.setFocusableInTouchMode(true);
            et_Name.requestFocus();
        }
        else if (et_Phone.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please enter phone number!");
            et_Phone.setFocusableInTouchMode(true);
            et_Phone.requestFocus();
        }
        else if (et_Phone.getText().toString().trim().length() != 10)
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Invalid Phone Number!");
            et_Phone.setFocusableInTouchMode(true);
            et_Phone.requestFocus();
        }
        else if (etReceiverName.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please enter receiver name!");
            etReceiverName.setFocusableInTouchMode(true);
            etReceiverName.requestFocus();
        }
        else if (etreceiverPhone.getText().toString().trim().equals(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please enter receiver phone number!");
            etreceiverPhone.setFocusableInTouchMode(true);
            etreceiverPhone.requestFocus();
        }
        else if (etreceiverPhone.getText().toString().trim().length() != 10)
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Invalid Receiver Phone Number!");
            etreceiverPhone.setFocusableInTouchMode(true);
            etreceiverPhone.requestFocus();
        }
//        else if(etreceiverMail.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            mySnackBar.showSnackBar(ll_RootLayout, "Please enter receiver email id");
//            etreceiverMail.setFocusableInTouchMode(true);
//            etreceiverMail.requestFocus();
//        }
//        else if(!isValidEmail(etreceiverMail.getText().toString().trim()))
//        {
//            mySnackBar.showSnackBar(ll_RootLayout, "Please enter receiver valid email id");
//            etreceiverMail.setFocusableInTouchMode(true);
//            etreceiverMail.requestFocus();
//        }
        else if (dateTimetxt.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please select date and time!");
        } else if (selectedPaymentType == null || selectedPaymentType.equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Please select payment method!");
        }
        else if (selectedTransportService == null ||
                selectedTransportService.equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_RootLayout, getString(R.string.please_select_goods_type));
        }
        else if (TextUtils.isEmpty(etParcelWeight.getText().toString().trim()))
        {
            mySnackBar.showSnackBar(ll_RootLayout, getString(R.string.please_enter_goods_weight));
        }
        else if(!getKgValidOrNot(Double.parseDouble(etParcelWeight.getText().toString())))
        {
            mySnackBar.showSnackBar(ll_RootLayout, "Goods weight should be less than  "+String.format("%.2f", (TruckCapacity * 1000.00))+" Kg");
            //etParcelWeight.setError("Goods weight should be less than "+String.format("%.2f", (Double.parseDouble(carType_beens.get(selectedCarPosition).getCapacity()) * 1000.00))+" Kg");
        }
//        else if (parcelID != null && parcelID.trim().equalsIgnoreCase("") &&
//                MainActivity.carType_beens.get(selectedCar).getCategoryId().equalsIgnoreCase("2"))
//        {
//            mySnackBar.showSnackBar(ll_RootLayout,activity.getResources().getString(R.string.please_select_transport_service));
//        }
//        else if (flight == true) {
//            if (et_Flight.getText().toString().trim().equals("")) {
//                mySnackBar.showSnackBar(ll_RootLayout, "Please enter flight number!");
//                et_Flight.setFocusableInTouchMode(true);
//                et_Flight.requestFocus();
//            }
//            else {
//                if (Global.isNetworkconn(activity)) {
//                    if (MainActivity.carType_beens != null && MainActivity.carType_beens.size() > 0) {
//                        call_BookLaterApi();
//                    } else {
//                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
//                        errorDialogClass.showDialog("Car model not found", "Error Message");
//                    }
//
//                } else {
//                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
//                    errorDialogClass.showDialog("Please check your internet connection!", "No internet connection");
//                }
//            }
//        }
        else
        {
            //PassengerId,
            // ModelId,
            // PickupLocation,
            // DropoffLocation,
            // PassengerType(myself,other),
            // PassengerName,
            // PassengerContact,
            // PickupDateTime,
            // FlightNumber

            if (!TextUtils.isEmpty(etreceiverMail.getText().toString().trim()))
            {
                if (isValidEmail(etreceiverMail.getText().toString().trim()))
                {
                    if (Global.isNetworkconn(activity))
                    {
                        if (MainActivity.carType_beens != null && MainActivity.carType_beens.size() > 0)
                        {
                            call_BookLaterApi();
                        }
                        else
                        {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(getResources().getString(R.string.car_model_not_found), getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                    }
                }
                else
                {
                    mySnackBar.showSnackBar(ll_RootLayout, "Please enter receiver valid email id");
                    etreceiverMail.setFocusableInTouchMode(true);
                    etreceiverMail.requestFocus();
                }

            }
            else
            {
                if (Global.isNetworkconn(activity))
                {
                    if (MainActivity.carType_beens != null && MainActivity.carType_beens.size() > 0)
                    {
                        call_BookLaterApi();
                    }
                    else
                    {
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getResources().getString(R.string.car_model_not_found), getResources().getString(R.string.error_message));
                    }
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
            }
        }
    }

    private boolean getKgValidOrNot(Double kg) {
        boolean flag = false;

        if((TruckCapacity * 1000.00) >= kg) {
            flag = true;
        }

        return flag;
    }

    private void call_BookLaterApi() {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            Date strDate = sdf.parse(dateTimetxt.getText().toString());

            if (!printDifference(strDate,new Date()))
            {
                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                errorDialogClass.showDialog(getResources().getString(R.string.book_later_thirty_minute_validation), getResources().getString(R.string.info_message));
                return;
            }
        }
        catch (Exception e)
        {}

        Log.e(TAG, "call_BookLaterApi()");

        String url = WebServiceAPI.API_ADVANCE_BOOKING;

        String passenger_type = "myself";

        if (mySelfOtherFlag == 0) {
            passenger_type = "myself";
        } else {
            passenger_type = "other";
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity));
        params.put(WebServiceAPI.PARAM_MODEL_ID, MainActivity.carType_beens.get(selectedCar).getId());
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION, pickupLocation);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, dropoffLocation);
        params.put(WebServiceAPI.PARAM_PASSENGER_TYPE, passenger_type);
        params.put(WebServiceAPI.PARAM_PASSENGER_NAME, et_Name.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PASSENGER_CONTACT, et_Phone.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PICKUP_DATE_TIME, dateTime);
        params.put(WebServiceAPI.PARAM_PROMO_CODE, promocodeStr);
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE, selectedPaymentType);
        params.put(WebServiceAPI.PARAM_RECEIVER_NAME, etReceiverName.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_RECEIVER_CONTACT_NO, etreceiverPhone.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PARCEL_WEIGHT, etParcelWeight.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_RECEIVER_EMAIL, etreceiverMail.getText().toString().trim());

        if (notes == true) {
            params.put(WebServiceAPI.PARAM_NOTES, et_Notes.getText().toString().trim());
        }

        if (selectedPaymentType.equalsIgnoreCase("card")) {
            params.put(WebServiceAPI.PARAM_CARD_ID, cardId);
        }

        if (flight == true) {
            params.put(WebServiceAPI.PARAM_FLIGHT_NUMBER, et_Flight.getText().toString().trim());
        }

        if (MainActivity.carType_beens.get(selectedCar).getCategoryId().equalsIgnoreCase("2")) {
            params.put(WebServiceAPI.PARAM_REQUEST_FOR, "delivery");
            params.put(WebServiceAPI.PARAM_PARCEL_ID,selectedTransportService);
//            params.put(WebServiceAPI.PARAM_LABOUR, selectedLabour);
        } else {
            params.put(WebServiceAPI.PARAM_REQUEST_FOR, "taxi");
        }


        Log.e(TAG, "call_BookLaterApi() url = " + url);
        Log.e(TAG, "call_BookLaterApi() param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"call_BookLaterApi() responseCode = " + responseCode);
                    Log.e(TAG,"call_BookLaterApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e(TAG,"call_BookLaterApi() book later send request successfully");
                                dialogClass.hideDialog();

                                String message = getString(R.string.your_booking_request_has_been_confirmed);

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                    showSuccessMessage(message);
                                }
                            }
                            else
                            {
                                Log.e(TAG,"call_BookLaterApi() book later status false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));*/

                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    String x = "pay your previous Due";
                                    if(json.getString("message").toLowerCase().contains(x.toLowerCase()) || json.getString("message").contains("first make payment"))
                                    {
                                        internetDialog.setListener(new InternetDialog.dialogPositiveClick() {
                                            @Override
                                            public void onOkClick() {
                                                Intent intent = new Intent(activity, PreviousDueActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                                finish();
                                            }
                                        });
                                    }
                                    internetDialog.showDialog(json.getString("message"), getResources().getString(R.string.success_message));
                                }
                                else
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG,"call_BookLaterApi() book later status not found");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"call_BookLaterApi() book later json null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"call_BookLaterApi() Exception "+e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void showSuccessMessage(String message) {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        dialog_title.setText(getResources().getString(R.string.success_message));

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectedCar = -1;
                        MainActivity.myLocation = true;
                        finish();
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    }
                }, 800);

            }
        });

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectedCar = -1;
                        MainActivity.myLocation = true;
                        finish();
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    }
                }, 800);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectedCar = -1;
                        MainActivity.myLocation = true;
                        finish();
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    }
                }, 800);
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);
        saveCardList();
        if (isCardSelected)
        {
            selectedPaymentType = "card";
            Spinner_paymentMethod.setSelection(0);
        }
    }

    public void saveCardList() {
        try {
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            MainActivity.cardListBeens.clear();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    if (json.has("cards")) {
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            for (int i = 0; i < cards.length(); i++) {
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData != null) {
                                    String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "", Expiry = "";

                                    if (cardsData.has("Id")) {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum")) {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2")) {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type")) {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias")) {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    if (cardsData.has("Expiry")) {
                                        Expiry = cardsData.getString("Expiry");
                                    }

                                    //MainActivity.cardListBeens.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias, Expiry));
                                }
                            }
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("", "", getString(R.string.cash), "", "", ""));
//                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                        } else {
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("", "", getString(R.string.cash), "", "", ""));
//                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                            MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                        }
                    } else {
                        MainActivity.cardListBeens.add(new CreditCard_List_Been("", "", getString(R.string.cash), "", "", ""));
//                        MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                        MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                    }
                } else {
                    MainActivity.cardListBeens.add(new CreditCard_List_Been("", "", getString(R.string.cash), "", "", ""));
//                    MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                    MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                }
            } else {
                MainActivity.cardListBeens.add(new CreditCard_List_Been("", "", getString(R.string.cash), "", "", ""));
//                MainActivity.cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                MainActivity.cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
            }
        } catch (Exception e) {

        }
    }

    public class CustomerAdapter extends ArrayAdapter<CreditCard_List_Been> {
        ArrayList<CreditCard_List_Been> customers, tempCustomer, suggestions;

        public CustomerAdapter(Context context, ArrayList<CreditCard_List_Been> objects) {
            super(context, R.layout.row_spinner_item, R.id.spinner_cardnumber, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<CreditCard_List_Been>(objects);
            this.suggestions = new ArrayList<CreditCard_List_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            CreditCard_List_Been customer = getItem(position);

            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, parent, false);
            }
            TextView txtCustomer = (TextView) convertView.findViewById(R.id.spinner_cardnumber);
            LinearLayout ll_Image = (LinearLayout) convertView.findViewById(R.id.spinner_image_layout);
            ImageView iv_Image = (ImageView) convertView.findViewById(R.id.image);
            LinearLayout ll_Add_Card = (LinearLayout) convertView.findViewById(R.id.ll_add_payment_method);
            ImageView iv_logo = convertView.findViewById(R.id.iv_logo);

            if (txtCustomer != null)
                txtCustomer.setText(customer.getCardNum_());

            if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("visa")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("mastercard")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_master);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("amex")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_american);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("diners")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_dinner);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("discover")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_discover);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("jcb")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_jcb);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("other")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("cash")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_cash);
                ll_Image.setVisibility(View.VISIBLE);
                iv_logo.setVisibility(View.GONE);
                txtCustomer.setVisibility(View.VISIBLE);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("wallet")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_wallet);
            }else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("Paytm")) {
                iv_logo.setImageResource(R.drawable.paytm_logo_);
                ll_Image.setVisibility(View.GONE);
                txtCustomer.setVisibility(View.GONE);
                iv_logo.setVisibility(View.VISIBLE);
            }

            if (position == (customers.size() - 1)) {
                if (checkCardList()) {
                    ll_Add_Card.setVisibility(View.GONE);
                } else {
                    ll_Add_Card.setVisibility(View.VISIBLE);
                }
            } else {
                ll_Add_Card.setVisibility(View.GONE);
            }

            ll_Add_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                    intent.putExtra("from","BookLaterActivity");
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                }
            });

            return convertView;
        }
    }

    public class CustomerTransportServiceAdapter extends ArrayAdapter<TransportService_Been> {
        ArrayList<TransportService_Been> customers, tempCustomer, suggestions;

        public CustomerTransportServiceAdapter(Context context, ArrayList<TransportService_Been> objects) {
            super(context, R.layout.row_spinner_item_transport_service, R.id.tv_spinner_transport_service, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<TransportService_Been>(objects);
            this.suggestions = new ArrayList<TransportService_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            TransportService_Been customer = getItem(position);

            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item_transport_service, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item_transport_service, parent, false);
            }
            TextView tv_spinner_transport_service = (TextView) convertView.findViewById(R.id.tv_spinner_transport_service);

            if (tv_spinner_transport_service != null)
                tv_spinner_transport_service.setText(customer.getName());

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        selectedCar = -1;
        MainActivity.myLocation = true;
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }

    public String commaFormat(Double x)
    {
        return new DecimalFormat("#,###.0").format(x);
    }

    public boolean printDifference(Date startDate, Date endDate) {
        //milliseconds
        boolean flag = false;
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.e("TAG","elapsedHours = "+elapsedHours);
        Log.e("TAG","elapsedMinutes = "+elapsedMinutes);
        if (elapsedMinutes>0 || elapsedHours == 0)//-59,-58
        {
            flag = false;//error
        }
        else
        {
            flag = true;
        }
        Log.e("call","diffenence minutes = days, hours, minutes, seconds = " + elapsedDays + "," + elapsedHours + "," + elapsedMinutes + "," + elapsedSeconds);
        Log.e("call","flag = "+flag);
        return flag;

    }
}
