package com.truckz.shipper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.adapter.Transfer_History_Adapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.Transfer_History_Been;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class Wallet_Transfer_History_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Transfer_History_Activity activity;

    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title;

    private RecyclerView rv_transferHistory;
    private List<Transfer_History_Been> list = new ArrayList<>();
    private Transfer_History_Adapter adapter;
    private AQuery aQuery;
    private DialogClass dialogClass;
    public static int resumeFlag = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer_history);

        activity = Wallet_Transfer_History_Activity.this;
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);
        initUI();
    }

    private void initUI()
    {
        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);

        tv_Title = (TextView) findViewById(R.id.title_textview);

        tv_Title.setText(getResources().getString(R.string.history));

        rv_transferHistory = (RecyclerView) findViewById(R.id.rv_transferHistory);

        adapter = new Transfer_History_Adapter(activity, list);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rv_transferHistory.setLayoutManager(mLayoutManager);
        rv_transferHistory.setItemAnimator(new DefaultItemAnimator());
        rv_transferHistory.setAdapter(adapter);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);

        if (Global.isNetworkconn(activity))
        {
            getMoneyHistory();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;

                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }

                if (Wallet_Transfer_Activity.activity!=null)
                {
                    Wallet_Transfer_Activity.activity.finish();
                }

                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    public void getMoneyHistory()
    {
        dialogClass.showDialog();
        list.clear();
        String url = WebServiceAPI.API_GET_TRANSACTION_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("walletBalance"))
                        {
                            Wallet__Activity.walleteBallence = json.getString("walletBalance");
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,Wallet__Activity.walleteBallence,activity);
                        }

                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject historyData = history.getJSONObject(i);

                                            if (historyData!=null)
                                            {
                                                String Id="",UserId="",WalletId="",UpdatedDate="",Amount="",Type="",Description="",Status="";

                                                if (historyData.has("Id"))
                                                {
                                                    Id = historyData.getString("Id");
                                                }

                                                if (historyData.has("UserId"))
                                                {
                                                    UserId = historyData.getString("UserId");
                                                }

                                                if (historyData.has("WalletId"))
                                                {
                                                    WalletId = historyData.getString("WalletId");
                                                }

                                                if (historyData.has("UpdatedDate"))
                                                {
                                                    UpdatedDate = historyData.getString("UpdatedDate");
                                                }

                                                if (historyData.has("Amount"))
                                                {
                                                    Amount = historyData.getString("Amount");
                                                }

                                                if (historyData.has("Type"))
                                                {
                                                    Type = historyData.getString("Type");
                                                }

                                                if (historyData.has("Description"))
                                                {
                                                    Description = historyData.getString("Description");
                                                }

                                                if (historyData.has("Status"))
                                                {
                                                    Status = historyData.getString("Status");
                                                }

                                                list.add(new Transfer_History_Been(Id,UserId,WalletId,UpdatedDate,Amount,Type,Description,Status));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        Log.e("call","no cards available");
                                        dialogClass.hideDialog();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                else
                                {
                                    Log.e("call","no cards found");
                                    dialogClass.hideDialog();
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            adapter.notifyDataSetChanged();
                        }

                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    adapter.notifyDataSetChanged();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}