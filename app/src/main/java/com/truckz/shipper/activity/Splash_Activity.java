package com.truckz.shipper.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.Constants;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;

import android.provider.Settings.Secure;

public class Splash_Activity extends AppCompatActivity {

    //Defining variable
    public String TAG = "Splash_Activity";
    public static Splash_Activity activity;
    private TruckzShipperApplication application;
    private Handler handler;
    private Runnable runnable;
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private String versionName;

    private AQuery aQuery;
    private DialogClass dialogClass;
    private String android_id;
    private TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        activity = this;
        application = (TruckzShipperApplication) getApplicationContext();

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 0);

        android_id = Secure.getString(getContentResolver(),Secure.ANDROID_ID);
        Common.USER_DEVICE_ID = android_id;
        Log.e("call","android_id = "+Common.USER_DEVICE_ID);

        try
        {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
            int versionCode = pInfo.versionCode;

            Log.e("call","version name = "+versionName);
            Log.e("call","version code = "+versionCode);

            PackageInfo info = getPackageManager().getPackageInfo("com.Pickngo", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Splash KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (Exception e)
        {
            Log.e("call","Splash Activity Exception = "+e.getMessage());
        }

        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,"",Splash_Activity.this);
        getToken();

        getHeightAndWidth();

        if (Global.isNetworkconn(activity))
        {

            if (SessionSave.getUserSession(Common.USER_ACCOUNT_BLOCK,activity) != null &&
                    !SessionSave.getUserSession(Common.USER_ACCOUNT_BLOCK,activity).equalsIgnoreCase("") &&
                    !SessionSave.getUserSession(Common.USER_ACCOUNT_BLOCK,activity).equalsIgnoreCase("null"))
            {
                callLogout(SessionSave.getUserSession(Common.USER_ACCOUNT_BLOCK,activity));
            }
            else
            {
                checkUpdate();
            }
        }
        else
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),
                    getResources().getString(R.string.no_internet_connection));

        }
    }

    private void callLogout(String message)
    {
        final Dialog dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_my_class);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_Message = dialog.findViewById(R.id.message);
        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        LinearLayout ll_Ok = dialog.findViewById(R.id.ok_layout);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        iv_close.setVisibility(View.GONE);

        tv_Message.setText(message);
        tv_title.setText(getString(R.string.info_message));

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                SessionSave.clearUserSession(activity);
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },1000);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                SessionSave.clearUserSession(activity);

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },1000);

            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                SessionSave.clearUserSession(activity);
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },1000);
            }
        });

        dialog.show();
    }

    public void getHeightAndWidth()
    {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        if (width > 5)
        {

            Log.e("abcd", "width width width : " + width + "height height height : " + height);
            Constants.DEVICE_WIDTH = width;
            Constants.DEVICE_HEIGHT = height;
        }
        else {
            Display display = getWindowManager().getDefaultDisplay();
            Method mGetRawH = null;
            try {
                mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                width = (Integer) mGetRawW.invoke(display);
                height = (Integer) mGetRawH.invoke(display);

                Log.e("abcd", "width width width 222 ;" + width + "height height height2222 : " + height);
                Constants.DEVICE_WIDTH = width;
                Constants.DEVICE_HEIGHT = height;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void getToken()
    {
        new Thread(new Runnable()
        {
            public void run()
            {

                /*try
                {
                    InstanceID instanceID = InstanceID.getInstance(Splash_Activity.this);

                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                    Log.e(TAG, "getToken() GCM Registration Token: " + token);
                }
                catch (Exception e)
                {
                    Log.e(TAG, "getToken() Failed to complete token refresh", e);
                }*/
            }
        }).start();
    }

    private void checkUpdate()
    {
//        dialogClass.showDialog();
        String url = WebServiceAPI.API_CHECK_VERSION + versionName + "/AndroidPassenger";

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null && !json.toString().equalsIgnoreCase("null"))
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                    boolean bool_status = false;
                                    boolean bool_maintenance= false;

                                    if (json.has("update"))
                                    {
                                        bool_status = json.getBoolean("update");
                                    }

                                    if (json.has("maintenance"))
                                    {
                                        bool_maintenance = json.getBoolean("maintenance");
                                    }

                                    showDialog(message,bool_status,bool_maintenance);
                                }
                                else
                                {
                                    init();
                                }
                            }
                            else
                            {
                                String message = getResources().getString(R.string.something_went_wrong);
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                boolean bool_status = false;
                                boolean bool_maintenance = false;

                                if (json.has("update"))
                                {
                                    bool_status = json.getBoolean("update");
                                }

                                if (json.has("maintenance"))
                                {
                                    bool_maintenance = json.getBoolean("maintenance");
                                }

                                dialogClass.hideDialog();
                                showDialog(message,bool_status,bool_maintenance);
                            }
                        }
                        else
                        {
                            String message = getResources().getString(R.string.something_went_wrong);
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();

                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                            errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        String message = getResources().getString(R.string.something_went_wrong);
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(message,getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));

                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showDialog(String message, final boolean update, final boolean maintenance)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_pick_update);

        TextView tv_update= (TextView) dialog.findViewById(R.id.update);
        TextView tv_later= (TextView) dialog.findViewById(R.id.later);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_update = (LinearLayout) dialog.findViewById(R.id.update_layout);
        LinearLayout ll_later = (LinearLayout) dialog.findViewById(R.id.later_layout);

        tv_Message.setText(message);

        if (update==true)
        {
            ll_later.setVisibility(View.GONE);
            tv_update.setText(getResources().getString(R.string.update));
        }
        else
        {
            if (maintenance == true)
            {
                tv_update.setText(getResources().getString(R.string.ok));
                ll_later.setVisibility(View.GONE);
            }
            else
            {
                tv_update.setText(getResources().getString(R.string.update_cap));
                ll_later.setVisibility(View.VISIBLE);
            }
        }

        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (maintenance==true)
                {
                    dialog.dismiss();
                    finish();
                }
                else
                {
                    gotoPlayStore();
                }
            }
        });

        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (maintenance==true)
                {
                    dialog.dismiss();
                    finish();
                }
                else
                {
                    gotoPlayStore();
                }
            }
        });

        tv_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                init();
            }
        });

        ll_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                init();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void gotoPlayStore()
    {
        final String appPackageName = getPackageName();
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    private void init() {

        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    startNextActivity();
                }
            }
        };
        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);

    }

    private void startNextActivity()
    {
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity)!=null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity).equals("") &&
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,activity).equals("1"))
        {
            Intent loginScreen = new Intent(Splash_Activity.this,MainActivity.class);
            loginScreen.putExtra("from", "Splash_Activity");
            startActivity(loginScreen);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            finish();
        }
        else
        {
            Intent loginScreen = new Intent(Splash_Activity.this,LoginActivity1.class);
            startActivity(loginScreen);
            finish();
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        }
    }
}

