package com.truckz.shipper.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.truckz.shipper.R;
import com.truckz.shipper.adapter.PreviousDueAdapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.Address_Been;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.been.previousDue.History;
import com.truckz.shipper.been.previousDue.PreviousDue;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.interfaces.CallbackPaytm;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.singleton.PaytmSingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PreviousDueActivity extends AppCompatActivity  implements View.OnClickListener, PreviousDueAdapter.onpayCliick
{
    PreviousDueActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    TextView tv_Title;

    private DialogClass dialogClass;
    private AQuery aQuery;

    TextView tv_NoDataFound;
    RecyclerView recyclerView;
    PreviousDueAdapter previousDueAdapter;
    LinearLayoutManager layoutManager;

    PreviousDue previousDue;
    CreditCard_List_Been selectedCard;
    TextView tv_type,tv_cardNo;//dialog
    ImageView iv_card;//dialog

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_due);
        activity = PreviousDueActivity.this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        init();
    }

    private void init()
    {
        tv_Title = findViewById(R.id.title_textview);
        tv_Title.setText("Previous Due");
        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        tv_NoDataFound = findViewById(R.id.tv_NoDataFound);
        recyclerView = findViewById(R.id.recyclerView);

        ll_Back.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        iv_call.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);

        if (Global.isNetworkconn(activity))
        {
            callPreviousDueApi();
        }
        else
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
        }
    }

    private void callPreviousDueApi()
    {
        dialogClass.showDialog();
        String id = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        String url = WebServiceAPI.API_PREVIOUS_DUE ;

        HashMap<String,Object> param = new HashMap<>();
        param.put(WebServiceAPI.API_PREVIOUS_DUE_PARAM_ID,id);
        param.put(WebServiceAPI.API_PREVIOUS_DUE_PARAM_UTYPE,"passenger");

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + param);

        aQuery.ajax(url.trim(), param, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                dialogClass.hideDialog();
                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    tv_NoDataFound.setVisibility(View.GONE);
                    previousDue = new Gson().fromJson(json.toString(),PreviousDue.class);
                    previousDueAdapter = new PreviousDueAdapter(activity,previousDue.getHistory(),PreviousDueActivity.this);
                    recyclerView.setAdapter(previousDueAdapter);
                    if(previousDue.getHistory().size() == 0)
                    {
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onClick(View view)
    {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Log.e("call","onBackPress favorite flag = "+MainActivity.favorite);
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
        finish();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

    }

    @Override
    public void onPayButton(final History history, final int position)
    {
        Log.e("PreviousDueActivity", "History : amount :"+history.getAmount()+" bookingId - "+history.getBookingId());
        PaytmSingleton.setCallBack(activity, new CallbackPaytm() {
            @Override
            public void onSuccess(String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }

            @Override
            public void onFailed(String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }

            @Override
            public void onNetworkError() {
                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                errorDialogClass.showDialog("Network Problem","Error");
            }

            @Override
            public void onClientAuthenticationFailed(String error, String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }

            @Override
            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl, String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }

            @Override
            public void onBackPressedCancelTransaction(String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }

            @Override
            public void onTransactionCancel(String inErrorMessage, Bundle inResponse, String status, String bookingType, String bookingId,String txId,String refId) {
                callPaytmPaymentApi(status,bookingType,bookingId,position,txId,refId,history.getAmount());
            }
        });

        Log.e("PreviousDueActivity", " HistoryData BookingId : "+history.getBookingId()+" cusId : "+history.getUserId()+" GrandTotal : "+history.getAmount()+" BookingType : "+history.getBookingType()+ " refId : "+history.getReferenceId());

        Intent intent = new Intent(activity,PaymentPaytmActivity.class);
        intent.putExtra("BookingId",history.getBookingId());
        intent.putExtra("cusId",history.getUserId());
        intent.putExtra("GrandTotal",history.getAmount());
        intent.putExtra("BookingType",history.getBookingType());
        intent.putExtra("refId",history.getReferenceId());
        startActivity(intent);

        //openDialog(history,position);
    }

    private void openDialog(final History history,final int position)
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_previous_due);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        ImageView dialog_close;
        LinearLayout submit_layout;
        LinearLayout select_card_layout;

        dialog_close = dialog.findViewById(R.id.dialog_close);
        submit_layout = dialog.findViewById(R.id.submit_layout);
        select_card_layout = dialog.findViewById(R.id.select_card_layout);
        iv_card = dialog.findViewById(R.id.iv_card);
        tv_type = dialog.findViewById(R.id.tv_type);
        tv_cardNo = dialog.findViewById(R.id.tv_cardNo);

        select_card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, Wallet_Add_Cards_Activity.class);
                intent.putExtra("from","due");
                startActivityForResult(intent,5);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPayApi(dialog,history,position);
            }
        });

        dialog.show();
    }

    private void callPayApi(final Dialog dialog, History history, final int position)
    {
        dialogClass.showDialog();
        String id = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);
        String url = WebServiceAPI.API_PAY_PREVIOUSDUE;

        HashMap<String,Object> param = new HashMap<>();
        param.put(WebServiceAPI.PARAM_AMOUNT,history.getAmount());
        param.put(WebServiceAPI.PARAM_CARD_ID,selectedCard.getId());
        param.put(WebServiceAPI.PARAM_BOOKING_ID,history.getBookingId());
        param.put(WebServiceAPI.PARAM_BOOKING_TYPE,history.getBookingType());

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + param);

        aQuery.ajax(url.trim(), param, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                dialogClass.hideDialog();
                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    previousDue.getHistory().remove(position);
                    previousDueAdapter.notifyDataSetChanged();

                    if(previousDue.getHistory().size() == 0)
                    {
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                    }

                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.setListener(new InternetDialog.dialogPositiveClick() {
                        @Override
                        public void onOkClick() {
                            finish();
                        }
                    });
                    internetDialog.showDialog(json.getString("payment_message"), getResources().getString(R.string.success_message));

                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(json.getString("payment_message"),getResources().getString(R.string.error_message));*/

                    dialog.dismiss();
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5)
        {
            if(resultCode == RESULT_OK)
            {
                selectedCard = (CreditCard_List_Been)data.getSerializableExtra("Card");
                if (selectedCard.getCardType().equalsIgnoreCase("visa"))
                {
                    iv_card.setImageResource(R.drawable.icon_creditcard_visa);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("discover"))
                {
                    iv_card.setImageResource(R.drawable.card_discover);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("amex"))
                {
                    iv_card.setImageResource(R.drawable.icon_creditcard_maestro);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("diners"))
                {
                    iv_card.setImageResource(R.drawable.card_dinner);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("jcb"))
                {
                    iv_card.setImageResource(R.drawable.card_jcbs);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("mastercard"))
                {
                    iv_card.setImageResource(R.drawable.card_master);
                }
                else if (selectedCard.getCardType().equalsIgnoreCase("maestro"))
                {
                    iv_card.setImageResource(R.drawable.icon_creditcard_maestro);
                }
                else
                {
                    iv_card.setImageResource(R.drawable.icon_creditcard_visa);
                }

                tv_cardNo.setVisibility(View.VISIBLE);
                tv_cardNo.setText(selectedCard.getCardNum_());
                tv_type.setText(selectedCard.getCardType());
            }
        }
    }

    private void callPaytmPaymentApi(final String status_, String bookingType, String bookingId,final int position,String txId,String refId,String amt)
    {
        final DialogClass dialogClass = new DialogClass(activity, 0);
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PAYMENT_PAYTM;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PAYMENT_PAYTM_BID, bookingId);
        params.put(WebServiceAPI.PAYMENT_PAYTM_BTYPE, bookingType);
        params.put(WebServiceAPI.PAYMENT_PAYTM_STATUS, status_);
        params.put(WebServiceAPI.PAYMENT_PAYTM_PAYMENTFOR, "PreviousDue");
        params.put(WebServiceAPI.PAYMENT_PAYTM_AMOUNT, amt);

        if(txId != null && !txId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_TXID, txId);
        if(refId != null && !refId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_REFID,refId);

        Log.e("TAG", "giveRate() url = " + url);
        Log.e("TAG", "giveRate() params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("TAG", "giveRate() responseCode = " + responseCode);
                    Log.e("TAG", "giveRate() Response = " + json);
                    dialogClass.hideDialog();

                    if(json.has("status"))
                    {
                        if (json.getBoolean("status") && status_.equalsIgnoreCase("success"))
                        {
                            previousDue.getHistory().remove(position);
                            previousDueAdapter.notifyDataSetChanged();

                            if(previousDue.getHistory().size() == 0)
                            {
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                            }

                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.setListener(new InternetDialog.dialogPositiveClick() {
                                @Override
                                public void onOkClick() {
                                    finish();
                                }
                            });
                            /*if(status_.equalsIgnoreCase("success"))
                            {
                                playNotificationSound();
                            }*/
                            internetDialog.showDialog(json.getString("message"), "Payment Status",false,false);
                        }
                        else
                        {
                            InternetDialog internetDialog1 = new InternetDialog(activity);
                            internetDialog1.setListener(new InternetDialog.dialogPositiveClick() {
                                @Override
                                public void onOkClick() {
                                    callPreviousDueApi();
                                }
                            });
                            internetDialog1.showDialog(json.getString("message"),"Payment Status",false,false);

                        }

                        if(json.getBoolean("payment_status"))
                        {playNotificationSound(true);}
                        else
                        {playNotificationSound(false);}

                    }


                } catch (Exception e) {
                    Log.e("TAG", "giveRate()  Exception " + e.toString());
                    dialogClass.hideDialog();

                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void playNotificationSound(boolean payment_status)
    {
        try {
            Log.e("TAG","33333333");
            Uri alarmSound;
            if(payment_status)
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/payment_1");}
            else
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/paytm_failed");}
            Log.e("TAG","2222222");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            Log.e("TAG","11111111111");
            r.play();
        } catch (Exception e)
        {
            Log.e("TAG","Error = "+e.getMessage());
            Log.e("TAG","Error = "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
}
