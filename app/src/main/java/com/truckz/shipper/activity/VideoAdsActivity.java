package com.truckz.shipper.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;


import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;

import java.util.ArrayList;
import java.util.Arrays;

public class VideoAdsActivity extends AppCompatActivity {


    VideoView videoView;
    ArrayList<String> arrayList = new ArrayList<>(Arrays.asList("https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"));

    int index = 0;
    TextView rel_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_ads);


        videoView = findViewById(R.id.videoView);
        final MediaController mediacontroller = new MediaController(this);
        mediacontroller.setAnchorView(videoView);

        rel_skip = findViewById(R.id.tvSkip);
        rel_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,VideoAdsActivity.this)!=null &&
                        !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,VideoAdsActivity.this).equals("") &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_STATUS,VideoAdsActivity.this).equals("1"))
                {
                    Intent loginScreen = new Intent(VideoAdsActivity.this,MainActivity.class);
                    loginScreen.putExtra("from", "Splash_Activity");
                    startActivity(loginScreen);
                    overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                    finish();
                }
                else
                {
                    Intent loginScreen = new Intent(VideoAdsActivity.this,LoginActivity1.class);
                    startActivity(loginScreen);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                }
            }
        });

        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width = (int) metrics.widthPixels;
        params.height = (int) metrics.heightPixels;*/

        /*videoView.setLayoutParams(params);*/
        /*try {
            videoView.setMediaController(mediacontroller);
            String path = "android.resource://" + getPackageName() + "/" + R.raw.shipper;
            videoView.setVideoURI(Uri.parse(path));

            videoView.requestFocus();
        }catch (Exception e) {
            Log.e("Error", "error : "+e.getMessage());
        }*/




        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                //Toast.makeText(getApplicationContext(), "Video over", Toast.LENGTH_SHORT).show();
                /*if (index++ == arrayList.size()) {
                    index = 0;
                    mp.release();
                    Toast.makeText(getApplicationContext(), "Video over", Toast.LENGTH_SHORT).show();
                } else {
                    videoView.setVideoURI(Uri.parse(arrayList.get(index)));
                    videoView.start();
                }*/

                videoView.start();

            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                mp.setLooping(true);
                mp.start();
                mp.setVolume(0,0);
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mps, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediacontroller.setAnchorView(videoView);
                    }
                });
            }
        });

        videoView.start();

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("API123", "What " + what + " extra " + extra);
                return false;
            }
        });
    }
}
