package com.truckz.shipper.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.been.Package_History_Been;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.R;
import com.truckz.shipper.adapter.Package_Booking_History_Adapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomPackageHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    public static CustomPackageHistoryActivity activity;
    private LinearLayout ll_rootView, ll_back;
    private ImageView iv_back, iv_call;
    private TextView tv_title;

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    private TextView tv_NoDataFound;
    public SwipeRefreshLayout swipeRefreshLayout;
    public static boolean isReffreshing = false;
    public static List<com.truckz.shipper.been.Package_History_Been> Package_History_Been = new ArrayList<Package_History_Been>();

    private DialogClass dialogClass;
    private AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_package_history);

        activity = CustomPackageHistoryActivity.this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        isReffreshing = false;

        Package_History_Been.clear();

        init();
    }

    private void init()
    {
        ll_rootView = (LinearLayout) findViewById(R.id.ll_rootView);
        ll_back = (LinearLayout) findViewById(R.id.back_layout);
        iv_back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        tv_title = (TextView) findViewById(R.id.title_textview);

        recyclerView = (ExpandableRecyclerView) findViewById(R.id.rv_package);
        tv_NoDataFound = (TextView) findViewById(R.id.tv_NoDataFound);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        tv_title.setText(getResources().getString(R.string.custom_package_history));

        ll_back.setOnClickListener(activity);
        iv_back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Package_Booking_History_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Global.isNetworkconn(activity))
                {
                    isReffreshing = true;
                    MyBookingActivity.pastBooking_beens.clear();
                    mAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(true);
                    call_BookingHistoryApi();
                }
                else
                {
                    isReffreshing = false;
                    swipeRefreshLayout.setRefreshing(false);
                    InternetDialog errorDialogClass = new InternetDialog(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        if (Global.isNetworkconn(activity))
        {
            call_BookingHistoryApi();
        }
        else
        {
            InternetDialog errorDialogClass = new InternetDialog(activity);
            errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
        }

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

        }
    }

    public void call_BookingHistoryApi()
    {
        if (isReffreshing == true)
        {
            Log.e("call", "isReffreshing = true");
        }
        else
        {
            dialogClass.showDialog();
        }

        String url = WebServiceAPI.API_BOOK_PACKAGE_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Package_History_Been.clear();

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("package"))
                                {
                                    JSONArray historyPackage = json.getJSONArray("package");

                                    if (historyPackage!=null && historyPackage.length()>0)
                                    {
                                        Log.e("historyPackage", " Length = " + historyPackage.length());
                                        for (int i=0; i<historyPackage.length(); i++)
                                        {
                                            JSONObject jsonObject = historyPackage.getJSONObject(i);

                                            if (jsonObject!=null)
                                            {
                                                String Id="", PassengerId="", PackageId="", PickupLocation="", PickupDate="", PickupTime="", TransactionId="", Status="", Date="", PaymentType="",
                                                        PaymentStatus="", details="";
                                                String detailsId="", detailsModelId="", detailsType="", detailsName="", detailsDescription="", detailsTime="", detailsKM="", detailsAmount="",
                                                        detailsNotes="", detailsDate="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("PackageId"))
                                                {
                                                    PackageId = jsonObject.getString("PackageId");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("PickupDate"))
                                                {
                                                    PickupDate = jsonObject.getString("PickupDate");
                                                }

                                                if (jsonObject.has("PickupTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupTime");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Date"))
                                                {
                                                    Date = jsonObject.getString("Date");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("details"))
                                                {
                                                    JSONObject objectDetial = jsonObject.getJSONObject("details");

                                                    if (objectDetial!=null)
                                                    {
                                                        if (objectDetial.has("Id"))
                                                        {
                                                            detailsId = objectDetial.getString("Id");
                                                        }

                                                        if (objectDetial.has("ModelId"))
                                                        {
                                                            detailsModelId = objectDetial.getString("ModelId");
                                                        }

                                                        if (objectDetial.has("Type"))
                                                        {
                                                            detailsType = objectDetial.getString("Type");
                                                        }

                                                        if (objectDetial.has("Name"))
                                                        {
                                                            detailsName = objectDetial.getString("Name");
                                                        }

                                                        if (objectDetial.has("Description"))
                                                        {
                                                            detailsDescription = objectDetial.getString("Description");
                                                        }

                                                        if (objectDetial.has("Time"))
                                                        {
                                                            detailsTime = objectDetial.getString("Time");
                                                        }

                                                        if (objectDetial.has("KM"))
                                                        {
                                                            detailsKM = objectDetial.getString("KM");
                                                        }

                                                        if (objectDetial.has("Amount"))
                                                        {
                                                            detailsAmount = objectDetial.getString("Amount");
                                                        }

                                                        if (objectDetial.has("Notes"))
                                                        {
                                                            detailsNotes = objectDetial.getString("Notes");
                                                        }

                                                        if (objectDetial.has("Date"))
                                                        {
                                                            detailsDate = objectDetial.getString("Date");
                                                        }
                                                    }
                                                }

                                                Package_History_Been.add(new Package_History_Been(Id,
                                                        PassengerId,
                                                        PackageId,
                                                        PickupLocation,
                                                        PickupDate,
                                                        PickupTime,
                                                        TransactionId,
                                                        Status,
                                                        Date,
                                                        PaymentType,
                                                        PaymentStatus,
                                                        details,
                                                        detailsId,
                                                        detailsModelId,
                                                        detailsType,
                                                        detailsName,
                                                        detailsDescription,
                                                        detailsTime,
                                                        detailsKM,
                                                        detailsAmount,
                                                        detailsNotes,
                                                        detailsDate));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                        isReffreshing = false;
                                    }
                                    else
                                    {
                                        Log.e("call","history null or lenth 0");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                        isReffreshing = false;
                                    }

                                }
                                else
                                {
                                    Log.e("call","history not found");
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    mAdapter.notifyDataSetChanged();
                                    isReffreshing = false;
                                }
                            }
                            else
                            {
                                Log.e("call","status false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                mAdapter.notifyDataSetChanged();
                                isReffreshing = false;
                            }
                        }
                        else
                        {
                            Log.e("call","status not found");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            mAdapter.notifyDataSetChanged();
                            isReffreshing = false;
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                        isReffreshing = false;
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    mAdapter.notifyDataSetChanged();
                    isReffreshing = false;
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);
    }
}
