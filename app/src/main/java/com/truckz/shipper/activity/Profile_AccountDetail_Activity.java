package com.truckz.shipper.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Profile_AccountDetail_Activity extends AppCompatActivity implements View.OnClickListener{

    private String TAG = "Profile_AccountDetail_Activity";
    private Profile_AccountDetail_Activity activity;

    private ImageView iv_back, iv_call;
    private LinearLayout ll_RootLayout, ll_call;
    private CardView cv_save;

    private CustomEditText et_acountHolder_name,  et_bank_name, et_bsb, et_bankAccount;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private String userId;
    private MySnackBar mySnackBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_accountdetail);

        activity = Profile_AccountDetail_Activity.this;

        aQuery = new AQuery(activity);
        mySnackBar = new MySnackBar(activity);

        initUI();
    }

    private void initUI()
    {
        Log.e(TAG,"initUI()");

        iv_back = findViewById(R.id.iv_back);
        iv_call = findViewById(R.id.iv_call);

        ll_call = findViewById(R.id.ll_call);
        ll_RootLayout = findViewById(R.id.main_layout);
        cv_save = findViewById(R.id.cv_save);

        et_acountHolder_name = findViewById(R.id.et_acountHolder_name);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_bsb = findViewById(R.id.et_bsb);
        et_bankAccount = findViewById(R.id.et_bankAccount);


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity).equalsIgnoreCase(""))
        {
            et_acountHolder_name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity).equalsIgnoreCase(""))
        {
            et_bank_name.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity).equalsIgnoreCase(""))
        {
            et_bsb.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BSB,activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity)!= null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_bankAccount.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,activity));
        }

        iv_back.setOnClickListener(this);
        cv_save.setOnClickListener(this);
        iv_call.setOnClickListener(this);
        ll_call.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.cv_save:
                CheckData();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.ll_call:
                MainActivity.call_sos(activity);
                break;
        }
    }

    private void CheckData()
    {
        Log.e(TAG,"CheckData() ");

        if (TextUtils.isEmpty(et_acountHolder_name.getText().toString()))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_account_holder_name));
        }
        else  if (TextUtils.isEmpty(et_bank_name.getText().toString()))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_bank_name));
        }
        else  if (TextUtils.isEmpty(et_bsb.getText().toString()))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_ifsc_code));
        }
        else  if (TextUtils.isEmpty(et_bankAccount.getText().toString()))
        {
            mySnackBar.showSnackBar(ll_RootLayout,getResources().getString(R.string.please_enter_bank_account_number));
        }
        else
        {
            userId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (Global.isNetworkconn(activity))
                {
                    UpdateAccountDetail(userId);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection),getResources().getString(R.string.no_internet_connection));
                }
            }
        }
    }

    private void UpdateAccountDetail(String userId)
    {
        Log.e(TAG,"UpdateAccountDetail()");

        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.API_UPDATE_BANK_ACCOUNT_DETAIL;

        params.put(WebServiceAPI.PARAM_PASSENGER_ID, userId);
        params.put(WebServiceAPI.PARAM_ACCOUNT_HOLDER_NAME, et_acountHolder_name.getText().toString());
        params.put(WebServiceAPI.PARAM_BANK_NAME, et_bank_name.getText().toString());
        params.put(WebServiceAPI.PARAM_BSB, et_bsb.getText().toString());
        params.put(WebServiceAPI.PARAM_BANK_ACCOUNT_NUMBER, et_bankAccount.getText().toString());

        Log.e(TAG,"UpdateAccountDetail() UpdateCarDetail = " + url);
        Log.e(TAG,"UpdateAccountDetail() UpdateCarDetail = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"UpdateAccountDetail() responseCode = " + responseCode);
                    Log.e(TAG,"UpdateAccountDetail() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject profile = json.getJSONObject("profile");

                                    if (profile!=null)
                                    {
                                        String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
                                        String Lat="",Lng="",Status="",CreatedDate="",Id="",Address="",QRCode="",Verify="";
                                        String CompanyName="",ABN="",LicenceImage="",referalCode="",BankName="",BSB="",BankAccountNo="",Description="";

                                        if (profile.has("Id"))
                                        {
                                            Id = profile.getString("Id");
                                        }

                                        if (profile.has("Fullname"))
                                        {
                                            Fullname = profile.getString("Fullname");
                                        }

                                        if (profile.has("Email"))
                                        {
                                            Email = profile.getString("Email");
                                        }

                                        if (profile.has("Password"))
                                        {
                                            Password = profile.getString("Password");
                                        }

                                        if (profile.has("MobileNo"))
                                        {
                                            MobileNo = profile.getString("MobileNo");
                                        }

                                        if (profile.has("Image"))
                                        {
                                            Image = profile.getString("Image");
                                        }

                                        if (profile.has("QRCode"))
                                        {
                                            QRCode = profile.getString("QRCode");
                                        }

                                        if (profile.has("CompanyName"))
                                        {
                                            CompanyName = profile.getString("CompanyName");
                                        }

                                        if (profile.has("ABN"))
                                        {
                                            ABN = profile.getString("ABN");
                                        }

                                        if (profile.has("BankName"))
                                        {
                                            BankName = profile.getString("BankName");
                                        }

                                        if (profile.has("BSB"))
                                        {
                                            BSB = profile.getString("BSB");
                                        }

                                        if (profile.has("BankAccountNo"))
                                        {
                                            BankAccountNo = profile.getString("BankAccountNo");
                                        }

                                        if (profile.has("Description"))
                                        {
                                            Description = profile.getString("Description");
                                        }

                                        if (profile.has("LicenceImage"))
                                        {
                                            LicenceImage = profile.getString("LicenceImage");
                                        }

                                        if (profile.has("Gender"))
                                        {
                                            Gender = profile.getString("Gender");
                                        }

                                        if (profile.has("ReferralCode"))
                                        {
                                            referalCode = profile.getString("ReferralCode");
                                        }

                                        if (profile.has("Address"))
                                        {
                                            Address = profile.getString("Address");
                                        }

                                        if (profile.has("DeviceType"))
                                        {
                                            DeviceType = profile.getString("DeviceType");
                                        }

                                        if (profile.has("Token"))
                                        {
                                            Token = profile.getString("Token");
                                        }

                                        if (profile.has("Lat"))
                                        {
                                            Lat = profile.getString("Lat");
                                        }

                                        if (profile.has("Lng"))
                                        {
                                            Lng = profile.getString("Lng");
                                        }

                                        if (profile.has("Status"))
                                        {
                                            Status = profile.getString("Status");
                                        }

                                        if (profile.has("CreatedDate"))
                                        {
                                            CreatedDate = profile.getString("CreatedDate");
                                        }

                                        if (profile.has("Verify"))
                                        {
                                            Verify = profile.getString("Verify");
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ID,Id,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME,Fullname,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_EMAIL,Email,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PASSWORD,Password,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER,MobileNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_GENDER,Gender,activity);
//                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_IMAGE,Image,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DEVICE_TYPE,DeviceType,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TOKEN,Token,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LATITUDE,Lat,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LONGITUDE,Lng,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_STATUS,Status,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CREATED_DATE,CreatedDate,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ADDRESS,Address,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_QR_CODE,QRCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_VERIFY_USER,Verify,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_COMPANY_NAME,CompanyName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_ABN,ABN,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_LICENCE_IMAGE,LicenceImage,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE,referalCode,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_NAME,BankName,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BSB,BSB,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER,BankAccountNo,activity);
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DESCRIPTION,Description,activity);

                                        dialogClass.hideDialog();
                                        showSuccessMessage(getResources().getString(R.string.success_message),getResources().getString(R.string.bank_detail_updated_successfully));
                                    }
                                }
                                else
                                {
                                    Log.e(TAG,"UpdateAccountDetail() profile not available");
                                    dialogClass.hideDialog();
                                }
                            }
                        }
                    }
                    dialogClass.hideDialog();
                }
                catch (Exception e)
                {
                    Log.e(TAG,"UpdateAccountDetail() Exception : " + e.toString());
                    dialogClass.hideDialog();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessMessage(String title,String message)
    {
        final Dialog dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        LinearLayout dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        ImageView dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        dialog_title.setText(title);

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                },800);

            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                },800);
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                },800);
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume()
    {
        Log.e(TAG,"onResume()");

        super.onResume();
        if (activity!=null)
        {
            TruckzShipperApplication.setCurrentActivity(activity);
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed() ");

        super.onBackPressed();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
        {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }
}