package com.truckz.shipper.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Wallet_Balance_TopUp_Activity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "Wallet_Balance_TopUp_Activity";
    public static Wallet_Balance_TopUp_Activity activity;

    private LinearLayout ll_Back, ll_right;
    private ImageView iv_Back, iv_call;
    private TextView tv_Title;
    private LinearLayout ll_SelectCard;
    public static String cardType="",cardNumber="",cardId="";
    private AQuery aQuery;
    private DialogClass dialogClass;
    private EditText et_Amount;
    private TextView tv_Submit,tv_Cards;
    public static int resumeFlag = 1;
    private MySnackBar mySnackBar;
    private LinearLayout ll_rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_addfunds);

        activity = Wallet_Balance_TopUp_Activity.this;
        cardType="";
        cardNumber="";
        cardId="";
        resumeFlag = 1;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity,0);
        initUI();

        ll_rootView = (LinearLayout)findViewById(R.id.ll_rootView);
        mySnackBar = new MySnackBar(activity);
    }

    private void initUI()
    {
        Log.e(TAG,"initUI()");

        ll_Back = (LinearLayout) findViewById(R.id.back_layout);
        ll_right = (LinearLayout) findViewById(R.id.ll_right);
        ll_right.setVisibility(View.INVISIBLE);
        iv_Back = (ImageView) findViewById(R.id.back_imageview);
        iv_call = (ImageView) findViewById(R.id.iv_call);
        ll_SelectCard = (LinearLayout) findViewById(R.id.select_card_layout);
        et_Amount = (EditText) findViewById(R.id.amount);

        tv_Title = (TextView) findViewById(R.id.title_textview);
        tv_Submit = (TextView) findViewById(R.id.tv_add_funds);
        tv_Cards = (TextView) findViewById(R.id.tv_cards);

        tv_Title.setText(activity.getResources().getString(R.string.activity_top_up));

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        ll_SelectCard.setOnClickListener(activity);
        tv_Submit.setOnClickListener(activity);

        et_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length()==1 && charSequence.toString().trim().equalsIgnoreCase("."))
                {
                    et_Amount.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.select_card_layout:
                resumeFlag = 0;
                gotoCardScreen();
                break;

            case R.id.tv_add_funds:
                checkValidation();
                break;
        }
    }

    public void checkValidation()
    {
        if (tv_Cards.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_card)))
        {
            mySnackBar.showSnackBar(ll_rootView,activity.getResources().getString(R.string.please_select_card));
        }
        else if (et_Amount.getText().toString().trim().equalsIgnoreCase(""))
        {
            mySnackBar.showSnackBar(ll_rootView,activity.getResources().getString(R.string.please_enter_amount));
        }
        else
        {
            if (Global.isNetworkconn(activity))
            {
                addMoney();
            }
            else
            {
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.please_check_internet_connection),activity.getResources().getString(R.string.no_internet_connection));
            }
        }
    }

    public void gotoCardScreen()
    {
        if (checkCardList()==false)
        {
            Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
            intent.putExtra("from","Wallet_Balance_TopUp_Activity");
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(activity,Wallet_Add_Cards_Activity.class);
            intent.putExtra("from","Wallet_Balance_TopUp_Activity");
            startActivity(intent);
        }
    }

    public boolean checkCardList()
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards!=null && cards.length()>0)
                        {
                            return true;
                        }
                        else
                        {
                            Log.e("call","no cards available");
                            return false;
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                        return false;
                    }
                }
                else
                {
                    Log.e("call","json null");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
            return false;
        }
    }

    public void addMoney()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_ADD_MONEY;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity));
        params.put(WebServiceAPI.PARAM_AMOUNT, et_Amount.getText().toString());
        params.put(WebServiceAPI.PARAM_CARD_ID, cardId);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();
                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String message = "Add money successfully";

                                if (json.has("walletBalance"))
                                {
                                    String walletBallence = json.getString("walletBalance");

                                    if (walletBallence!=null && !walletBallence.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,walletBallence,activity);
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",activity);
                                    }
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE,"0",activity);
                                }

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }

                                dialogClass.hideDialog();
                                showSuccessDialog(message);
                            }
                            else
                            {
                                String message = getResources().getString(R.string.transaction_failed);
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message,getResources().getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        Log.e("call","json null");
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void showSuccessDialog(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);
        tv_title.setText(getResources().getString(R.string.success_message));

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Wallet_Balance_Activity.callGetMonny = true;
                finish();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Wallet_Balance_Activity.callGetMonny = true;
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        if (!cardType.equalsIgnoreCase("") && !cardId.equalsIgnoreCase(""))
        {
            tv_Cards.setText(cardType+"  "+cardNumber);
        }
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Common.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                if (Wallet__Activity.activity!=null)
                {
                    Wallet__Activity.activity.finish();
                }

                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }

                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                intent.putExtra("from","Wallet__Activity");
                startActivity(intent);
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
        {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }
}