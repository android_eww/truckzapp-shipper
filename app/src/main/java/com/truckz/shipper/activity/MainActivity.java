package com.truckz.shipper.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.maps.android.SphericalUtil;
import com.truckz.shipper.adapter.CarAdapter;
import com.truckz.shipper.adapter.MenuListAdapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.BookingDriverLocation_Been;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.been.LatLong_Been;
import com.truckz.shipper.been.MenuList_Been;
import com.truckz.shipper.been.TransportService_Been;
import com.truckz.shipper.interfaces.CallbackPaytm;
import com.truckz.shipper.kalmani.KalmanLocationManager;
import com.truckz.shipper.other.DoubleArrayEvaluator;
import com.truckz.shipper.roundedimage.RoundedTransformationBuilder;
import com.truckz.shipper.singleton.PaytmSingleton;
import com.truckz.shipper.view.CTextViewMedium;
import com.truckz.shipper.view.CustomEditText;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.been.BookingRequest_Been;
import com.truckz.shipper.been.CarType_Been;
import com.truckz.shipper.been.GetEstimateFare_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.Constants;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.SocketSingleObject;
import com.truckz.shipper.comman.TaxiUtil;
import com.truckz.shipper.comman.Utility;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.map.DirectionsJSONParser;
import com.truckz.shipper.notification.FCMUtil;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.GPSTracker;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.other.MyAlertDialog;
import com.truckz.shipper.other.RequestPendingDialogClass;
import com.truckz.shipper.view.CustomSpinner;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static android.view.View.X;
import static com.androidquery.util.AQUtility.getContext;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        View.OnClickListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraChangeListener /*,GoogleMap.OnMarkerDragListener*/ {

    private String TAG = "MainActivity";
    public static MainActivity activity;
    private Toolbar toolbar;
    private MenuListAdapter drawerListAdapter;
    private ImageView ivMarkerCurrent;
    private ListView lv_drawer;
    private DrawerLayout drawer;
    private List<MenuList_Been> menuList_beens = new ArrayList<MenuList_Been>();
    private LinearLayout ll_DrawerLayout, ll_Menu, ll_Favorite, ll_call;
    private ImageView iv_Menu, iv_MyLocation, iv_Favorite, iv_call;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private LatLng coordinate;
    private GoogleMap googleMap;
    private Polyline polyline;
    private Marker originMarker, destinationMarker, markerDropoff, markerPickup;
    private Marker[] marker;
    List<Marker> listMarker = new ArrayList<>();
    public static boolean myLocation = true;
    private GPSTracker gpsTracker;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private CircleImageView iv_ProfileImage;
    private Transformation mTransformation;
    private CTextViewMedium tv_UserName, tvUserEmail, tv_UserPhone;
    private Runnable runnable = null;
    private Runnable runnableProfile = null;
    private Runnable runnableDriver = null;
    private List<LatLong_Been> latLong_beens = new ArrayList<LatLong_Been>();
    public static JSONArray carClass = null;
    public static List<CarType_Been> carType_beens = new ArrayList<CarType_Been>();

    private TextView tv_BookNow, tv_BookLater;
    private String passangerId = "";
    private Handler handler;
    private Handler handlerProfile;
    private Handler handlerDriver;

    long delay = 600; // 1 seconds after user stops typing
    long last_text_edit = 0;
    Handler handlerType = new Handler();
    private int pickDropoffType = 1; //1 for the pick up 2 for the dropoff
    private String search_text[];

    private AutoCompleteTextView auto_Pickup, auto_Dropoff;
    private String jsonurl = "";
    private ArrayList<String> names, Place_id_type;
    private ParseData parse;
    public static JSONObject json;
    private JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    private String place_id = "";
    public static int addFlag = 1;
    private DialogClass dialogClass;
    private RequestPendingDialogClass requestPendingDialogClass;
    private AQuery aQuery;
    public static List<BookingRequest_Been> bookingRequest_beens = new ArrayList<BookingRequest_Been>();
    public static List<BookingDriverLocation_Been> bookingDriverLocation_beens = new ArrayList<BookingDriverLocation_Been>();
    private LinearLayout ll_BookingLayout;
    private LinearLayout ll_AllCarLayout;
    public static int tripFlag = 0;
    public static int selectedCarPosition = -1;
    public static JSONObject requestConfirmJsonObject = null;
    public static String driverId = "";
    public static int zoomFlag = 0;
    public static LatLng pickUp = null;
    public static LatLng driverLatLng = null;
    public static String BookingId = "";
    private LinearLayout ll_BottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView peakView;
    private CTextViewMedium tv_CancelRequest, tv_DriverInfo;
    private LinearLayout ll_AfterRequestAccept;
    private TextView tv_CarModel, tv_VehicleRegistrationNo, tv_CarCompany, tv_Pickup, tv_Dropoff, tv_DriverName;
    private ImageView iv_CarImage, iv_Close, iv_DriverImage, iv_CallDriver;
    public static String driverPhone = "";
    private LinearLayout ll_ClearPickup, ll_ClearDropoff, address_Layout, llPickup, llDropoff;
    private LinearLayout ll_ProfileDetail;
    private TextView tv_rating;
    private LinearLayout ll_RootLayour, ll_rating;
    private MySnackBar mySnackBar;
    public static int advanceBooking = 0;
    public static List<GetEstimateFare_Been> getEstimateFare_beens = new ArrayList<GetEstimateFare_Been>();
    public static String from = "";
    public static int favorite = 0;
    public static String favorite_address = "";
    private boolean pickup_Touch = false;
    private boolean dropoff_Touch = false;
    private String promocodeStr = "";
    private String notes = "";
    public static int timeFlag = 0;
    public static double getEstimated_lat = 0, getEstimated_long = 0;
    public static String getEstimated_Pickup = "";
    public static boolean callResumeScreen = false;
    public static ArrayList<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

    public static String cardId = "";
    public static String cardNumber = "";
    public static String cardType = "";
    public static boolean isCardSelected = false;
    public static String selectedPaymentType = "";
    public static int spinnerSelectedPosition = 0;
    private CustomSpinner Spinner_paymentMethod;
    private CustomerAdapter customerAdapter;
    public EditText etFullName, etSenderAddress, etPhoneNumber, etParcelWeight, etEmail;
    public LinearLayout ll_EstimateFare, ll_Discount, ll_EstimateFare_pay;
    public TextView tv_EstimateFare, tv_Discount, tv_EstimateFare_pay;
    public CheckBox cbReceiverInfo;
    public boolean isPromocodeCheck = false, isValidPromoCode = false;

    //kalmani........
    private static final long GPS_TIME = 5000;
    private static final long NET_TIME = 5000;
    private static final long FILTER_TIME = 5000;
    private KalmanLocationManager mKalmanLocationManager;
    public static double kalmanLat = 0;
    public static double kalmanLong = 0;

    private Dialog commonDialog;
    private InternetDialog commonInternetDialog;
    public static boolean isAlreadySendRequest = false;
    public static int handlerCount = 0;
    private Handler handlerSendRequest;

    public static int PasscodeBackPress = 0;
    public static String token = "";
    private ArrayList<LatLng> points1 = null;

    //    public static MediaPlayer ringTone = null;
    public static String driverName = "";
    public static float ratting = 0;
    private TextView tv_Rate;
    private LinearLayout current_marker_layout;
    public static int addCardFlag = 0;
    private String fromSplash = "";
    public static String redirectBooking, redirectBookingAdrs;
    public static int isPickupDropoff = 1; //1 for pickup, 2 for dropoff.
    public static int acceptBooking = 0;  //0 for current, 1 for acceptnow.
    public static int isDone = 0;
    public static boolean isAutocomplete = true;
    private static SharedPreferences permissionStatus;
    private static String helpLineNumber = "";
    private List<LatLng> ployList;
    public static int specialRequest = 0;
    String driverList = "";
    private LinearLayout ll_Done, llSelectService;
    private CardView cvTaxi, cvTransport, card_pickupLocation, card_dropoffLoc;
    private String RequestFor = "", TAXI = "taxi", DELIVERY = "delivery";
    private RecyclerView recyclerViewCar;
    private CarAdapter carAdapter;
    private GridLayoutManager carLayoutManager;

    //and new
    public String receverFullName;
    public String receverPhoneNumber;

    //Google Address Picker
    public int AUTOCOMPLETE_REQUEST_CODE = 1;
    public PlacesClient placesClient;
    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,
            Place.Field.LAT_LNG, Place.Field.ADDRESS);
    public TextView tvPickup, tvDropoff;
    public int addressSelection = 0;


    public ImageView img_faverite;
    boolean isCardPickUpSelected = true;

    TextView txt_driver_number;
    RatingBar ratingbar_driver;
    public static float defaltZoomLavel = 15.5f;


    private ScaleGestureDetector gestureDetector;
    private long lastZoomTime = 0;
    private float lastSpan = -1;

    String bookingType = "",paymentType="";//saurav
    boolean isDirectPaytmPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = MainActivity.this;
        Places.initialize(this, getString(R.string.google_api_key));
        placesClient = Places.createClient(this);

        addressSelection = 0;
        driverName = "";
        ratting = 0;
        addCardFlag = 0;
        polyline = null;
        specialRequest = 0;
        token = "";
        isDone = 0;
        handlerCount = 0;
        isAutocomplete = true;
//        ringTone = null;
        PasscodeBackPress = 0;
        isAlreadySendRequest = false;
        isPromocodeCheck = false;
        isValidPromoCode = false;
//        ringTone = MediaPlayer.create(activity, R.raw.tone);
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 0);
        callResumeScreen = true;
        advanceBooking = 0;
        cardListBeens.clear();
        favorite_address = "";
        kalmanLat = 0;
        kalmanLong = 0;
        isPickupDropoff = 1;
        acceptBooking = 0;
        handlerSendRequest = new Handler();
        handlerProfile = new Handler();
        commonDialog = new Dialog(activity);
        commonInternetDialog = new InternetDialog(activity);
        mKalmanLocationManager = new KalmanLocationManager(this);
        redirectBooking = "";
        redirectBookingAdrs = "";
        helpLineNumber = "";
        RequestFor = "";
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (this.getIntent() != null) {
            if (this.getIntent().getStringExtra("from") != null) {
                fromSplash = this.getIntent().getStringExtra("from");
            } else {
                fromSplash = "";
            }
        } else {
            fromSplash = "";
        }

        Log.e(TAG, "from fromSplash = " + fromSplash);
        Handler handlerToken = new Handler();
        handlerToken.postDelayed(new Runnable() {
            @Override
            public void run() {

                String strToken = FCMUtil.getFcmToken(activity);
                token = strToken;
                Log.e(TAG, "token@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ = " + FCMUtil.getFcmToken(activity));
            }
        }, 500);

        points1 = new ArrayList<LatLng>();

        /*points1.add(new LatLng(23.072287, 72.516225));
        points1.add(new LatLng(23.072259, 72.516274));
        points1.add(new LatLng(23.072238, 72.516312));
        points1.add(new LatLng(23.072219, 72.516371));
        points1.add(new LatLng(23.072190, 72.516417));
        points1.add(new LatLng(23.072175, 72.516465));
        points1.add(new LatLng(23.072154, 72.516511));
        points1.add(new LatLng(23.072131, 72.516558));
        points1.add(new LatLng(23.072109, 72.516602));
        points1.add(new LatLng(23.072084, 72.516647));
        points1.add(new LatLng(23.072060, 72.516684));
        points1.add(new LatLng(23.072037, 72.516733));
        points1.add(new LatLng(23.072009, 72.516778));
        points1.add(new LatLng(23.071983, 72.516824));
        points1.add(new LatLng(23.071959, 72.516870));
        points1.add(new LatLng(23.071934, 72.516913));
        points1.add(new LatLng(23.071902, 72.516878));
        points1.add(new LatLng(23.071859, 72.516865));
        points1.add(new LatLng(23.071808, 72.516878));
        points1.add(new LatLng(23.071776, 72.516909));
        points1.add(new LatLng(23.071755, 72.516952));
        points1.add(new LatLng(23.071752, 72.517004));
        points1.add(new LatLng(23.071755, 72.517055));
        points1.add(new LatLng(23.071751, 72.517054));*/

        ll_Done = (LinearLayout) findViewById(R.id.ll_Done);
        llSelectService = (LinearLayout) findViewById(R.id.llSelectService);
        img_faverite = findViewById(R.id.img_faverite);
        img_faverite.setOnClickListener(this);
        if (Global.isNetworkconn(activity)) {
            resumeScreen();
        } else {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
            errorDialogClass.showDialog(activity.getResources().getString(R.string.please_check_internet_connection),
                    activity.getResources().getString(R.string.no_internet_connection));
        }

        Locale defaultLocale = Locale.getDefault();
        displayCurrencyInfoForLocale(defaultLocale);

        getPaymentIfFromNotification();
    }

    private void getPaymentIfFromNotification()
    {
        Log.e("TAG","11111111111111111111111111111111111111111111111");
        String paymentsData = SessionSave.getUserSession("paymentddata", this);
        if(paymentsData != null && !paymentsData.equalsIgnoreCase(""))
        {

            String object="",BookingId="",cusId="",GrandTotal="",BookingType="",refId="",PaymentType="";
            try {
                JSONObject jsonObject = new JSONObject(paymentsData);
                if(jsonObject.has("PaymentType"))
                {PaymentType = jsonObject.getString("PaymentType");}

                if(jsonObject.has("PassengerId"))
                {cusId = jsonObject.getString("PassengerId");}

                if(jsonObject.has("ReferenceId"))
                {refId=jsonObject.getString("ReferenceId");}

                if(jsonObject.has("BookingId"))
                {BookingId=jsonObject.getString("BookingId");}

                if(jsonObject.has("BookingType"))
                {BookingType = jsonObject.getString("BookingType");}

                if(jsonObject.has("GrandTotal"))
                {GrandTotal = jsonObject.getString("GrandTotal");}
            } catch (Exception e) {

            }
            Log.e("TAG","222222222222222222222222222222222222");
            final String amount = GrandTotal;
            Log.e("TAG","amount = "+amount);
            SessionSave.saveUserSession("paymentddata", "", this);

            PaytmSingleton.setCallBack(activity, new CallbackPaytm() {
                @Override
                public void onSuccess(String status,String BookingType,String bookingId,String txId,String refId) {
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId, amount);
                }

                @Override
                public void onFailed(String status,String BookingType,String bookingId,String txId,String refId) {
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                }

                @Override
                public void onNetworkError() {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog("Network Problem","Error");
                }

                @Override
                public void onClientAuthenticationFailed(String error,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog(error,"Error");*/
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog(inErrorMessage,"Error");*/
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                }

                @Override
                public void onBackPressedCancelTransaction(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog("Cancel Transaction","Error");*/
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                }

                @Override
                public void onTransactionCancel(String inErrorMessage, Bundle inResponse,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog("Cancel Transaction","Error");*/
                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                }
            });



            if(amount == null || amount.equalsIgnoreCase(""))
            {return;}
            playNotificationSound1();

            isDirectPaytmPage = true;
            Intent intent = new Intent(activity,PaymentPaytmActivity.class);
            intent.putExtra("BookingId",BookingId);
            intent.putExtra("cusId",cusId);
            intent.putExtra("GrandTotal",amount);
            intent.putExtra("BookingType",BookingType);
            intent.putExtra("refId",refId);
            startActivity(intent);

        }
    }

    public void resumeScreen() {
        Log.e(TAG, "resumeScreen()");

        dialogClass.showDialog();
        String url = WebServiceAPI.API_CURRENT_BOOCING + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        Log.e(TAG, "resumeScreen() url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    cardListBeens.clear();
                    int responseCode = status.getCode();
                    Log.e(TAG, "resumeScreen() responseCode = " + responseCode);
                    Log.e(TAG, "resumeScreen() Response  = " + json);
                    if (json != null) {
                        if (json.has("rating")) {
                            if (json.getString("rating") != null && !json.getString("rating").equalsIgnoreCase("") && !json.getString("rating").equalsIgnoreCase("NULL") && !json.getString("rating").equalsIgnoreCase("null")) {
                                ratting = Float.parseFloat(json.getString("rating"));
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, ratting + "", activity);
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                            }
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                        }

                        Log.e(TAG, "resumeScreen() rating:- " + ratting);

                        if (json.has("cards")) {
                            JSONArray cards = json.getJSONArray("cards");

                            if (cards != null && cards.length() > 0) {
                                for (int i = 0; i < cards.length(); i++) {
                                    JSONObject cardsData = cards.getJSONObject(i);

                                    if (cardsData != null) {
                                        String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "", Expiry = "";

                                        if (cardsData.has("Id")) {
                                            Id = cardsData.getString("Id");
                                        }

                                        if (cardsData.has("CardNum")) {
                                            CardNum = cardsData.getString("CardNum");
                                        }

                                        if (cardsData.has("CardNum2")) {
                                            CardNum_ = cardsData.getString("CardNum2");
                                        }

                                        if (cardsData.has("Type")) {
                                            Type = cardsData.getString("Type");
                                        }

                                        if (cardsData.has("Alias")) {
                                            Alias = cardsData.getString("Alias");
                                        }

                                        if (cardsData.has("Expiry")) {
                                            Expiry = cardsData.getString("Expiry");
                                        }

                                        //cardListBeens.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias, Expiry));
                                    }
                                }
                            }
                        }

                        cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
                        cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));

                        if (cardListBeens.size() > 1) {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, json.toString(), activity);
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, "", activity);
                        }

                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("BookingType")) {
                                    if (json.getString("BookingType") != null && json.getString("BookingType").equalsIgnoreCase("BookLater")) {
                                        advanceBooking = 1;
                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("accepted")) {
                                                if (json.has("BookingInfo")) {
                                                    JSONArray BookingInfo = json.getJSONArray("BookingInfo");

                                                    if (BookingInfo != null && BookingInfo.length() > 0) {
                                                        JSONObject BookingInfoData = BookingInfo.getJSONObject(0);

                                                        if (BookingInfoData != null) {
                                                            if (BookingInfoData.has("OnTheWay")) {
                                                                if (BookingInfoData.getString("OnTheWay") != null && BookingInfoData.getString("OnTheWay").equalsIgnoreCase("1")) {
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                                                    callSelectService(2);
                                                                    //for on the way
                                                                    if (json.has("CarInfo")) {
                                                                        JSONArray jsonArray = json.getJSONArray("CarInfo");

                                                                        if (jsonArray != null && jsonArray.length() > 0) {
                                                                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                                            if (jsonObject != null) {
                                                                                if (jsonObject.has("VehicleModel")) {
                                                                                    String model = jsonObject.getString("VehicleModel");

                                                                                    int selectedCar = -1;

                                                                                    if (model.equalsIgnoreCase("1")) {
                                                                                        selectedCar = 0;
                                                                                    } else if (model.equalsIgnoreCase("2")) {
                                                                                        selectedCar = 1;
                                                                                    } else if (model.equalsIgnoreCase("3")) {
                                                                                        selectedCar = 2;
                                                                                    } else if (model.equalsIgnoreCase("4")) {
                                                                                        selectedCar = 3;
                                                                                    } else if (model.equalsIgnoreCase("5")) {
                                                                                        selectedCar = 4;
                                                                                    } else if (model.equalsIgnoreCase("6")) {
                                                                                        selectedCar = 5;
                                                                                    } else if (model.equalsIgnoreCase("7")) {
                                                                                        selectedCar = 6;
                                                                                    }

                                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                                                }
                                                                            }

                                                                        }
                                                                    }

                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);
                                                                    dialogClass.hideDialog();
                                                                    initOnCreate();
                                                                } else {
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                                    Log.e(TAG, "resumeScreen() status false");

                                                                    dialogClass.hideDialog();
                                                                    initOnCreate();
                                                                }
                                                            } else {
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                                Log.e(TAG, "resumeScreen() status false");

                                                                dialogClass.hideDialog();
                                                                initOnCreate();
                                                            }
                                                        } else {
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                            Log.e(TAG, "resumeScreen() BookingInfoData:- null ");

                                                            dialogClass.hideDialog();
                                                            initOnCreate();
                                                        }
                                                    } else {
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                                        Log.e(TAG, "resumeScreen() BookingInfo null");

                                                        dialogClass.hideDialog();
                                                        initOnCreate();
                                                    }
                                                }
                                            } else if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                callSelectService(2);

                                                if (json.has("CarInfo")) {
                                                    JSONArray jsonArray = json.getJSONArray("CarInfo");

                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                        if (jsonObject != null) {
                                                            if (jsonObject.has("VehicleModel")) {
                                                                String model = jsonObject.getString("VehicleModel");

                                                                int selectedCar = -1;

                                                                if (model.equalsIgnoreCase("1")) {
                                                                    selectedCar = 0;
                                                                } else if (model.equalsIgnoreCase("2")) {
                                                                    selectedCar = 1;
                                                                } else if (model.equalsIgnoreCase("3")) {
                                                                    selectedCar = 2;
                                                                } else if (model.equalsIgnoreCase("4")) {
                                                                    selectedCar = 3;
                                                                } else if (model.equalsIgnoreCase("5")) {
                                                                    selectedCar = 4;
                                                                } else if (model.equalsIgnoreCase("6")) {
                                                                    selectedCar = 5;
                                                                } else if (model.equalsIgnoreCase("7")) {
                                                                    selectedCar = 6;
                                                                }

                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                            }
                                                        }

                                                    }
                                                }

                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                                dialogClass.hideDialog();
                                                initOnCreate();
                                            }
                                        }

                                    } else {
                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("accepted")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                            } else if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);
                                            } else {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                            }
                                        } else {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                        }
                                        callSelectService(2);
                                        advanceBooking = 0;
                                        if (json.has("CarInfo")) {
                                            JSONArray jsonArray = json.getJSONArray("CarInfo");

                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                if (jsonObject != null) {
                                                    if (jsonObject.has("VehicleModel")) {
                                                        String model = jsonObject.getString("VehicleModel");

                                                        int selectedCar = -1;

                                                        if (model.equalsIgnoreCase("1")) {
                                                            selectedCar = 0;
                                                        } else if (model.equalsIgnoreCase("2")) {
                                                            selectedCar = 1;
                                                        } else if (model.equalsIgnoreCase("3")) {
                                                            selectedCar = 2;
                                                        } else if (model.equalsIgnoreCase("4")) {
                                                            selectedCar = 3;
                                                        } else if (model.equalsIgnoreCase("5")) {
                                                            selectedCar = 4;
                                                        } else if (model.equalsIgnoreCase("6")) {
                                                            selectedCar = 5;
                                                        } else if (model.equalsIgnoreCase("7")) {
                                                            selectedCar = 6;
                                                        }

                                                        if (carType_beens != null && carType_beens.size() > 0) {
                                                            for (int s = 0; s < carType_beens.size(); s++) {
                                                                if (s == selectedCar) {
                                                                    carType_beens.get(selectedCar).setSelectedCar(selectedCar);
                                                                } else {
                                                                    carType_beens.get(s).setSelectedCar(-1);
                                                                }
                                                            }
                                                        }
//                                                        else if (model.equalsIgnoreCase("6"))
//                                                        {
//                                                            selectedCar = 6;
//                                                        }
//                                                        else if (model.equalsIgnoreCase("7"))
//                                                        {
//                                                            selectedCar = 5;
//                                                        }

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                    }
                                                }

                                            }
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                        dialogClass.hideDialog();

                                        initOnCreate();
                                    }
                                } else {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                    Log.e(TAG, "resumeScreen() BookingType not found");
                                    dialogClass.hideDialog();
                                    initOnCreate();
                                }
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                Log.e(TAG, "resumeScreen() status false");
                                dialogClass.hideDialog();
                                initOnCreate();
                            }
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                            callResumeScreen = false;
                            dialogClass.hideDialog();
                            Log.e(TAG, "resumeScreen() status not found");
                        }
                    } else {
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                        callResumeScreen = false;
                        dialogClass.hideDialog();
                        Log.e(TAG, "resumeScreen() json null ");
                    }
                } catch (Exception e) {
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                    callResumeScreen = false;
                    Log.e(TAG, "resumeScreen() Exception " + e.toString());
                    dialogClass.hideDialog();
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog("Something went wrong!", "Error Message");

                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void initOnCreate() {
        Log.e(TAG, "initOnCreate()");

        callResumeScreen = false;
        myLocation = true;
        from = "";
        getEstimated_lat = 0;
        getEstimated_long = 0;
        getEstimated_Pickup = "";
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        gpsTracker = new GPSTracker(activity);
        initMap();
        handler = new Handler();
        handlerDriver = new Handler();
        passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        requestPendingDialogClass = new RequestPendingDialogClass(activity, 0);

        ll_RootLayour = (LinearLayout) findViewById(R.id.main_content);
        mySnackBar = new MySnackBar(activity);

        getEstimateFare_beens.clear();
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "1", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "2", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "3", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "4", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "5", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "6", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "7", "0"));

        pickup_Touch = false;
        dropoff_Touch = false;
        addFlag = 1;
        carClass = null;
        pickUp = null;
        driverLatLng = null;
        zoomFlag = 0;
        BookingId = "";
        driverPhone = "";
        carType_beens.clear();
        driverId = "";
        selectedCarPosition = -1;
        bookingRequest_beens.clear();
        bookingDriverLocation_beens.clear();
        requestConfirmJsonObject = null;
        timeFlag = 0;


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
            Log.e(TAG, "initOnCreate() tripFlag if = " + tripFlag);
        } else {
            tripFlag = 0;
            Log.e(TAG, "initOnCreate() tripFlag else = " + tripFlag);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
            selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
            try {
                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
            } catch (Exception e) {
                Log.e(TAG, "initOnCreate() getting requestConfirmJsonObject exception = " + e.getMessage());
            }

        }

        Log.e(TAG, "initOnCreate() tripFlag = " + tripFlag);
        Log.e(TAG, "initOnCreate() selectedCarPosition = " + selectedCarPosition);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(getResources().getColor(R.color.colorWhite))
                .borderWidthDp(2)
                .oval(true)
                .build();

        init();
    }

    private void init() {

        disconnectSocket();
        toolbar = new Toolbar(activity);
        setSupportActionBar(toolbar);
        connectSocket();

        ivMarkerCurrent = findViewById(R.id.marker);
        lv_drawer = (ListView) findViewById(R.id.lv_drawer);
        ll_DrawerLayout = (LinearLayout) findViewById(R.id.ll_drawer_layout);
        ll_Menu = (LinearLayout) findViewById(R.id.menu_layout);
        ll_Favorite = (LinearLayout) findViewById(R.id.favorite_layout);
        ll_call = (LinearLayout) findViewById(R.id.call_layout);
        iv_Menu = (ImageView) findViewById(R.id.menu_image);
        iv_Favorite = (ImageView) findViewById(R.id.favorite_image);
        iv_call = (ImageView) findViewById(R.id.call_image);
        iv_MyLocation = (ImageView) findViewById(R.id.iv_my_location);

        ll_BookingLayout = (LinearLayout) findViewById(R.id.booking_layout);
        ll_AllCarLayout = (LinearLayout) findViewById(R.id.all_cars);
        int tripFlag = 0;
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        } else {
            tripFlag = 0;
        }

        if (tripFlag == 0) {
            Log.e("TAG", "init() " + tripFlag);
//            llSelectService.setVisibility(View.VISIBLE);
            ll_Done.setVisibility(View.VISIBLE);
            callSelectService(2);
        } else {
//            llSelectService.setVisibility(View.GONE);
            ll_Done.setVisibility(View.GONE);
        }

        cvTaxi = (CardView) findViewById(R.id.cvTaxi);
        cvTransport = (CardView) findViewById(R.id.cvTransport);
        card_pickupLocation = (CardView) findViewById(R.id.card_pickupLocation);
        card_dropoffLoc = (CardView) findViewById(R.id.card_dropoffLoc);

        ll_ProfileDetail = (LinearLayout) findViewById(R.id.profile_detail_layout);

        auto_Pickup = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_pickup);
        auto_Dropoff = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_drop_off);
        tvPickup = findViewById(R.id.tvPickup);
        tvPickup.setSelected(true);
        tvDropoff = findViewById(R.id.tvDropoff);
        //tvDropoff.setSelected(true);

        tvPickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ivMarkerCurrent.setImageResource(R.drawable.pin_green/*ic_current_location*//*ic_pin_pick*/);
                }
            }
        });

        tvDropoff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ivMarkerCurrent.setImageResource(R.drawable.pin_red/*ic_drop_pin*/);
                }
            }
        });

        iv_ProfileImage = (CircleImageView) findViewById(R.id.image_profile_drawer);
        tv_UserName = findViewById(R.id.user_name);
        tvUserEmail = findViewById(R.id.tvUserEmail);
        tv_UserPhone = findViewById(R.id.user_phone);
        tv_rating = findViewById(R.id.tv_rating);
        ll_rating = (LinearLayout) findViewById(R.id.ll_rating);

        ll_ClearDropoff = (LinearLayout) findViewById(R.id.close_dropoff);
        ll_ClearPickup = (LinearLayout) findViewById(R.id.close_pickup);
        llPickup = findViewById(R.id.llPickup);
        llDropoff = findViewById(R.id.llDropoff);

        ll_BottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet_Layout);

        address_Layout = (LinearLayout) findViewById(R.id.address_layout);
        current_marker_layout = findViewById(R.id.current_marker_layout);
        tv_CancelRequest = findViewById(R.id.cancel_request);
        tv_DriverInfo = findViewById(R.id.driver_info);
        ll_AfterRequestAccept = (LinearLayout) findViewById(R.id.after_accept_layout);

        tv_Dropoff = findViewById(R.id.dropoff_location_bottomsheet);
        tv_Pickup = findViewById(R.id.pickup_location_bottomsheet);
        tv_CarCompany = findViewById(R.id.car_company_bottomsheet);
        tv_CarModel = findViewById(R.id.car_model_bottomsheet);
        tv_VehicleRegistrationNo = findViewById(R.id.vehicle_registration_no);
        tv_DriverName = findViewById(R.id.driver_name_bottomsheet);
        iv_CarImage = (ImageView) findViewById(R.id.car_image_bottomsheet);
        iv_DriverImage = (ImageView) findViewById(R.id.driver_image_bottomsheet);
        iv_CallDriver = (ImageView) findViewById(R.id.call_to_driver_bottomsheet);
        tv_Rate = findViewById(R.id.tv_Rate);
        txt_driver_number = findViewById(R.id.txt_driver_number);
        ratingbar_driver = findViewById(R.id.ratingbar_driver);

        iv_Close = (ImageView) findViewById(R.id.close);

        tv_BookNow = findViewById(R.id.book_now_textview);
        tv_BookLater = findViewById(R.id.book_later_textview);

        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.my_shipment), "", "", R.drawable.ic_menu_my_shipment));
        //menuList_beens.add(new MenuList_Been(getResources().getString(R.string.payment_options), "", "", R.drawable.ic_menu_payment_option));
//        menuList_beens.add(new MenuList_Been("Wallet","","",R.drawable.pick_ic_menu_wallet));
        menuList_beens.add(new MenuList_Been("Favourite Location", "", "", R.drawable.ic_menu_favorite));
        menuList_beens.add(new MenuList_Been("Previous Due", "", "", R.drawable.ic_due_amount));
        //menuList_beens.add(new MenuList_Been(getResources().getString(R.string.my_receipts),"","",R.drawable.ic_menu_my_receipts));
//        menuList_beens.add(new MenuList_Been("Package History","","",R.drawable.pick_ic_menu_pkg_history));
        menuList_beens.add(new MenuList_Been("Invite Friends"/*getResources().getString(R.string.invite_friends)*/, "", "", R.drawable.ic_menu_invite_friends));
        menuList_beens.add(new MenuList_Been("Settings", "", "", R.drawable.ic_menu_settings));
        menuList_beens.add(new MenuList_Been(getString(R.string.activity_notification), "", "", R.drawable.ic_notification));
//        menuList_beens.add(new MenuList_Been("New Booking","","",R.drawable.pick_ic_menu_driver));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.contact_us), "", "", R.drawable.ic_menu_contact_us));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.logout), "", "", R.drawable.ic_menu_logout));

        tv_BookLater.setOnClickListener(activity);
        tv_BookNow.setOnClickListener(activity);
        iv_Close.setOnClickListener(activity);
        iv_CallDriver.setOnClickListener(activity);
        tv_DriverInfo.setOnClickListener(activity);
        tv_CancelRequest.setOnClickListener(activity);
        ll_ProfileDetail.setOnClickListener(activity);

        ll_Favorite.setOnClickListener(activity);
        iv_Favorite.setOnClickListener(activity);
        ll_call.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
        ll_Done.setOnClickListener(activity);
        cvTaxi.setOnClickListener(activity);
        cvTransport.setOnClickListener(activity);
        llPickup.setOnClickListener(activity);
        llDropoff.setOnClickListener(activity);

        helpLineNumber = getResources().getString(R.string.helpLineNumber);

        auto_Pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                Log.e(TAG, "auto_Pickup.addTextChangedListener() text = " + s.toString());

                //You need to remove this to run only once
                handlerType.removeCallbacks(input_finish_checker);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
//                    if (s.toString().startsWith(" "))
//                    {
//                        auto_Pickup.setText(s.toString().trim());
//                    }

                    pickDropoffType = 1;
                    last_text_edit = System.currentTimeMillis();
                    handlerType.postDelayed(input_finish_checker, delay);
                } else {
                    pickup_Touch = false;
                    TaxiUtil.picup_Long = 0;
                    TaxiUtil.picup_Lat = 0;
                    TaxiUtil.picup_Address = "";
                    //ll_Done.setVisibility(View.GONE);
                    setTimeAndPriceToZero();
                }
            }
        });

        auto_Dropoff.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //You need to remove this to run only once
                handlerType.removeCallbacks(input_finish_checker);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
//                    if (s.toString().startsWith(" "))
//                    {
//                        auto_Dropoff.setText(s.toString().trim());
//                    }

                    pickDropoffType = 2;
                    last_text_edit = System.currentTimeMillis();
                    handlerType.postDelayed(input_finish_checker, delay);
                } else {
                    //ll_Done.setVisibility(View.GONE);
                    dropoff_Touch = false;
                    TaxiUtil.dropoff_Long = 0;
                    TaxiUtil.dropoff_Lat = 0;
                    TaxiUtil.dropoff_Address = "";

                    setTimeAndPriceToZero();
                }
            }
        });

        auto_Pickup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Pickup.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Pickup.getText().toString());
                    } else {
                        TaxiUtil.picup_Lat = 0;
                        TaxiUtil.picup_Long = 0;
                        TaxiUtil.picup_Address = "";
                        pickup_Touch = false;
                    }
                    // finish();
                }
                return false;
            }
        });

        auto_Dropoff.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
//                        getLocationFromAddress(auto_Dropoff.getText().toString());
                    } else {
                        TaxiUtil.dropoff_Lat = 0;
                        TaxiUtil.dropoff_Long = 0;
                        TaxiUtil.dropoff_Address = "";
                        dropoff_Touch = false;
                    }
                    // finish();
                }
                return false;
            }
        });

        auto_Pickup.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
                        //ll_Done.setVisibility(View.VISIBLE);
                    } else {
                        //ll_Done.setVisibility(View.GONE);
                    }
                }
            }
        });

        auto_Dropoff.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (auto_Dropoff.getText().toString().trim().length() > 0) {
                        //ll_Done.setVisibility(View.VISIBLE);
                    } else {
                        //ll_Done.setVisibility(View.GONE);
                    }
                }
            }
        });

        Log.e("tripFlag", "tripFlag*******" + tripFlag);
        if (tripFlag == 0) {
            myLocation = true;
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.VISIBLE);
            ll_AfterRequestAccept.setVisibility(View.GONE);
            ll_Done.setVisibility(View.VISIBLE);

            if (googleMap != null) {
                googleMap.clear();
            }
        } else {
            ll_Done.setVisibility(View.GONE);
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.GONE);
            ll_AfterRequestAccept.setVisibility(View.VISIBLE);
            tv_DriverInfo.setVisibility(View.VISIBLE);
            if (tripFlag == 1) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.VISIBLE);

            } else if (tripFlag == 2) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.GONE);
            }
        }

        ll_ClearDropoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPickupDropoff = 2;
                isCardPickUpSelected = false;
                card_dropoffLoc.setCardElevation(5);
                card_pickupLocation.setCardElevation(-5);

                auto_Dropoff.setText("");
                tvDropoff.setText("");
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Lat = 0;
                TaxiUtil.dropoff_Long = 0;
                dropoff_Touch = false;
                favorite = 0;
                favorite_address = "";
                from = "";
                setTimeAndPriceToZero();
                isPickupDropoff = 2;
                isDone = 0;
                current_marker_layout.setVisibility(View.VISIBLE);
                if (markerDropoff != null) {
                    markerDropoff.remove();
                }
                if (polyline != null) {
                    polyline.remove();
                }

                //SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, "", activity);

                if (googleMap != null) {
                    googleMap.clear();
                }
                ll_AllCarLayout.setVisibility(View.GONE);
                ll_BookingLayout.setVisibility(View.GONE);

                //ll_Done.setVisibility(View.GONE);
                callSelectService(2);
//                llSelectService.setVisibility(View.VISIBLE);

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, "", activity);
                auto_Dropoff.setFocusableInTouchMode(true);
                auto_Dropoff.requestFocus();
                setNoSelectedCarBackGround();
                getEstimatedFareCheckValidation();
                //
            }
        });

        ll_ClearPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPickupDropoff = 1;
                isCardPickUpSelected = true;
                card_dropoffLoc.setCardElevation(-5);
                card_pickupLocation.setCardElevation(5);

                auto_Pickup.setText("");
                tvPickup.setText("");
                TaxiUtil.picup_Address = "";
                TaxiUtil.picup_Long = 0;
                TaxiUtil.picup_Lat = 0;
                pickup_Touch = false;
                isDone = 0;
                current_marker_layout.setVisibility(View.VISIBLE);
                setTimeAndPriceToZero();
                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();
                if (markerPickup != null) {
                    markerPickup.remove();
                }

                if (polyline != null) {
                    polyline.remove();
                }

                if (googleMap != null) {
                    googleMap.clear();
                }



                ll_AllCarLayout.setVisibility(View.GONE);
                ll_BookingLayout.setVisibility(View.GONE);

                //ll_Done.setVisibility(View.GONE);
                callSelectService(2);
//                llSelectService.setVisibility(View.VISIBLE);

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, "", activity);
                isPickupDropoff = 1;
                getEstimated_lat = 0;
                getEstimated_long = 0;
                getEstimated_Pickup = "";
                setNoSelectedCarBackGround();

                getEstimatedFareCheckValidation();

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListAdapter = new MenuListAdapter(MainActivity.this, menuList_beens, drawer, ll_DrawerLayout);
        lv_drawer.setAdapter(drawerListAdapter);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            private float scaleFactor = 6f;

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                auto_Dropoff.setEnabled(true);
                auto_Pickup.setEnabled(true);
                ll_RootLayour.setEnabled(true);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                auto_Dropoff.setEnabled(false);
                auto_Pickup.setEnabled(false);
                ll_RootLayour.setEnabled(false);
                setUserDetail();
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                Log.e(TAG, "slideOffset:- " + slideOffset);
                Log.e(TAG, "slideOffset slideOffset:- " + (1 - (slideOffset / scaleFactor)));

                float slideX = drawerView.getWidth() * slideOffset;
                ll_RootLayour.setTranslationX(slideX);
                ll_RootLayour.setScaleX(1 - (slideOffset / scaleFactor));
                ll_RootLayour.setScaleY(1 - (slideOffset / scaleFactor));
            }
        };


        //drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawer.setScrimColor(Color.TRANSPARENT);
        drawer.setDrawerElevation(0f);
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        iv_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                } else {
                    drawer.openDrawer(ll_DrawerLayout);
                }
            }
        });

        ll_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                } else {
                    drawer.openDrawer(ll_DrawerLayout);
                }
            }
        });

        Log.e(TAG, "setUserDetail() ?????????????????????????????????? onCreate ???????????????????????????????");

        iv_MyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPickupDropoff = 1;
                isCardPickUpSelected = true;
                card_dropoffLoc.setCardElevation(-5);
                card_pickupLocation.setCardElevation(5);


                myLocation = true;
                current_marker_layout.setVisibility(View.VISIBLE);
                ll_Done.setVisibility(View.VISIBLE);
                callSelectService(2);
                ll_AllCarLayout.setVisibility(View.GONE);
                ll_BookingLayout.setVisibility(View.GONE);
                isPickupDropoff = 1;
                auto_Dropoff.setText("");
                tvDropoff.setText("");
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Long = 0;
                TaxiUtil.dropoff_Lat = 0;
                dropoff_Touch = false;
                isDone = 0;

                if (carType_beens != null && carType_beens.size() > 0) {
                    for (int i = 0; i < carType_beens.size(); i++) {
                        carType_beens.get(i).setSelectedCar(-1);
                    }
                }

                if (carAdapter != null) {
                    carAdapter.notifyDataSetChanged();
                    recyclerViewCar.scrollToPosition(0);
                }
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, "", activity);
                checkGPS();
            }
        });

        getAddressFromLatLong();

        call_DriverInfo();

        String notificationType = SessionSave.getUserSession(Common.PREFRENCE_NOTIFICATION, activity);

        Log.e("call", "notificationType notificationType = " + notificationType + " activity :"+fromSplash);

        if (fromSplash != null && !fromSplash.trim().equalsIgnoreCase("") && !fromSplash.trim().equalsIgnoreCase("Splash_Activity")) {

            Log.e(TAG,"notificationType fromSplash : splash_Activity");


            if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AcceptBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AcceptBooking");
                startActivity(loginScreen);
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("OnTheWay")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "OnTheWay");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("RejectBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "RejectBooking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("Booking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "Booking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AdvanceBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AdvanceBooking");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AddMoney")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, Wallet_Transfer_History_Activity.class);
                loginScreen.putExtra("notification", "AddMoney");
                startActivity(loginScreen);
                finish();
            } else if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("TransferMoney")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, Wallet_Transfer_History_Activity.class);
                loginScreen.putExtra("notification", "TransferMoney");
                startActivity(loginScreen);
                finish();
            }
        } else {
            if (notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AcceptBooking")) {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AcceptBooking");
                startActivity(loginScreen);
            }
            else if(notificationType != null && !notificationType.equalsIgnoreCase("") && notificationType.equalsIgnoreCase("AdvanceBooking"))
            {
                SessionSave.saveUserSession(Common.PREFRENCE_NOTIFICATION, "", activity);
                Intent loginScreen = new Intent(activity, MyBookingActivity.class);
                loginScreen.putExtra("notification", "AdvanceBooking");
                startActivity(loginScreen);
            }
        }

        setUserDetail();
    }

    private void SetDrawer() {
        displayImage();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.layout_car_one:
                call_One();
                break;

            case R.id.layout_car_two:
                call_Two();
                break;

            case R.id.layout_car_three:
                call_Three();
                break;

            case R.id.layout_car_four:
                call_Four();
                break;

            case R.id.layout_car_five:
                call_Five();
                break;

            case R.id.layout_car_six:
//                call_Six();
                break;

            case R.id.layout_car_seven:
//                call_Seven();
                break;

            case R.id.layout_car_package:
//                call_package();
                break;

            case R.id.book_later_textview:
                call_Booking(1);
                break;

            case R.id.ll_Done:
                callAddressDone();
                break;

            case R.id.cvTaxi:
                callSelectService(2);
                break;

            case R.id.cvTransport:
                callSelectService(2);
                break;

            case R.id.book_now_textview:
                if (isAlreadySendRequest == false) {
                    getTransportService();
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.you_can_not_send_request_brfor_30_second),
                            activity.getResources().getString(R.string.info_message));
                }
                break;

            case R.id.cancel_request:

                openConfimDailog();
                break;

            case R.id.driver_info:
                if (mBottomSheetBehavior != null) {
                    if (requestConfirmJsonObject != null) {
                        showDriverInfoPopup();
                    } else {
                        Log.e(TAG, "driver_info click() requestConfirmJsonObject");
                    }
                } else {
                    Log.e(TAG, "driver_info click() mBottomSheetBehavior");
                }
                break;

            case R.id.close:
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                break;

            case R.id.call_to_driver_bottomsheet:
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                if (driverPhone != null && !driverPhone.trim().equalsIgnoreCase("")) {
                    callToDriver();
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.phone_number_not_found), 0);
                }
                break;

            case R.id.profile_detail_layout:
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(ll_DrawerLayout);
                    //gotoProfileScreen();
                }

                break;

            case R.id.img_faverite:

                if (tvPickup.getText() != null &&
                        !tvPickup.getText().toString().trim().equals("") && TaxiUtil.picup_Lat != 0 && TaxiUtil.picup_Long != 0) {
                    if (tvDropoff.getText() != null &&
                            !tvDropoff.getText().toString().trim().equals("") && TaxiUtil.dropoff_Lat != 0 && TaxiUtil.dropoff_Long != 0) {
                        AddToFavotie();
                    } else {
                        mySnackBar.showSnackBar(ll_RootLayour,
                                activity.getResources().getString(R.string.please_enter_destination_add), 0);
                    }
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour,
                            activity.getResources().getString(R.string.please_enter_pickup_add), 0);
                }
                break;

            case R.id.call_layout:
                call_sos(activity);
                break;

            case R.id.call_image:
                call_sos(activity);
                break;

            case R.id.llPickup:
                addressSelection = 1;
                if (!isCardPickUpSelected) {
                    isPickupDropoff = 1;
                    isCardPickUpSelected = true;
                    card_dropoffLoc.setCardElevation(-5);
                    card_pickupLocation.setCardElevation(5);
                } else {
                    callGoogleAddress();
                }
                break;

            case R.id.llDropoff:
                addressSelection = 2;
                if (isCardPickUpSelected) {
                    isPickupDropoff = 2;
                    isCardPickUpSelected = false;
                    card_dropoffLoc.setCardElevation(5);
                    card_pickupLocation.setCardElevation(-5);
                    callGoogleAddress();
                } else {
                    callGoogleAddress();
                }
                break;
        }
    }

    private void openConfimDailog() {
        Log.e(TAG, "openConfimDailog()");
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_confirm);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tvTitle, tvMessage, tvYes, tvNo;

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvMessage = dialog.findViewById(R.id.tvMessage);
        tvYes = dialog.findViewById(R.id.tvYes);
        tvNo = dialog.findViewById(R.id.tvNo);

        tvTitle.setText(getString(R.string.confirm_message));

        int carSelected = -1;
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
            carSelected = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        addSelectedService("2");

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int s = 0; s < carType_beens.size(); s++) {
                if (s == carSelected) {
                    carType_beens.get(carSelected).setSelectedCar(carSelected);
                } else {
                    carType_beens.get(s).setSelectedCar(-1);
                }
            }
        }

        String x = "";
        if (carType_beens != null && carType_beens.size() > 0)
        {
            for (int i = 0; i < carType_beens.size(); i++)
            {
                if (carType_beens.get(i).getSelectedCar() == carSelected)
                {
                    x = carType_beens.get(i).getCancellationFee();
                    break;
                }
            }
        }

        tvMessage.setText("Cancellation Charge of  "+getString(R.string.currency)+""+x+"  will be charged. Do you want to Cancel trip ?");

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (advanceBooking == 1) {
                    cancelAdvanceBookingRequest();
                } else {
                    CancelTripByPassenger();
                }
            }
        });

        dialog.show();
    }

    private void callGoogleAddress() {
        Log.e(TAG, "callGoogleAddress()");

        GPSTracker gpsTracker = new GPSTracker(this);

        double distanceFromCenterToCorner = 100 * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()),
                        distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()),
                        distanceFromCenterToCorner, 45.0);

        RectangularBounds bounds = RectangularBounds.newInstance(
                southwestCorner,
                northeastCorner
        );

        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("IN")
                .setLocationBias(bounds)
//                .setTypeFilter(TypeFilter.ADDRESS)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

    }

    public static int isAddressSet = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                Log.e(TAG, "onActivityResult() place.getAddress():- " + place.getAddress());
                Log.e(TAG, "onActivityResult() place.getLatLng():- " + place.getLatLng());
                LatLng latLng = place.getLatLng();
                Log.e(TAG, "onActivityResult() LAT:- " + latLng.latitude);
                Log.e(TAG, "onActivityResult() LONG:- " + latLng.longitude);

                if (addressSelection == 1) {
                    isAddressSet = 1;
                    pickup_Touch = true;
                    tvPickup.setText(place.getAddress());
                    TaxiUtil.picup_Address = place.getAddress();
                    TaxiUtil.picup_Lat = latLng.latitude;
                    TaxiUtil.picup_Long = latLng.longitude;

                    callMoveeMarker(latLng.latitude, latLng.longitude);
                    return;
                } else if (addressSelection == 2) {
                    isAddressSet = 1;
                    dropoff_Touch = true;
                    tvDropoff.setText(place.getAddress());
                    TaxiUtil.dropoff_Address = place.getAddress();
                    TaxiUtil.dropoff_Lat = latLng.latitude;
                    TaxiUtil.dropoff_Long = latLng.longitude;

                    callMoveeMarker(latLng.latitude, latLng.longitude);
                    return;

                }
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(place.getLatLng()).zoom(defaltZoomLavel).build()));

                if (!TextUtils.isEmpty(tvPickup.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tvDropoff.getText().toString().trim())) {
                    callAddressDone();
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callMoveeMarker(double latitude, double longitude) {

        CameraPosition camPos = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(defaltZoomLavel)
                .build();
        CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.moveCamera(camUpdate);

        /*googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);*/
    }

    private ArrayList<TransportService_Been> transportService_beens = new ArrayList<TransportService_Been>();
    //serviceType 1 for car
    //serviceType 2 for truck.

    public void getTransportService() {

        Log.e(TAG, "getTransportService()");

        transportService_beens.clear();
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GET_TRANSPORT_SERVICE;
//        transportService_beens.add(new TransportService_Been("", "Select Transport Service", "", "", "", false));
        Log.e(TAG, "getTransportService() url:- " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "getTransportService() Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("percel")) {
                                    JSONArray percel = json.getJSONArray("percel");

                                    if (percel != null && percel.length() > 0) {
                                        for (int i = 0; i < percel.length(); i++) {
                                            JSONObject percelData = percel.getJSONObject(i);

                                            if (percelData != null) {
                                                String Id = "", Name = "", Image = "", Status = "", CreatedDate = "";
                                                boolean isSelected = false;

                                                if (percelData.has("Id")) {
                                                    Id = percelData.getString("Id");
                                                }

                                                if (percelData.has("Name")) {
                                                    Name = percelData.getString("Name");
                                                }

                                                if (percelData.has("Image")) {
                                                    Image = percelData.getString("Image");
                                                }

                                                if (percelData.has("Status")) {
                                                    Status = percelData.getString("Status");
                                                }

                                                if (percelData.has("CreatedDate")) {
                                                    CreatedDate = percelData.getString("CreatedDate");
                                                }

                                                transportService_beens.add(new TransportService_Been(Id, Name, Image, Status, CreatedDate, isSelected));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        call_Booking(0);
                                    } else {
                                        Log.e(TAG, "getTransportService() no percel available");
                                        dialogClass.hideDialog();
                                    }
                                } else {
                                    Log.e(TAG, "getTransportService() no percel found");
                                    dialogClass.hideDialog();
                                }
                            } else {
                                Log.e(TAG, "getTransportService() status false");
                                dialogClass.hideDialog();
                            }
                        } else {
                            Log.e("call", "status not found");
                            dialogClass.hideDialog();
                        }

                    } else {
                        Log.e("call", "json null");
                        dialogClass.hideDialog();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callSelectService(int serviceType) {
        Log.e(TAG, "init() callSelectService");
        Log.e(TAG, "callSelectService() serviceType:- " + serviceType);

        if (serviceType == 1) {
            int tripFlag = 0;
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null &&
                    !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {

                tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                Log.e("TAG", "init() tripFlag = " + tripFlag);
            } else {
                tripFlag = 0;
                Log.e("TAG", "init() tripFlag else = " + tripFlag);
            }

            if (tripFlag == 0) {
                ll_Done.setVisibility(View.VISIBLE);
            } else {
                ll_Done.setVisibility(View.GONE);
            }


            llSelectService.setVisibility(View.GONE);
            RequestFor = TAXI;
            addSelectedService("1");
            if (Common.socket != null && Common.socket.connected()) {
                if (tripFlag == 0) {
                    Log.e("tag", "init() startTimer call 1111");
                    startTimer();
                } else {
                    updatePassenger();
                    startTimerForDriverLocation();
                }
            }
        } else if (serviceType == 2) {
            int tripFlag = 0;
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null &&
                    !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
                tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                Log.e("TAG", "init() tripFlag = " + tripFlag);
            } else {
                tripFlag = 0;
                Log.e("TAG", "init() tripFlag = " + tripFlag);
            }

            if (tripFlag == 0) {
                ll_Done.setVisibility(View.VISIBLE);
                if (isDone != 0) {
                    ll_AllCarLayout.setVisibility(View.VISIBLE);
                    ll_BookingLayout.setVisibility(View.VISIBLE);
                } else {
                    ll_AllCarLayout.setVisibility(View.GONE);
                    ll_BookingLayout.setVisibility(View.GONE);
                }
            } else {
                ll_Done.setVisibility(View.GONE);
            }

            llSelectService.setVisibility(View.GONE);
            RequestFor = DELIVERY;
            addSelectedService("2");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "onConnect onConnect onConnect tripFlag = " + tripFlag);
                if (tripFlag == 0) {
                    Log.e(TAG, "init() startTimer() 222222");
                    startTimer();
                } else {
                    updatePassenger();
                    startTimerForDriverLocation();
                }

            } else {

                if (tripFlag == 0) {
                    Log.e(TAG, "init() startTimer() 222222");
                    startTimer();
                }

                //Log.e(TAG, "init() socket not connect");
            }
        } else {
            ll_Done.setVisibility(View.GONE);
//            llSelectService.setVisibility(View.VISIBLE);
            ll_AllCarLayout.setVisibility(View.GONE);
            ll_BookingLayout.setVisibility(View.GONE);
            RequestFor = "";
        }
    }

    public void addSelectedService(String strCatId) {
        try {
            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity) != null &&
                    !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity).equals("")) {
                carClass = new JSONArray(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CAR_CLASS, activity));
            } else {
                carClass = null;
            }

            if (carClass != null && carClass.length() > 0) {
                carType_beens.clear();

                for (int i = 0; i < carClass.length(); i++) {
                    JSONObject data = carClass.getJSONObject(i);

                    if (data != null) {
                        if (data.has("CategoryId")) {
                            String Id = "", CategoryId = "", Name = "", Sort = "", BaseFare = "", MinKm = "", BelowAndAboveKmLimit = "";
                            String BelowPerKmCharge = "", AbovePerKmCharge = "", CancellationFee = "", NightChargeApplicable = "";
                            String NightCharge = "", NightTimeFrom = "", NightTimeTo = "", SpecialEventSurcharge = "";
                            String SpecialEventTimeFrom = "", SpecialEventTimeTo = "", WaitingTimePerMinuteCharge = "", MinuteFare = "";
                            String BookingFee = "", SpecialExtraCharge = "0", Capacity = "", Image = "", Description = "", Status = "";
                            String Commission = "", Height = "", Width = "", ModelSizeImage = "";

                            CategoryId = data.getString("CategoryId");

                            if (CategoryId != null && !CategoryId.equals("") && CategoryId.equals(strCatId)) {
                                if (data.has("Id")) {
                                    Id = data.getString("Id");
                                }

                                if (data.has("Name")) {
                                    Name = data.getString("Name");
                                }

                                if (data.has("Sort")) {
                                    Sort = data.getString("Sort");
                                }

                                if (data.has("BaseFare")) {
                                    BaseFare = data.getString("BaseFare");
                                }

                                if (data.has("MinKm")) {
                                    MinKm = data.getString("MinKm");
                                }

                                if (data.has("BelowAndAboveKmLimit")) {
                                    BelowAndAboveKmLimit = data.getString("BelowAndAboveKmLimit");
                                }

                                if (data.has("BelowPerKmCharge")) {
                                    BelowPerKmCharge = data.getString("BelowPerKmCharge");
                                }

                                if (data.has("AbovePerKmCharge")) {
                                    AbovePerKmCharge = data.getString("AbovePerKmCharge");
                                }

                                if (data.has("CancellationFee")) {
                                    CancellationFee = data.getString("CancellationFee");
                                }

                                if (data.has("NightChargeApplicable")) {
                                    NightChargeApplicable = data.getString("NightChargeApplicable");
                                }

                                if (data.has("NightCharge")) {
                                    NightCharge = data.getString("NightCharge");
                                }

                                if (data.has("NightTimeFrom")) {
                                    NightTimeFrom = data.getString("NightTimeFrom");
                                }

                                if (data.has("NightTimeTo")) {
                                    NightTimeTo = data.getString("NightTimeTo");
                                }

                                if (data.has("SpecialEventSurcharge")) {
                                    SpecialEventSurcharge = data.getString("SpecialEventSurcharge");
                                }

                                if (data.has("SpecialEventTimeFrom")) {
                                    SpecialEventTimeFrom = data.getString("SpecialEventTimeFrom");
                                }

                                if (data.has("SpecialEventTimeTo")) {
                                    SpecialEventTimeTo = data.getString("SpecialEventTimeTo");
                                }

                                if (data.has("WaitingTimePerMinuteCharge")) {
                                    WaitingTimePerMinuteCharge = data.getString("WaitingTimePerMinuteCharge");
                                }

                                if (data.has("MinuteFare")) {
                                    MinuteFare = data.getString("MinuteFare");
                                }

                                if (data.has("BookingFee")) {
                                    BookingFee = data.getString("BookingFee");
                                }

                                if (data.has("SpecialExtraCharge")) {
                                    if (data.getString("SpecialExtraCharge") != null &&
                                            !data.getString("SpecialExtraCharge").equalsIgnoreCase("") &&
                                            !data.getString("SpecialExtraCharge").equalsIgnoreCase("null") &&
                                            !data.getString("SpecialExtraCharge").equalsIgnoreCase("NULL")) {
                                        SpecialExtraCharge = data.getString("SpecialExtraCharge");
                                    } else {
                                        SpecialExtraCharge = "0";
                                    }
                                } else {
                                    SpecialExtraCharge = "0";
                                }

                                if (data.has("Capacity")) {
                                    Capacity = data.getString("Capacity");
                                }

                                if (data.has("Image")) {
                                    Image = data.getString("Image");
                                }

                                if (data.has("Description")) {
                                    Description = data.getString("Description");
                                }

                                if (data.has("Status")) {
                                    Status = data.getString("Status");
                                }

                                if (data.has("Commission")) {
                                    Commission = data.getString("Commission");
                                }

                                if (data.has("Height")) {
                                    Height = data.getString("Height");
                                }

                                if (data.has("Width")) {
                                    Width = data.getString("Width");
                                }

                                if (data.has("ModelSizeImage")) {
                                    ModelSizeImage = /*WebServiceAPI.BASE_URL_IMAGE +*/ data.getString("ModelSizeImage");
                                }

                                carType_beens.add(new CarType_Been(
                                        Id,
                                        CategoryId,
                                        Name,
                                        Sort,
                                        BaseFare,
                                        MinKm,
                                        BelowAndAboveKmLimit,
                                        BelowPerKmCharge,
                                        AbovePerKmCharge,
                                        CancellationFee,
                                        NightChargeApplicable,
                                        NightCharge,
                                        NightTimeFrom,
                                        NightTimeTo,
                                        SpecialEventSurcharge,
                                        SpecialEventTimeFrom,
                                        SpecialEventTimeTo,
                                        WaitingTimePerMinuteCharge,
                                        MinuteFare,
                                        BookingFee,
                                        SpecialExtraCharge,
                                        Capacity,
                                        Image,
                                        Description,
                                        Status,
                                        Commission,
                                        Height,
                                        Width,
                                        ModelSizeImage,
                                        0,
                                        -1
                                ));
                            }
                        }
                    }
                }

                /*Collections.sort(
                        carType_beens,
                        new Comparator<CarType_Been>()
                        {
                            public int compare(CarType_Been lhs, CarType_Been rhs)
                            {
                                return lhs.getSort().compareTo(rhs.getSort());
                            }
                        }
                );*/
            } else {
                carType_beens.clear();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception e: - " + e.getMessage());
        }

        // Layout Managers:

        Log.e(TAG, "carType_beens carType_beens carType_beens size = " + carType_beens.size());
//        carLayoutManager = new GridLayoutManager(activity,carType_beens.size());
        recyclerViewCar = (RecyclerView) findViewById(R.id.recyclerViewCar);
        recyclerViewCar.setLayoutManager(new GridLayoutManager(activity, carType_beens.size()));
//        recyclerViewCar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        carAdapter = new CarAdapter(activity);//carType_beens,
        recyclerViewCar.setAdapter(carAdapter);
        recyclerViewCar.scrollToPosition(0);
    }

    public void connectSocket() {
        Log.e(TAG, "connectSocket()");
        try {
            if (Common.socket != null && Common.socket.connected()) {
                Log.e(TAG, "connectSocket() Ping socket already connected connectSocket()");
            } else {
                Log.e(TAG, "connectSocket() Ping socket try to connecting...connectSocket()");

                Common.socket = null;
                SocketSingleObject.instance = null;
                Common.socket = SocketSingleObject.get(MainActivity.this).getSocket();
                Common.socket.on("NearByDriverList", onDriverList);
                Common.socket.on("PassengerCancelTripNotification", onPassengerCancelTripNotification);
                Common.socket.on(Socket.EVENT_CONNECT, onConnect);
                Common.socket.on("AcceptBookingRequestNotification", onAcceptBookingRequestNotification);
                Common.socket.on("RejectBookingRequestNotification", onRejectBookingRequestNotification);
                Common.socket.on("GetDriverLocation", onGetDriverLocation);
//                Common.socket.on("TripHoldNotification",onTripHoldNotification);
                Common.socket.on("PickupPassengerNotification", onPickupPassengerNotification);
                Common.socket.on("DriverArrivedAtPickupLocation", onDriverArrivedAtPickupLocation);
                Common.socket.on("DriverReachedAtDropoffLocation", onDriverReachedAtDropoffLocation);
                Common.socket.on("ResendOTPForArrivedAtPickuplocation", onResendOTPForArrivedAtPickuplocation);
                Common.socket.on("ResendOTPForReachedAtDropofflocation", onResendOTPForReachedAtDropofflocation);

                Common.socket.on("BookingDetails", onBookingDetails);
                Common.socket.on("GetEstimateFare", onGetEstimateFare);
                Common.socket.on("ReceiveMoneyNotify", onReceiveMoneyNotify);
                Common.socket.on("RatingDetails", onRatingDetails);

                Common.socket.on("AcceptAdvancedBookingRequestNotification", onAcceptAdvancedBookingRequestNotification);
                Common.socket.on("RejectAdvancedBookingRequestNotification", onRejectAdvancedBookingRequestNotification);
//                Common.socket.on("InformPassengerForAdvancedTrip",InformPassengerForAdvancedTrip);
                Common.socket.on("AcceptAdvancedBookingRequestNotify", AcceptAdvancedBookingRequestNotify);
                Common.socket.on("AdvancedBookingPickupPassengerNotification", onAdvancedBookingPickupPassengerNotification);
                Common.socket.on("AdvancedBookingTripHoldNotification", onAdvancedBookingTripHoldNotification);
                Common.socket.on("AdvancedBookingDetails", onAdvancedBookingDetails);
                Common.socket.on("AdvanceBookingDriverArrivedAtPickupLocation", onAdvanceBookingDriverArrivedAtPickupLocation);
                Common.socket.on("AdvanceBookingDriverReachedAtDropoffLocation", onAdvanceBookingDriverReachedAtDropoffLocation);

                Common.socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
                Common.socket.connect();
            }
        } catch (Exception e) {
            Log.e(TAG, "connectSocket() Socket connect exception = " + e.getMessage());
        }
    }

    private Emitter.Listener onBookingDetails = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "onBookingDetails()");
                        if (args != null && args.length > 0) {
                            Log.e(TAG, "onBookingDetails() response = " + args[0].toString());

                            JSONObject tripCompleteObject = new JSONObject(args[0].toString());

                            if (tripCompleteObject != null) {
                                if (tripCompleteObject.has("Info") && !tripCompleteObject.getString("Info").equalsIgnoreCase("")) {
                                    JSONArray InfoArray = tripCompleteObject.getJSONArray("Info");

                                    if (InfoArray != null && InfoArray.length() > 0) {
                                        final JSONObject InfoData = InfoArray.getJSONObject(0);
                                        if (InfoData != null) {
                                            if (InfoData.has("Id") && InfoData.getString("Id") != null && !InfoData.getString("Id").equalsIgnoreCase("")) {
                                                if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                    if (BookingId.equalsIgnoreCase(InfoData.getString("Id"))) {
                                                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                            commonInternetDialog.hideDialog();
                                                        }

                                                        if (commonDialog != null && commonDialog.isShowing()) {
                                                            commonDialog.dismiss();
                                                        }

                                                        String walletBalance = "0";

                                                        if (tripCompleteObject.has("UpdatedBal")) {
                                                            walletBalance = tripCompleteObject.getString("UpdatedBal");
                                                        }

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE, walletBalance, activity);

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                                                        ll_BookingLayout.setVisibility(View.GONE);
                                                        ll_AllCarLayout.setVisibility(View.GONE);
                                                        //ll_Done.setVisibility(View.VISIBLE);
                                                        isDone = 0;
                                                        callSelectService(2);
                                                        ll_AfterRequestAccept.setVisibility(View.GONE);
                                                        tv_CancelRequest.setVisibility(View.GONE);
                                                        address_Layout.setVisibility(View.VISIBLE);

                                                        removeCarMarker();

                                                        if (originMarker != null) {
                                                            originMarker.remove();
                                                        }

                                                        if (destinationMarker != null) {
                                                            destinationMarker.remove();
                                                        }

                                                        if (googleMap != null) {
                                                            googleMap.clear();
                                                        }

                                                        if (handlerDriver != null && runnableDriver != null) {
                                                            handlerDriver.removeCallbacks(runnableDriver, null);
                                                        }

                                                        String message = "Your trip has been completed successfully!";

                                                        if (tripCompleteObject.has("message")) {
                                                            message = tripCompleteObject.getString("message");
                                                        }
                                                        SessionSave.saveUserSession("TripCompleted", tripCompleteObject.toString(), activity);

                                                        bookingType =WebServiceAPI.BOOK_NOW;

                                                        if(InfoData.has("PaymentType"))
                                                        {
                                                            paymentType = InfoData.getString("PaymentType");
                                                        }
                                                        final String amount = InfoData.getString("GrandTotal");

                                                        if(paymentType.equalsIgnoreCase("paytm"))
                                                        {
                                                            PaytmSingleton.setCallBack(activity, new CallbackPaytm() {
                                                                @Override
                                                                public void onSuccess(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }

                                                                @Override
                                                                public void onFailed(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }

                                                                @Override
                                                                public void onNetworkError() {
                                                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog("Network Problem","Error");
                                                                }

                                                                @Override
                                                                public void onClientAuthenticationFailed(String error,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog(error,"Error");*/
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }

                                                                @Override
                                                                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog(inErrorMessage,"Error");*/
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }

                                                                @Override
                                                                public void onBackPressedCancelTransaction(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog("Cancel Transaction","Error");*/
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }

                                                                @Override
                                                                public void onTransactionCancel(String inErrorMessage, Bundle inResponse,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                    /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                    errorDialogClass.showDialog("Cancel Transaction","Error");*/
                                                                    callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                }
                                                            });

                                                            playNotificationSound1();

                                                            Intent intent = new Intent(activity,PaymentPaytmActivity.class);
                                                            intent.putExtra("BookingId",InfoData.getString("Id"));
                                                            intent.putExtra("cusId",InfoData.getString("PassengerId"));
                                                            intent.putExtra("GrandTotal",InfoData.getString("GrandTotal"));
                                                            intent.putExtra("BookingType",InfoData.getString("BookingType"));
                                                            intent.putExtra("refId",InfoData.getString("ReferenceId"));
                                                            startActivity(intent);
                                                            return;
                                                        }

                                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                            openTripCompleteScreen();///30-7-19
                                                            //showTripCompletePopup(message, TruckzShipperApplication.getCurrentActivity(), BookingId, WebServiceAPI.BOOK_NOW);
                                                        } else {
                                                            openTripCompleteScreen();///30-7-19
                                                            //showTripCompletePopup(message, activity, BookingId, WebServiceAPI.BOOK_NOW);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onBookingDetails() Exception onBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };

    private void callPaytmPaymentApi(final String status_, String bookingType, String bookingId,String txId,String refId,String amount)
    {
        final DialogClass dialogClass = new DialogClass(activity, 0);
        dialogClass.showDialog();
        String url = WebServiceAPI.API_PAYMENT_PAYTM;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PAYMENT_PAYTM_BID, bookingId);
        params.put(WebServiceAPI.PAYMENT_PAYTM_BTYPE, bookingType);
        params.put(WebServiceAPI.PAYMENT_PAYTM_STATUS, status_);
        params.put(WebServiceAPI.PAYMENT_PAYTM_PAYMENTFOR, "Booking");
        params.put(WebServiceAPI.PAYMENT_PAYTM_AMOUNT, amount);

        if(txId != null && !txId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_TXID, txId);
        if(refId != null && !refId.equalsIgnoreCase(""))
            params.put(WebServiceAPI.PAYMENT_PAYTM_REFID,refId);

        Log.e(TAG, "giveRate() url = " + url);
        Log.e(TAG, "giveRate() params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "giveRate() responseCode = " + responseCode);
                    Log.e(TAG, "giveRate() Response = " + json);
                    dialogClass.hideDialog();

                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.setListener(new InternetDialog.dialogPositiveClick() {
                        @Override
                        public void onOkClick() {
                            if(!isDirectPaytmPage)
                            {openTripCompleteScreen();}
                        }
                    });
                    /*if(status_.equalsIgnoreCase("success"))
                    {
                        playNotificationSound();
                    }*/
                    internetDialog.showDialog(json.getString("message"), "Payment Status",false,false);

                    if(json.getBoolean("payment_status"))
                    {playNotificationSound(true);}
                    else
                    {playNotificationSound(false);}

                } catch (Exception e) {
                    Log.e(TAG, "giveRate()  Exception " + e.toString());
                    dialogClass.hideDialog();

                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void showTripCompletePopup(String message, final Activity activity, final String bookingId, final String bookingType) {
        Log.e(TAG, "showTripCompletePopup()");

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_trip_complete_rate);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Message = dialog.findViewById(R.id.message);
        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);
        final EditText et_Comment = (EditText) dialog.findViewById(R.id.et_Comment);
        final MaterialRatingBar rate = (MaterialRatingBar) dialog.findViewById(R.id.rate);
        ImageView dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);


        try {
            if (requestConfirmJsonObject != null) {
                if (requestConfirmJsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = requestConfirmJsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject DriverInfoData = DriverInfoArray.getJSONObject(0);

                        if (DriverInfoData != null) {
                            if (DriverInfoData.has("Fullname")) {
                                driverName = DriverInfoData.getString("Fullname");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "showTripCompletePopup() exception = " + e.getMessage());
        }

        tv_Message.setText(getResources().getString(R.string.how_was_your_experience_with) + " " + driverName + "?");

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    giveRate(rate.getRating() + "", et_Comment.getText().toString(), bookingId, bookingType);
                } else {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection), getResources().getString(R.string.no_internet_connection));
                }

            }
        });

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    giveRate(rate.getRating() + "", et_Comment.getText().toString(), bookingId, bookingType);
                } else {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection), getResources().getString(R.string.no_internet_connection));
                }

            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    giveRate(rate.getRating() + "", et_Comment.getText().toString(), bookingId, bookingType);
                } else {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.please_check_your_internet_connection), getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog.show();
        commonDialog = dialog;
    }

    public void giveRate(String rate, String comment, String bookingId, String bookingtype) {
        final DialogClass dialogClass = new DialogClass(activity, 0);
        dialogClass.showDialog();
        String url = WebServiceAPI.API_GIVE_RATTING;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_BOOKING_ID, bookingId);
        params.put(WebServiceAPI.PARAM_BOOKING_TYPE, bookingtype);
        params.put(WebServiceAPI.PARAM_RATTING, rate);
        params.put(WebServiceAPI.PARAM_COMMENT, comment);

        Log.e(TAG, "giveRate() url = " + url);
        Log.e(TAG, "giveRate() params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "giveRate() responseCode = " + responseCode);
                    Log.e(TAG, "giveRate() Response = " + json);
                    dialogClass.hideDialog();
                    //openTripCompleteScreen();///30-7-19
                } catch (Exception e) {
                    Log.e(TAG, "giveRate()  Exception " + e.toString());
                    dialogClass.hideDialog();
                    //openTripCompleteScreen();///30-7-19
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void openTripCompleteScreen() {

        passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserId", passangerId);
            jsonObject.put("UserType", "passenger");

            Common.socket.emit("ReviewRating", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (TruckzShipperApplication.getCurrentActivity() instanceof MainActivity) {
            Log.e(TAG, "openTripCompleteScreen() PickNGo_Driver_App");
        } else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }

        from = "TripCompleteActivity";
        Intent intent = new Intent(MainActivity.this, TripCompleteActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private Emitter.Listener onRatingDetails = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "onRatingDetails()");
                        if (args != null && args.length > 0) {
                            Log.e(TAG, "onRatingDetails() response = " + args[0].toString());

                            JSONObject userRating = new JSONObject(args[0].toString());

                            if (userRating != null) {
                                if (userRating.has("RatingInfo")) {
                                    String ratting = userRating.getString("RatingInfo");

                                    if (ratting != null && !ratting.toString().trim().equalsIgnoreCase("")) {
                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, userRating.getString("RatingInfo").trim() + "", activity);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onRatingDetails() Exception:- " + e.getMessage());
                    }
                }
            });
        }
    };

    private int onPickUpPassengerShow = 0;
    private Emitter.Listener onPickupPassengerNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onPickupPassengerNotification");
                        if (args != null && args.length > 0) {
                            Log.e("call", "onPickupPassengerNotification response = " + args[0].toString());
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingInfo") && jsonObject.getString("BookingInfo") != null && !jsonObject.getString("BookingInfo").equalsIgnoreCase("")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("BookingInfo");
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        JSONObject objectBookingInfo = jsonArray.getJSONObject(0);
                                        if (objectBookingInfo != null) {
                                            if (objectBookingInfo.has("Id") && objectBookingInfo.getString("Id") != null && !objectBookingInfo.getString("Id").equalsIgnoreCase("")) {
                                                if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                    if (BookingId.equalsIgnoreCase(objectBookingInfo.getString("Id"))) {
                                                        if (commonDialog != null && commonDialog.isShowing()) {
                                                            commonDialog.dismiss();
                                                        }

                                                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                            commonInternetDialog.hideDialog();
                                                        }

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                                                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                                        } else {
                                                            tripFlag = 0;
                                                        }

                                                        if (originMarker != null) {
                                                            originMarker.remove();
                                                        }

                                                        if (destinationMarker != null) {
                                                            destinationMarker.remove();
                                                        }

                                                        if (googleMap != null) {
                                                            googleMap.clear();
                                                        }

                                                        if (args != null && args.length > 0) {
                                                            Log.e("call", "onPickupPassengerNotification response = " + args[0].toString());
                                                            tv_CancelRequest.setVisibility(View.GONE);
                                                            ll_AllCarLayout.setVisibility(View.GONE);
                                                            ll_BookingLayout.setVisibility(View.GONE);
                                                            //ll_Done.setVisibility(View.GONE);
                                                            tv_DriverInfo.setVisibility(View.VISIBLE);

                                                            showPathFromPickupToDropoff(requestConfirmJsonObject);
                                                            String message = "Your trip is pick_start now";
                                                            if (jsonObject.has("message")) {
                                                                message = jsonObject.getString("message");

                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                                                                    commonInternetDialog = internetDialog;
                                                                } else {
                                                                    InternetDialog internetDialog = new InternetDialog(activity);
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                                                                    commonInternetDialog = internetDialog;
                                                                }


                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onDriverArrivedAtPickupLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        if (args[0] != null) {
                            Log.e(TAG, "onDriverArrivedAtPickupLocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
//                                ringTonePlayer.ringToneStart();

                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);


                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }


                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "Exception onDriverArrivedAtPickupLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onResendOTPForArrivedAtPickuplocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        if (args[0] != null) {
                            Log.e(TAG, "onResendOTPForArrivedAtPickuplocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);

                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception onResendOTPForArrivedAtPickuplocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onResendOTPForReachedAtDropofflocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        if (args[0] != null) {
                            Log.e(TAG, "onResendOTPForReachedAtDropofflocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);
                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception onResendOTPForReachedAtDropofflocation = " + e.getMessage());
                    }
                }
            });
        }
    };


    private Emitter.Listener onAdvanceBookingDriverArrivedAtPickupLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (args[0] != null) {
                            Log.e(TAG, "onAdvanceBookingDriverArrivedAtPickupLocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
//                                ringTonePlayer.ringToneStart();
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);


                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }


                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception onAdvancedBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };


    private Emitter.Listener onDriverReachedAtDropoffLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        if (args[0] != null) {
                            Log.e(TAG, "onDriverArrivedAtPickupLocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
//                                ringTonePlayer.ringToneStart();

                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);


                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }


                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "Exception onDriverArrivedAtPickupLocation = " + e.getMessage());
                    }
                }
            });
        }
    };


    private Emitter.Listener onAdvanceBookingDriverReachedAtDropoffLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (args[0] != null) {
                            Log.e(TAG, "onAdvanceBookingDriverArrivedAtPickupLocation() args:- " + args[0].toString());

                            if (args != null && args.length > 0) {
//                                ringTonePlayer.ringToneStart();
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        tv_CancelRequest.setVisibility(View.GONE);

                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, "Message");
                                            commonInternetDialog = internetDialog;
                                        }


                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception onAdvancedBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };


    private Emitter.Listener onReceiveMoneyNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        if (args != null && args.length > 0) {
                            Log.e("call", "arg not null and lenth greater 0 = " + args[0].toString());

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");

                                    if (TruckzShipperApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                        internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

//    private Emitter.Listener onTripHoldNotification = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    try
//                    {
//                        if (args!=null && args.length>0)
//                        {
//                            if (commonInternetDialog!=null && commonInternetDialog.isShowing())
//                            {
//                                commonInternetDialog.hideDialog();
//                            }
//
//                            if (commonDialog!=null && commonDialog.isShowing())
//                            {
//                                commonDialog.dismiss();
//                            }
//
////                            {"message":"Your trip waiting time ended"}
//                            Log.e("call","arg not null and lenth greater 0 = "+args[0].toString());
//
//                            JSONObject jsonObject = new JSONObject(args[0].toString());
//
//                            if (jsonObject!=null)
//                            {
//                                if (jsonObject.has("message"))
//                                {
//                                    String message = jsonObject.getString("message");
//                                    if (CabRideApplication.getCurrentActivity()!=null)
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(CabRideApplication.getCurrentActivity());
//                                        internetDialog.showDialog(message,"Messasge");
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                    else
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(activity);
//                                        internetDialog.showDialog(message,"Messasge");
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        Log.e("call","Exception onGetDriverLocation = "+e.getMessage());
//                    }
//                }
//            });
//        }
//    };

    private int indext = 0;

    private Emitter.Listener onGetDriverLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onGetDriverLocation 11111");
                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        } else {
                            tripFlag = 0;
                        }

                        int selectedCar = -1;
                        String strCategoryId = "1";
                        if (carType_beens != null && carType_beens.size() > 0) {
                            strCategoryId = carType_beens.get(0).getCategoryId();

                            for (int i = 0; i < carType_beens.size(); i++) {
                                if (carType_beens.get(i).getSelectedCar() != -1) {
                                    selectedCar = carType_beens.get(i).getSelectedCar();
                                }
                            }
                        }

                        Log.e("call", "onGetDriverLocation tripFlag:- " + tripFlag);
                        if (tripFlag != 0) {
                            bookingDriverLocation_beens.clear();

                            Log.e("call", "driverLocation args[0] ============================== " + args[0]);
                            if (args != null && args.length > 0) {
                                String driverLocation = args[0].toString();
                                Log.e("call", "driverLocation args[0].toString() ============================== " + driverLocation);
                                if (driverLocation != null && !driverLocation.equals("")) {
                                    JSONObject driverLocationObject = new JSONObject(driverLocation);

                                    if (driverLocationObject != null) {
                                        if (driverLocationObject.has("Location")) {
                                            JSONArray Location = driverLocationObject.getJSONArray("Location");

                                            if (Location != null && Location.length() > 0) {
                                                bookingDriverLocation_beens.add(new BookingDriverLocation_Been(Location.getDouble(0), Location.getDouble(1)));

                                                if (bookingDriverLocation_beens != null && bookingDriverLocation_beens.size() > 0) {
                                                    if (googleMap != null) {
                                                        removeCarMarker();

                                                        coordinate = new LatLng(bookingDriverLocation_beens.get(0).getLatitude(), bookingDriverLocation_beens.get(0).getLongitude());
/*
                                                        if(indext <= 21) {
                                                            coordinate = new LatLng(points1.get(indext).latitude, points1.get(indext).longitude);
                                                            indext++;*/
                                                        //sdflkjsdlfj


                                                        if (originMarker == null) {
                                                            if (googleMap != null) {
                                                                Log.e("call", "originMarker is null");

                                                                if (strCategoryId != null && strCategoryId.equals("1")) {
                                                                    if (selectedCar == 0) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 1) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 2) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 3) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 4) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 5) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 6) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    }
                                                                } else if (strCategoryId != null && strCategoryId.equals("2")) {
                                                                    if (selectedCar == 0) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 1) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 2) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 3) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 4) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 5) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    } else if (selectedCar == 6) {
                                                                        originMarker = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                                                                    }
                                                                }

                                                                if (ployList != null && ployList.size() > 0) {
                                                                    if (PolyUtil.isLocationOnEdge(coordinate, ployList, true, 100)) {
                                                                        animateMarker(originMarker, coordinate);
                                                                    } else {
                                                                        if (polyline != null) {
                                                                            polyline.remove();
                                                                        }
                                                                        driverLatLng = new LatLng(coordinate.latitude, coordinate.longitude);
                                                                        showRoute(1);
                                                                    }
                                                                } else {
                                                                    animateMarker(originMarker, coordinate);
                                                                }
                                                            }
                                                        } else {
                                                            if (ployList != null && ployList.size() > 0) {
                                                                if (PolyUtil.isLocationOnEdge(coordinate, ployList, true, 100)) {
                                                                    animateMarker(originMarker, coordinate);
                                                                } else {
                                                                    if (polyline != null) {
                                                                        polyline.remove();
                                                                    }
                                                                    driverLatLng = new LatLng(coordinate.latitude, coordinate.longitude);
                                                                    showRoute(1);
                                                                }
                                                            } else {
                                                                animateMarker(originMarker, coordinate);
                                                            }
                                                        }
                                                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
                                                        zoomFlag = 1;

                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

//    int i=1;

    public void animateMarker(final Marker marker, final LatLng destLatLng) {
        Log.e("call", "destLatLng lat = " + destLatLng.latitude);
        Log.e("call", "destLatLng long = " + destLatLng.longitude);

        double[] startValues = new double[]{marker.getPosition().latitude, marker.getPosition().longitude};
        double[] endValues = new double[]{destLatLng.latitude, destLatLng.longitude};
        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(4900);
        latLngAnimator.setInterpolator(new LinearInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                double[] animatedValue = (double[]) animation.getAnimatedValue();

                float bearing = getBearing(marker.getPosition(), destLatLng);
                if (bearing > 0) {
                    marker.setFlat(true);
                    marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
                    marker.setRotation(bearing);
                }
            }
        });
        latLngAnimator.start();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) ((Math.toDegrees(Math.atan(lng / lat))));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (Common.socket == null) {
                        if (handler != null && runnable != null) {
                            handler.removeCallbacks(runnable);
                        }
                        Log.e("call", "socket disconnected successfully");
                    }
                }
            });
        }
    };

    public void disconnectSocket() {
        Log.e("call", "disconnectSocket()==============================");
        try {
            if (Common.socket != null && Common.socket.connected()) {
                Common.socket.disconnect();
                Common.socket.off("NearByDriverList", onDriverList);
                Common.socket.off("PassengerCancelTripNotification", onPassengerCancelTripNotification);
                Common.socket.off(Socket.EVENT_CONNECT, onConnect);
                Common.socket.off("AcceptBookingRequestNotification", onAcceptBookingRequestNotification);
                Common.socket.off("RejectBookingRequestNotification", onRejectBookingRequestNotification);
                Common.socket.off("GetDriverLocation", onGetDriverLocation);
//                Common.socket.off("TripHoldNotification",onTripHoldNotification);
                Common.socket.off("PickupPassengerNotification", onPickupPassengerNotification);
                Common.socket.off("DriverArrivedAtPickupLocation", onDriverArrivedAtPickupLocation);
                Common.socket.off("DriverReachedAtDropoffLocation", onDriverReachedAtDropoffLocation);
                Common.socket.off("ResendOTPForArrivedAtPickuplocation", onResendOTPForArrivedAtPickuplocation);
                Common.socket.off("ResendOTPForReachedAtDropofflocation", onResendOTPForReachedAtDropofflocation);

                Common.socket.off("BookingDetails", onBookingDetails);
                Common.socket.off("GetEstimateFare", onGetEstimateFare);
                Common.socket.off("ReceiveMoneyNotify", onReceiveMoneyNotify);
                Common.socket.off("RatingDetails", onRatingDetails);

                Common.socket.off("AcceptAdvancedBookingRequestNotification", onAcceptAdvancedBookingRequestNotification);
                Common.socket.off("RejectAdvancedBookingRequestNotification", onRejectAdvancedBookingRequestNotification);
//                Common.socket.off("InformPassengerForAdvancedTrip",InformPassengerForAdvancedTrip);
                Common.socket.off("AcceptAdvancedBookingRequestNotify", AcceptAdvancedBookingRequestNotify);
                Common.socket.off("AdvancedBookingPickupPassengerNotification", onAdvancedBookingPickupPassengerNotification);
                Common.socket.off("AdvancedBookingTripHoldNotification", onAdvancedBookingTripHoldNotification);
                Common.socket.off("AdvancedBookingDetails", onAdvancedBookingDetails);
                Common.socket.off("AdvanceBookingDriverArrivedAtPickupLocation", onAdvanceBookingDriverArrivedAtPickupLocation);
                Common.socket.off("AdvanceBookingDriverReachedAtDropoffLocation", onAdvanceBookingDriverReachedAtDropoffLocation);


                Common.socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
                Common.socket = null;
                SocketSingleObject.instance = null;
                Log.e("call", "disconnectSocket()111111==============================");
            } else {
                Log.e("call", "Socket already disconnected.");
            }
            setTimeAndPriceToZero();
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("call", "onDestroy onDestroy onDestroy onDestroy");
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }
        if (handlerProfile != null && runnableProfile != null) {
            handlerProfile.removeCallbacks(runnableProfile, null);
        }
        if (handlerSendRequest != null && runnableSendRequest != null) {
            handlerSendRequest.removeCallbacks(runnableSendRequest, null);
        }
        if (handlerDriver != null && runnableDriver != null) {
            handlerDriver.removeCallbacks(runnableDriver, null);
        }

        disconnectSocket();
    }


    public void startTimer() {

        Log.e(TAG, "startTimer() timer is started 111 ");

        int tripFlag = 0;

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        }

        if (tripFlag == 0) {
            if (handlerDriver != null && runnableDriver != null) {
                handlerDriver.removeCallbacks(runnableDriver, null);
            }
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable, null);
            }
            runnable = new Runnable() {

                @Override
                public void run() {
                    try {
                        if (Common.socket != null && Common.socket.connected()) {
                            if (gpsTracker.canGetLocation()) {
                                gpsTracker = new GPSTracker(MainActivity.this);
                                Constants.newgpsLatitude = gpsTracker.location.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.location.getLongitude() + "";

                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("PassengerId", passangerId);

                                if (TaxiUtil.picup_Lat != 0 && TaxiUtil.picup_Long != 0) {
                                    Log.e(TAG, "startTimer() +++++++++++++++++++ pass pickup lat long");
                                    jsonObject.put("Lat", TaxiUtil.picup_Lat);
                                    jsonObject.put("Long", TaxiUtil.picup_Long);
                                } else {
                                    if (kalmanLat != 0 && kalmanLong != 0) {
                                        Log.e(TAG, "startTimer() +++++++++++++++++++ pass kalmanLat lat long");

                                        jsonObject.put("Lat", kalmanLat);
                                        jsonObject.put("Long", kalmanLong);
                                    } else {
                                        Log.e("call", "+++++++++++++++++++ pass gpsTracker lat long");

                                        jsonObject.put("Lat", gpsTracker.getLatitude());
                                        jsonObject.put("Long", gpsTracker.getLongitude());
                                    }
                                }


                                jsonObject.put("Token", token);

                                if (token != null && !token.equalsIgnoreCase("")) {
                                    Log.e(TAG, "startTimer() UpdatePassengerLatLong json = " + jsonObject.toString());
                                    Common.socket.emit("UpdatePassengerLatLong", jsonObject);
                                }
                            } else {
                                Constants.newgpsLatitude = "0";
                                Constants.newgpsLongitude = "0";
                            }
                        } else {
                            Log.e(TAG, "startTimer() socket is not connected.......");
                            setTimeAndPriceToZero();
                        }
                        handler.postDelayed(runnable, 5000);
                    } catch (Exception e) {
                        Log.e(TAG, "startTimer() error in sending latlong");
                        setTimeAndPriceToZero();
                    }
                }
            };
            handler.postDelayed(runnable, 0);
        } else {
            Log.e(TAG, "startTimer() trip flag from start timer = " + tripFlag);
        }

    }

    public void updatePassenger() {
        Log.e("call", "updatePassenger()");
        try {
            if (Common.socket != null && Common.socket.connected()) {
                if (gpsTracker.canGetLocation()) {
                    gpsTracker = new GPSTracker(MainActivity.this);
                    Constants.newgpsLatitude = gpsTracker.location.getLatitude() + "";
                    Constants.newgpsLongitude = gpsTracker.location.getLongitude() + "";

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("PassengerId", passangerId);

                    if (TaxiUtil.picup_Lat != 0 && TaxiUtil.picup_Long != 0) {
                        Log.e("call", "+++++++++++++++++++ pass pickup lat long");
                        jsonObject.put("Lat", TaxiUtil.picup_Lat);
                        jsonObject.put("Long", TaxiUtil.picup_Long);
                    } else {
                        if (kalmanLat != 0 && kalmanLong != 0) {
                            Log.e("call", "+++++++++++++++++++ pass kalmanLat lat long");

                            jsonObject.put("Lat", kalmanLat);
                            jsonObject.put("Long", kalmanLong);
                        } else {
                            Log.e("call", "+++++++++++++++++++ pass gpsTracker lat long");

                            jsonObject.put("Lat", gpsTracker.getLatitude());
                            jsonObject.put("Long", gpsTracker.getLongitude());
                        }
                    }


                    jsonObject.put("Token", token);

                    if (token != null && !token.equalsIgnoreCase("")) {
                        Log.e("call", "UpdatePassengerLatLong json = " + jsonObject.toString());
                        Common.socket.emit("UpdatePassengerLatLong", jsonObject);
                    }
                } else {
                    Constants.newgpsLatitude = "0";
                    Constants.newgpsLongitude = "0";
                }
            } else {
                Log.e("call", "socket is not connected.......");
                setTimeAndPriceToZero();
            }
            handler.postDelayed(runnable, 5000);
        } catch (Exception e) {
            Log.e("call", "error in sending latlong");

            setTimeAndPriceToZero();
        }
    }

    public void startTimerForDriverLocation() {
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        }

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }
        if (handlerDriver != null && runnableDriver != null) {
            handlerDriver.removeCallbacks(runnableDriver, null);
        }
        Log.e("call", "startTimerForDriverLocation() tripFlag = " + tripFlag);
        runnableDriver = new Runnable() {

            @Override
            public void run() {
                try {
                    if (tripFlag != 0) {
                        Log.e("call", "startTimerForDriverLocation() tripFlag !=0 " + tripFlag);
                        if (Common.socket != null) {
                            Log.e("call", "startTimerForDriverLocation() tripFlag !=null " + tripFlag);
                            if (Common.socket.connected()) {
                                Log.e("call", "startTimerForDriverLocation() Common.socket.connected()");
                                if (requestConfirmJsonObject != null) {
                                    Log.e("call", "requestConfirmJsonObject!=null");
                                    if (requestConfirmJsonObject.has("DriverInfo")) {
                                        Log.e("call", "requestConfirmJsonObject.has(DriverInfo)");
                                        JSONArray DriverInfo = requestConfirmJsonObject.getJSONArray("DriverInfo");
                                        if (DriverInfo != null && DriverInfo.length() > 0) {
                                            Log.e("call", "DriverInfo!=null");
                                            JSONObject driverObject = DriverInfo.getJSONObject(0);

                                            if (driverObject != null) {
                                                Log.e("call", "driverObject!=null");
                                                driverId = driverObject.getString("Id");

                                                if (driverId != null && !driverId.equals("")) {
                                                    Log.e("call", "driverId!=null");

                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("PassengerId", passangerId + "");
                                                    jsonObject.put("DriverId", driverId + "");
                                                    Log.e("call", "startTimerForDriverLocation object = " + jsonObject.toString());
                                                    Common.socket.emit("DriverLocation", jsonObject);
                                                } else {
                                                    Log.e("callElse", "driverId!=null");
                                                }
                                            } else {
                                                Log.e("callElse", "driverObject!=null");
                                            }
                                        } else {
                                            Log.e("callElse", "DriverInfo!=null");
                                        }
                                    } else {
                                        Log.e("callElse", "requestConfirmJsonObject.has(DriverInfo)");
                                    }
                                } else {
                                    Log.e("callElse", "requestConfirmJsonObject!=null");
                                }
                            } else {
                                Log.e("callElse", "startTimerForDriverLocation() Common.socket.connected()");
                            }
                        } else {
                            Log.e("callElse", "startTimerForDriverLocation() tripFlag !=null " + tripFlag);
                        }
                        handlerDriver.postDelayed(runnableDriver, 5000);
                    } else {
                        Log.e("callElse", "startTimerForDriverLocation() tripFlag !=0 " + tripFlag);
                    }
                } catch (Exception e) {
                    Log.e("call", "error in sending latlong");
                }
            }
        };
        handlerDriver.postDelayed(runnableDriver, 0);
    }

    Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                // TODO: do what you need here
                Log.e(TAG, "Typing getting slow");
                if (pickDropoffType == 1) {
                    //ll_Done.setVisibility(View.VISIBLE);
                    addFlag = 1;
                    double latDoulle = 0, longDouble = 0;
                    if (gpsTracker.canGetLocation()) {
                        latDoulle = gpsTracker.getLatitude();
                        longDouble = gpsTracker.getLongitude();
                    }

                    /*AIzaSyBOjgWLA5U0xHC-RZ0xZqv2eAxK8m28DJg*/
                    search_text = auto_Pickup.getText().toString().split(",");
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" +
                            search_text[0].replace(" ", "%20") + "&location=" + latDoulle + "," + longDouble + "&radius=1000&sensor=true&key=" + getResources().getString(R.string.map_api_key) + "&components=&language=en";
                    Log.e(TAG, "auto_Pickup autocomplete url :- " + jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();

                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();
                } else {
                    //ll_Done.setVisibility(View.VISIBLE);
                    addFlag = 2;
                    double latDoulle = 0, longDouble = 0;
                    if (gpsTracker.canGetLocation()) {
                        latDoulle = gpsTracker.getLatitude();
                        longDouble = gpsTracker.getLongitude();
                    }
                    search_text = auto_Dropoff.getText().toString().split(",");
                    jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20") + "&location=" + latDoulle + "," + longDouble + "&radius=1000&sensor=true&key=" + getResources().getString(R.string.map_api_key) + "&components=&language=en";
                    Log.e(TAG, "auto_Dropoff autocomplete url :- " + jsonurl);
                    names = new ArrayList<String>();
                    Place_id_type = new ArrayList<String>();
                    if (parse != null) {
                        parse.cancel(true);
                        parse = null;
                    }
                    parse = new ParseData();
                    parse.execute();
                }
            }
        }
    };

    private void setNoSelectedCarBackGround() {
        removeCarMarker();

        myLocation = true;
        selectedCarPosition = -1;

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                carType_beens.get(i).setSelectedCar(-1);
            }
        }

        if (carAdapter != null) {
            carAdapter.notifyDataSetChanged();
            recyclerViewCar.scrollToPosition(0);
        }
    }

    private void setUserDetail() {
        Log.e(TAG, "setUserDetail()");
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity).equalsIgnoreCase("")
                && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity).equalsIgnoreCase("null")) {
            ratting = Float.parseFloat(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity));
        }

        if (ratting > 0) {
            tv_Rate.setText(ratting + "");
        } else {
            tv_Rate.setText("0");
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this).equals("")) {
            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, MainActivity.this))
                    .into(iv_ProfileImage);
        } else {
            iv_ProfileImage.setImageResource(R.mipmap.man);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this).equals("")) {
            tv_UserName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, MainActivity.this));
        } else {
            tv_UserName.setVisibility(View.GONE);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, MainActivity.this) != null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, MainActivity.this).equals("") &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, MainActivity.this).equals("null")) {
            tvUserEmail.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, MainActivity.this));
        } else {
            tvUserEmail.setVisibility(View.GONE);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, MainActivity.this) != null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, MainActivity.this).equals("")) {
            ll_rating.setVisibility(View.VISIBLE);
            String rating = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, MainActivity.this);
            if(rating != null && !rating.equalsIgnoreCase("") && !rating.equalsIgnoreCase("null") && !rating.equalsIgnoreCase("NULL"))
            {tv_rating.setText(String.format("%.1f", Double.parseDouble(rating)));}
        } else {
            ll_rating.setVisibility(View.GONE);
            tv_rating.setVisibility(View.GONE);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this).equals("")) {
            tv_UserPhone.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, MainActivity.this));
        } else {
            tv_UserPhone.setVisibility(View.GONE);
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    } // Author: silentnuke

    public void getAddressFromLatLong() {
        try {
            Log.e("111111111111", "getAddressFromLatLong trip flag = " + tripFlag);

            if (tripFlag == 0) {
                gpsTracker = new GPSTracker(activity);

                if (gpsTracker != null && gpsTracker.canGetLocation()) {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(this, Locale.getDefault());

                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();

                    if (latitude != 0 && longitude != 0) {
                        getEstimated_lat = latitude;
                        getEstimated_long = longitude;

                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        Log.e("111111111111", "address = " + address);
                        Log.e("111111111111", "city = " + city);
                        Log.e("111111111111", "state = " + state);
                        Log.e("111111111111", "country = " + country);
                        Log.e("111111111111", "postalCode = " + postalCode);
                        Log.e("111111111111", "knownName = " + knownName);

                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        getEstimated_Pickup = address;
                        auto_Pickup.setText(address);
                        tvPickup.setText(address);
                        TaxiUtil.picup_Address = address;
                        TaxiUtil.picup_Lat = latitude;
                        TaxiUtil.picup_Long = longitude;
                        pickup_Touch = true;
                        auto_Pickup.dismissDropDown();
                        //ll_Done.setVisibility(View.VISIBLE);

                        auto_Pickup.setFocusableInTouchMode(true);
                        auto_Pickup.requestFocus();
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap1) {

        Log.e("call", "onMapReady()");
        googleMap = googleMap1;
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        MapsInitializer.initialize(MainActivity.this);

        gestureDetector = new ScaleGestureDetector(activity, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                if (lastSpan == -1) {
                    lastSpan = detector.getCurrentSpan();
                } else if (detector.getEventTime() - lastZoomTime >= 50) {
                    lastZoomTime = detector.getEventTime();
                    googleMap.animateCamera(CameraUpdateFactory.zoomBy(getZoomValue(detector.getCurrentSpan(), lastSpan)), 50, null);
                    lastSpan = detector.getCurrentSpan();
                }
                return false;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                lastSpan = -1;
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                lastSpan = -1;

            }
        });


//        googleMap.setOnMarkerDragListener(this);
        googleMap.setOnCameraChangeListener(this);
        googleMap.setOnCameraIdleListener(this);

        googleMap.setMaxZoomPreference(18.3f);

        googleMap.setOnCameraMoveStartedListener(this);

        int tripFlag = 0;
        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        }
        Log.e("call", "onMapReady tripFlag = " + tripFlag);
        if (tripFlag != 0) {
            removeCarMarker();

            tv_DriverInfo.setVisibility(View.VISIBLE);
            if (tripFlag == 1) {
                tv_CancelRequest.setVisibility(View.VISIBLE);
                Log.e("call", "parseAcceptRequestObject()");
                parseAcceptRequestObject(requestConfirmJsonObject, 1);
            } else if (tripFlag == 2) {
                tv_CancelRequest.setVisibility(View.GONE);
                Log.e("call", "showPathFromPickupToDropoff()");
                showPathFromPickupToDropoff(requestConfirmJsonObject);
            }
        } else {
            Log.e("call", "myLocation:-  " + myLocation);
            if (myLocation) {
                googleMap.clear();
                Log.e("call", "initMap 3333333333");
                gpsTracker = new GPSTracker(activity);
                Log.e("call", "initMap gpsTracker.getLatitude() = " + gpsTracker.getLatitude());
                Log.e("call", "initMap gpsTracker.getLongitude() = " + gpsTracker.getLongitude());

                coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

//                markerPickup = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                markerPickup.setTag("1");
//                markerPickup.setDraggable(true);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
            } else {
                Log.e("call", "myLocation = " + myLocation);
            }
        }
    }

    private float getZoomValue(float currentSpan, float lastSpan) {
        double value = (Math.log(currentSpan / lastSpan) / Math.log(1.55d));
        return (float) value;
    }


    public void checkGPS() {
        Log.e("call", "1111111111111");
        if (checkPermission()) {
            gpsTracker = new GPSTracker(activity);
            Log.e("call", "222222222222");
            if (gpsTracker.canGetLocation()) {
                Log.e("call", "3333333333333");
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                Log.e("3333333333333 latitude", "" + Constants.newgpsLatitude);
                Log.e("3333333333333 longitude", "" + Constants.newgpsLongitude);

                getMyLocation();
            } else {
                Log.e("call", "44444444444444");
                AlertMessageNoGps();
            }
        } else {
            Log.e("call", "5555555555555");
            requestPermission();
        }
    }

    public void initMap() {
        Log.e("call", "initMap");
        try {
            gpsTracker = new GPSTracker(activity);
            coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (googleMap == null) {
                Log.e("call", "initMap googleMap null");
                SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFrag.getMapAsync(this);
            } else {
                Log.e("call", "initMap googleMap not null");
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setRotateGesturesEnabled(false);
//                googleMap.setOnMarkerDragListener(this);

                MapsInitializer.initialize(MainActivity.this);
                googleMap.setOnCameraChangeListener(this);
                googleMap.setOnCameraIdleListener(this);
                //googleMap.setOnCameraMoveListener(this);
                googleMap.setOnCameraMoveStartedListener(this);

                removeCarMarker();

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

                googleMap.clear();
                Log.e("call", "initMap 3333333333");
//                markerPickup = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                markerPickup.setDraggable(true);
//                markerPickup.setTag("1");
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Log.e("call", "intMap map load exception" + e.getMessage());
        }
    }

    public void getMyLocation() {
        Log.e("call", "initMap");
        try {
            coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (googleMap == null) {
                Log.e("call", "initMap 11111111111111");
                SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFrag.getMapAsync(this);
            } else {
                Log.e("call", "initMap 22222222222");
                if (googleMap != null) {
                    googleMap.clear();
                }
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setRotateGesturesEnabled(false);
                //googleMap.getUiSettings().setZoomGesturesEnabled(false);
                MapsInitializer.initialize(MainActivity.this);
//                googleMap.setOnMarkerDragListener(this);
                googleMap.setOnCameraChangeListener(this);
                googleMap.setOnCameraIdleListener(this);
                //googleMap.setOnCameraMoveListener(this);
                googleMap.setOnCameraMoveStartedListener(this);

                getAddressFromLatLong();
//
//                auto_Dropoff.setText("");
//                TaxiUtil.dropoff_Address = "";
//                TaxiUtil.dropoff_Lat= 0;
//                TaxiUtil.dropoff_Long= 0;
//                dropoff_Touch = false;
//                favorite = 0;
//                favorite_address = "";
//                from = "";
//                setTimeAndPriceToZero();

                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();

                Log.e("call", "initMap 3333333333");
//                markerPickup = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                markerPickup.setDraggable(true);
//                markerPickup.setTag("1");
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));

//                getEstimatedFareCheckValidation();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            System.out.println("map load" + e.getMessage());
        }
    }

    private boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("call", "onRequestPermissionsResult() 11 = " + requestCode);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("call", "onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation()) {
                        Log.e("call", "onRequestPermissionsResult() 33 = ");
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    } else {
                        Log.e("call", "onRequestPermissionsResult() 44 = ");
                        AlertMessageNoGps();
                    }
                } else {
                    Log.e("call", "onRequestPermissionsResult() 55 = ");
                    MyAlertDialog dialog = new MyAlertDialog(MainActivity.this);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

    public void AlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>" + getResources().getString(R.string.new_gps_settings_message) + "</font>"));

        String positiveText = getResources().getString(R.string.new_gps_settings_positive_button);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        String negativeText = getResources().getString(R.string.new_gps_settings_nagative_button);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorThemeDark));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorThemeDark));
            }
        });

        dialog.show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (Constants.newgpsLongitude.equals("0") && Constants.newgpsLongitude.equals("0")) {
                gpsTracker = new GPSTracker(activity);

                if (gpsTracker.canGetLocation()) {
                    Log.e("onPostResum", "call");
                }
            }
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("call", "onConnect onConnect onConnect onConnect");

                    if (Common.socket != null && Common.socket.connected()) {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }
                        Log.e("call", "onConnect onConnect onConnect tripFlag = " + tripFlag);
                        if (tripFlag == 0) {
                            /*--------------28-6-19----------------*/
                            startTimer();
                            /*--------------28-6-19----------------*/
                        } else {
                            updatePassenger();
                            startTimerForDriverLocation();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onAcceptBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        isAlreadySendRequest = false;
                        handlerCount = 0;
                        if (handlerSendRequest != null && runnableSendRequest != null) {
                            handlerSendRequest.removeCallbacks(runnableSendRequest, null);
                        }

                        if (args != null && args.length > 0) {
//                            try
//                            {
//                                ringTone = MediaPlayer.create(activity, R.raw.tone);
//                                ringTone.start();
//                            }
//                            catch (Exception e)
//                            {
//                                e.printStackTrace();
//                            }


                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            advanceBooking = 0;
                            zoomFlag = 0;

                            tripFlag = 1;
                            Log.e("call", "onAcceptBookingRequestNotification response = " + args[0].toString());

                            int selectedCar = -1;

                            if (carType_beens != null && carType_beens.size() > 0) {
                                for (int i = 0; i < carType_beens.size(); i++) {
                                    if (carType_beens.get(i).getSelectedCar() != -1) {
                                        selectedCar = carType_beens.get(i).getSelectedCar();
                                    }
                                }
                            }

                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (selectedCar != -1) {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                            }

                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, jsonObject.toString(), activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);

                            acceptBooking = 1;

                            requestPendingDialogClass.hideDialog();
                            parseAcceptRequestObject(jsonObject, 0);
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception in onAcceptBookingRequestNotification = " + e.getMessage());
                    }
                }
            });
        }
    };

    public void parseAcceptRequestObject(JSONObject jsonObject, int flag) {
        try {
            Log.e("call", "parseAcceptRequestObject");
            if (jsonObject != null) {
                if (jsonObject.has("BookingInfo")) {
                    JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");

                    if (BookingInfo != null && BookingInfo.length() > 0) {
                        JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                        if (BookingInfoObject != null) {
                            double lat = 0, lng = 0;

                            if (BookingInfoObject.has("PickupLat")) {
                                lat = BookingInfoObject.getDouble("PickupLat");
                            }

                            if (BookingInfoObject.has("PickupLng")) {
                                lng = BookingInfoObject.getDouble("PickupLng");
                            }

                            if (BookingInfoObject.has("Id")) {
                                BookingId = BookingInfoObject.getString("Id");
                            }

                            Log.e("Call", "parseAcceptRequestObject Details ====================================== pickup lat = " + lat);
                            Log.e("Call", "parseAcceptRequestObject Details ====================================== pickup long = " + lng);
                            pickUp = new LatLng(lat, lng);
                        }
                    }
                }

                if (jsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = jsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject dropObject = DriverInfoArray.getJSONObject(0);

                        if (dropObject != null) {
                            double lat = 0, lng = 0;

                            if (dropObject.has("Lat")) {
                                lat = Double.parseDouble(dropObject.getString("Lat"));
                            }

                            if (dropObject.has("Lng")) {
                                lng = Double.parseDouble(dropObject.getString("Lng"));
                            }

                            if (lat != 0.0 && lng != 0.0) {
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lat);
                                Log.e("Call", "parseAcceptRequestObject DriverInfo ====================================== driverLatLng = " + lng);
                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                            } else {
                                driverLatLng = null;
                            }
                        }
                    }
                }
            }


            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));

            Log.e("call", "-----------------flag------------ = " + flag);
            if (flag == 1) {
                if (pickUp != null && driverLatLng != null) {
                    Log.e("call", "-----------------showRoute------------ = ");
                    showRoute(1);
                }
            } else {
                if (requestConfirmJsonObject != null) {
                    ll_BookingLayout.setVisibility(View.GONE);
                    ll_AllCarLayout.setVisibility(View.GONE);
                    ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                    tv_CancelRequest.setVisibility(View.VISIBLE);
                    tv_DriverInfo.setVisibility(View.VISIBLE);
                    address_Layout.setVisibility(View.GONE);


                    if (pickUp != null && driverLatLng != null) {
                        Log.e("call", "----------------- displayAcceptRequestPopup showRoute--------------");
                        showRoute(1);
                    } else {
                        Log.e("call", "----------------- displayAcceptRequestPopup showRoute not call--------------");
                    }

                    Log.e("call", "-----------------displayAcceptRequestPopup requestConfirmJsonObject!=null ------------ = ");
                    String message = getResources().getString(R.string.your_booking_request_has_been_confirmed);
                    if (requestConfirmJsonObject.has("message")) {
                        message = requestConfirmJsonObject.getString("message");
                    }

                    if (TruckzShipperApplication.getCurrentActivity() != null) {
                        displayAcceptRequestPopup(message, TruckzShipperApplication.getCurrentActivity());
                    } else {
                        displayAcceptRequestPopup(message, activity);
                    }

                }
            }
        } catch (Exception e) {
            Log.e("call", "parseAcceptRequestObject exception = " + e.getMessage());
        }

    }

    public void showPathFromPickupToDropoff(JSONObject jsonObject) {
        try {
            Log.e("call", "showPathFromPickupToDropoff");
            if (jsonObject != null) {
                if (jsonObject.has("BookingInfo")) {
                    JSONArray BookingInfoArray = jsonObject.getJSONArray("BookingInfo");

                    if (BookingInfoArray != null && BookingInfoArray.length() > 0) {
                        JSONObject BookingInfo = BookingInfoArray.getJSONObject(0);

                        if (BookingInfo != null) {
                            double latPickup = 0, lngPickup = 0;
                            double latDropoff = 0, lngDropoff = 0;

                            if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
                                tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                Log.e("call", "onCreate() tripFlag if = " + tripFlag);
                            }
                            if (tripFlag == 0) {
                                if (BookingInfo.has("PickupLat")) {
                                    latPickup = BookingInfo.getDouble("PickupLat");
                                }

                                if (BookingInfo.has("PickupLng")) {
                                    lngPickup = BookingInfo.getDouble("PickupLng");
                                }

                                if (BookingInfo.has("DropOffLat")) {
                                    latDropoff = BookingInfo.getDouble("DropOffLat");
                                }

                                if (BookingInfo.has("DropOffLon")) {
                                    lngDropoff = BookingInfo.getDouble("DropOffLon");
                                }

                                driverLatLng = new LatLng(latPickup, lngPickup);
                            } else if (tripFlag == 1) {
                                if (BookingInfo.has("PickupLat")) {
                                    latDropoff = BookingInfo.getDouble("PickupLat");
                                }

                                if (BookingInfo.has("PickupLng")) {
                                    lngDropoff = BookingInfo.getDouble("PickupLng");
                                }

                                if (jsonObject.has("DriverInfo")) {
                                    Log.e("call", "DriverInfo 111");
                                    JSONArray DriverInfo = jsonObject.getJSONArray("DriverInfo");
                                    Log.e("call", "DriverInfo 222");
                                    if (DriverInfo != null && DriverInfo.length() > 0) {
                                        Log.e("call", "DriverInfo 333");
                                        JSONObject dropObject = DriverInfo.getJSONObject(0);

                                        if (dropObject != null) {
                                            Log.e("call", "DriverInfo 444");
                                            double lat = 0, lng = 0;

                                            if (dropObject.has("Lat")) {
                                                Log.e("call", "DriverInfo 555");
                                                lat = Double.parseDouble(dropObject.getString("Lat"));
                                                Log.e("call", "DriverInfo 666");
                                            }

                                            if (dropObject.has("Lng")) {
                                                Log.e("call", "DriverInfo 777");
                                                lng = Double.parseDouble(dropObject.getString("Lng"));
                                                Log.e("call", "DriverInfo 888");
                                            }

                                            if (lat != 0.0 && lng != 0.0) {
                                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                                            } else {
                                                driverLatLng = null;
                                            }
                                        }
                                    }
                                }
                            } else {
                                Log.e("bbbbbbbbbbbbbbb", "1111111111 : jsonObject : " + jsonObject.toString());
                                if (jsonObject.has("DriverInfo")) {
                                    Log.e("bbbbbbbbbbbbbbb", "2222222222222 : ");
                                    if (BookingInfo.has("DropOffLat")) {
                                        latDropoff = BookingInfo.getDouble("DropOffLat");
                                    }

                                    if (BookingInfo.has("DropOffLon")) {
                                        lngDropoff = BookingInfo.getDouble("DropOffLon");
                                    }
                                    Log.e("call", "DriverInfo 111");
                                    JSONArray DriverInfo = jsonObject.getJSONArray("DriverInfo");
                                    Log.e("call", "DriverInfo 222");
                                    if (DriverInfo != null && DriverInfo.length() > 0) {
                                        Log.e("call", "DriverInfo 333");
                                        JSONObject dropObject = DriverInfo.getJSONObject(0);

                                        if (dropObject != null) {
                                            Log.e("call", "DriverInfo 444");
                                            double lat = 0, lng = 0;

                                            if (dropObject.has("Lat")) {
                                                Log.e("call", "DriverInfo 555");
                                                lat = Double.parseDouble(dropObject.getString("Lat"));
                                                Log.e("call", "DriverInfo 666");
                                            }

                                            if (dropObject.has("Lng")) {
                                                Log.e("call", "DriverInfo 777");
                                                lng = Double.parseDouble(dropObject.getString("Lng"));
                                                Log.e("call", "DriverInfo 888");
                                            }

                                            if (lat != 0.0 && lng != 0.0) {
                                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                                            } else {
                                                driverLatLng = null;
                                            }
                                        }
                                    }
                                }
                            }


                            if (BookingInfo.has("Id")) {
                                BookingId = BookingInfo.getString("Id");
                            }

                            Log.e("Call", "BookingInfo ====================================== pickup lat = " + latPickup);
                            Log.e("Call", "BookingInfo ====================================== pickup long = " + lngPickup);
                            Log.e("Call", "BookingInfo ====================================== dropoff lat = " + latDropoff);
                            Log.e("Call", "BookingInfo ====================================== dropoff long = " + lngDropoff);

                            pickUp = new LatLng(latDropoff, lngDropoff);
                        }
                    }
                }
            }

            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));

            if (pickUp != null && driverLatLng != null) {
                showRoute(1);
            }
        } catch (Exception e) {
            Log.e("call", "parseAcceptRequestObject exception = " + e.getMessage());
        }

    }

    public void displayAcceptRequestPopup(String message, Activity activity) {
        Log.e("call", "11111111111111 displayAcceptRequestPopup");
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        TextView tv_Message = dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
//                if (ringTone!=null && ringTone.isPlaying())
//                {
//                    ringTone.stop();
//                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                if (ringTone!=null && ringTone.isPlaying())
//                {
//                    ringTone.stop();
//                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                if (ringTone!=null && ringTone.isPlaying())
//                {
//                    ringTone.stop();
//                }
            }
        });
        commonDialog = dialog;
    }

    public void showDriverInfoPopup() {
        Log.e(TAG, "showDriverInfoPopup()");
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        String Name = "", company = "", pickupLocation = "", dropOffLocation = "", image = "";
        String PickupLocation = "", DropoffLocation = "", driver_name = "", driver_image = "", driver_phone = "", vehicleRegistrationNo = "", rating = "";

        try {
            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));

            Log.e("call", "requestConfirmJsonObject 111= " + requestConfirmJsonObject.toString());
            if (requestConfirmJsonObject != null) {
                if (requestConfirmJsonObject.has("BookingInfo")) {
                    JSONArray BookingInfoArray = requestConfirmJsonObject.getJSONArray("BookingInfo");

                    if (BookingInfoArray != null && BookingInfoArray.length() > 0) {
                        JSONObject BookingInfo = BookingInfoArray.getJSONObject(0);

                        if (BookingInfo != null) {
                            if (BookingInfo.has("PickupLocation")) {
                                PickupLocation = BookingInfo.getString("PickupLocation");
                            }

                            if (BookingInfo.has("DropoffLocation")) {
                                DropoffLocation = BookingInfo.getString("DropoffLocation");
                            }
                        }
                    }
                }

                if (acceptBooking == 0) {
                    Log.e("acceptBooking", "acceptBooking0000000000");

                    if (requestConfirmJsonObject.has("CarInfo")) {
                        JSONArray CarInfoArray = requestConfirmJsonObject.getJSONArray("CarInfo");

                        if (CarInfoArray != null && CarInfoArray.length() > 0) {
                            JSONObject CarInfo = CarInfoArray.getJSONObject(0);

                            if (CarInfo != null) {
                                JSONObject Model = null;

                                if (CarInfo.has("Model")) {
                                    Model = CarInfo.getJSONObject("Model");
                                }

                                if (CarInfo.has("VehicleRegistrationNo")) {
                                    vehicleRegistrationNo = CarInfo.getString("VehicleRegistrationNo");
                                }

                                if (CarInfo.has("Company")) {
                                    company = CarInfo.getString("Company");
                                }

                                if (CarInfo.has("Color")) {
                                    company = company + " " + CarInfo.getString("Color");
                                }

                                if (CarInfo.has("VehicleImage")) {
                                    image = WebServiceAPI.BASE_URL_IMAGE + CarInfo.getString("VehicleImage");
                                }

                                if (Model != null) {
                                    if (Model.has("Name")) {
                                        Name = Model.getString("Name");
                                    } else {
                                        Name = "";
                                    }

//                                    if (Model.has("Image"))
//                                    {
//                                        image = WebServiceAPI.BASE_URL_IMAGE + Model.getString("Image");
//                                    }
//                                    else
//                                    {
//                                        image = "";
//                                    }
                                }
                            }
                        }
                    }
                } else {
                    Log.e("acceptBooking", "acceptBooking11111111");

                    if (requestConfirmJsonObject.has("CarInfo")) {
                        JSONArray CarInfoArray = requestConfirmJsonObject.getJSONArray("CarInfo");

                        if (CarInfoArray != null && CarInfoArray.length() > 0) {
                            JSONObject CarInfo = CarInfoArray.getJSONObject(0);

                            if (CarInfo != null) {
                                JSONObject Model = null;
                                JSONArray ModelInfo = null;

                                if (requestConfirmJsonObject.has("ModelInfo")) {
                                    ModelInfo = requestConfirmJsonObject.getJSONArray("ModelInfo");

                                    if (ModelInfo != null && ModelInfo.length() > 0) {
                                        Model = ModelInfo.getJSONObject(0);
                                    }
                                }

                                if (CarInfo.has("Company")) {
                                    company = CarInfo.getString("Company");
                                }

                                if (CarInfo.has("VehicleRegistrationNo")) {
                                    vehicleRegistrationNo = CarInfo.getString("VehicleRegistrationNo");
                                }

                                if (CarInfo.has("Color")) {
                                    company = company + " " + CarInfo.getString("Color");
                                }

                                if (CarInfo.has("VehicleImage")) {
                                    image = WebServiceAPI.BASE_URL_IMAGE + CarInfo.getString("VehicleImage");
                                }

                                if (Model != null) {
                                    if (Model.has("Name")) {
                                        Name = Model.getString("Name");
                                    } else {
                                        Name = "";
                                    }

//                                    if (Model.has("Image"))
//                                    {
//                                        image = WebServiceAPI.BASE_URL_IMAGE + Model.getString("Image");
//                                    }
//                                    else
//                                    {
//                                        image = "";
//                                    }
                                }
                            }
                        }
                    }
                }


                if (requestConfirmJsonObject.has("DriverInfo")) {
                    JSONArray DriverInfoArray = requestConfirmJsonObject.getJSONArray("DriverInfo");

                    if (DriverInfoArray != null && DriverInfoArray.length() > 0) {
                        JSONObject DriverInfo = DriverInfoArray.getJSONObject(0);

                        if (DriverInfo != null) {
                            if (DriverInfo.has("Fullname")) {
                                driver_name = DriverInfo.getString("Fullname");
                            }

                            if (DriverInfo.has("DriverRating")) {
                                rating = DriverInfo.getString("DriverRating");
                            }

                            if (DriverInfo.has("MobileNo")) {
                                driver_phone = DriverInfo.getString("MobileNo");
                            }

                            if (DriverInfo.has("Image")) {
                                driver_image = WebServiceAPI.BASE_URL_IMAGE + DriverInfo.getString("Image");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception =" + e.getMessage());
        }

        if (Name != null && !Name.equalsIgnoreCase("")) {
            tv_CarModel.setVisibility(View.VISIBLE);

            tv_CarModel.setText(Name);
        } else {
            tv_CarModel.setVisibility(View.GONE);
        }

        Log.e("call", "vehicleRegistrationNo = " + vehicleRegistrationNo);
        if (vehicleRegistrationNo != null && !vehicleRegistrationNo.equalsIgnoreCase("")) {
            Log.e("call", "1111111111111111");
            tv_VehicleRegistrationNo.setVisibility(View.VISIBLE);
            tv_VehicleRegistrationNo.setText(vehicleRegistrationNo + "");
        } else {
            Log.e("call", "22222222222222222222");
            tv_VehicleRegistrationNo.setVisibility(View.GONE);
        }

        if (company != null && !company.equalsIgnoreCase("")) {
            tv_CarCompany.setVisibility(View.VISIBLE);
            tv_CarCompany.setText(company);
        } else {
            tv_CarCompany.setVisibility(View.GONE);
        }


        if (PickupLocation != null && !PickupLocation.equalsIgnoreCase("")) {
            tv_Pickup.setVisibility(View.VISIBLE);
            tv_Pickup.setText(getResources().getString(R.string.pick_up_location) + " " + getResources().getString(R.string.colon) + " " + PickupLocation);
        } else {
            tv_Pickup.setVisibility(View.GONE);
        }

        if (DropoffLocation != null && !DropoffLocation.equalsIgnoreCase("")) {
            tv_Dropoff.setVisibility(View.VISIBLE);
            tv_Dropoff.setText(getResources().getString(R.string.drop_off_location) + " " + getResources().getString(R.string.colon) + " " + DropoffLocation);
        } else {
            tv_Dropoff.setVisibility(View.GONE);
        }

        if (image != null && !image.trim().equals("")) {
            Log.e(TAG, "iv_CarImage iv_CarImage iv_CarImage = " + image);
            iv_CarImage.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(image)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_CarImage);
        } else {
            iv_CarImage.setVisibility(View.GONE);

        }

        if (driver_name != null && !driver_name.trim().equalsIgnoreCase("")) {
            tv_DriverName.setVisibility(View.VISIBLE);
            tv_DriverName.setText(driver_name);
        } else {
            tv_DriverName.setVisibility(View.GONE);
        }


        if (rating != null && !rating.equalsIgnoreCase("")) {
            ratingbar_driver.setVisibility(View.VISIBLE);
            ratingbar_driver.setRating(Float.parseFloat(rating));
        } else {
            ratingbar_driver.setVisibility(View.GONE);
        }

        if (driver_phone != null && !driver_phone.trim().equalsIgnoreCase("")) {
            iv_CallDriver.setVisibility(View.VISIBLE);
            txt_driver_number.setVisibility(View.VISIBLE);
            driverPhone = driver_phone;
            txt_driver_number.setText(driver_phone);
        } else {
            iv_CallDriver.setVisibility(View.GONE);
            driverPhone = "";
            txt_driver_number.setVisibility(View.GONE);
        }

        Log.e("call", "image = " + image);

        if (driver_image != null && !driver_image.trim().equals("")) {
            Log.e(TAG, "iv_DriverImage iv_DriverImage iv_DriverImage = " + driver_image);
            iv_DriverImage.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(driver_image)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_DriverImage);
        } else {
            iv_DriverImage.setVisibility(View.GONE);
        }
    }

    public void CancelTripByPassenger() {
        isPickupDropoff = 1;
        isCardPickUpSelected = true;
        card_dropoffLoc.setCardElevation(-5);
        card_pickupLocation.setCardElevation(5);

        try {
            Log.e("call", "CancelTripByPassenger");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "socket connected");
                Log.e("call", "BookingId = " + BookingId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", BookingId);
                Common.socket.emit("CancelTripByPassenger", jsonObject);
                Log.e("call", "CancelTripByPassenger = " + jsonObject.toString());
                gpsTracker = new GPSTracker(activity);

                coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                if (googleMap != null) {
                    googleMap.clear();
                }

                removeCarMarker();

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

//                markerPickup = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                markerPickup.setTag("1");
//                markerPickup.setDraggable(true);
//                googleMap.setOnCameraChangeListener(this);

                ll_BookingLayout.setVisibility(View.GONE);
                ll_AllCarLayout.setVisibility(View.GONE);
                ll_AfterRequestAccept.setVisibility(View.GONE);
                address_Layout.setVisibility(View.VISIBLE);
                //ll_Done.setVisibility(View.VISIBLE);
                current_marker_layout.setVisibility(View.VISIBLE);

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                tripFlag = 0;
                isDone = 0;
                isPickupDropoff = 1;
                selectedCarPosition = -1;

                if (carType_beens != null && carType_beens.size() > 0) {
                    for (int i = 0; i < carType_beens.size(); i++) {
                        carType_beens.get(i).setSelectedCar(-1);
                    }
                }
//                InternetDialog internetDialog = new InternetDialog(activity);
//                internetDialog.showDialog("Your request canceled successfully!","Success Message");
                myLocation = true;

                if (handlerDriver != null && runnableDriver != null) {
                    handlerDriver.removeCallbacks(runnableDriver, null);
                }


                isAutocomplete = true;
                auto_Dropoff.setText("");
                tvDropoff.setText("");
                dropoff_Touch = false;
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Long = 0;
                TaxiUtil.dropoff_Lat = 0;
                displayImage();
                getAddressFromLatLong();
                if (handler != null && runnable != null) {
                    handler.removeCallbacks(runnable, null);
                }

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));

                if (carAdapter != null) {
                    carAdapter.notifyDataSetChanged();
                    recyclerViewCar.scrollToPosition(0);
                }

                startTimer();

            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void cancelAdvanceBookingRequest() {
        isPickupDropoff = 1;
        isCardPickUpSelected = true;
        card_dropoffLoc.setCardElevation(-5);
        card_pickupLocation.setCardElevation(5);
        try {
            Log.e("call", "cancelAdvanceBookingRequest");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e("call", "socket connected");
                Log.e("call", "BookingId = " + BookingId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", BookingId);
                Common.socket.emit("AdvancedBookingCancelTripByPassenger", jsonObject);
                Log.e("call", "AdvancedBookingCancelTripByPassenger = " + jsonObject.toString());
                gpsTracker = new GPSTracker(activity);

                coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                if (googleMap != null) {
                    googleMap.clear();
                }

                removeCarMarker();

                if (originMarker != null) {
                    originMarker.remove();
                }

                if (destinationMarker != null) {
                    destinationMarker.remove();
                }

//                markerPickup = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                markerPickup.setTag("1");
//                markerPickup.setDraggable(true);
//                googleMap.setOnCameraChangeListener(this);

                auto_Dropoff.setText("");
                tvDropoff.setText("");
                dropoff_Touch = false;
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Long = 0;
                TaxiUtil.dropoff_Lat = 0;

                ll_BookingLayout.setVisibility(View.GONE);
                ll_AllCarLayout.setVisibility(View.GONE);
                ll_AfterRequestAccept.setVisibility(View.GONE);
                address_Layout.setVisibility(View.VISIBLE);
                //ll_Done.setVisibility(View.VISIBLE);
                current_marker_layout.setVisibility(View.VISIBLE);

                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("0")) {

                }

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                tripFlag = 0;
                isDone = 0;
                isPickupDropoff = 1;
                selectedCarPosition = -1;
//                InternetDialog internetDialog = new InternetDialog(activity);
//                internetDialog.showDialog("Your request canceled successfully!", "Success Message");
                if (carType_beens != null && carType_beens.size() > 0) {
                    for (int i = 0; i < carType_beens.size(); i++) {
                        carType_beens.get(i).setSelectedCar(-1);
                    }
                }
                myLocation = true;

//                ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
//                ll_CarBackgroundSeven.setBackgroundResource(R.drawable.circle_white_border);

                if (handlerDriver != null && runnableDriver != null) {
                    handlerDriver.removeCallbacks(runnableDriver, null);
                }


                isAutocomplete = true;
                displayImage();
                getAddressFromLatLong();

                if (handler != null && runnable != null) {
                    handler.removeCallbacks(runnable, null);
                }

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
                if (carAdapter != null) {
                    carAdapter.notifyDataSetChanged();
                    recyclerViewCar.scrollToPosition(0);
                }
                startTimer();
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void showRoute(int flag) //for trip or normal location chage flage  = 0 for normal, flag = 1 for trip.
    {
        Log.e(TAG, "showRoute():- " + flag);

        dialogClass.showDialog();

        if (googleMap != null) {
            googleMap.clear();
        }

        current_marker_layout.setVisibility(View.GONE);

        Log.e(TAG, "showRoute() flag = " + flag);

        gpsTracker = new GPSTracker(activity);
        MarkerOptions options = new MarkerOptions();
        Log.e(TAG, "showRoute() 111");
        if (flag == 0) {
            Log.e(TAG, "showRoute() 222");
            pickUp = new LatLng(TaxiUtil.picup_Lat, TaxiUtil.picup_Long);
        }
        Log.e(TAG, "showRoute() 333");
        options.position(pickUp);
//        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_pick));
        if (tripFlag == 0) {
            options.icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.pin_green))/*ic_current_location*//*ic_pin_pick*/);
        } else if (tripFlag == 1) {
            //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_green/*ic_current_location*//*ic_pin_pick*/));
            options.icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.pin_green)));
        } else {
            //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_red/*ic_drop_pin*/));
            options.icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.pin_red)/*ic_drop_pin*/));
        }


        MarkerOptions options1 = new MarkerOptions();

        Log.e(TAG, "showRoute() 444");

        if (flag == 0) {
            Log.e(TAG, "showRoute() 222");
            driverLatLng = new LatLng(TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
        }

        options1.position(driverLatLng);


        int selectedCar = 0;
        String strCategoryId = "1";

        if (carType_beens != null && carType_beens.size() > 0) {
            strCategoryId = carType_beens.get(0).getCategoryId();

            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                }
            }
        }

        Log.e(TAG, "showRoute() 666 selectedCar = " + selectedCar);
        if (flag == 1)// || flag == 2
        {
            Log.e(TAG, "showRoute() 777");
            if (strCategoryId != null && strCategoryId.equals("1")) {
                if (selectedCar == 0) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 1) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 2) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 3) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 4) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 5) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 6) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                }
            } else if (strCategoryId != null && strCategoryId.equals("2")) {
                if (selectedCar == 0) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 1) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 2) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 3) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 4) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 5) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else if (selectedCar == 6) {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                } else {
                    options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
                }
            } else {
                options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car));
            }

        } else {
            Log.e(TAG, "showRoute() 888");
            options1.icon(getMarkerIconFromDrawable(getDrawable(R.drawable.pin_red/*ic_drop_pin*/)));
        }

        /*if(flag == 1)
        {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_pin));
        }*/


        options1.anchor(0.5f, 0.5f);


        if (flag == 1)// || flag==2
        {
//            if (destinationMarker!=null)
//            {
//                destinationMarker.remove();
//            }

            if (googleMap != null) {
                destinationMarker = googleMap.addMarker(options);
            }
        } else {
            if (googleMap != null) {
                markerPickup = googleMap.addMarker(options);
            }
        }

        if (flag == 1)// || flag==2
        {
//            if (originMarker!=null)
//            {
//                originMarker.remove();
//            }

            if (googleMap != null) {
                originMarker = googleMap.addMarker(options1);
            }
        } else {
            if (googleMap != null) {
                markerDropoff = googleMap.addMarker(options1);
            }
        }

        /*LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(driverLatLng);
        builder.include(pickUp);
        LatLngBounds bounds = builder.build();
        int padding = 500; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.moveCamera(cu);*/

        Log.e(TAG, "showRoute() 1010");
        String url = getDirectionsUrl(driverLatLng, pickUp);
        Log.e("url", "--------- " + url);
        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

        Log.e(TAG, "startTimerForDriverLocation()" + flag);
        if (flag == 1) {
            startTimerForDriverLocation();
        }
    }

    private float previousZoomLevel = defaltZoomLavel;
    public static boolean isAddressCahngable = true;

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Log.e(TAG, "onCameraChange()" + defaltZoomLavel);

        if (previousZoomLevel == cameraPosition.zoom) {
            isAddressCahngable = true;
        } else {
            isAddressCahngable = false;
            previousZoomLevel = cameraPosition.zoom;
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                Log.e("call", "jsonData[0] = " + jsonData[0]);
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            ployList = new ArrayList<LatLng>();
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            // Traversing through all the routes
            if (result != null) {
                int pointSize = 0;
                Log.e("call", "result.size() =" + result.size());
                for (int i = 0; i < result.size(); i++) {

                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

//                    Log.e("call","path.size() ="+path.size());
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));

//                        Log.e("call","point.lat ="+lat);
//                        Log.e("call","point.lng ="+lng);
                        LatLng position = new LatLng(lat, lng);
                        ployList.add(position);
                        builder.include(position);
                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);

                    if (i == 0) {
                        lineOptions.color(getResources().getColor(R.color.colorPoliLine));
                    } else {
                        lineOptions.color(getResources().getColor(R.color.colorGray));
                    }

                    lineOptions.geodesic(true);
                    lineOptions.zIndex(8);
                    pointSize = pointSize + 1;
                }


                Log.e("call", "pointSize = " + pointSize);
                // Drawing polyline in the Google Map for the i-th route

                Log.e("lineOptions", "lineOptions1111111111111  = ");
                if (lineOptions != null) {
                    Log.e("lineOptions", "lineOptions22222222222222222");
                    polyline = googleMap.addPolyline(lineOptions);

                    Location loc1 = new Location("");
                    loc1.setLatitude(driverLatLng.latitude);
                    loc1.setLongitude(driverLatLng.longitude);

                    Location loc2 = new Location("");
                    loc2.setLatitude(pickUp.latitude);
                    loc2.setLongitude(pickUp.longitude);

                    ////https://stackoverflow.com/questions/38264483/google-map-zoom-in-between-latlng-bounds
                    double circleRad = ((loc1.distanceTo(loc2))) * 1000;//multiply by 1000 to make units in KM

                    float zoomLevel = getZoomLevel(circleRad);

                    int tripFlag = 1;
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                        tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                    }

                    if (tripFlag == 1) {
                        dialogClass.hideDialog();

                        //ll_Done.setVisibility(View.GONE);
                        isDone = 1;
                        ll_AllCarLayout.setVisibility(View.GONE);
                        ll_BookingLayout.setVisibility(View.GONE);

                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;

                        LatLngBounds bounds = builder.build();
                        int padding = 150; // offset from edges of the map in pixels  450
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                        googleMap.animateCamera(cu);
                    } else if (tripFlag == 2) {
                        dialogClass.hideDialog();

                        //ll_Done.setVisibility(View.GONE);
                        isDone = 1;
                        ll_AllCarLayout.setVisibility(View.GONE);
                        ll_BookingLayout.setVisibility(View.GONE);

                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;

                        LatLngBounds bounds = builder.build();
                        int padding =150; // offset from edges of the map in pixels  450
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                        googleMap.animateCamera(cu);
                    } else {
                        dialogClass.hideDialog();

                        isDone = 1;
                        ll_Done.setVisibility(View.GONE);
                        ll_AllCarLayout.setVisibility(View.VISIBLE);
                        ll_BookingLayout.setVisibility(View.VISIBLE);

                        if (RequestFor.equalsIgnoreCase(DELIVERY)) {
                            tv_BookNow.setVisibility(View.VISIBLE);
                        } else {
                            tv_BookNow.setVisibility(View.VISIBLE);
                        }

                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels-625;
                        int width = displayMetrics.widthPixels;

                        LatLngBounds bounds = builder.build();
                        int padding = 290; // offset from edges of the map in pixels
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                        googleMap.animateCamera(cu);
                    }
                } else {
                    Log.e("lineOptions", "lineOptions33333333333333333333333");
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.no_route_found), getResources().getString(R.string.info_message));
                }
            } else {
                Log.e("lineOptions", "lineOptions44444444444444444");
                dialogClass.hideDialog();
                InternetDialog internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(getResources().getString(R.string.no_route_found), getResources().getString(R.string.info_message));
            }
            dialogClass.hideDialog();
        }
    }

    private int getZoomLevel(double radius) {
        double scale = radius / 500000;
        return ((int) (16 - Math.log(scale) / Math.log(2)));
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);

                Log.e("call", " datad fjlsdjflsdjlfjsdjl = " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("call", " result = " + result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        Log.e("call", "showRoute() 11 11");
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=true&key=" + getResources().getString(R.string.map_api_key);

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        Log.e("call", "showRoute() 12 12 = url = " + url);
        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("call", "Exception while downloading url" + e.getMessage());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }


    private Emitter.Listener onPassengerCancelTripNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "onPassengerCancelTripNotification");
                        if (args != null && args.length > 0) {
                            Log.e(TAG, "onPassengerCancelTripNotification response = " + args[0].toString());
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingInfo") && jsonObject.getString("BookingInfo") != null && !jsonObject.getString("BookingInfo").equalsIgnoreCase("")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("BookingInfo");
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        JSONObject objectBookingInfo = jsonArray.getJSONObject(0);
                                        if (objectBookingInfo != null) {
                                            if (objectBookingInfo.has("Id") && objectBookingInfo.getString("Id") != null && !objectBookingInfo.getString("Id").equalsIgnoreCase("")) {
                                                if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                    if (BookingId.equalsIgnoreCase(objectBookingInfo.getString("Id"))) {
                                                        gpsTracker = new GPSTracker(activity);

                                                        coordinate = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                                                        removeCarMarker();

                                                        if (googleMap != null) {
                                                            googleMap.clear();
                                                        }

                                                        if (originMarker != null) {
                                                            originMarker.remove();
                                                        }

                                                        if (destinationMarker != null) {
                                                            destinationMarker.remove();
                                                        }

                                                        ll_BookingLayout.setVisibility(View.GONE);
                                                        ll_AllCarLayout.setVisibility(View.GONE);
                                                        ll_AfterRequestAccept.setVisibility(View.GONE);
                                                        address_Layout.setVisibility(View.VISIBLE);
                                                        //ll_Done.setVisibility(View.VISIBLE);
                                                        current_marker_layout.setVisibility(View.VISIBLE);

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                                                        advanceBooking = 0;
                                                        handlerCount = 0;
                                                        isAlreadySendRequest = false;
                                                        tripFlag = 0;
                                                        isDone = 0;
                                                        isPickupDropoff = 1;
                                                        selectedCarPosition = -1;
                                                        if (carType_beens != null && carType_beens.size() > 0) {
                                                            for (int i = 0; i < carType_beens.size(); i++) {
                                                                carType_beens.get(i).setSelectedCar(-1);
                                                            }
                                                        }
                                                        myLocation = true;

                                                        callSelectService(2);

                                                        auto_Dropoff.setText("");
                                                        tvDropoff.setText("");
                                                        dropoff_Touch = false;
                                                        TaxiUtil.dropoff_Address = "";
                                                        TaxiUtil.dropoff_Long = 0;
                                                        TaxiUtil.dropoff_Lat = 0;

                                                        selectPickup();

//                                                        ll_CarBackgroundOne.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundTwo.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundThree.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundFour.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundFive.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundSix.setBackgroundResource(R.drawable.circle_white_border);
//                                                        ll_CarBackgroundSeven.setBackgroundResource(R.drawable.circle_white_border);

                                                        if (handlerDriver != null && runnableDriver != null) {
                                                            handlerDriver.removeCallbacks(runnableDriver, null);
                                                        }

                                                        if (handlerSendRequest != null && runnableSendRequest != null) {
                                                            handlerSendRequest.removeCallbacks(runnableSendRequest, null);
                                                        }

                                                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                            commonInternetDialog.hideDialog();
                                                        }

                                                        if (requestPendingDialogClass != null) {
                                                            requestPendingDialogClass.hideDialog();
                                                        }

                                                        if (commonDialog != null && commonDialog.isShowing()) {
                                                            commonDialog.dismiss();
                                                        }

                                                        isAutocomplete = true;
                                                        displayImage();
                                                        getAddressFromLatLong();

                                                        if (handler != null && runnable != null) {
                                                            handler.removeCallbacks(runnable, null);
                                                        }

                                                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));

                                                        if (carAdapter != null) {
                                                            carAdapter.notifyDataSetChanged();
                                                            recyclerViewCar.scrollToPosition(0);
                                                        }

                                                        if (jsonObject.has("message")) {
                                                            //if(i == )
                                                            String message = jsonObject.getString("message");
                                                            if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                                                internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                commonInternetDialog = internetDialog;
                                                            } else {
                                                                InternetDialog internetDialog = new InternetDialog(activity);
                                                                internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                commonInternetDialog = internetDialog;
                                                            }
                                                            startTimer();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "onRejectBookingRequestNotification exception = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onRejectBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
//                        try
//                        {
//                            ringTone = MediaPlayer.create(activity, R.raw.tone);
//                            ringTone.pick_start();
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }

                        handlerCount = 0;
                        isAlreadySendRequest = false;

                        if (handlerSendRequest != null && runnableSendRequest != null) {
                            handlerSendRequest.removeCallbacks(runnableSendRequest, null);
                        }

                        if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                            commonInternetDialog.hideDialog();
                        }

                        if (commonDialog != null && commonDialog.isShowing()) {
                            commonDialog.dismiss();
                        }
                        Log.e("call", "onRejectBookingRequestNotification ");
                        advanceBooking = 0;
                        tripFlag = 0;
                        selectedCarPosition = -1;
                        isDone = 0;
                        requestConfirmJsonObject = null;
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);

                        /*tvDropoff.setText("");
                        TaxiUtil.dropoff_Address = "";
                        TaxiUtil.dropoff_Lat = Double.parseDouble("0.0");
                        TaxiUtil.dropoff_Long = Double.parseDouble("0.0");

                        isPickupDropoff = 1;
                        isCardPickUpSelected = true;
                        card_dropoffLoc.setCardElevation(-5);
                        card_pickupLocation.setCardElevation(5);*/


                        callSelectService(2);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (requestPendingDialogClass != null) {
                                    requestPendingDialogClass.hideDialog();
                                }
                            }
                        }, 1000);

                        if (args != null && args.length > 0) {
                            Log.e("call", "onRejectBookingRequestNotification response = " + args[0]);
                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TruckzShipperApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                        internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        } else {
                            Log.e("call", "onRejectBookingRequestNotification args[0].leth = 0");
                        }
                    } catch (Exception e) {
                        Log.e("call", "onRejectBookingRequestNotification exception = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onDriverList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("call", "onDriverList onDriverList");
                    if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                        tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                    } else {
                        tripFlag = 0;
                    }
                    Log.e("call", "onDriverList tripFlag = " + tripFlag);

                    if (tripFlag == 0) {
                        try {
                            driverList = args[0].toString();
                            GetCarList_OnClickType();
                        } catch (Exception e) {
                            setTimeAndPriceToZero();
                            Log.e("call", "onDriverList exception = " + e.getMessage());
                        }
                    }
                }
            });
        }
    };


    public void GetCarList_OnClickType() {
        Log.e(TAG, "GetCarList_OnClickType()");

        String driverIdSession = "";
        try {

            latLong_beens.clear();
            {
                Log.e(TAG, "GetCarList_OnClickType() onDriverList():- " + driverList);

                int totalAvailableDriver = 0;

                if (carType_beens != null && carType_beens.size() > 0) {
                    for (int i = 0; i < carType_beens.size(); i++) {
                        totalAvailableDriver = totalAvailableDriver + carType_beens.get(i).getAvai();
                    }
                }
                Log.e(TAG, "GetCarList_OnClickType() totalAvailableDriver:- " + totalAvailableDriver);
                if (totalAvailableDriver == 0) {
                    zoomFlag = 0;
                }

                if (driverList != null && !driverList.equals("")) {
                    Log.e(TAG, "GetCarList_OnClickType() onDriverList() = 111");
                    JSONObject jsonObject = new JSONObject(driverList);

                    if (jsonObject != null) {
                        Log.e(TAG, "GetCarList_OnClickType() onDriverList() = 222");
                        if (jsonObject.has("driver")) {
                            Log.e(TAG, "GetCarList_OnClickType() onDriverList() = 333");
                            JSONArray jsonArray = jsonObject.getJSONArray("driver");

                            if (jsonArray != null && jsonArray.length() > 0) {
                                Log.e(TAG, "GetCarList_OnClickType() onDriverList() = 444 = " + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    if (jsonObject1 != null) {
                                        if (jsonObject1.has("DriverId")) {
                                            if (i == 0) {
                                                driverIdSession = jsonObject1.getString("DriverId");
                                            } else if (driverIdSession != null && driverIdSession.equalsIgnoreCase("")) {
                                                driverIdSession = jsonObject1.getString("DriverId");
                                            } else {
                                                driverIdSession = driverIdSession + "," + jsonObject1.getString("DriverId");
                                            }
                                        }


                                        if (jsonObject1.has("CarType")) {
                                            JSONArray CarType = jsonObject1.getJSONArray("CarType");

                                            String arraySortFlag[] = new String[CarType.length()];

                                            Log.e(TAG, "GetCarList_OnClickType() onDriverList carType_beens.size() = " + carType_beens.size());

                                            if (CarType != null && CarType.length() > 0) {
                                                for (int j = 0; j < CarType.length(); j++) {
                                                    if (carType_beens.size() > 0) {
                                                        for (int ct = 0; ct < carType_beens.size(); ct++) {
                                                            if (CarType.getString(j).equals(carType_beens.get(ct).getId())) {
                                                                arraySortFlag[j] = carType_beens.get(ct).getSort();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            Log.e(TAG, "GetCarList_OnClickType() onDriverList arraySortFlag.length = " + arraySortFlag.length);

                                            if (arraySortFlag.length > 0) {
                                                for (int j = 0; j < arraySortFlag.length; j++) {
                                                    Log.e(TAG, "GetCarList_OnClickType() j arraySortFlag = " + j + ", " + arraySortFlag[j]);
                                                    if (carType_beens != null && carType_beens.size() > 0) {
                                                        Log.e(TAG, "GetCarList_OnClickType() carType_beens.size() = " + carType_beens.size());
                                                        for (int k = 0; k < carType_beens.size(); k++) {
                                                            Log.e(TAG, "GetCarList_OnClickType() categoryId = " + carType_beens.get(k).getCategoryId());
                                                        }

                                                        if (carType_beens.get(0).getCategoryId().equals("1")) {
                                                            if (arraySortFlag[j].equals("1")) {
                                                                int avail = carType_beens.get(0).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 0 = " + (avail + 1));
                                                                carType_beens.get(0).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 0 = " + carType_beens.get(0).getAvai());
                                                            } else if (arraySortFlag[j].equals("2")) {
                                                                int avail = carType_beens.get(1).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 1 = " + (avail + 1));
                                                                carType_beens.get(1).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 1 = " + carType_beens.get(1).getAvai());
                                                            } else if (arraySortFlag[j].equals("3")) {
                                                                int avail = carType_beens.get(2).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 2 = " + (avail + 1));
                                                                carType_beens.get(2).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 2 = " + carType_beens.get(2).getAvai());
                                                            } else if (arraySortFlag[j].equals("4")) {
                                                                int avail = carType_beens.get(3).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 3 = " + (avail + 1));
                                                                carType_beens.get(3).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 3 = " + carType_beens.get(3).getAvai());
                                                            } else if (arraySortFlag[j].equals("5")) {
                                                                int avail = carType_beens.get(4).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 4 = " + (avail + 1));
                                                                carType_beens.get(4).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 4 = " + carType_beens.get(4).getAvai());
                                                            } else if (arraySortFlag[j].equals("6")) {
                                                                int avail = carType_beens.get(5).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 4 = " + (avail + 1));
                                                                carType_beens.get(5).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 4 = " + carType_beens.get(5).getAvai());
                                                            } else if (arraySortFlag[j].equals("7")) {
                                                                int avail = carType_beens.get(6).getAvai();
                                                                Log.e("call", "GetCarList_OnClickType set avail 4 = " + (avail + 1));
                                                                carType_beens.get(6).setAvai(avail + 1);
                                                                Log.e("call", "GetCarList_OnClickType get avail 4 = " + carType_beens.get(6).getAvai());
                                                            }
                                                        } else if (carType_beens.get(0).getCategoryId().equals("2")) {
                                                            if (arraySortFlag[j].equals("1")) {
                                                                int avail = carType_beens.get(0).getAvai();
                                                                carType_beens.get(0).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("2")) {
                                                                int avail = carType_beens.get(1).getAvai();
                                                                carType_beens.get(1).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("3")) {
                                                                int avail = carType_beens.get(2).getAvai();
                                                                carType_beens.get(2).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("4")) {
                                                                int avail = carType_beens.get(3).getAvai();
                                                                carType_beens.get(3).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("5")) {
                                                                int avail = carType_beens.get(4).getAvai();
                                                                carType_beens.get(4).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("6")) {
                                                                int avail = carType_beens.get(5).getAvai();
                                                                carType_beens.get(5).setAvai(avail + 1);
                                                            } else if (arraySortFlag[j].equals("7")) {
                                                                int avail = carType_beens.get(6).getAvai();
                                                                carType_beens.get(6).setAvai(avail + 1);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                Log.e(TAG, "GetCarList_OnClickType() getEstimateFare_beens = " + getEstimateFare_beens.size());
                                int selectedCar = -1;
                                String strCategoryId = "1";

                                if (carType_beens != null && carType_beens.size() > 0) {
                                    strCategoryId = carType_beens.get(0).getCategoryId();

                                    for (int i = 0; i < carType_beens.size(); i++) {
                                        if (carType_beens.get(i).getSelectedCar() != -1) {
                                            selectedCar = carType_beens.get(i).getSelectedCar();
                                        }
                                    }
                                }

                                Log.e(TAG, "GetCarList_OnClickType() onDriverList() = selectedCar = " + selectedCar);
                                String carTypeTemp = "";

                                /*if (selectedCar>=0)
                                {*/
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    if (jsonObject1 != null) {
                                        if (jsonObject1.has("CarType")) {
                                            JSONArray CarType = jsonObject1.getJSONArray("CarType");
                                            carTypeTemp = CarType.getString(0);

                                            if (CarType != null &&
                                                    CarType.length() > 0) {
                                                if (selectedCar >= 0) {
                                                    int flag = 0;
                                                    first:
                                                    for (int k = 0; k < CarType.length(); k++) {
                                                            /*if (carType_beens.get(selectedCar).getId().equals(CarType.getString(k)))
                                                            {
                                                                flag = 1;
                                                                break first;
                                                            }*/

                                                        if (jsonObject1.has("Location")) {
                                                            JSONArray jsonArray1 = jsonObject1.getJSONArray("Location");

                                                            if (jsonArray1 != null && jsonArray1.length() > 0) {
                                                                double lat = 0, longi = 0;
                                                                int cartType = -1;

                                                                for (int j = 0; j < jsonArray1.length(); j++) {
                                                                    if (j == 0) {
                                                                        lat = jsonArray1.getDouble(j);
                                                                    } else {
                                                                        longi = jsonArray1.getDouble(j);
                                                                    }
                                                                }

                                                                if (jsonObject1.has("CarType")) {
                                                                    JSONArray carty = jsonObject1.getJSONArray("CarType");
                                                                    cartType = Integer.parseInt(carty.get(0).toString());
                                                                }

                                                                latLong_beens.add(new LatLong_Been(lat, longi, cartType));
                                                            }
                                                        }
                                                    }

                                                        /*if (flag==1)
                                                        {
                                                            if (jsonObject1.has("Location"))
                                                            {
                                                                JSONArray jsonArray1 = jsonObject1.getJSONArray("Location");

                                                                if (jsonArray1!=null && jsonArray1.length()>0)
                                                                {
                                                                    double lat=0,longi=0;

                                                                    for (int j=0; j<jsonArray1.length(); j++)
                                                                    {
                                                                        if (j==0)
                                                                        {
                                                                            lat = jsonArray1.getDouble(j);
                                                                        }
                                                                        else
                                                                        {
                                                                            longi = jsonArray1.getDouble(j);
                                                                        }
                                                                    }
                                                                    latLong_beens.add(new LatLong_Been(lat,longi));
                                                                }
                                                            }
                                                        }*/
                                                } else {
                                                    if (jsonObject1.has("Location")) {
                                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("Location");

                                                        if (jsonArray1 != null && jsonArray1.length() > 0) {
                                                            double lat = 0, longi = 0;
                                                            int cartType = -1;

                                                            for (int j = 0; j < jsonArray1.length(); j++) {
                                                                if (j == 0) {
                                                                    lat = jsonArray1.getDouble(j);
                                                                } else {
                                                                    longi = jsonArray1.getDouble(j);
                                                                }
                                                            }

                                                            if (jsonObject1.has("CarType")) {
                                                                JSONArray carty = jsonObject1.getJSONArray("CarType");
                                                                cartType = Integer.parseInt(carty.get(0).toString());
                                                            }

                                                            if (carType_beens.get(i).getAvai() >= 0) {
                                                                latLong_beens.add(new LatLong_Been(lat, longi, cartType));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                // }

                                Log.e(TAG, "GetCarList_OnClickType() onDriverList latLong_Beens size = " + latLong_beens.size());
                                Log.e(TAG, "myLocation myLocation = " + myLocation);

                                if (myLocation == false) {
                                    if (latLong_beens.size() > 0) {
                                        Log.e(TAG, "GetCarList_OnClickType() coordinate selectedCar ================= latLong_beens.size = " + latLong_beens.size());

                                        removeCarMarker();
                                        //                                        marker = new Marker[latLong_beens.size()];

                                        for (int i = 0; i < latLong_beens.size(); i++) {
                                            coordinate = new LatLng(latLong_beens.get(i).getLat(), latLong_beens.get(i).getLong());

                                            Log.e(TAG, "GetCarList_OnClickType() coordinate1 coordinate ================= lat = " + coordinate.latitude);
                                            Log.e(TAG, "GetCarList_OnClickType() coordinate2 coordinate ================= lat = " + coordinate.longitude);
                                            Log.e(TAG, "GetCarList_OnClickType() coordinate3 selectedCar ================= lat = " + selectedCar);

                                            if (googleMap != null) {
                                                if (strCategoryId != null && (strCategoryId.equals("1") || strCategoryId.equals("2"))) {
//                                                    if (selectedCar>=0)
//                                                    {
                                                    if (selectedCar + 1 == latLong_beens.get(i).getCarType()) {
                                                        listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                                                    } else if (selectedCar == -1) {
                                                        listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                                                    }
//                                                    }
                                                }

//                                                listMarker.add( googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
//                                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                if (strCategoryId!=null &&
//                                                        strCategoryId.equals("1"))
//                                                {
//                                                    if (selectedCar==0)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==1)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==2)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==3)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==4)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==5)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==6)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                }
//                                                else if (strCategoryId!=null &&
//                                                        strCategoryId.equals("2"))
//                                                {
//                                                    if (selectedCar==0)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==1)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==2)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==3)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==4)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==5)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                    else if (selectedCar==6)
//                                                    {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
//                                                    }
//                                                }
//
                                                if (zoomFlag == 0) {
                                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
                                                    zoomFlag = 1;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    removeCarMarker();

//                                    marker = new Marker[latLong_beens.size()];
                                    JSONArray CarType;
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        if (jsonObject1 != null) {
                                            if (jsonObject1.has("CarType")) {
                                                CarType = jsonObject1.getJSONArray("CarType");
                                                carTypeTemp = CarType.getString(0);
                                                coordinate = new LatLng(latLong_beens.get(i).getLat(), latLong_beens.get(i).getLong());
                                                if (googleMap != null) {
                                                    if (carTypeTemp != null && (carTypeTemp.trim().equalsIgnoreCase("1")
                                                            || carTypeTemp.trim().equalsIgnoreCase("2")
                                                            || carTypeTemp.trim().equalsIgnoreCase("3"))) {
                                                        if (selectedCar + 1 == latLong_beens.get(i).getCarType()) {
                                                            listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).
                                                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                                                        } else if (selectedCar == -1) {
                                                            listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).
                                                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                                                        }
                                                    }
//                                                    marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).
//                                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));

//                                                    if (carTypeTemp != null && carTypeTemp.trim().equalsIgnoreCase("1")) {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ezy_top_car)));
//                                                    } else if (carTypeTemp != null && carTypeTemp.trim().equalsIgnoreCase("2")) {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ezy_top_car)));
//                                                    } else if (carTypeTemp != null && carTypeTemp.trim().equalsIgnoreCase("3")) {
//                                                        marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ezy_top_van)));
//                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                driverIdSession = "";
                                setTimeAndPriceToZero();
                            }
                        } else {
                            driverIdSession = "";
                            setTimeAndPriceToZero();
                        }
                    } else {
                        driverIdSession = "";
                        setTimeAndPriceToZero();
                    }
                } else {
                    driverIdSession = "";
                    setTimeAndPriceToZero();
                }
            }
        } catch (Exception e) {
            driverIdSession = "";
            setTimeAndPriceToZero();
            Log.e(TAG, "GetCarList_OnClickType() onDriverList exception = " + e.getMessage());
        }

        String driverIdPrivious = "";

        driverIdPrivious = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, activity);
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, driverIdSession, activity); //USER_PREFERENCE_KEY_DRIVER_ID

        //driverIdPrivious = "";
        Log.e(TAG, "GetCarList_OnClickType() driverIdPrivious:-  " + driverIdPrivious);
        Log.e(TAG, "GetCarList_OnClickType() driverIdSession :- " + driverIdSession);

        Log.e(TAG, "GetCarList_OnClickType() isDone = " + isDone);
//        boolean isCarChange=false;
//        if (driverIdSession!= null && driverIdSession.isEmpty() && !driverIdPrivious.equals(driverIdSession)){
//            isCarChange=true;
//        }else if (carType_beens!=null && driverIdSession!=null){
//            String[] list;
//            for (int i=0;i<carType_beens.size();i++){
//                if (carType_beens.get(0).getId()){
//
//                }
//            }
//        }
        //driverIdSession = "";
        if (isDone == 1 && !driverIdPrivious.equals(driverIdSession)) {
            getEstimatedFareCheckValidation();
        } else {
            if (carAdapter != null) {
                carAdapter.notifyDataSetChanged();
                recyclerViewCar.scrollToPosition(0);
            }
        }
    }

    public void clickLeftArrow() {
//        ll_HorizontalListView.fullScroll(HorizontalScrollView.FOCUS_LEFT);
    }

    public void clickRightArrow() {
//        ll_HorizontalListView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
    }


    private void call_package() {
        Intent intentPackage = new Intent(MainActivity.this, CustomPackageActivity.class);
        startActivity(intentPackage);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public static void call_sos(Activity activity) {
        Log.e("SOS", "Press Call Button");
        takePermission(activity);
    }

    //forselect image
    public static void takePermission(final Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.e("call", " permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle(activity.getResources().getString(R.string.need_permission));
                builder.setMessage(activity.getResources().getString(R.string.this_app_need_phone_permission));
                builder.setPositiveButton(activity.getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
                    }
                });
                builder.setNegativeButton(activity.getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_PHONE_STATE, true);
            editor.commit();
        } else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Log.e("call", " permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder.setTitle(activity.getResources().getString(R.string.need_permission));
                builder.setMessage(activity.getResources().getString(R.string.this_app_need_phone_permission));
                builder.setPositiveButton(activity.getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 100);
                    }
                });
                builder.setNegativeButton(activity.getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CALL_PHONE, true);
            editor.commit();
        } else {
            Log.e("call", "call helpLineNumber:- " + helpLineNumber);
            TelephonyManager telMgr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            int simState = telMgr.getSimState();
            InternetDialog internetDialog = null;
            switch (simState) {
                case TelephonyManager.SIM_STATE_ABSENT:
                    internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.sim_card_not_availble), activity.getResources().getString(R.string.error_message));
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                    internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_network_lock), activity.getResources().getString(R.string.error_message));
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                    internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_pin_required), activity.getResources().getString(R.string.error_message));
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                    internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_puk_required), activity.getResources().getString(R.string.error_message));
                    break;
                case TelephonyManager.SIM_STATE_READY:
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + helpLineNumber));
                    Log.e("SOS", "Press Call Button Permision Enter");
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Log.e("SOS", "Press Call Button Permision Exit");
                    activity.startActivity(intent);
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN:
                    internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_unknown), activity.getResources().getString(R.string.error_message));
                    break;
            }
        }
    }

    Runnable runnableSendRequest = new Runnable() {
        @Override
        public void run() {
            isAlreadySendRequest = false;
            handlerCount = 0;
        }
    };

    public void callAddressDone() {
        Log.e("call", "callAddressDone()");
        Log.e("call", "callAddressDone()=pickup_Touch " + pickup_Touch);
        Log.e("call", "callAddressDone()=dropoff_Touch " + dropoff_Touch);
        Log.e("call", "isPickupDropoff = " + isPickupDropoff);

        myLocation = false;

        if (tvPickup.getText().toString().equalsIgnoreCase("")) {
            mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_pickup_loc), 0);
            return;
        } else if (tvDropoff.getText().toString().equalsIgnoreCase("")) {
            mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_drop_loc), 0);
            return;
        }

//        if (isPickupDropoff==1)
//        {
//            Log.e("call","callAddressDone() 11");
//            if (!auto_Dropoff.getText().toString().trim().equalsIgnoreCase("") && dropoff_Touch && !auto_Pickup.getText().toString().trim().equalsIgnoreCase("") && pickup_Touch )
//            {
//                callSelectService(2);
//                SetDrawer();
//                Log.e("call","callAddressDone() 22");
//                showRoute(0);
//            }
//            else
//            {
//                Log.e("call","callAddressDone() 33");
//                ll_AllCarLayout.setVisibility(View.GONE);
//                ll_BookingLayout.setVisibility(View.GONE);
//                isPickupDropoff = 2;
//                auto_Dropoff.setFocusableInTouchMode(true);
//                auto_Dropoff.requestFocus();
//            }
//        }
//        else
//        {
//            Log.e("call","callAddressDone() 44");
//            if (!auto_Pickup.getText().toString().trim().equalsIgnoreCase("") && pickup_Touch && !auto_Dropoff.getText().toString().trim().equalsIgnoreCase("") && dropoff_Touch)
//            {
//                callSelectService(2);
//                SetDrawer();
//                Log.e("call","callAddressDone() 55");
//                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID,"",activity);
//                isDone = 1;
//                showRoute(0);
//            }
//            else
//            {
//                Log.e("call","callAddressDone() 66");
//                ll_AllCarLayout.setVisibility(View.GONE);
//                ll_BookingLayout.setVisibility(View.GONE);
//                isPickupDropoff = 1;
//                auto_Pickup.setFocusableInTouchMode(true);
//                auto_Pickup.requestFocus();
//            }
//        }

        callSelectService(2);
        SetDrawer();
        Log.e("call", "callAddressDone() 55");
        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, "", activity);
        isDone = 1;
        showRoute(0);
    }

    public void callToDriver() {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+driverPhone));
        startActivity(intent);

        /*TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        InternetDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.sim_card_not_availble), activity.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_network_lock), activity.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_pin_required), activity.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_puk_required), activity.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + driverPhone));
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
                startActivity(intent);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new InternetDialog(activity);
                internetDialog.showDialog(activity.getResources().getString(R.string.sim_state_unknown), activity.getResources().getString(R.string.error_message));
                break;
        }*/
    }

    public void getEstimatedFareCheckValidation() {
        if (pickup_Touch == false) {
            Log.e("call", "availableCountZero22222 = ");
            if (getEstimated_Pickup != null && !getEstimated_Pickup.toString().trim().equals("") &&
                    getEstimated_lat != 0 && getEstimated_long != 0) {
                Log.e("call", "availableCountZero333333 = " + timeFlag);
                if (timeFlag == 0) {
                    Log.e("call", "availableCountZero44444 = ");
//                    timeFlag = 1;
                    //findPriceAndTime();                                                                                                                                                                                                                                                                                                                                     findPriceAndTime();
                } else {
                    Log.e("call", "availableCountZero55555 = ");
                    timeFlag = 0;
                    Log.e("timeFlag == ", " === " + timeFlag);
                }
            }
        } else {
            Log.e("call", "availableCountZero66666 = ");
            if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") &&
                    TaxiUtil.picup_Address != null && !TaxiUtil.picup_Address.trim().equals("") && pickup_Touch == true ) {
                Log.e("call", "availableCountZero77777 = " + timeFlag);
                if (timeFlag == 0) {
                    Log.e("call", "availableCountZero8888 = ");
//                    timeFlag = 1;
                    findPriceAndTime();
                } else {
                    Log.e("call", "availableCountZero999999 = ");
                    timeFlag = 0;
                }
            }
        }
    }

    private void AddToFavotie() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_favorite_option, null);

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(view);

        LinearLayout ll_Home, ll_Office, ll_Airport, ll_Others, dialog_ok_layout;
        TextView dialog_ok_textview;
        ImageView dialog_close;

        ll_Home = (LinearLayout) dialog.findViewById(R.id.favorite_option_home);
        ll_Office = (LinearLayout) dialog.findViewById(R.id.favorite_option_office);
        ll_Airport = (LinearLayout) dialog.findViewById(R.id.favorite_option_airport);
        ll_Others = (LinearLayout) dialog.findViewById(R.id.favorite_option_others);

        dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        dialog_ok_textview = dialog.findViewById(R.id.dialog_ok_textview);
        dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

        ll_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Home", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection), getResources().getString(R.string.no_internet_connection));
                }

            }
        });

        ll_Office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Office", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection), getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        ll_Airport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    call_AddToFavorite("Airport", tvDropoff.getText().toString(), TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection), getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        ll_Others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(activity)) {
                    callOtherOption(tvDropoff.getText().toString().trim());
//                    call_AddToFavorite("Others",tvDropoff.getText().toString(),TaxiUtil.dropoff_Lat,TaxiUtil.dropoff_Long);
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection),
                            getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_ok_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void callOtherOption(final String address) {
        Log.e(TAG, "callOtherOption()");

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_favorite_option_other);

        LinearLayout dialog_ok_layout;
        ImageView dialog_close;
        final CustomEditText etAddressTitle;

        dialog_ok_layout = dialog.findViewById(R.id.dialog_ok_layout);
        dialog_close = dialog.findViewById(R.id.dialog_close);
        etAddressTitle = dialog.findViewById(R.id.etAddressTitle);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Global.isNetworkconn(activity)) {
                    if (!TextUtils.isEmpty(etAddressTitle.getText().toString().trim())) {
                        dialog.dismiss();
                        call_AddToFavorite(etAddressTitle.getText().toString().trim(),
                                address, TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
                    } else {
                        etAddressTitle.setError("Please enter valid address title");
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection),
                            getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog.show();
    }

    public void call_AddToFavorite(String type, String address, double latitude, double longitude) {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_ADD_ADDRESS;//PassengerId,Type,Address,Lat,Lng

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity));
        params.put(WebServiceAPI.PARAM_TYPE, type);
        params.put(WebServiceAPI.PARAM_ADDRESS, address);
        params.put(WebServiceAPI.PARAM_LAT, latitude);
        params.put(WebServiceAPI.PARAM_LONG, longitude);

        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                String message = "Address added to favourite.";
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                            } else {
                                String message = getResources().getString(R.string.something_went_wrong);
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(activity);
                                internetDialog.showDialog(message, getResources().getString(R.string.error_message));
                            }
                        } else {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                        }
                    } else {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void gotoProfileScreen() {
        runnableProfile = new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent(MainActivity.this, UpdateProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                if (handlerProfile != null && runnableProfile != null) {
                    handlerProfile.removeCallbacks(runnableProfile, null);
                }
            }
        };
        handlerProfile.postDelayed(runnableProfile, 300);
    }

    public void call_DriverInfo() {
        FrameLayout parentThatHasBottomSheetBehavior = (FrameLayout) ll_BottomSheet.getParent().getParent();
        mBottomSheetBehavior = BottomSheetBehavior.from(parentThatHasBottomSheetBehavior);

        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
        }
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    public void showCreaditCardPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_yes_no_layout);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Message = dialog.findViewById(R.id.message);

        TextView tv_Yes = dialog.findViewById(R.id.yes);
        TextView tv_No = dialog.findViewById(R.id.no);

        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText(getResources().getString(R.string.do_you_want_to_add_card));

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openPromocodePopup();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openPromocodePopup();
            }
        });

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addCardFlag = 1;
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
            }
        });

        tv_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addCardFlag = 1;
                Intent intent = new Intent(activity, Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);

            }
        });

        dialog.show();
    }

    public void showPickingupCharge() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_yes_no_layout);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Message = dialog.findViewById(R.id.message);

        TextView tv_Yes = dialog.findViewById(R.id.yes);
        TextView tv_No = dialog.findViewById(R.id.no);

        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        int selectedCar = -1;

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                }
            }
        }

        if (selectedCar != -1) {
            tv_Message.setText(getResources().getString(R.string.pickingupcharge_message_one) + " " + getResources().getString(R.string.currency) + carType_beens.get(selectedCar).getSpecialExtraCharge() + " " + getResources().getString(R.string.pickingupcharge_message_two));
        }

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                call_missride_request(0);
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                call_missride_request(0);
            }
        });

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                specialRequest = 1;
                call_missride_request(1);
            }
        });

        tv_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                specialRequest = 1;
                call_missride_request(1);
            }
        });

        dialog.show();
    }

    public void call_missride_request(final int flag) {
        int selectedCar = -1;

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                }
            }
        }

        dialogClass.showDialog();
        String url = WebServiceAPI.API_BOOK_MISS_RIDE;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, passangerId);
        if (selectedCar != -1) {
            params.put(WebServiceAPI.PARAM_MODEL_ID, carType_beens.get(selectedCar).getId());
        }
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION, TaxiUtil.picup_Address);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, TaxiUtil.dropoff_Address);
        params.put(WebServiceAPI.PARAM_PICKUP_LAT, TaxiUtil.picup_Lat);
        params.put(WebServiceAPI.PARAM_PICKUP_LONG, TaxiUtil.picup_Long);
        params.put(WebServiceAPI.PARAM_DROPOFF_LAT, TaxiUtil.dropoff_Lat);
        params.put(WebServiceAPI.PARAM_DROPOFF_LONG, TaxiUtil.dropoff_Long);
        params.put(WebServiceAPI.PARAM_NOTES, notes);

        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    dialogClass.hideDialog();
                    if (flag == 1) {
                        openPickingupRequest();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    if (flag == 1) {
                        openPickingupRequest();
                    }
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void openPickingupRequest() {
        Log.e("call", "11111111118");
        /*if (checkCardList()) {
            Log.e("call", "11111111119");
            openPromocodePopup(); //0 for normal request and 1 for special request.
        } else {
            showCreaditCardPopup();
        }*/
        openPromocodePopup(); //0 for normal request and 1 for special request.
    }

    public boolean checkCardList() {
        try {
            Log.e("call", "111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            List<CreditCard_List_Been> cardListBeens = new ArrayList<CreditCard_List_Been>();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                Log.e("call", "222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    Log.e("call", "33333333333333");
                    if (json.has("cards")) {
                        Log.e("call", "6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            return true;
                        } else {
                            Log.e("call", "no cards available");
                            return false;
                        }
                    } else {
                        Log.e("call", "no cards found");
                        return false;
                    }
                } else {
                    Log.e("call", "json null");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("call", "Exception in getting card list = " + e.getMessage());
            return false;
        }
    }

    private void call_Booking(int flag) {
        Log.e("call", "1111111111");
        int selectedCar = -1;
        String selectCar = "";

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                    selectCar = carType_beens.get(i).getName();
                    selectedCarPosition = i;

                }
            }
        }

        if (flag == 0) {
            Log.e("call", "11111111112");
            if (tvPickup.getText() != null && !tvPickup.getText().toString().trim().equals("") &&
                    TaxiUtil.picup_Address != null && !TaxiUtil.picup_Address.trim().equals("")) {
                Log.e("call", "11111111113");
                if (tvDropoff.getText() != null && !tvDropoff.getText().toString().trim().equals("") &&
                        TaxiUtil.dropoff_Address != null && !TaxiUtil.dropoff_Address.trim().equals("")) {
                    Log.e("call", "11111111114");
                    if (pickup_Touch == true) {
                        Log.e("call", "11111111115");
                        if (dropoff_Touch == true) {
                            Log.e("call", "11111111116");
                            if (selectedCar != -1) {
                                Log.e("call", "11111111117");
                                if (carType_beens.get(selectedCar).getAvai() > 0) {
                                    Log.e("call", "11111111118");
                                    specialRequest = 0;
                                    /*if (checkCardList()) {
                                        Log.e("call", "11111111119");
                                        openPromocodePopup();//0 for normal request and 1 for special request.
                                    } else {
                                        showCreaditCardPopup();//0 for normal request and 1 for special request.
                                    }*/
                                    openPromocodePopup();//0 for normal request and 1 for special request.
                                } else {
//                                    showPickingupCharge();
                                    mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.msg_vehicle_not_found, selectCar), 0);
                                }
                            } else {
                                mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_truck_model), 0);
                            }

                        } else {
                            mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.invalid_destination_location), 0);
                            auto_Dropoff.setFocusableInTouchMode(true);
                            auto_Dropoff.requestFocus();
                        }
                    } else {
                        mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.invalid_pickup_location), 0);
                        auto_Pickup.setFocusableInTouchMode(true);
                        auto_Pickup.requestFocus();
                    }
                } else {
                    mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_enter_valid_destination), 0);
                    auto_Dropoff.setFocusableInTouchMode(true);
                    auto_Dropoff.requestFocus();
                }
            } else {
                mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_enter_valid_pickup_location), 0);
                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();
            }
        } else {
            if (selectedCar >= 0) {
                from = "BookLater";
                Intent intent = new Intent(MainActivity.this, BookLaterActivity.class);
                intent.putExtra(Common.INTENT_CAPACITY, carType_beens.get(selectedCarPosition).getCapacity());
                startActivity(intent);
            } else {
                mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_car), 0);
            }
        }
    }

    private void call_BookNow()//0 for normal request and 1 for special request.
    {
        int selectedCar = -1;

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                }
            }
        }

        dialogClass.showDialog();
        String url = WebServiceAPI.API_BOOKING_REQUEST;

        bookingRequest_beens.clear();
        //Booking now parameter
        // PassengerId,ModelId,PickupLocation,DropoffLocation,PickupLat,PickupLng,DropOffLat,DropOffLon;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PASSENGER_ID, passangerId);
        if (selectedCar != -1) {
            params.put(WebServiceAPI.PARAM_MODEL_ID, carType_beens.get(selectedCar).getId());
        }
        params.put(WebServiceAPI.PARAM_PICKUP_LOCATION, TaxiUtil.picup_Address);
        params.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, TaxiUtil.dropoff_Address);
        params.put(WebServiceAPI.PARAM_PICKUP_LAT, TaxiUtil.picup_Lat);
        params.put(WebServiceAPI.PARAM_PICKUP_LONG, TaxiUtil.picup_Long);
        params.put(WebServiceAPI.PARAM_DROPOFF_LAT, TaxiUtil.dropoff_Lat);
        params.put(WebServiceAPI.PARAM_DROPOFF_LONG, TaxiUtil.dropoff_Long);
        params.put(WebServiceAPI.PARAM_PROMO_CODE, promocodeStr);
        params.put(WebServiceAPI.PARAM_NOTES, notes);
        params.put(WebServiceAPI.PARAM_PAYMENT_TYPE, selectedPaymentType);
        params.put(WebServiceAPI.PARAM_SPECIAL, specialRequest + "");
        params.put(WebServiceAPI.PARAM_PARCEL_ID, selectedTransportService + "");
        params.put(WebServiceAPI.PARAM_REQUEST_FOR, "delivery");
        params.put(WebServiceAPI.PARAM_RECEIVER_NAME, receverFullName);
        params.put(WebServiceAPI.PARAM_RECEIVER_CONTACT_NO, receverPhoneNumber);
        params.put(WebServiceAPI.PARAM_PARCEL_WEIGHT, etParcelWeight.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_RECEIVER_EMAIL, etEmail.getText().toString().trim());

        if (selectedPaymentType.equalsIgnoreCase("card")) {
            params.put(WebServiceAPI.PARAM_CARD_ID, cardId);
        }
        Log.e("call", "cardNumber = " + cardNumber);
        Log.e("call", "cardId = " + cardId);
        Log.e("call", "selectedPaymentType = " + selectedPaymentType);
        Log.e("call", "url = " + url);
        Log.e("call", "param = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                isAlreadySendRequest = true;
                                handlerCount = 1;
                                handlerSendRequest.postDelayed(runnableSendRequest, 30000);
                                if (json.has("details")) {
                                    JSONObject details = json.getJSONObject("details");

                                    if (details != null) {
                                        String PassengerId = "", ModelId = "", PickupLocation = "", DropoffLocation = "";
                                        double PickupLat = 0, PickupLng = 0, DropOffLat = 0, DropOffLon = 0;
                                        String CreatedDate = "", Status = "", BookingId = "";

                                        if (details.has("PassengerId")) {
                                            PassengerId = details.getString("PassengerId");
                                        }

                                        if (details.has("ModelId")) {
                                            ModelId = details.getString("ModelId");
                                        }

                                        if (details.has("PickupLocation")) {
                                            PickupLocation = details.getString("PickupLocation");
                                        }

                                        if (details.has("DropoffLocation")) {
                                            DropoffLocation = details.getString("DropoffLocation");
                                        }

                                        if (details.has("PickupLat")) {
                                            PickupLat = details.getDouble("PickupLat");
                                        }

                                        if (details.has("PickupLng")) {
                                            PickupLng = details.getDouble("PickupLng");
                                        }

                                        if (details.has("DropOffLat")) {
                                            DropOffLat = details.getDouble("DropOffLat");
                                        }

                                        if (details.has("DropOffLon")) {
                                            DropOffLon = details.getDouble("DropOffLon");
                                        }

                                        if (details.has("CreatedDate")) {
                                            CreatedDate = details.getString("CreatedDate");
                                        }

                                        if (details.has("Status")) {
                                            Status = details.getString("Status");
                                        }

                                        if (details.has("BookingId")) {
                                            BookingId = details.getString("BookingId");
                                        }

                                        bookingRequest_beens.add(new BookingRequest_Been(
                                                PassengerId,
                                                ModelId,
                                                PickupLocation,
                                                DropoffLocation,
                                                PickupLat,
                                                PickupLng,
                                                DropOffLat,
                                                DropOffLon,
                                                CreatedDate,
                                                Status,
                                                BookingId
                                        ));

                                        dialogClass.hideDialog();
                                        requestPendingDialogClass.showDialog();
                                    } else {
                                        dialogClass.hideDialog();
                                    }
                                } else {
                                    dialogClass.hideDialog();
                                }
                            } else {
                                isAlreadySendRequest = false;
                                handlerCount = 0;
                                dialogClass.hideDialog();
                                if (json.has("message")) {

                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    String x = "pay your previous Due";
                                    if(json.getString("message").toLowerCase().contains(x.toLowerCase()) || json.getString("message").contains("first make payment"))
                                    {
                                        internetDialog.setListener(new InternetDialog.dialogPositiveClick() {
                                            @Override
                                            public void onOkClick() {
                                                Intent intent = new Intent(activity, PreviousDueActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                            }
                                        });
                                    }
                                    internetDialog.showDialog(json.getString("message"), getResources().getString(R.string.success_message));
                                } else {
                                    InternetDialog internetDialog = new InternetDialog(activity);
                                    internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                                }
                            }
                        } else {
                            isAlreadySendRequest = false;
                            handlerCount = 0;
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                        }
                    } else {
                        isAlreadySendRequest = false;
                        handlerCount = 0;
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    isAlreadySendRequest = false;
                    handlerCount = 0;
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void carClicked() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

//        if (handler != null && runnable != null)
//        {
        handler.removeCallbacks(runnable, null);
//        }
//
        startTimer();

        Log.e(TAG, "myLocation myLocation = " + myLocation);

        int selectedCar = -1;
        String strCategoryId = "1";
        if (carType_beens != null && carType_beens.size() > 0) {
            strCategoryId = carType_beens.get(0).getCategoryId();

            for (int i = 0; i < carType_beens.size(); i++) {
                if (carType_beens.get(i).getSelectedCar() != -1) {
                    selectedCar = carType_beens.get(i).getSelectedCar();
                }
            }
        }

        if (myLocation == false) {
            if (latLong_beens.size() > 0) {
                Log.e(TAG, "GetCarList_OnClickType() coordinate selectedCar ================= latLong_beens.size = " + latLong_beens.size());

                removeCarMarker();
//                marker = new Marker[latLong_beens.size()];

/*
                for (int i=0; i<latLong_beens.size(); i++)
                {
                    coordinate = new LatLng(latLong_beens.get(i).getLat(), latLong_beens.get(i).getLong());

                    Log.e(TAG,"GetCarList_OnClickType() coordinate1 coordinate ================= lat = "+coordinate.latitude);
                    Log.e(TAG,"GetCarList_OnClickType() coordinate2 coordinate ================= lat = "+coordinate.longitude);
                    Log.e(TAG,"GetCarList_OnClickType() coordinate3 selectedCar ================= lat = "+selectedCar);

                    if (googleMap != null)
                    {
                        if (strCategoryId!=null && (strCategoryId.equals("1") || strCategoryId.equals("2")) && selectedCar>=0)
                        {
//                            listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                        }
                        if (strCategoryId!=null && strCategoryId.equals("1"))
                        {
//                            listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                           */
/* if (selectedCar==0)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==1)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==2)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==3)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==4)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==5)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==6)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }*//*

                        }
                        else if (strCategoryId!=null && strCategoryId.equals("2"))
                        {
//                            listMarker.add(googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car))));
                            */
/*if (selectedCar==0)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==1)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==2)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==3)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==4)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==5)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }
                            else if (selectedCar==6)
                            {
                                marker[i] = googleMap.addMarker(new MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_coomon_car)));
                            }*//*

                        }

                        if (zoomFlag==0)
                        {
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));
                            zoomFlag = 1;
                        }
                    }
                }
*/
            }
        }
    }

    private void removeCarMarker() {
        if (listMarker != null && listMarker.size() > 0) {
            for (int i = 0; i < listMarker.size(); i++) {
                if (listMarker.get(i) != null) {
                    listMarker.get(i).remove();
                }
            }
            listMarker.clear();
        }
    }

    //For first car
    private void call_One() {
        Log.e("call", "carType_beens null");

        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }

        startTimer();
    }

    //For second car
    private void call_Two() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }

        startTimer();
    }

    //For third car
    private void call_Three() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }


        startTimer();
    }

    //For four car
    private void call_Four() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }

        startTimer();
    }

    //For Fifth car
    private void call_Five() {
        current_marker_layout.setVisibility(View.GONE);
        zoomFlag = 0;
        myLocation = false;
        removeCarMarker();

        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
        }

        startTimer();
    }

    /////////////for pickup location......................................................................

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress) {
        try {
            Log.e("call", "getLocationFromAddress()");
            if (addFlag == 1) {
                Log.e("call", "getLocationFromAddress() 111");
                if (auto_Pickup.getText().toString().trim().length() > 0) {
                    Log.e("call", "getLocationFromAddress() 222");
                    TaxiUtil.picup_Address = auto_Pickup.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + getResources().getString(R.string.map_api_key);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                } else {
                    Log.e("call", "getLocationFromAddress() 333");
                }
            } else if (addFlag == 2) {
                Log.e("call", "getLocationFromAddress() 444");
                if (auto_Dropoff.getText().toString().trim().length() > 0) {
                    Log.e("call", "getLocationFromAddress() 555");
                    TaxiUtil.dropoff_Address = auto_Dropoff.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + getResources().getString(R.string.map_api_key);
                    new GetGeoCoderAddress(encode_url, strAddress).execute();
                } else {
                    Log.e("call", "getLocationFromAddress() 666");
                }
            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            Log.e("call", "getLocationFromAddress() exception = " + ex.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            if (addFlag == 1) {
                Log.e("call", "getLocationFromAddress() exception 111= " + e.getMessage());
                TaxiUtil.picup_Lat = 0.0;
                TaxiUtil.picup_Long = 0.0;
                TaxiUtil.picup_Address = "";
            } else if (addFlag == 2) {
                Log.e("call", "getLocationFromAddress() exception 222= " + e.getMessage());
                TaxiUtil.dropoff_Lat = 0.0;
                TaxiUtil.dropoff_Long = 0.0;
                TaxiUtil.dropoff_Address = "";
            }
        }
    }

    // Parse the google API result and loaded into list
    public class ParseData extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                json = GetAddress(jsonurl.toString());

                Log.e("call", "json ParseData = " + json.toString());

                if (json != null) {
                    names.clear();
                    Place_id_type.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        names.add(description);
                        Place_id_type.add(plc_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("names", "onPostExecute addFlag = " + addFlag);

            if (addFlag == 1) {
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                    }

                                    if (googleMap != null) {
                                        googleMap.clear();
                                    }

                                    current_marker_layout.setVisibility(View.VISIBLE);
                                    //ll_Done.setVisibility(View.VISIBLE);
                                    ll_BookingLayout.setVisibility(View.GONE);
                                    ll_AllCarLayout.setVisibility(View.GONE);

                                    pickup_Touch = true;
                                    isPickupDropoff = 1;
                                    auto_Pickup.setText(text.getText().toString());
                                    tvPickup.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Pickup.dismissDropDown();
                                    auto_Dropoff.setFocusableInTouchMode(true);
                                    auto_Dropoff.requestFocus();
                                }
                            });
                            return view;
                        }
                    };
                    auto_Pickup.setAdapter(adp);
                    adp.notifyDataSetChanged();

//                    if (isAutocomplete==false)
//                    {
//                        getEstimatedFareCheckValidation();
//                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    Log.e("call", "ParseData exception = " + e.getMessage());
                }
            } else if (addFlag == 2) {
                try {
                    ArrayAdapter<String> adp;
                    adp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, names) {
                        @Override
                        public View getView(final int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            final TextView text = view.findViewById(android.R.id.text1);
                            text.setTextColor(getResources().getColor(R.color.colorBlack));
                            text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    if (Place_id_type.size() > 0) {
                                        place_id = Place_id_type.get(position);
                                        System.out.println("Place_id_type" + Place_id_type.get(position));
                                    }

                                    dropoff_Touch = true;
                                    isPickupDropoff = 2;
                                    current_marker_layout.setVisibility(View.VISIBLE);
                                    //ll_Done.setVisibility(View.VISIBLE);
                                    ll_BookingLayout.setVisibility(View.GONE);
                                    ll_AllCarLayout.setVisibility(View.GONE);
                                    auto_Dropoff.setText(text.getText().toString());
                                    getLocationFromAddress(text.getText().toString());
                                    auto_Dropoff.dismissDropDown();
                                    if (auto_Pickup.getText().toString().trim().length() <= 0) {
                                        auto_Pickup.setFocusableInTouchMode(true);
                                        auto_Pickup.requestFocus();
                                    }
                                }
                            });
                            return view;
                        }
                    };

                    auto_Dropoff.setAdapter(adp);
                    adp.notifyDataSetChanged();

//                    if (isAutocomplete==false)
//                    {
//                        getEstimatedFareCheckValidation();
//                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    Log.e("call", "ParseData exception = " + e.getMessage());
                }
            }
        }
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        private String strAddress;

        public GetGeoCoderAddress(String url, String strAddress) {
            this.Urlcoreconfig = url;
            this.strAddress = strAddress;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                JSONObject json = new JSONObject(jsonResult);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                Log.e("call", "GetGeoCoderAddress onPostExecute ");
                Geocoder coder = new Geocoder(activity);
                List<Address> address;
                Address location = null;

                if (lat != null && !lat.equalsIgnoreCase("") && lng != null && !lng.equalsIgnoreCase("")) {
                    Log.e("call", "GetGeoCoderAddress onPostExecute 111");
                    if (addFlag == 1) {
                        Log.e("call", "GetGeoCoderAddress onPostExecute 222");
                        TaxiUtil.picup_Lat = Double.valueOf(lat);
                        TaxiUtil.picup_Long = Double.valueOf(lng);
                        if (auto_Pickup.getText().toString().trim().length() > 0) {
                            TaxiUtil.picup_Address = auto_Pickup.getText().toString();
                        }
                        if (googleMap != null) {
                            LatLng pickupLanlong = new LatLng(TaxiUtil.picup_Lat, TaxiUtil.picup_Long);
//                            if (markerPickup!=null)
//                            {
//                                markerPickup.remove();
//                            }
//                            markerPickup = googleMap.addMarker(new MarkerOptions().position(pickupLanlong).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                            markerPickup.setDraggable(true);
//                            markerPickup.setTag("1");
//                            if (dropoff_Touch && pickup_Touch)
//                            {
//                                showRoute(0);
//                            }

                            isAutocomplete = true;
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(pickupLanlong).zoom(defaltZoomLavel).build()));
                        } else {
                            Log.e("call", "googleMapnull");
                        }
                    } else if (addFlag == 2) {
                        Log.e("call", "GetGeoCoderAddress onPostExecute 333");
                        TaxiUtil.dropoff_Lat = Double.valueOf(lat);
                        TaxiUtil.dropoff_Long = Double.valueOf(lng);
                        if (auto_Dropoff.getText().toString().trim().length() > 0) {
                            TaxiUtil.dropoff_Address = auto_Dropoff.getText().toString();
                        }
                        Log.e("call", "GetGeoCoderAddress onPostExecute 444");
                        if (googleMap != null) {
                            Log.e("call", "GetGeoCoderAddress onPostExecute 555");
                            LatLng dropoffLanlong = new LatLng(TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
//                            if (markerDropoff!=null)
//                            {
//                                markerDropoff.remove();
//                            }
//                            markerDropoff = googleMap.addMarker(new MarkerOptions().position(dropoffLanlong).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                            markerDropoff.setDraggable(true);
//                            markerDropoff.setTag("2");
//                            if (dropoff_Touch && pickup_Touch)
//                            {
//                                Log.e("call","GetGeoCoderAddress onPostExecute 666");
//                                showRoute(2);
//                            }
                            isAutocomplete = true;
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(dropoffLanlong).zoom(defaltZoomLavel).build()));
                        } else {
                            Log.e("call", "googleMapnull");
                        }
                    }
                } else {
                    Log.e("call", "GetGeoCoderAddress onPostExecute 444");
                    address = coder.getFromLocationName(strAddress, 5);
                    if (!address.isEmpty()) {
                        location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();
                    }

                    if (!address.isEmpty()) {
                        Log.e("call", "GetGeoCoderAddress onPostExecute 555");
                        if (addFlag == 1) {
                            TaxiUtil.picup_Lat = location.getLatitude();
                            TaxiUtil.picup_Long = location.getLongitude();
                            if (auto_Pickup.getText().toString().trim().length() > 0) {
                                TaxiUtil.picup_Address = auto_Pickup.getText().toString();
                            }

                            if (googleMap != null) {
                                LatLng pickupLanlong = new LatLng(TaxiUtil.picup_Lat, TaxiUtil.picup_Long);
//                                if (markerPickup!=null)
//                                {
//                                    markerPickup.remove();
//                                }
//                                markerPickup = googleMap.addMarker(new MarkerOptions().position(pickupLanlong).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
//                                markerPickup.setDraggable(true);
//                                markerPickup.setTag("1");
//                                if (dropoff_Touch && pickup_Touch)
//                                {
//                                    showRoute(0);
//                                }
                                isAutocomplete = true;
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(pickupLanlong).zoom(defaltZoomLavel).build()));
                            } else {
                                Log.e("call", "googleMapnull");
                            }
                        } else if (addFlag == 2) {
                            Log.e("call", "GetGeoCoderAddress onPostExecute 666");
                            TaxiUtil.dropoff_Lat = location.getLatitude();
                            TaxiUtil.dropoff_Long = location.getLongitude();
                            if (auto_Dropoff.getText().toString().trim().length() > 0) {
                                TaxiUtil.dropoff_Address = auto_Dropoff.getText().toString();
                            }

                            if (googleMap != null) {
                                LatLng dropoffLanlong = new LatLng(TaxiUtil.dropoff_Lat, TaxiUtil.dropoff_Long);
//                                if (markerDropoff!=null)
//                                {
//                                    markerDropoff.remove();
//                                }
//                                markerDropoff = googleMap.addMarker(new MarkerOptions().position(dropoffLanlong).icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location)));
////                                markerDropoff.setDraggable(true);
////                                markerDropoff.setTag("2");
//                                if (dropoff_Touch && pickup_Touch)
//                                {
//                                    showRoute(2);
//                                }
                                isAutocomplete = true;
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(dropoffLanlong).zoom(defaltZoomLavel).build()));
                            } else {
                                Log.e("call", "googleMapnull");
                            }
                        }
                    }
                }
                favorite = 0;
                favorite_address = "";
                from = "";
//                getEstimatedFareCheckValidation();
            } catch (Exception e) {
                Log.e("call", "Exception " + e.getMessage());
            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public void saveCardList() {
        try {
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST, activity);
            cardListBeens.clear();

            if (strCardList != null && !strCardList.equalsIgnoreCase("")) {
                JSONObject json = new JSONObject(strCardList);

                if (json != null) {
                    if (json.has("cards")) {
                        JSONArray cards = json.getJSONArray("cards");

                        if (cards != null && cards.length() > 0) {
                            for (int i = 0; i < cards.length(); i++) {
                                JSONObject cardsData = cards.getJSONObject(i);

                                if (cardsData != null) {
                                    String Id = "", CardNum = "", CardNum_ = "", Type = "", Alias = "", Expiry = "";

                                    if (cardsData.has("Id")) {
                                        Id = cardsData.getString("Id");
                                    }

                                    if (cardsData.has("CardNum")) {
                                        CardNum = cardsData.getString("CardNum");
                                    }

                                    if (cardsData.has("CardNum2")) {
                                        CardNum_ = cardsData.getString("CardNum2");
                                    }

                                    if (cardsData.has("Type")) {
                                        Type = cardsData.getString("Type");
                                    }

                                    if (cardsData.has("Alias")) {
                                        Alias = cardsData.getString("Alias");
                                    }

                                    if (cardsData.has("Expiry")) {
                                        Expiry = cardsData.getString("Expiry");
                                    }

                                    //cardListBeens.add(new CreditCard_List_Been(Id, CardNum, CardNum_, Type, Alias, Expiry));
                                }
                            }
                            cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
                            cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                        } else {
                            cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
//                            cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                            cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                        }
                    } else {
                        cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
//                        cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                        cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                    }
                } else {
                    cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
//                    cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                    cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
                }
            } else {
                cardListBeens.add(new CreditCard_List_Been("", "", getResources().getString(R.string.cash), "", "", ""));
//                cardListBeens.add(new CreditCard_List_Been("","","wallet","","",""));
                cardListBeens.add(new CreditCard_List_Been("","","Paytm","","",""));
            }
        } catch (Exception e) {

        }
    }

    /**
     * Listener used to get updates from KalmanLocationManager (the good old Android LocationListener).
     */
    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {


            } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {


            } else {

                kalmanLat = location.getLatitude();
                kalmanLong = location.getLongitude();
                Log.e("call", "LocationManager.KALMAN_PROVIDER");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            String statusString = getResources().getString(R.string.unknown);

            switch (status) {

                case LocationProvider.OUT_OF_SERVICE:

                    break;

                case LocationProvider.TEMPORARILY_UNAVAILABLE:

                    break;

                case LocationProvider.AVAILABLE:

                    break;
            }
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        if (mKalmanLocationManager != null) {
            mKalmanLocationManager.requestLocationUpdates(KalmanLocationManager.UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, mLocationListener, true);
        }

        TruckzShipperApplication.setCurrentActivity(activity);
        saveCardList();

//        if (upcomming_pickup_long!=0 && upcomming_pickup_lat!=0 && upcomming_dropoff_long!=0 && upcomming_dropoff_lat!=0)
//        {
//            showUpCommingTripLocation();
//        }
//        else
        if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("TripCompleteActivity")) {
            showTripCompletePopup("", activity, BookingId, bookingType);
            bookingType="";
            //showTripCompletePopup("", activity, BookingId, WebServiceAPI.BOOK_NOW);
            //showTripCompletePopup("", activity, BookingId, WebServiceAPI.BOOK_LATER);
            startNew();
        } else if (addCardFlag == 1) {
            addCardFlag = 0;
            openPromocodePopup();
        }

        if (favorite != 0 &&
                from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("FavoriteActivity")) {
            dropoff_Touch = true;
            if (auto_Dropoff != null) {

            } else {
                auto_Dropoff = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_drop_off);
            }
            tvDropoff.setText(favorite_address);
            auto_Dropoff.dismissDropDown();
            if (!TextUtils.isEmpty(tvPickup.getText().toString().trim()) &&
                    !TextUtils.isEmpty(tvDropoff.getText().toString().trim())) {
                callAddressDone();
            }
            from = "";
        }

        if (redirectBooking != null && !redirectBooking.equalsIgnoreCase("")) {
            if (redirectBooking.equalsIgnoreCase("booknow")) {
                mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_car_model), 0);
                dropoff_Touch = true;
                if (redirectBookingAdrs != null && !redirectBookingAdrs.equalsIgnoreCase("")) {
                    auto_Dropoff.setText(redirectBookingAdrs);
                    auto_Dropoff.dismissDropDown();
                }
                redirectBooking = "";
                redirectBookingAdrs = "";
            } else if (redirectBooking.equalsIgnoreCase("booklater")) {
                auto_Dropoff.setText("");
                tvDropoff.setText("");
                mySnackBar.showSnackBar(ll_RootLayour, getResources().getString(R.string.please_select_car_model_and_click_booklater), 0);
            }
        } else if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("BookLater")) {
            redirectBooking = "";
            redirectBookingAdrs = "";
            from = "";
            callSelectService(2);
            startNew();
        }
    }

    public void selectPickup() {
        isPickupDropoff = 1;
        isCardPickUpSelected = true;
        card_dropoffLoc.setCardElevation(-5);
        card_pickupLocation.setCardElevation(5);
    }

    public void startNew() {
        isPickupDropoff = 1;
        isCardPickUpSelected = true;
        card_dropoffLoc.setCardElevation(-5);
        card_pickupLocation.setCardElevation(5);

        auto_Pickup = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_pickup);
        auto_Dropoff = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_drop_off);

        driverName = "";
        from = "";
        myLocation = true;
        isDone = 0;
//        disconnectSocket();
        favorite_address = "";
        isPickupDropoff = 1;
        polyline = null;
        gpsTracker = new GPSTracker(activity);
        timeFlag = 0;
        isAutocomplete = true;
        activity = MainActivity.this;
        handler = new Handler();
        /*--------------28-6-19----------------*/
        startTimer();
        /*--------------28-6-19----------------*/
        handlerDriver = new Handler();
        passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
        dialogClass = new DialogClass(activity, 0);
        requestPendingDialogClass = new RequestPendingDialogClass(activity, 0);
        aQuery = new AQuery(activity);
        ll_RootLayour = (LinearLayout) findViewById(R.id.main_content);
        mySnackBar = new MySnackBar(activity);

        TaxiUtil.dropoff_Lat = 0;
        TaxiUtil.dropoff_Long = 0;
        TaxiUtil.dropoff_Address = "";
        dropoff_Touch = false;
        auto_Dropoff.setText("");
        tvDropoff.setText("");
        auto_Pickup.setFocusableInTouchMode(true);
        auto_Pickup.requestFocus();
        isPickupDropoff = 1;


        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity) != null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity).equalsIgnoreCase("") &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity).equalsIgnoreCase("null")) {
            ratting = Float.parseFloat(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_RATTING, activity));
        }

        getEstimateFare_beens.clear();
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "1", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "2", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "3", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "4", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "5", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "6", "0"));
        getEstimateFare_beens.add(new GetEstimateFare_Been("", "", "", "", "", "", "", "0", "0", "7", "0"));
        pickup_Touch = false;
        dropoff_Touch = false;
        advanceBooking = 0;
        addFlag = 1;
        carClass = null;
        pickUp = null;
        driverLatLng = null;
        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                carType_beens.get(i).setSelectedCar(-1);
                carType_beens.get(i).setAvai(0);
            }
        }
        zoomFlag = 0;
        BookingId = "";
        driverPhone = "";
        carType_beens.clear();
        tripFlag = 0;
        driverId = "";
        getEstimated_lat = 0;
        getEstimated_long = 0;
        getEstimated_Pickup = "";
        selectedCarPosition = -1;
        bookingRequest_beens.clear();
        bookingDriverLocation_beens.clear();
        requestConfirmJsonObject = null;

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity).equalsIgnoreCase("")) {
            favorite = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FAVORITE_TYPE, activity));

            if (favorite != 0) {
                iv_Favorite.setImageResource(R.drawable.ic_fav_star);
            } else {
                iv_Favorite.setImageResource(R.drawable.ic_fav_star);
            }
        } else {
            iv_Favorite.setImageResource(R.drawable.ic_fav_star);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
        } else {
            tripFlag = 0;
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
            selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
            try {
                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
            } catch (Exception e) {

            }
        }

        displayImage();
//        connectSocket();
        if (tripFlag == 0) {
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.VISIBLE);
            ll_AfterRequestAccept.setVisibility(View.GONE);
            //ll_Done.setVisibility(View.VISIBLE);
            current_marker_layout.setVisibility(View.VISIBLE);
            myLocation = true;
        } else {
            current_marker_layout.setVisibility(View.GONE);
            //ll_Done.setVisibility(View.GONE);
            ll_BookingLayout.setVisibility(View.GONE);
            ll_AllCarLayout.setVisibility(View.GONE);
            address_Layout.setVisibility(View.GONE);
            ll_AfterRequestAccept.setVisibility(View.VISIBLE);
            tv_DriverInfo.setVisibility(View.VISIBLE);
            if (tripFlag == 1) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.VISIBLE);
                parseAcceptRequestObject(requestConfirmJsonObject, 1);
            } else if (tripFlag == 2) {
                myLocation = false;
                tv_CancelRequest.setVisibility(View.GONE);
                showPathFromPickupToDropoff(requestConfirmJsonObject);
            }
        }

        checkGPS();
        auto_Pickup.setText("");
        tvPickup.setText("");
        auto_Dropoff.setText("");
        tvDropoff.setText("");
        TaxiUtil.dropoff_Address = "";
        TaxiUtil.picup_Address = "";
        TaxiUtil.picup_Lat = 0;
        TaxiUtil.picup_Long = 0;
        TaxiUtil.dropoff_Lat = 0;
        TaxiUtil.dropoff_Long = 0;

        iv_MyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myLocation = true;
                isPickupDropoff = 1;
                current_marker_layout.setVisibility(View.VISIBLE);
                ll_Done.setVisibility(View.VISIBLE);
                callSelectService(2);
                auto_Dropoff.setText("");
                tvDropoff.setText("");
                TaxiUtil.dropoff_Address = "";
                TaxiUtil.dropoff_Long = 0;
                TaxiUtil.dropoff_Lat = 0;
                dropoff_Touch = false;
                isDone = 0;
                if (carType_beens != null && carType_beens.size() > 0) {
                    for (int i = 0; i < carType_beens.size(); i++) {
                        carType_beens.get(i).setSelectedCar(-1);
                    }
                }

                if (carAdapter != null) {
                    carAdapter.notifyDataSetChanged();
                    recyclerViewCar.scrollToPosition(0);
                }
                checkGPS();

                ll_AllCarLayout.setVisibility(View.GONE);
                ll_BookingLayout.setVisibility(View.GONE);
            }
        });

        removeCarMarker();

        if (carAdapter != null) {
            carAdapter.notifyDataSetChanged();
            recyclerViewCar.scrollToPosition(0);
        }
        getAddressFromLatLong();
        call_DriverInfo();
    }

    public void displayImage() {
        Log.e(TAG, "displayImage()");

//        if (tripFlag==0)
//        {
//            if (carType_beens!=null && carType_beens.size()>0)
//            {
//                if (carType_beens.get(0).getImage()!=null && !carType_beens.get(0).getImage().equalsIgnoreCase(""))
//                {
//                    Log.e(TAG,"displayImage() iv_CarOne:- " + carType_beens.get(0).getImage());
//
//                    Picasso.with(activity).load(carType_beens.get(0).getImage()).into(iv_CarOne);
//                }
//                else
//                {
//                    iv_CarOne.setVisibility(View.INVISIBLE);
//                }
//
//                if (carType_beens.get(1).getImage()!=null && !carType_beens.get(1).getImage().equalsIgnoreCase(""))
//                {
//                    Log.e(TAG,"displayImage() iv_CarTwo:- " + carType_beens.get(1).getImage());
//                    Picasso.with(activity).load(carType_beens.get(1).getImage()).into(iv_CarTwo);
//                }
//                else
//                {
//                    iv_CarTwo.setVisibility(View.INVISIBLE);
//                }
//
//                if (carType_beens.get(2).getImage()!=null && !carType_beens.get(2).getImage().equalsIgnoreCase(""))
//                {
//                    Log.e(TAG,"displayImage() iv_CarThree:- " + carType_beens.get(2).getImage());
//                    Picasso.with(activity).load(carType_beens.get(2).getImage()).into(iv_CarThree);
//                }
//                else
//                {
//                    iv_CarThree.setVisibility(View.INVISIBLE);
//                }
//
//                if (carType_beens.get(3).getImage()!=null && !carType_beens.get(3).getImage().equalsIgnoreCase(""))
//                {
//                    Log.e(TAG,"displayImage() iv_CarFour:- " + carType_beens.get(3).getImage());
//                    Picasso.with(activity).load(carType_beens.get(3).getImage()).into(iv_CarFour);
//                }
//                else
//                {
//                    iv_CarFour.setVisibility(View.INVISIBLE);
//                }
//
//                if (carType_beens.get(4).getImage()!=null && !carType_beens.get(4).getImage().equalsIgnoreCase(""))
//                {
//                    Log.e(TAG,"displayImage() iv_CarFive:- " + carType_beens.get(4).getImage());
//                    Picasso.with(activity).load(carType_beens.get(4).getImage()).into(iv_CarFive);
//                }
//                else
//                {
//                    iv_CarFive.setVisibility(View.INVISIBLE);
//                }
//
////                if (carType_beens.get(5).getImage()!=null && !carType_beens.get(5).getImage().equalsIgnoreCase(""))
////                {
////                    Log.e(TAG,"displayImage() iv_CarSix:- " + carType_beens.get(5).getImage());
////                    Picasso.with(activity).load(carType_beens.get(5).getImage()).into(iv_CarSix);
////                }
////                else
////                {
////                    iv_CarSix.setVisibility(View.INVISIBLE);
////                }
//
////                if (carType_beens.get(6).getImage()!=null && !carType_beens.get(6).getImage().equalsIgnoreCase(""))
////                {
////                    Picasso.with(activity).load(carType_beens.get(6).getImage()).into(iv_CarSeven);
////                }
////                else
////                {
////                    iv_CarSeven.setVisibility(View.INVISIBLE);
////                }
//            }
//        }
    }

//    private void showUpCommingTripLocation()
//    {
//        handler.removeCallbacks(runnable,null);
//        handlerDriver.removeCallbacks(runnableDriver,null);
//
//        MarkerOptions options = new MarkerOptions();
//        options.position(new LatLng(upcomming_pickup_lat,upcomming_pickup_long));
////        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
//        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_current_location));
//
//        MarkerOptions options1 = new MarkerOptions();
//        options1.position(new LatLng(upcomming_dropoff_lat,upcomming_dropoff_long));
//
//        if (selectedCar==0)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==1)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==2)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==3)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==4)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==5)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//        else if (selectedCar==6)
//        {
//            options1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pick_coomon_car));
//        }
//
//        // Add new marker to the Google Map Android API V2
//
//        removeCarMarker();
//
//        if (originMarker!=null)
//        {
//            originMarker.remove();
//        }
//
//
//        if (destinationMarker!=null)
//        {
//            destinationMarker.remove();
//        }
//
//        originMarker = googleMap.addMarker(options);
    //        destinationMarker = googleMap.addMarker(options1);
//        String url = getDirectionsUrl(new LatLng(upcomming_pickup_lat,upcomming_pickup_long), new LatLng(upcomming_dropoff_lat,upcomming_dropoff_long));
//
//        Log.e("url","--------- "+url);
//        DownloadTask downloadTask = new DownloadTask();
//
//        // Start downloading json data from Google Directions API
//        downloadTask.execute(url);
//    }

    public void findPriceAndTime() {
        try {
            if (Common.socket != null && Common.socket.connected()) {
                passangerId = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, activity);
                String pickup_Location = tvPickup.getText().toString().trim();
                String dropoff_Location = tvDropoff.getText().toString().trim();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(WebServiceAPI.PARAM_PASSENGER_ID, passangerId);
                if (pickup_Touch == true) {
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LOCATION, pickup_Location);
                    if (dropoff_Touch == true) {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, dropoff_Location);
                    } else {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, pickup_Location);
                    }
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LAT, TaxiUtil.picup_Lat);
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LONG_, TaxiUtil.picup_Long);
                } else {
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LOCATION, getEstimated_Pickup);
                    if (dropoff_Touch == true) {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, dropoff_Location);
                    } else {
                        jsonObject.put(WebServiceAPI.PARAM_DROPOFF_LOCATION, getEstimated_Pickup);
                    }
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LAT, getEstimated_lat);
                    jsonObject.put(WebServiceAPI.PARAM_PICKUP_LONG_, getEstimated_long);
                }
                jsonObject.put(WebServiceAPI.PARAM_IDS, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_DRIVER_ID, activity));

                Log.e("call", "EstimateFare EstimateFare EstimateFare ==== " + jsonObject.toString());
                Common.socket.emit("EstimateFare", jsonObject);
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    private Emitter.Listener onGetEstimateFare = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "" + " onGetEstimateFare");
                        if (args != null && args.length > 0) {
                            Log.e("call", "onGetEstimateFare response = " + args[0].toString());
                            JSONObject json = new JSONObject(args[0].toString());
                            getEstimateFare_beens.clear();
                            if (json != null) {
                                if (json.has("status")) {
                                    if (json.getBoolean("status")) {
                                        if (json.has("estimate_fare")) {
                                            JSONArray estimate_fare = json.getJSONArray("estimate_fare");

                                            if (estimate_fare != null && estimate_fare.length() > 0) {
                                                String arraySortFlag[] = new String[estimate_fare.length()];

                                                for (int i = 0; i < estimate_fare.length(); i++) {
                                                    JSONObject jsonObject = estimate_fare.getJSONObject(i);

                                                    if (jsonObject != null) {
                                                        String per_km_charge = "", id = "", name = "", base_fare = "", km = "";
                                                        String trip_fare = "", booking_fee = "", total = "0", duration = "No Truck", sortFlag = "", capacity = "0";

                                                        if (jsonObject.has("per_km_charge")) {
                                                            per_km_charge = jsonObject.getString("per_km_charge");
                                                        }

                                                        if (jsonObject.has("id")) {
                                                            id = jsonObject.getString("id");
                                                        }

                                                        if (jsonObject.has("name")) {
                                                            name = jsonObject.getString("name");
                                                        }

                                                        if (jsonObject.has("base_fare")) {
                                                            base_fare = jsonObject.getString("base_fare");
                                                        }

                                                        if (jsonObject.has("km")) {
                                                            km = jsonObject.getString("km");
                                                        }

                                                        if (jsonObject.has("trip_fare")) {
                                                            trip_fare = jsonObject.getString("trip_fare");
                                                        }

                                                        if (jsonObject.has("booking_fee")) {
                                                            booking_fee = jsonObject.getString("booking_fee");
                                                        }

                                                        if (jsonObject.has("total")) {
                                                            double totalDouble = Double.parseDouble(jsonObject.getString("total"));
                                                            total = String.format("%.2f", totalDouble);
                                                        }

                                                        if (jsonObject.has("duration")) {
                                                            duration = jsonObject.getString("duration");
                                                        }

                                                        if (jsonObject.has("capacity")) {
                                                            capacity = jsonObject.getString("capacity");
                                                        }

                                                        if (carType_beens.size() > 0) {
                                                            inner:
                                                            for (int j = 0; j < carType_beens.size(); j++) {
                                                                if (id != null && !id.trim().equalsIgnoreCase("") && id.trim().equalsIgnoreCase(carType_beens.get(j).getId())) {
                                                                    sortFlag = carType_beens.get(j).getSort();
                                                                    break inner;
                                                                }
                                                            }
                                                        }

                                                        getEstimateFare_beens.add(new GetEstimateFare_Been(
                                                                per_km_charge,
                                                                id,
                                                                name,
                                                                base_fare,
                                                                km,
                                                                trip_fare,
                                                                booking_fee,
                                                                total,
                                                                duration,
                                                                sortFlag,
                                                                capacity
                                                        ));
                                                        Log.e("call", "getEstimateFare_beens sort flag = " + getEstimateFare_beens.get(i).getSort());
                                                    }
                                                }


                                                /*Collections.sort(
                                                        getEstimateFare_beens,
                                                        new Comparator<GetEstimateFare_Been>()
                                                        {
                                                            public int compare(GetEstimateFare_Been lhs, GetEstimateFare_Been rhs)
                                                            {
                                                                return lhs.getSort().compareTo(rhs.getSort());
                                                            }
                                                        }
                                                );*/

                                                timeFlag = 0;

                                                if (carAdapter != null && recyclerViewCar != null) {
//                                                  carAdapter.notifyDataSetChanged();

                                                    recyclerViewCar.setAdapter(carAdapter);
                                                    recyclerViewCar.scrollToPosition(0);

                                                    carAdapter.notifyDataSetChanged();
                                                }
                                            } else {
                                                timeFlag = 0;
                                                setTimeAndPriceToZero();
                                            }
                                        } else {
                                            timeFlag = 0;
                                            setTimeAndPriceToZero();
                                        }
                                    } else {
                                        timeFlag = 0;
                                        setTimeAndPriceToZero();
                                    }
                                } else {
                                    timeFlag = 0;
                                    setTimeAndPriceToZero();
                                }
                            } else {
                                timeFlag = 0;
                                setTimeAndPriceToZero();
                            }
                        }
                    } catch (Exception e) {
                        setTimeAndPriceToZero();
                    }
                }
            });
        }
    };

    public void setTimeAndPriceToZero() {
        if (getEstimateFare_beens != null && getEstimateFare_beens.size() > 0) {
            for (int i = 0; i < getEstimateFare_beens.size(); i++) {
                getEstimateFare_beens.get(i).setDuration("No Truck");
                getEstimateFare_beens.get(i).setTotal("0");
            }
        }

        if (carType_beens != null && carType_beens.size() > 0) {
            for (int i = 0; i < carType_beens.size(); i++) {
                carType_beens.get(i).setAvai(0);
            }
        }

        if (carAdapter != null && recyclerViewCar != null) {
//          carAdapter.notifyDataSetChanged();
            recyclerViewCar.setAdapter(carAdapter);
            recyclerViewCar.scrollToPosition(0);
        }
    }

    // FOR ADVANCE BOOKING SOCKET CALL..............................................................................

    private Emitter.Listener onAcceptAdvancedBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        Log.e("call", "onAcceptAdvancedBookingRequestNotification()");
                        if (args != null && args.length > 0) {
                            Log.e("call", "onAcceptAdvancedBookingRequestNotification response = " + args[0].toString());
                            int selectedCar = -1;
                            JSONObject tripCompleteObject = new JSONObject(args[0].toString());

                            if (tripCompleteObject != null) {
                                if (tripCompleteObject.has("BookingInfo") && !tripCompleteObject.getString("BookingInfo").equalsIgnoreCase("")) {
                                    JSONArray InfoArray = tripCompleteObject.getJSONArray("BookingInfo");

                                    if (InfoArray != null && InfoArray.length() > 0) {
                                        JSONObject InfoData = InfoArray.getJSONObject(0);
                                        if (InfoData != null) {
                                            if (InfoData.has("Id") && InfoData.getString("Id") != null && !InfoData.getString("Id").equalsIgnoreCase("")) {
                                                if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                    if (BookingId.equalsIgnoreCase(InfoData.getString("Id"))) {
                                                        if (tripFlag == 0) {
                                                            advanceBooking = 1;

                                                            if (args != null && args.length > 0) {
                                                                Log.e("call", "onAcceptAdvancedBookingRequestNotification = " + args[0].toString());
                                                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                                    commonInternetDialog.hideDialog();
                                                                }

                                                                if (commonDialog != null && commonDialog.isShowing()) {
                                                                    commonDialog.dismiss();
                                                                }

                                                                zoomFlag = 0;

                                                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                                                Log.e("call", "jsonObject = " + jsonObject.toString());
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, args[0].toString(), activity);

                                                                if (jsonObject != null) {
                                                                    if (jsonObject.has("BookingInfo")) {
                                                                        Log.e("call", "BookingInfo 111");
                                                                        JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");
                                                                        Log.e("call", "BookingInfo 222");
                                                                        if (BookingInfo != null && BookingInfo.length() > 0) {
                                                                            Log.e("call", "BookingInfo 333");
                                                                            JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                                                                            if (BookingInfoObject != null) {
                                                                                Log.e("call", "BookingInfo 444");
                                                                                double lat = 0, lng = 0;

                                                                                if (BookingInfoObject.has("PickupLat")) {
                                                                                    Log.e("call", "BookingInfo 555");
                                                                                    lat = BookingInfoObject.getDouble("PickupLat");
                                                                                    Log.e("call", "BookingInfo 666");
                                                                                }

                                                                                if (BookingInfoObject.has("PickupLng")) {
                                                                                    Log.e("call", "BookingInfo 777");
                                                                                    lng = BookingInfoObject.getDouble("PickupLng");
                                                                                    Log.e("call", "BookingInfo 888");
                                                                                }

                                                                                if (BookingInfoObject.has("Id")) {
                                                                                    Log.e("call", "BookingInfo 999");
                                                                                    BookingId = BookingInfoObject.getString("Id");
                                                                                    Log.e("call", "BookingInfo 10 10 10");
                                                                                }

                                                                                if (BookingInfoObject.has("ModelId")) {
                                                                                    Log.e("call", "BookingInfo 999");
                                                                                    String model = BookingInfoObject.getString("ModelId");

                                                                                    selectedCar = -1;

                                                                                    if (model.equalsIgnoreCase("1")) {
                                                                                        selectedCar = 0;
                                                                                    } else if (model.equalsIgnoreCase("2")) {
                                                                                        selectedCar = 1;
                                                                                    } else if (model.equalsIgnoreCase("3")) {
                                                                                        selectedCar = 2;
                                                                                    } else if (model.equalsIgnoreCase("4")) {
                                                                                        selectedCar = 3;
                                                                                    } else if (model.equalsIgnoreCase("5")) {
                                                                                        selectedCar = 4;
                                                                                    } else if (model.equalsIgnoreCase("6")) {
                                                                                        selectedCar = 5;
                                                                                    } else if (model.equalsIgnoreCase("7")) {
                                                                                        selectedCar = 6;
                                                                                    }

                                                                                    if (carType_beens != null && carType_beens.size() >= selectedCar)
                                                                                        ;
                                                                                    {
                                                                                        for (int i = 0; i < carType_beens.size(); i++) {
                                                                                            if (i == selectedCar) {
                                                                                                carType_beens.get(i).setSelectedCar(selectedCar);
                                                                                            } else {
                                                                                                carType_beens.get(i).setSelectedCar(-1);
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    if (carAdapter != null) {
                                                                                        carAdapter.notifyDataSetChanged();
                                                                                        recyclerViewCar.scrollToPosition(0);
                                                                                    }
                                                                                }

                                                                                pickUp = new LatLng(lat, lng);
                                                                            }
                                                                        }
                                                                    }

                                                                    if (jsonObject.has("DriverInfo")) {
                                                                        Log.e("call", "DriverInfo 111");
                                                                        JSONArray DriverInfo = jsonObject.getJSONArray("DriverInfo");
                                                                        Log.e("call", "DriverInfo 222");
                                                                        if (DriverInfo != null && DriverInfo.length() > 0) {
                                                                            Log.e("call", "DriverInfo 333");
                                                                            JSONObject dropObject = DriverInfo.getJSONObject(0);

                                                                            if (dropObject != null) {
                                                                                Log.e("call", "DriverInfo 444");
                                                                                double lat = 0, lng = 0;

                                                                                if (dropObject.has("Lat")) {
                                                                                    Log.e("call", "DriverInfo 555");
                                                                                    lat = Double.parseDouble(dropObject.getString("Lat"));
                                                                                    Log.e("call", "DriverInfo 666");
                                                                                }

                                                                                if (dropObject.has("Lng")) {
                                                                                    Log.e("call", "DriverInfo 777");
                                                                                    lng = Double.parseDouble(dropObject.getString("Lng"));
                                                                                    Log.e("call", "DriverInfo 888");
                                                                                }

                                                                                if (lat != 0.0 && lng != 0.0) {
                                                                                    driverLatLng = new LatLng(lat, lng);//23.0158596,
                                                                                } else {
                                                                                    driverLatLng = null;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);


                                                                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
                                                                tripFlag = 1;
                                                                acceptBooking = 1;

                                                                if (jsonObject != null) {
                                                                    Log.e("call", "BookingInfo 000");
                                                                    if (jsonObject.has("BookingInfo")) {
                                                                        Log.e("call", "BookingInfo 111");
                                                                        JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");
                                                                        Log.e("call", "BookingInfo 222");
                                                                        if (BookingInfo != null && BookingInfo.length() > 0) {
                                                                            Log.e("call", "BookingInfo 333");
                                                                            JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                                                                            if (BookingInfoObject != null) {
                                                                                Log.e("call", "BookingInfo 444");
                                                                                if (BookingInfoObject.has("RequestFor") &&
                                                                                        BookingInfoObject.getString("RequestFor").toLowerCase().equalsIgnoreCase("taxi")) {
                                                                                    callSelectService(2);
                                                                                } else {
                                                                                    callSelectService(2);
                                                                                }
                                                                            } else {
                                                                                Log.e("call", "BookingInfo 444 else");
                                                                            }

                                                                        } else {
                                                                            Log.e("call", "BookingInfo 333 else");
                                                                        }

                                                                    } else {
                                                                        Log.e("call", "BookingInfo 111 else");
                                                                    }

                                                                } else {
                                                                    Log.e("call", "BookingInfo 000 else");
                                                                }

                                                                Log.e("call", "requestConfirmJsonObject = " + requestConfirmJsonObject.toString());

                                                                requestPendingDialogClass.hideDialog();

                                                                if (requestConfirmJsonObject != null) {
                                                                    ll_BookingLayout.setVisibility(View.GONE);
                                                                    ll_AllCarLayout.setVisibility(View.GONE);
                                                                    ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                                                                    tv_CancelRequest.setVisibility(View.VISIBLE);
                                                                    tv_DriverInfo.setVisibility(View.VISIBLE);
                                                                    address_Layout.setVisibility(View.GONE);

                                                                    Log.e("call", "requestConfirmJsonObject 111");
                                                                    String message = getResources().getString(R.string.your_booking_request_has_been_confirmed);
                                                                    if (requestConfirmJsonObject.has("message")) {
                                                                        Log.e("call", "requestConfirmJsonObject 222");
                                                                        message = requestConfirmJsonObject.getString("message");
                                                                        Log.e("call", "requestConfirmJsonObject 333");
                                                                    }

                                                                    if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                        Log.e("call", "requestConfirmJsonObject 444");
                                                                        Log.e("call", "11111111111111");
                                                                        displayAcceptRequestPopup(message, TruckzShipperApplication.getCurrentActivity());
                                                                    } else {
                                                                        Log.e("call", "22222222222222222");
                                                                        displayAcceptRequestPopup(message, activity);
                                                                    }

                                                                    if (pickUp != null && driverLatLng != null) {
                                                                        showRoute(1);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (tripFlag == 0) {
                                                        advanceBooking = 1;

                                                        if (args != null && args.length > 0) {
                                                            Log.e("call", "onAcceptAdvancedBookingRequestNotification = " + args[0].toString());
                                                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                                commonInternetDialog.hideDialog();
                                                            }

                                                            if (commonDialog != null && commonDialog.isShowing()) {
                                                                commonDialog.dismiss();
                                                            }

                                                            zoomFlag = 0;

                                                            JSONObject jsonObject = new JSONObject(args[0].toString());
                                                            Log.e("call", "jsonObject = " + jsonObject.toString());
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, args[0].toString(), activity);

                                                            if (jsonObject != null) {
                                                                if (jsonObject.has("BookingInfo")) {
                                                                    Log.e("call", "BookingInfo 111");
                                                                    JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");
                                                                    Log.e("call", "BookingInfo 222");
                                                                    if (BookingInfo != null && BookingInfo.length() > 0) {
                                                                        Log.e("call", "BookingInfo 333");
                                                                        JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                                                                        if (BookingInfoObject != null) {
                                                                            Log.e("call", "BookingInfo 444");
                                                                            double lat = 0, lng = 0;

                                                                            if (BookingInfoObject.has("PickupLat")) {
                                                                                Log.e("call", "BookingInfo 555");
                                                                                lat = BookingInfoObject.getDouble("PickupLat");
                                                                                Log.e("call", "BookingInfo 666");
                                                                            }

                                                                            if (BookingInfoObject.has("PickupLng")) {
                                                                                Log.e("call", "BookingInfo 777");
                                                                                lng = BookingInfoObject.getDouble("PickupLng");
                                                                                Log.e("call", "BookingInfo 888");
                                                                            }

                                                                            if (BookingInfoObject.has("Id")) {
                                                                                Log.e("call", "BookingInfo 999");
                                                                                BookingId = BookingInfoObject.getString("Id");
                                                                                Log.e("call", "BookingInfo 10 10 10");
                                                                            }

                                                                            if (BookingInfoObject.has("ModelId")) {
                                                                                Log.e("call", "BookingInfo 999");
                                                                                String model = BookingInfoObject.getString("ModelId");

                                                                                selectedCar = -1;

                                                                                if (model.equalsIgnoreCase("1")) {
                                                                                    selectedCar = 0;
                                                                                } else if (model.equalsIgnoreCase("2")) {
                                                                                    selectedCar = 1;
                                                                                } else if (model.equalsIgnoreCase("3")) {
                                                                                    selectedCar = 2;
                                                                                } else if (model.equalsIgnoreCase("4")) {
                                                                                    selectedCar = 3;
                                                                                } else if (model.equalsIgnoreCase("5")) {
                                                                                    selectedCar = 4;
                                                                                } else if (model.equalsIgnoreCase("6")) {
                                                                                    selectedCar = 5;
                                                                                } else if (model.equalsIgnoreCase("7")) {
                                                                                    selectedCar = 6;
                                                                                }

                                                                                if (carType_beens != null && carType_beens.size() >= selectedCar)
                                                                                    ;
                                                                                {
                                                                                    for (int i = 0; i < carType_beens.size(); i++) {
                                                                                        if (i == selectedCar) {
                                                                                            carType_beens.get(i).setSelectedCar(selectedCar);
                                                                                        } else {
                                                                                            carType_beens.get(i).setSelectedCar(-1);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if (carAdapter != null) {
                                                                                    carAdapter.notifyDataSetChanged();
                                                                                    recyclerViewCar.scrollToPosition(0);
                                                                                }
                                                                            }

                                                                            pickUp = new LatLng(lat, lng);
                                                                        }
                                                                    }
                                                                }

                                                                if (jsonObject.has("DriverInfo")) {
                                                                    Log.e("call", "DriverInfo 111");
                                                                    JSONArray DriverInfo = jsonObject.getJSONArray("DriverInfo");
                                                                    Log.e("call", "DriverInfo 222");
                                                                    if (DriverInfo != null && DriverInfo.length() > 0) {
                                                                        Log.e("call", "DriverInfo 333");
                                                                        JSONObject dropObject = DriverInfo.getJSONObject(0);

                                                                        if (dropObject != null) {
                                                                            Log.e("call", "DriverInfo 444");
                                                                            double lat = 0, lng = 0;

                                                                            if (dropObject.has("Lat")) {
                                                                                Log.e("call", "DriverInfo 555");
                                                                                lat = Double.parseDouble(dropObject.getString("Lat"));
                                                                                Log.e("call", "DriverInfo 666");
                                                                            }

                                                                            if (dropObject.has("Lng")) {
                                                                                Log.e("call", "DriverInfo 777");
                                                                                lng = Double.parseDouble(dropObject.getString("Lng"));
                                                                                Log.e("call", "DriverInfo 888");
                                                                            }

                                                                            if (lat != 0.0 && lng != 0.0) {
                                                                                driverLatLng = new LatLng(lat, lng);//23.0158596,
                                                                            } else {
                                                                                driverLatLng = null;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "1", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);

                                                            if (jsonObject != null) {
                                                                Log.e("call", "BookingInfo 000");
                                                                if (jsonObject.has("BookingInfo")) {
                                                                    Log.e("call", "BookingInfo 111");
                                                                    JSONArray BookingInfo = jsonObject.getJSONArray("BookingInfo");
                                                                    Log.e("call", "BookingInfo 222");
                                                                    if (BookingInfo != null && BookingInfo.length() > 0) {
                                                                        Log.e("call", "BookingInfo 333");
                                                                        JSONObject BookingInfoObject = BookingInfo.getJSONObject(0);

                                                                        if (BookingInfoObject != null) {
                                                                            Log.e("call", "BookingInfo 444");
                                                                            if (BookingInfoObject.has("RequestFor") &&
                                                                                    BookingInfoObject.getString("RequestFor").toLowerCase().equalsIgnoreCase("taxi")) {
                                                                                callSelectService(2);
                                                                            } else {
                                                                                callSelectService(2);
                                                                            }
                                                                        } else {
                                                                            Log.e("call", "BookingInfo 444 else");
                                                                        }

                                                                    } else {
                                                                        Log.e("call", "BookingInfo 333 else");
                                                                    }

                                                                } else {
                                                                    Log.e("call", "BookingInfo 111 else");
                                                                }

                                                            } else {
                                                                Log.e("call", "BookingInfo 000 else");
                                                            }

                                                            requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
                                                            tripFlag = 1;
                                                            acceptBooking = 1;

                                                            Log.e("call", "requestConfirmJsonObject 222= " + requestConfirmJsonObject.toString());

                                                            requestPendingDialogClass.hideDialog();

                                                            if (requestConfirmJsonObject != null) {
                                                                ll_BookingLayout.setVisibility(View.GONE);
                                                                ll_AllCarLayout.setVisibility(View.GONE);
                                                                ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                                                                tv_CancelRequest.setVisibility(View.VISIBLE);
                                                                tv_DriverInfo.setVisibility(View.VISIBLE);
                                                                address_Layout.setVisibility(View.GONE);

                                                                Log.e("call", "requestConfirmJsonObject 111");
                                                                String message = getResources().getString(R.string.your_booking_request_has_been_confirmed);
                                                                if (requestConfirmJsonObject.has("message")) {
                                                                    Log.e("call", "requestConfirmJsonObject 222");
                                                                    message = requestConfirmJsonObject.getString("message");
                                                                    Log.e("call", "requestConfirmJsonObject 333");
                                                                }

                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    Log.e("call", "requestConfirmJsonObject 444");
                                                                    Log.e("call", "11111111111111");
                                                                    displayAcceptRequestPopup(message, TruckzShipperApplication.getCurrentActivity());
                                                                } else {
                                                                    Log.e("call", "22222222222222222");
                                                                    displayAcceptRequestPopup(message, activity);
                                                                }

                                                                if (pickUp != null && driverLatLng != null) {
                                                                    showRoute(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "onAcceptAdvancedBookingRequestNotification = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onRejectAdvancedBookingRequestNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "onRejectAdvancedBookingRequestNotification");
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag == 0) {
//                            try
//                            {
//                                ringTone = MediaPlayer.create(activity, R.raw.tone);
//                                ringTone.pick_start();
//                            }
//                            catch (Exception e)
//                            {
//                                e.printStackTrace();
//                            }
                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            advanceBooking = 0;
                            tripFlag = 0;
                            selectedCarPosition = -1;
                            requestConfirmJsonObject = null;
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);

                            isDone = 0;
                            callSelectService(2);

                            requestPendingDialogClass.hideDialog();

                            if (args != null && args.length > 0) {
                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, getResources().getString(R.string.success_message));
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, getResources().getString(R.string.error_message));
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }
    };

//    private Emitter.Listener InformPassengerForAdvancedTrip = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    try
//                    {
//                        if (args!=null && args.length>0)
//                        {
//                            if (commonInternetDialog!=null && commonInternetDialog.isShowing())
//                            {
//                                commonInternetDialog.hideDialog();
//                            }
//
//                            if (commonDialog!=null && commonDialog.isShowing())
//                            {
//                                commonDialog.dismiss();
//                            }
//
//                            JSONObject jsonObject = new JSONObject(args[0].toString());
//
//                            if (jsonObject!=null)
//                            {
//                                if (jsonObject.has("message"))
//                                {
//                                    String message = jsonObject.getString("message");
//                                    if (CabRideApplication.getCurrentActivity()!=null)
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(CabRideApplication.getCurrentActivity());
//                                        internetDialog.showDialog(message);
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                    else
//                                    {
//                                        InternetDialog internetDialog = new InternetDialog(activity);
//                                        internetDialog.showDialog(message);
//                                        commonInternetDialog = internetDialog;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        Log.e("call","Exception = "+e.getMessage());
//                    }
//                }
//            });
//        }
//    };


    private Emitter.Listener AcceptAdvancedBookingRequestNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.e("call", "AcceptAdvancedBookingRequestNotify = 111");

                        if (args != null && args.length > 0) {
                            callSelectService(2);

                            Log.e("call", "AcceptAdvancedBookingRequestNotify = " + args[0].toString());
                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                commonInternetDialog.hideDialog();
                            }

                            if (commonDialog != null && commonDialog.isShowing()) {
                                commonDialog.dismiss();
                            }

                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    String message = jsonObject.getString("message");
                                    if (TruckzShipperApplication.getCurrentActivity() != null) {
                                        InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                        internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                        commonInternetDialog = internetDialog;
                                    } else {
                                        InternetDialog internetDialog = new InternetDialog(activity);
                                        internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                        commonInternetDialog = internetDialog;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onAdvancedBookingPickupPassengerNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        Log.e("call", "onAdvancedBookingPickupPassengerNotification");
                        if (args != null && args.length > 0) {
                            Log.e("call", "onAdvancedBookingPickupPassengerNotification response = " + args[0].toString());
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingInfo") && jsonObject.getString("BookingInfo") != null && !jsonObject.getString("BookingInfo").equalsIgnoreCase("")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("BookingInfo");
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        JSONObject objectBookingInfo = jsonArray.getJSONObject(0);
                                        if (objectBookingInfo != null) {
                                            if (objectBookingInfo.has("Id") && objectBookingInfo.getString("Id") != null && !objectBookingInfo.getString("Id").equalsIgnoreCase("")) {
                                                if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                    if (BookingId.equalsIgnoreCase(objectBookingInfo.getString("Id"))) {
                                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                                                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                                        } else {
                                                            tripFlag = 0;
                                                        }

                                                        Log.e("call", "tripFlag tripFlag tripFlag = " + tripFlag);

                                                        if (tripFlag != 0) {
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                            if (objectBookingInfo.has("RequestFor") &&
                                                                    objectBookingInfo.getString("RequestFor").toLowerCase().equalsIgnoreCase("taxi")) {
                                                                callSelectService(2);
                                                            } else {
                                                                callSelectService(2);
                                                            }

                                                            if (originMarker != null) {
                                                                originMarker.remove();
                                                            }

                                                            if (destinationMarker != null) {
                                                                destinationMarker.remove();
                                                            }

                                                            if (googleMap != null) {
                                                                googleMap.clear();
                                                            }

                                                            tv_CancelRequest.setVisibility(View.GONE);
                                                            ll_AllCarLayout.setVisibility(View.GONE);
                                                            ll_BookingLayout.setVisibility(View.GONE);
                                                            //ll_Done.setVisibility(View.GONE);
                                                            tv_DriverInfo.setVisibility(View.VISIBLE);

                                                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                                commonInternetDialog.hideDialog();
                                                            }

                                                            if (commonDialog != null && commonDialog.isShowing()) {
                                                                commonDialog.dismiss();
                                                            }

                                                            if (requestConfirmJsonObject != null) {
                                                                showPathFromPickupToDropoff(requestConfirmJsonObject);
                                                                String message = getResources().getString(R.string.your_trip_is_start_now);
                                                                if (jsonObject.has("message")) {
                                                                    message = jsonObject.getString("message");
                                                                }

                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                    commonInternetDialog = internetDialog;
                                                                } else {
                                                                    InternetDialog internetDialog = new InternetDialog(activity);
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                    commonInternetDialog = internetDialog;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (BookingId.equalsIgnoreCase(objectBookingInfo.getString("Id"))) {
                                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                                                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                                        } else {
                                                            tripFlag = 0;
                                                        }

                                                        Log.e("call", "tripFlag tripFlag tripFlag = " + tripFlag);

                                                        if (tripFlag != 0) {
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                            if (objectBookingInfo.has("RequestFor") &&
                                                                    objectBookingInfo.getString("RequestFor").toLowerCase().equalsIgnoreCase("taxi")) {
                                                                callSelectService(2);
                                                            } else {
                                                                callSelectService(2);
                                                            }

                                                            if (originMarker != null) {
                                                                originMarker.remove();
                                                            }

                                                            if (destinationMarker != null) {
                                                                destinationMarker.remove();
                                                            }

                                                            if (googleMap != null) {
                                                                googleMap.clear();
                                                            }

                                                            tv_CancelRequest.setVisibility(View.GONE);
                                                            ll_AllCarLayout.setVisibility(View.GONE);
                                                            ll_BookingLayout.setVisibility(View.GONE);
                                                            //ll_Done.setVisibility(View.GONE);
                                                            tv_DriverInfo.setVisibility(View.VISIBLE);

                                                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                                commonInternetDialog.hideDialog();
                                                            }

                                                            if (commonDialog != null && commonDialog.isShowing()) {
                                                                commonDialog.dismiss();
                                                            }

                                                            if (requestConfirmJsonObject != null) {
                                                                showPathFromPickupToDropoff(requestConfirmJsonObject);
                                                                String message = getResources().getString(R.string.your_trip_is_start_now);
                                                                if (jsonObject.has("message")) {
                                                                    message = jsonObject.getString("message");
                                                                }

                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                    commonInternetDialog = internetDialog;
                                                                } else {
                                                                    InternetDialog internetDialog = new InternetDialog(activity);
                                                                    internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                                                    commonInternetDialog = internetDialog;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onGetDriverLocation = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onAdvancedBookingTripHoldNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag != 0) {

                            if (args != null && args.length > 0) {
                                if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                    commonInternetDialog.hideDialog();
                                }

                                if (commonDialog != null && commonDialog.isShowing()) {
                                    commonDialog.dismiss();
                                }

                                JSONObject jsonObject = new JSONObject(args[0].toString());

                                if (jsonObject != null) {
                                    if (jsonObject.has("message")) {
                                        String message = jsonObject.getString("message");
                                        if (TruckzShipperApplication.getCurrentActivity() != null) {
                                            InternetDialog internetDialog = new InternetDialog(TruckzShipperApplication.getCurrentActivity());
                                            internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                            commonInternetDialog = internetDialog;
                                        } else {
                                            InternetDialog internetDialog = new InternetDialog(activity);
                                            internetDialog.showDialog(message, getResources().getString(R.string.info_message));
                                            commonInternetDialog = internetDialog;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onAdvancedBookingTripHoldNotification = " + e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onAdvancedBookingDetails = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("call", "onAdvancedBookingDetails()");
                        int tripFlag = 0;

                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equalsIgnoreCase("")) {
                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                        }

                        if (tripFlag != 0) {
                            if (args != null && args.length > 0) {
                                Log.e("call", "onAdvancedBookingDetails = " + args[0].toString());

                                JSONObject tripCompleteObject = new JSONObject(args[0].toString());
                                if (tripCompleteObject != null) {
                                    if (tripCompleteObject.has("Info")) {
                                        JSONArray InfoArray = tripCompleteObject.getJSONArray("Info");

                                        if (InfoArray != null && InfoArray.length() > 0) {
                                            JSONObject InfoData = InfoArray.getJSONObject(0);

                                            if (InfoData != null) {
                                                if (InfoData.has("Id") && InfoData.getString("Id") != null && !InfoData.getString("Id").equalsIgnoreCase("")) {
                                                    if (BookingId != null && !BookingId.equalsIgnoreCase("")) {
                                                        if (BookingId.equalsIgnoreCase(InfoData.getString("Id"))) {
                                                            if (commonInternetDialog != null && commonInternetDialog.isShowing()) {
                                                                commonInternetDialog.hideDialog();
                                                            }

                                                            if (commonDialog != null && commonDialog.isShowing()) {
                                                                commonDialog.dismiss();
                                                            }
                                                            String walletBalance = "0";

                                                            String passengerType = "";

                                                            if (InfoData != null) {
                                                                if (InfoData.has("PassengerType")) {
                                                                    passengerType = InfoData.getString("PassengerType");
                                                                } else {
                                                                    passengerType = "";
                                                                }
                                                            } else {
                                                                passengerType = "";
                                                            }

                                                            if (tripCompleteObject.has("UpdatedBal")) {
                                                                walletBalance = tripCompleteObject.getString("UpdatedBal");
                                                            }

                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_WALLET_BALLENCE, walletBalance, activity);

                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);

                                                            ll_BookingLayout.setVisibility(View.GONE);
                                                            ll_AllCarLayout.setVisibility(View.GONE);
                                                            //ll_Done.setVisibility(View.VISIBLE);
                                                            isDone = 0;
                                                            callSelectService(2);
                                                            ll_AfterRequestAccept.setVisibility(View.GONE);
                                                            tv_CancelRequest.setVisibility(View.GONE);
                                                            address_Layout.setVisibility(View.VISIBLE);

                                                            removeCarMarker();

                                                            if (originMarker != null) {
                                                                originMarker.remove();
                                                            }

                                                            if (destinationMarker != null) {
                                                                destinationMarker.remove();
                                                            }

                                                            if (googleMap != null) {
                                                                googleMap.clear();
                                                            }

                                                            if (handlerDriver != null && runnableDriver != null) {
                                                                handlerDriver.removeCallbacks(runnableDriver, null);
                                                            }

                                                            String message = getResources().getString(R.string.your_trip_has_been_completed_successfully);
                                                            if (tripCompleteObject.has("message")) {
                                                                message = tripCompleteObject.getString("message");
                                                            }
                                                            SessionSave.saveUserSession("TripCompleted", tripCompleteObject.toString(), activity);

                                                            bookingType = WebServiceAPI.BOOK_LATER;


                                                            if(InfoData.has("PaymentType"))
                                                            {
                                                                paymentType = InfoData.getString("PaymentType");
                                                            }
                                                            final String amount = InfoData.getString("GrandTotal");

                                                            if(paymentType.equalsIgnoreCase("paytm"))
                                                            {
                                                                PaytmSingleton.setCallBack(activity, new CallbackPaytm() {
                                                                    @Override
                                                                    public void onSuccess(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }

                                                                    @Override
                                                                    public void onFailed(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }

                                                                    @Override
                                                                    public void onNetworkError() {
                                                                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                        errorDialogClass.showDialog("Network Problem","Error");
                                                                    }

                                                                    @Override
                                                                    public void onClientAuthenticationFailed(String error,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                        errorDialogClass.showDialog(error,"Error");*/
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }

                                                                    @Override
                                                                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                        errorDialogClass.showDialog(inErrorMessage,"Error");*/
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }

                                                                    @Override
                                                                    public void onBackPressedCancelTransaction(String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                        errorDialogClass.showDialog("Cancel Transaction","Error");*/
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }

                                                                    @Override
                                                                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse,String status,String BookingType,String bookingId,String txId,String refId) {
                                                                        /*ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                                        errorDialogClass.showDialog("Cancel Transaction","Error");*/
                                                                        callPaytmPaymentApi(status,BookingType,bookingId,txId,refId,amount);
                                                                    }
                                                                });

                                                                playNotificationSound1();

                                                                Intent intent = new Intent(activity,PaymentPaytmActivity.class);
                                                                intent.putExtra("BookingId",InfoData.getString("Id"));
                                                                intent.putExtra("cusId",InfoData.getString("PassengerId"));
                                                                intent.putExtra("GrandTotal",InfoData.getString("GrandTotal"));
                                                                intent.putExtra("BookingType",InfoData.getString("BookingType"));
                                                                intent.putExtra("refId",InfoData.getString("ReferenceId"));
                                                                startActivity(intent);
                                                                return;
                                                            }


                                                            if (passengerType != null && (passengerType.equalsIgnoreCase("other") || passengerType.equalsIgnoreCase("others"))) {
                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    showTripCompletePopupOk(TruckzShipperApplication.getCurrentActivity(), message);
                                                                } else {
                                                                    showTripCompletePopupOk(activity, message);
                                                                }
                                                            } else {
                                                                if (TruckzShipperApplication.getCurrentActivity() != null) {
                                                                    openTripCompleteScreen();///30-7-19
                                                                    //showTripCompletePopup(message, TruckzShipperApplication.getCurrentActivity(), BookingId, WebServiceAPI.BOOK_LATER);
                                                                } else {
                                                                    openTripCompleteScreen();///30-7-19
                                                                    //showTripCompletePopup(message, activity, BookingId, WebServiceAPI.BOOK_LATER);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("call", "Exception onAdvancedBookingDetails = " + e.getMessage());
                    }
                }
            });
        }
    };

    public void showTripCompletePopupOk(Context context, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        TextView tv_Message = dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openTripCompleteScreen();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openTripCompleteScreen();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

//                if (MainActivity.ringTone!=null)
//                {
//                    MainActivity.ringTone.stop();
//                }
            }
        });

        dialog.show();

        commonDialog = dialog;
    }

    @Override
    protected void onPause() {
        if (mKalmanLocationManager != null && mLocationListener != null) {
            mKalmanLocationManager.removeUpdates(mLocationListener);
        }
        if (Spinner_paymentMethod != null) {
            Spinner_paymentMethod.onDetachedFromWindow();
        }

        if (Spinner_transportService != null) {
            Spinner_transportService.onDetachedFromWindow();
        }

        super.onPause();
    }

    private CustomerTransportServiceAdapter customerTransportServiceAdapter;
    public static String selectedTransportService = "";

    public class CustomerTransportServiceAdapter extends ArrayAdapter<TransportService_Been> {
        ArrayList<TransportService_Been> customers, tempCustomer, suggestions;

        public CustomerTransportServiceAdapter(Context context, ArrayList<TransportService_Been> objects) {
            super(context, R.layout.row_spinner_item_transport_service, R.id.tv_spinner_transport_service, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<TransportService_Been>(objects);
            this.suggestions = new ArrayList<TransportService_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            TransportService_Been customer = getItem(position);

            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item_transport_service, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item_transport_service, parent, false);
            }
            TextView tv_spinner_transport_service = convertView.findViewById(R.id.tv_spinner_transport_service);

            if (tv_spinner_transport_service != null)
                tv_spinner_transport_service.setText(customer.getName());

            return convertView;
        }
    }

    private CustomSpinner Spinner_transportService;

    public void openPromocodePopup()//0 for normal request and 1 for special request.
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_promocode_booknow_layout);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        ImageView dialog_close;
        final LinearLayout ll_promocode, dialog_ok_layout;
        final EditText et_Promocode, et_Notes;
        final CTextViewMedium tv_RequestNow;
        final TextView apply_textview;
        CheckBox checkBox;

        tv_RequestNow = dialog.findViewById(R.id.request_now_textview);
        apply_textview = dialog.findViewById(R.id.apply_textview);
        dialog_ok_layout = dialog.findViewById(R.id.dialog_ok_layout);
        dialog_close = dialog.findViewById(R.id.dialog_close);

        Spinner_transportService = dialog.findViewById(R.id.Spinner_transportService);
        etFullName = dialog.findViewById(R.id.etFullName);
        etSenderAddress = dialog.findViewById(R.id.etSenderAddress);
        etPhoneNumber = dialog.findViewById(R.id.etPhoneNumber);
        etParcelWeight = dialog.findViewById(R.id.etParcelWeight);
        Spinner_paymentMethod = dialog.findViewById(R.id.Spinner_paymentMethod);
        et_Promocode = dialog.findViewById(R.id.promocode_edittext);
        checkBox = dialog.findViewById(R.id.checkBox);
        ll_promocode = dialog.findViewById(R.id.ll_promocode);
        ll_EstimateFare = dialog.findViewById(R.id.ll_EstimateFare);
        ll_Discount = dialog.findViewById(R.id.ll_Discount);
        ll_EstimateFare_pay = dialog.findViewById(R.id.ll_EstimateFare_pay);
        tv_EstimateFare = dialog.findViewById(R.id.tv_EstimateFare);
        tv_Discount = dialog.findViewById(R.id.tv_Discount);
        tv_EstimateFare_pay = dialog.findViewById(R.id.tv_EstimateFare_pay);
        et_Notes = dialog.findViewById(R.id.input_note);
        etEmail = dialog.findViewById(R.id.etEmail);

        cbReceiverInfo = dialog.findViewById(R.id.cbReceiverInfo);

        etFullName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, activity));
        etPhoneNumber.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, activity));
        //etEmail.setText("");

        cbReceiverInfo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etFullName.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, activity));
                    etPhoneNumber.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_PHONE_NUMBER, activity));
                } else {
                    etFullName.setText("");
                    etPhoneNumber.setText("");
                    //etEmail.setText("");
                }
            }
        });

        cardId = "";
        cardNumber = "";
        cardType = "";
        isCardSelected = false;
        isPromocodeCheck = false;
        isValidPromoCode = false;
        selectedPaymentType = "";
        spinnerSelectedPosition = 0;
        ll_promocode.setVisibility(View.GONE);
        apply_textview.setVisibility(View.GONE);
        ll_Discount.setVisibility(View.GONE);
        ll_EstimateFare_pay.setVisibility(View.GONE);
        tv_EstimateFare.setText(getString(R.string.currency) + commaFormat(Double.parseDouble(getEstimateFare_beens.get(selectedCarPosition).getTotal())));

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                etFullName.setText("");
                etSenderAddress.setText("");
                etPhoneNumber.setText("");
                etParcelWeight.setText("");
                et_Promocode.setText("");
                promocodeStr = "";
                et_Notes.setText("");
                notes = "";
                tv_Discount.setText(getString(R.string.currency) + "0");
                tv_EstimateFare_pay.setText(getString(R.string.currency) + "0");
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked == true) {
                    isPromocodeCheck = true;
                    ll_promocode.setVisibility(View.VISIBLE);
                } else {
                    isPromocodeCheck = false;
                    ll_promocode.setVisibility(View.GONE);
                    et_Promocode.setText("");
                    promocodeStr = "";
                    ll_EstimateFare_pay.setVisibility(View.GONE);
                }
            }
        });

        customerAdapter = new CustomerAdapter(activity, cardListBeens);
        Spinner_paymentMethod.setAdapter(customerAdapter);

        customerTransportServiceAdapter = new CustomerTransportServiceAdapter(activity, transportService_beens);
        Spinner_transportService.setAdapter(customerTransportServiceAdapter);
        Spinner_transportService.setSelection(0);
        selectedTransportService = transportService_beens.get(0).getId();

        Spinner_transportService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedTransportService = transportService_beens.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

                Log.e(TAG, "setOnItemSelectedListener() onNothing selected");
            }
        });

        Spinner_paymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cardId = cardListBeens.get(position).getId();
                cardNumber = cardListBeens.get(position).getCardNum_();

//                if (position == (cardListBeens.size() -1))
//                {
//                    selectedPaymentType = "wallet";//PaymentType : cash,wallet,card
//
//                }
//                else
                if (position == (cardListBeens.size() - 1)) {
                    selectedPaymentType = "paytm";//PaymentType : cash,wallet,card
                }
                else if(position == (cardListBeens.size() - 2))
                {
                    selectedPaymentType ="cash";
                }
                else {
                    selectedPaymentType = "card";//PaymentType : cash,wallet,card
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        promocodeStr = "";
        notes = "";
        et_Promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isValidPromoCode = false;
                tv_Discount.setText(getString(R.string.currency) + "0");
                tv_EstimateFare_pay.setText(getString(R.string.currency) + commaFormat(Double.parseDouble(getEstimateFare_beens.get(selectedCarPosition).getTotal())));

                if (charSequence.toString().length() > 0) {
                    et_Promocode.setError(null);
                    apply_textview.setVisibility(View.VISIBLE);
                } else {
                    ll_Discount.setVisibility(View.GONE);
                    apply_textview.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        apply_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promocodeStr = et_Promocode.getText().toString();

                if (TextUtils.isEmpty(et_Promocode.getText().toString().trim())) {
                    et_Promocode.setError("Please enter promo code");
                    et_Promocode.setFocusableInTouchMode(true);
                    et_Promocode.requestFocus();
                } else {
                    callCheckPromoCode(promocodeStr, dialog, et_Promocode);
                }
            }
        });

//        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                promocodeStr = et_Promocode.getText().toString();
//                notes = et_Notes.getText().toString().trim();
//                if (Global.isNetworkconn(activity))
//                {
//                    if (TaxiUtil.picup_Lat == 0 || TaxiUtil.picup_Long == 0)
//                    {
//                        InternetDialog internetDialog = new InternetDialog(activity);
//                        internetDialog.showDialog(getResources().getString(R.string.please_enter_your_pickup_location_again),getResources().getString(R.string.error_message));
//                    }
//                    else
//                    {
//                        if ( TaxiUtil.dropoff_Lat == 0 || TaxiUtil.dropoff_Long ==0)
//                        {
//                            InternetDialog internetDialog = new InternetDialog(activity);
//                            internetDialog.showDialog(getResources().getString(R.string.please_enter_your_destination_again),getResources().getString(R.string.error_message));
//                        }
//                        else
//                        {
//                            if (TextUtils.isEmpty(etParcelWeight.getText().toString().trim()))
//                            {
//                                etParcelWeight.setError("Please enter parcel weight");
//                                etParcelWeight.setFocusableInTouchMode(true);
//                                etParcelWeight.requestFocus();
//                            }
//                            else
//                            {
//                                if (TextUtils.isEmpty(etFullName.getText().toString().trim()))
//                                {
//                                    etFullName.setError("Please enter full name");
//                                    etFullName.setFocusableInTouchMode(true);
//                                    etFullName.requestFocus();
//                                }
//                                else
//                                {
//                                    if (TextUtils.isEmpty(etPhoneNumber.getText().toString().trim()) ||
//                                            etPhoneNumber.getText().length() < 10)
//                                    {
//                                        etPhoneNumber.setError("Please enter valid phone number");
//                                        etPhoneNumber.setFocusableInTouchMode(true);
//                                        etPhoneNumber.requestFocus();
//                                    }
//                                    else
//                                    {
//                                        receverFullName = etFullName.getText().toString().trim();
//                                        receverPhoneNumber = etPhoneNumber.getText().toString().trim();
//
//                                        if (isPromocodeCheck)
//                                        {
//                                            if (TextUtils.isEmpty(et_Promocode.getText().toString().trim()))
//                                            {
//                                                et_Promocode.setError("Please enter promo code");
//                                                et_Promocode.setFocusableInTouchMode(true);
//                                                et_Promocode.requestFocus();
//                                            }
//                                            else
//                                            {
////                                                    dialog.dismiss();
////                                                    call_BookNow();
//                                                callCheckPromoCode(promocodeStr,dialog,et_Promocode);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            dialog.dismiss();
//                                            call_BookNow();
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    InternetDialog internetDialog = new InternetDialog(activity);
//                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection), getResources().getString(R.string.no_internet_connection));
//                }
//            }
//        });

        tv_RequestNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                promocodeStr = et_Promocode.getText().toString();
                notes = et_Notes.getText().toString().trim();
                if (Global.isNetworkconn(activity)) {
                    if (TaxiUtil.picup_Lat == 0 || TaxiUtil.picup_Long == 0) {
                        InternetDialog internetDialog = new InternetDialog(activity);
                        internetDialog.showDialog(getResources().getString(R.string.please_enter_your_pickup_location_again), getResources().getString(R.string.error_message));
                    } else {
                        if (TaxiUtil.dropoff_Lat == 0 || TaxiUtil.dropoff_Long == 0) {
                            InternetDialog internetDialog = new InternetDialog(activity);
                            internetDialog.showDialog(getResources().getString(R.string.please_enter_your_destination_again), getResources().getString(R.string.error_message));
                        } else {
                            if (TextUtils.isEmpty(etParcelWeight.getText().toString().trim())) {
                                etParcelWeight.setError(getString(R.string.please_enter_goods_weight));
                                etParcelWeight.setFocusableInTouchMode(true);
                                etParcelWeight.requestFocus();
                            } else if (!getKgValidOrNot(Double.parseDouble(etParcelWeight.getText().toString()))) {
                                // etParcelWeight.setError("Please enter weight Should be less than "+ String.format("%.2f", (Double.parseDouble(carType_beens.get(selectedCarPosition).getCapacity()) * 1000.00))+" Kg");
                                etParcelWeight.setError("Goods weight should be less than " + String.format("%.2f", (Double.parseDouble(carType_beens.get(selectedCarPosition).getCapacity()) * 1000.00)) + " Kg");
                                etParcelWeight.setFocusableInTouchMode(true);
                                etParcelWeight.requestFocus();
                            } else {
                                if (TextUtils.isEmpty(etFullName.getText().toString().trim())) {
                                    etFullName.setError("Please enter full name");
                                    etFullName.setFocusableInTouchMode(true);
                                    etFullName.requestFocus();
                                } else {
                                    if (TextUtils.isEmpty(etPhoneNumber.getText().toString().trim()) ||
                                            etPhoneNumber.getText().length() < 10) {
                                        etPhoneNumber.setError("Please enter valid phone number");
                                        etPhoneNumber.setFocusableInTouchMode(true);
                                        etPhoneNumber.requestFocus();
                                    } else {
                                        if (!TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                                            if (isValidEmail(etEmail.getText().toString().trim())) {
                                                receverFullName = etFullName.getText().toString().trim();
                                                receverPhoneNumber = etPhoneNumber.getText().toString().trim();

                                                if (isPromocodeCheck) {
                                                    if (TextUtils.isEmpty(et_Promocode.getText().toString().trim())) {
                                                        et_Promocode.setError("Please enter promo code");
                                                        et_Promocode.setFocusableInTouchMode(true);
                                                        et_Promocode.requestFocus();
                                                    } else {
                                                        if (isValidPromoCode) {
                                                            dialog.dismiss();
                                                            call_BookNow();
                                                        } else {
                                                            et_Promocode.setError("Please enter valid promo code");
                                                            et_Promocode.setFocusableInTouchMode(true);
                                                            et_Promocode.requestFocus();
                                                        }
//                                                        callCheckPromoCode(promocodeStr,dialog,et_Promocode);
                                                    }
                                                } else {
                                                    dialog.dismiss();
                                                    call_BookNow();
                                                }
                                            } else {
                                                etEmail.setError("Please enter valid email");
                                                etEmail.setFocusableInTouchMode(true);
                                                etEmail.requestFocus();
                                            }
                                        } else {
                                            receverFullName = etFullName.getText().toString().trim();
                                            receverPhoneNumber = etPhoneNumber.getText().toString().trim();

                                            if (isPromocodeCheck) {
                                                if (TextUtils.isEmpty(et_Promocode.getText().toString().trim())) {
                                                    et_Promocode.setError("Please enter promo code");
                                                    et_Promocode.setFocusableInTouchMode(true);
                                                    et_Promocode.requestFocus();
                                                } else {
                                                    if (isValidPromoCode) {
                                                        dialog.dismiss();
                                                        call_BookNow();
                                                    } else {
                                                        et_Promocode.setError("Please enter valid promo code");
                                                        et_Promocode.setFocusableInTouchMode(true);
                                                        et_Promocode.requestFocus();
                                                    }
//                                                    callCheckPromoCode(promocodeStr,dialog,et_Promocode);
                                                }
                                            } else {
                                                dialog.dismiss();
                                                call_BookNow();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(getResources().getString(R.string.please_check_internet_connection), getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog.show();
    }

    private boolean getKgValidOrNot(Double kg) {
        boolean flag = false;

        if ((Double.parseDouble(carType_beens.get(selectedCarPosition).getCapacity()) * 1000.00) >= kg) {
            flag = true;
        }

        return flag;
    }

    //email validation
    private boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callCheckPromoCode(final String promocode, final Dialog dialog, final EditText et_Promocode) {
        Log.e(TAG, "callCheckPromoCode() promocodeStr:- " + promocode);

        String url = WebServiceAPI.API_CHECK_PROMOCODE + "/" + promocode + "/" + getEstimateFare_beens.get(selectedCarPosition).getTotal();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_PROMO_CODE, promocode);

        Log.e(TAG, "callCheckPromoCode() url = " + url);
        Log.e(TAG, "callCheckPromoCode() param = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "callCheckPromoCode() responseCode = " + responseCode);
                    Log.e(TAG, "callCheckPromoCode() Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            String Message = "";

                            if (json.getBoolean("status")) {
                                Log.e(TAG, "callCheckPromoCode() book later send request successfully");
                                dialogClass.hideDialog();
                                isValidPromoCode = true;
                                ll_Discount.setVisibility(View.VISIBLE);
                                ll_EstimateFare_pay.setVisibility(View.VISIBLE);

                                String new_estimation_fare = "", discount = "";

                                if (json.has("new_estimate_fare")) {
                                    new_estimation_fare = json.getString("new_estimate_fare");
                                }

                                if (json.has("promocode")) {
                                    JSONObject jsonObjectPromo = json.getJSONObject("promocode");

                                    if (jsonObjectPromo.has("DiscountValue")) {
                                        discount = jsonObjectPromo.getString("DiscountValue");
                                    }
                                }

                                tv_EstimateFare_pay.setText(getString(R.string.currency) + commaFormat(Double.parseDouble(new_estimation_fare)));
                                tv_Discount.setText(getResources().getString(R.string.currency) + commaFormat(Double.parseDouble(discount)));
//                                dialog.dismiss();
//                                call_BookNow();
                            } else {
                                Log.e(TAG, "callCheckPromoCode() book later status false");
                                dialogClass.hideDialog();

                                isValidPromoCode = false;
                                if (json.has("message")) {
                                    Message = json.getString("message");
                                    et_Promocode.setError(Message);
                                    et_Promocode.setText("");
                                    promocodeStr = "";
                                }
                            }
                        } else {
                            Log.e(TAG, "callCheckPromoCode() book later status not found");
                            dialogClass.hideDialog();
                        }
                    } else {
                        Log.e(TAG, "callCheckPromoCode() book later json null");
                        dialogClass.hideDialog();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "callCheckPromoCode() Exception " + e.toString());
                    dialogClass.hideDialog();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public class CustomerAdapter extends ArrayAdapter<CreditCard_List_Been> {
        ArrayList<CreditCard_List_Been> customers, tempCustomer, suggestions;

        public CustomerAdapter(Context context, ArrayList<CreditCard_List_Been> objects) {
            super(context, R.layout.row_spinner_item, R.id.spinner_cardnumber, objects);
            this.customers = objects;
            this.tempCustomer = new ArrayList<CreditCard_List_Been>(objects);
            this.suggestions = new ArrayList<CreditCard_List_Been>(objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, null);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            CreditCard_List_Been customer = getItem(position);
            if (convertView == null) {
                if (parent == null)
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, null);
                else
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_item, parent, false);
            }
            TextView txtCustomer = convertView.findViewById(R.id.spinner_cardnumber);
            LinearLayout ll_Image = (LinearLayout) convertView.findViewById(R.id.spinner_image_layout);
            ImageView iv_Image = (ImageView) convertView.findViewById(R.id.image);
            LinearLayout ll_AddPaymentMethod = (LinearLayout) convertView.findViewById(R.id.ll_add_payment_method);
            ImageView iv_logo = convertView.findViewById(R.id.iv_logo);
            if (txtCustomer != null)
                txtCustomer.setText(customer.getCardNum_());

            if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("visa")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("mastercard")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_master);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("amex")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_american);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("diners")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_dinner);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("discover")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_discover);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("jcb")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_jcb);
            } else if (customer.getCardType() != null && customer.getCardType().equalsIgnoreCase("other")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_visa);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("cash")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_cash);
                ll_Image.setVisibility(View.VISIBLE);
                iv_logo.setVisibility(View.GONE);
                txtCustomer.setVisibility(View.VISIBLE);
            } else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("wallet")) {
                iv_Image.setImageResource(R.drawable.ic_card_icon_wallet);
            }else if (customer.getCardNum_() != null && customer.getCardNum_().equalsIgnoreCase("Paytm")) {
                iv_logo.setImageResource(R.drawable.paytm_logo_);
                ll_Image.setVisibility(View.GONE);
                txtCustomer.setVisibility(View.GONE);
                iv_logo.setVisibility(View.VISIBLE);
            }

            ll_AddPaymentMethod.setVisibility(View.GONE);

            ll_AddPaymentMethod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            return convertView;
        }
    }

    public void getAddressFromDropoffMarkerLatLong(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            Log.e("call", "getAddressFromDropoffMarkerLatLong()");
            if (latitude != 0 && longitude != 0) {
//                getEstimated_lat = latitude;
//                getEstimated_long = longitude;

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("111111111111", "address = " + address);
                Log.e("111111111111", "city = " + city);
                Log.e("111111111111", "state = " + state);
                Log.e("111111111111", "country = " + country);
                Log.e("111111111111", "postalCode = " + postalCode);
                Log.e("111111111111", "knownName = " + knownName);
//                getEstimated_Pickup= address;
                auto_Dropoff.setText(address);
                TaxiUtil.dropoff_Address = address;
                TaxiUtil.dropoff_Lat = latitude;
                TaxiUtil.dropoff_Long = longitude;
                dropoff_Touch = true;
                auto_Dropoff.dismissDropDown();

                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();

                if (dropoff_Touch == true && pickup_Touch == true) {
                    showRoute(2);
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }


    @Override
    public void onCameraMoveStarted(int i) {
        Log.e("call", "onCamer aMoveStarted()");
        //ll_Done.setVisibility(View.GONE);
    }

    @Override
    public void onCameraIdle() {
        Log.e(TAG, "onCameraIdle()");

        if (isAddressCahngable) {
            if (isAddressSet == 0) {
                if (googleMap != null) {
//            Log.e("call","onCameraIdle() 1111111");
//            Log.e("call","onCameraIdle() selectedCar = "+selectedCar);
//            Log.e("call","onCameraIdle() myLocation = "+myLocation);
//            Log.e("call","onCameraIdle() isDone = "+isDone);

                    int selectedCar = -1;

                    if (carType_beens != null && carType_beens.size() > 0) {
                        for (int i = 0; i < carType_beens.size(); i++) {
                            if (carType_beens.get(i).getSelectedCar() != -1) {
                                selectedCar = carType_beens.get(i).getSelectedCar();
                            }
                        }
                    }

                    if (selectedCar == -1 && myLocation == true) {
                        if (tripFlag == 0) {
                            if (isDone == 0) {
//                        Log.e("call","onCameraIdle() 222222222");
                                //ll_Done.setVisibility(View.VISIBLE);
                                LatLng centerOfMap = googleMap.getCameraPosition().target;
//                        Log.e("call","centerOfMap long = "+centerOfMap.longitude);
//                        Log.e("call","centerOfMap lat = "+centerOfMap.latitude);
                                if (isPickupDropoff == 1) {
                                    if (isAutocomplete == false) {
                                        getPickupAddressFromMarkerLatLong(centerOfMap.latitude, centerOfMap.longitude);
                                    } else {
                                        isAutocomplete = false;
                                    }
                                } else {
                                    if (isAutocomplete == false) {
                                        getDropoffAddressFromMarkerLatLong(centerOfMap.latitude, centerOfMap.longitude);
                                    } else {
                                        isAutocomplete = false;
                                    }
                                }
                            } else {
                                //ll_Done.setVisibility(View.GONE);
                                current_marker_layout.setVisibility(View.GONE);
                            }
                        } else {
                            //ll_Done.setVisibility(View.GONE);
                            current_marker_layout.setVisibility(View.GONE);
                        }
                    } else {
//                Log.e("call","onCameraIdle() 333333");
                        current_marker_layout.setVisibility(View.GONE);
                    }
                } else {
//            Log.e("call","onCameraIdle() 4444444444");
                    current_marker_layout.setGravity(View.GONE);
                }
            } else {
                isAddressSet = 0;
            }
        }
    }

    /*@Override
    public void onCameraMove() {
        //ll_Done.setVisibility(View.GONE);
    }*/


    public void getPickupAddressFromMarkerLatLong(double latitude, double longitude) {
        try {
            Log.e("call", "call getPickupAddressFromMarkerLatLong()");
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            if (latitude != 0 && longitude != 0) {
                getEstimated_lat = latitude;
                getEstimated_long = longitude;

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("111111111111", "address = " + address);
                Log.e("111111111111", "city = " + city);
                Log.e("111111111111", "state = " + state);
                Log.e("111111111111", "country = " + country);
                Log.e("111111111111", "postalCode = " + postalCode);
                Log.e("111111111111", "knownName = " + knownName);
                getEstimated_Pickup = address;
                auto_Pickup.setText(address);
                tvPickup.setText(address);
                TaxiUtil.picup_Address = address;
                TaxiUtil.picup_Lat = latitude;
                TaxiUtil.picup_Long = longitude;
                pickup_Touch = true;
                auto_Pickup.dismissDropDown();

                auto_Pickup.setFocusableInTouchMode(true);
                auto_Pickup.requestFocus();
                current_marker_layout.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(tvPickup.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tvDropoff.getText().toString().trim())) {
                    //callAddressDone();
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void getDropoffAddressFromMarkerLatLong(double latitude, double longitude) {
        try {
            Log.e("call", "call getDropoffAddressFromMarkerLatLong()");
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            if (latitude != 0 && longitude != 0) {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("111111111111", "address = " + address);
                Log.e("111111111111", "city = " + city);
                Log.e("111111111111", "state = " + state);
                Log.e("111111111111", "country = " + country);
                Log.e("111111111111", "postalCode = " + postalCode);
                Log.e("111111111111", "knownName = " + knownName);
                auto_Dropoff.setText(address);
                tvDropoff.setText(address);
                TaxiUtil.dropoff_Address = address;
                TaxiUtil.dropoff_Lat = latitude;
                TaxiUtil.dropoff_Long = longitude;
                dropoff_Touch = true;
                auto_Dropoff.dismissDropDown();

                auto_Dropoff.setFocusableInTouchMode(true);
                auto_Dropoff.requestFocus();
                current_marker_layout.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(tvPickup.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tvDropoff.getText().toString().trim())) {
                    //callAddressDone();
                }
            }
        } catch (Exception e) {
            Log.e("call", "Exception = " + e.getMessage());
        }
    }

    public void ClearWholeData(String TripId) {
//        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,"0",activity);
//        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR,"-1",activity);
//        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL,"",activity);
//
//        ll_BookingLayout.setVisibility(View.GONE);
//        ll_AllCarLayout.setVisibility(View.GONE);
//        //ll_Done.setVisibility(View.VISIBLE);
//        ll_AfterRequestAccept.setVisibility(View.GONE);
//        tv_CancelRequest.setVisibility(View.GONE);
//        address_Layout.setVisibility(View.VISIBLE);
//
//        removeCarMarker();
//
//        if (originMarker!=null)
//        {
//            originMarker.remove();
//        }
//
//        if (destinationMarker!=null)
//        {
//            destinationMarker.remove();
//        }
//
//        if (googleMap!=null)
//        {
//            googleMap.clear();
//        }
//
//        if (handler != null && runnable != null)
//        {
//            handler.removeCallbacks(runnable,null);
//        }
//
//        if (handlerDriver != null && runnableDriver != null)
//        {
//            handlerDriver.removeCallbacks(runnableDriver,null);
//        }
//
//        startNew();
//
//        ll_BookingLayout.setVisibility(View.GONE);
//        ll_AllCarLayout.setVisibility(View.GONE);
//        address_Layout.setVisibility(View.VISIBLE);
//        ll_AfterRequestAccept.setVisibility(View.GONE);
//        //ll_Done.setVisibility(View.VISIBLE);
//        current_marker_layout.setVisibility(View.VISIBLE);
//
//        myLocation = true;
//
//        if (carType_beens!=null && carType_beens.size()>0)
//        {
//            for (int i=0; i<carType_beens.size(); i++)
//            {
//                carType_beens.get(i).setSelectedCar(-1);
//            }
//        }
//
//        if (carAdapter!=null)
//        {
//            carAdapter.notifyDataSetChanged();
//            recyclerViewCar.scrollToPosition(0);
//        }
//
//        if (!TripId.equalsIgnoreCase(""))
//        {
//            CallTrackTripApi(TripId);
//        }

        coordinate = new LatLng(bookingDriverLocation_beens.get(0).getLatitude(), bookingDriverLocation_beens.get(0).getLongitude());

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition
                (new CameraPosition.Builder().target(coordinate).zoom(defaltZoomLavel).build()));

    }

    private void CallTrackTripApi(String TripId) {
        dialogClass.showDialog();
        String url = WebServiceAPI.API_TRACK_RUNNING_TRIP + TripId;

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    cardListBeens.clear();
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    if (json != null) {
//                        if (json.has("rating"))
//                        {
//                            if (json.getString("rating")!=null && !json.getString("rating").equalsIgnoreCase("") && !json.getString("rating").equalsIgnoreCase("NULL") && !json.getString("rating").equalsIgnoreCase("null"))
//                            {
//                                ratting = Float.parseFloat(json.getString("rating"));
//                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING,ratting+"",activity);
//                            }
//                            else
//                            {
//                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING,"0",activity);
//                            }
//                        }
//                        else
//                        {
//                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING,"0",activity);
//                        }
//                        Log.e("call","sdjflsjfljsdflj = "+ratting);

                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                if (json.has("BookingType")) {
                                    if (json.getString("BookingType") != null && json.getString("BookingType").equalsIgnoreCase("BookLater")) {
                                        advanceBooking = 1;
                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);

                                                if (json.has("CarInfo")) {
                                                    JSONArray jsonArray = json.getJSONArray("CarInfo");

                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                        if (jsonObject != null) {
                                                            if (jsonObject.has("VehicleModel")) {
                                                                String model = jsonObject.getString("VehicleModel");

                                                                int selectedCar = -1;

                                                                if (model.equalsIgnoreCase("1")) {
                                                                    selectedCar = 0;
                                                                } else if (model.equalsIgnoreCase("2")) {
                                                                    selectedCar = 1;
                                                                } else if (model.equalsIgnoreCase("3")) {
                                                                    selectedCar = 2;
                                                                } else if (model.equalsIgnoreCase("4")) {
                                                                    selectedCar = 3;
                                                                } else if (model.equalsIgnoreCase("5")) {
                                                                    selectedCar = 4;
                                                                } else if (model.equalsIgnoreCase("6")) {
                                                                    selectedCar = 5;
                                                                } else if (model.equalsIgnoreCase("7")) {
                                                                    selectedCar = 6;
                                                                }

                                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                            }
                                                        }

                                                    }
                                                }

                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                                dialogClass.hideDialog();
                                                timeFlag = 0;


                                                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
                                                    tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                                    Log.e("call", "onCreate() tripFlag if = " + tripFlag);
                                                } else {
                                                    tripFlag = 0;
                                                    Log.e("call", "onCreate() tripFlag else = " + tripFlag);
                                                }

                                                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
                                                    selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
                                                }

                                                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
                                                    try {
                                                        requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
                                                    } catch (Exception e) {
                                                        Log.e("call", "getting requestConfirmJsonObject exception = " + e.getMessage());
                                                    }

                                                }
                                            }
                                        }
                                    } else {
                                        advanceBooking = 0;
                                        if (json.has("CarInfo")) {
                                            JSONArray jsonArray = json.getJSONArray("CarInfo");

                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(0);

                                                if (jsonObject != null) {
                                                    if (jsonObject.has("VehicleModel")) {
                                                        String model = jsonObject.getString("VehicleModel");

                                                        int selectedCar = -1;
                                                        if (model.equalsIgnoreCase("1")) {
                                                            selectedCar = 0;
                                                        } else if (model.equalsIgnoreCase("2")) {
                                                            selectedCar = 1;
                                                        } else if (model.equalsIgnoreCase("3")) {
                                                            selectedCar = 2;
                                                        } else if (model.equalsIgnoreCase("4")) {
                                                            selectedCar = 3;
                                                        } else if (model.equalsIgnoreCase("5")) {
                                                            selectedCar = 4;
                                                        } else if (model.equalsIgnoreCase("6")) {
                                                            selectedCar = 5;
                                                        } else if (model.equalsIgnoreCase("7")) {
                                                            selectedCar = 6;
                                                        }

                                                        Log.e("selllllllleeeeee", "selectedCar : " + selectedCar);

                                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, selectedCar + "", activity);
                                                    }
                                                }

                                            }
                                        }

                                        if (json.has("Status")) {
                                            if (json.getString("Status").equalsIgnoreCase("traveling")) {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "2", activity);
                                            } else {
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                            }
                                        } else {
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                        }

                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);

                                        dialogClass.hideDialog();

                                        timeFlag = 0;


                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity).equals("")) {
                                            tripFlag = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, activity));
                                            Log.e("call", "onCreate() tripFlag if = " + tripFlag);
                                        } else {
                                            tripFlag = 0;
                                            Log.e("call", "onCreate() tripFlag else = " + tripFlag);
                                        }

                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity).equals("")) {
                                            selectedCarPosition = Integer.parseInt(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, activity));
                                        }

                                        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity).equals("")) {
                                            try {
                                                requestConfirmJsonObject = new JSONObject(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, activity));
                                            } catch (Exception e) {
                                                Log.e("call", "getting requestConfirmJsonObject exception = " + e.getMessage());
                                            }

                                        }
                                    }

                                    {
                                        if (originMarker != null) {
                                            originMarker.remove();
                                        }

                                        if (destinationMarker != null) {
                                            destinationMarker.remove();
                                        }

                                        if (googleMap != null) {
                                            googleMap.clear();
                                        }
                                        tv_CancelRequest.setVisibility(View.GONE);
                                        ll_AllCarLayout.setVisibility(View.GONE);
                                        ll_BookingLayout.setVisibility(View.GONE);
                                        //ll_Done.setVisibility(View.GONE);
                                        tv_DriverInfo.setVisibility(View.VISIBLE);
                                        ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                                        address_Layout.setVisibility(View.GONE);


                                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, json.toString(), activity);
                                        requestConfirmJsonObject = json;
                                        if (tripFlag == 0) {
                                            ll_BookingLayout.setVisibility(View.GONE);
                                            ll_AllCarLayout.setVisibility(View.GONE);
                                            address_Layout.setVisibility(View.VISIBLE);
                                            ll_AfterRequestAccept.setVisibility(View.GONE);
                                            //ll_Done.setVisibility(View.VISIBLE);
                                            current_marker_layout.setVisibility(View.VISIBLE);
                                            myLocation = true;

                                            if (carType_beens != null && carType_beens.size() > 0) {
                                                for (int i = 0; i < carType_beens.size(); i++) {
                                                    carType_beens.get(i).setSelectedCar(-1);
                                                }
                                            }

                                            if (carAdapter != null) {
                                                carAdapter.notifyDataSetChanged();
                                                recyclerViewCar.scrollToPosition(0);
                                            }
                                        } else {
                                            current_marker_layout.setVisibility(View.GONE);
                                            //ll_Done.setVisibility(View.GONE);
                                            ll_BookingLayout.setVisibility(View.GONE);
                                            ll_AllCarLayout.setVisibility(View.GONE);
                                            address_Layout.setVisibility(View.GONE);
                                            ll_AfterRequestAccept.setVisibility(View.VISIBLE);
                                            tv_DriverInfo.setVisibility(View.VISIBLE);
                                            if (tripFlag == 1) {
                                                myLocation = false;
                                                tv_CancelRequest.setVisibility(View.VISIBLE);
                                                parseAcceptRequestObject(requestConfirmJsonObject, 1);
                                            } else if (tripFlag == 2) {
                                                myLocation = false;
                                                tv_CancelRequest.setVisibility(View.GONE);
                                                showPathFromPickupToDropoff(requestConfirmJsonObject);
                                            }
                                        }

                                        showPathFromPickupToDropoff(requestConfirmJsonObject);
                                        startTimerForDriverLocation();
                                    }
                                } else {
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                    Log.e("call", "status false");
                                    dialogClass.hideDialog();
                                    ClearWholeData("");
                                }
                            } else {
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                                Log.e("call", "status false");
                                dialogClass.hideDialog();
                                ClearWholeData("");
                            }
                        } else {
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                            callResumeScreen = false;
                            dialogClass.hideDialog();
                            ClearWholeData("");
                            Log.e("call", "status not found");
                        }
                    } else {
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                        SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_RATTING, "0", activity);
                        callResumeScreen = false;
                        dialogClass.hideDialog();
                        ClearWholeData("");
                        Log.e("call", "json null ");
                    }
                } catch (Exception e) {
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", activity);
                    SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", activity);
                    callResumeScreen = false;
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.error_message));
                    ClearWholeData("");
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    int fingers,count;
    long firstClick,lastClick;


    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d("on dispatchTouchEvent ", " dispatchTouchEvent ");
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_POINTER_DOWN:
                fingers = fingers + 1;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                fingers = fingers - 1;
                break;
            case MotionEvent.ACTION_UP:
                fingers = 0;
                break;
            case MotionEvent.ACTION_DOWN:
                if (firstClick != 0 && System.currentTimeMillis() - firstClick > 300) {
                    Log.e("TAG", " count=0 ");
                    count = 0;
                }
                count++;
                if (count == 1) {
                    Log.e("TAG", " count=1 ");
                    firstClick = System.currentTimeMillis();
                } else if (count == 2) {
                    Log.e("TAG", " count=2 ");
                    lastClick = System.currentTimeMillis();
                    if (lastClick - firstClick < 300) {
                        CameraUpdate getzoom = CameraUpdateFactory.zoomIn();
                        googleMap.animateCamera(getzoom, 400, null);
                        Log.e("TAG", " event ");
                    }
                }
                fingers=1;
                break;
        }
        if (fingers >1) {
            Log.e("TAG", "2fingersaction ");
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            // mMap.getUiSettings().setRotateGesturesEnabled(true);         // disableScrolling()
        }
        else if (fingers < 1 )
        {
            Log.e("TAG", "onefinger ");
            if(googleMap != null) {
                googleMap.getUiSettings().setAllGesturesEnabled(false);

                googleMap.getUiSettings().setScrollGesturesEnabled(true);
            }
            // enableScrolling();
        }
        if (fingers > 1) {
            Log.e("TAG", "doubleifcondition ");
            return gestureDetector.onTouchEvent(ev);
            //return onTouchEvent(ev);
        } else
            return super.dispatchTouchEvent(ev);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(100/*drawable.getIntrinsicWidth()*/, 100/*drawable.getIntrinsicHeight()*/, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, 100/*drawable.getIntrinsicWidth()*/, 100/*drawable.getIntrinsicHeight()*/);
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static void displayCurrencyInfoForLocale(Locale locale) {
        Log.e("TAG","Locale: " + locale.getDisplayName());
        Currency currency = Currency.getInstance(locale);
        Log.e("TAG","Currency Code: " + currency.getCurrencyCode());
        Log.e("TAG","Symbol: " + currency.getSymbol());
        Log.e("TAG","Default Fraction Digits: " + currency.getDefaultFractionDigits());
    }

    public String commaFormat(Double x)
    {
        return new DecimalFormat("#,###.0").format(x);
    }

    public void playNotificationSound(boolean payment_status)
    {
        try {
            Log.e("TAG","33333333");
            Uri alarmSound;
            if(payment_status)
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/payment_1");}
            else
            {alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/paytm_failed");}
            Log.e("TAG","2222222");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            Log.e("TAG","11111111111");
            r.play();
        } catch (Exception e)
        {
            Log.e("TAG","Error = "+e.getMessage());
            Log.e("TAG","Error = "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public void playNotificationSound1()
    {
        try {
            Log.e("TAG","33333333");
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/open_paytm");
            Log.e("TAG","2222222");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            Log.e("TAG","11111111111");
            r.play();
        } catch (Exception e)
        {
            Log.e("TAG","Error = "+e.getMessage());
            Log.e("TAG","Error = "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
}