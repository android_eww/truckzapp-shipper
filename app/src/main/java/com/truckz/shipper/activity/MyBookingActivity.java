package com.truckz.shipper.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.been.PastBooking_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.fragment.OnGoing_Fragment;
import com.truckz.shipper.fragment.PastBooking_Fragment;
import com.truckz.shipper.fragment.UpComming_Fragment;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CTextViewMedium;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class MyBookingActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "MyBookingActivity";
    public static MyBookingActivity activity;
    private LinearLayout ll_Back;
    private ImageView iv_Back, iv_call;
    private CTextViewBold tv_Title;
    private LinearLayout ll_OnGoing,ll_UpComming,ll_PastBooking;
    private CTextViewMedium tv_onGoing,tv_upComing,tv_pastBooking;
    int selectedTab = 1;
    int preSelectedTab = 1;
    public static DialogClass dialogClass;
    public static AQuery aQuery;

    public static List<PastBooking_Been> upComming_beens = new ArrayList<PastBooking_Been>();
    public static List<PastBooking_Been> MainList_Upcoming_Booking = new ArrayList<PastBooking_Been>();

    public static List<PastBooking_Been> onGoing_beens = new ArrayList<PastBooking_Been>();
    public static List<PastBooking_Been> MainList_OnGoing_Booking = new ArrayList<PastBooking_Been>();

    public static List<PastBooking_Been> pastBooking_beens = new ArrayList<PastBooking_Been>();
//    public static List<PastBooking_Been> MainList_Past_Booking = new ArrayList<PastBooking_Been>();

    private String notificationType = "";

    private CardView cv_taxi, cv_transport;
    public static String selectService = "delivery";

    private UpComming_Fragment upComming_fragment;
    private OnGoing_Fragment onGoing_fragment;
    private PastBooking_Fragment pastBooking_fragment;

    public static int pastPageIndex = 1;
    public static int priviousListSize = 0;

    public View view_past_booking, view_on_going, view_up_comming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);

        activity = MyBookingActivity.this;
        selectedTab = 1;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);
        selectService = "delivery";
        pastPageIndex = 1;
        priviousListSize = 0;

        upComming_beens.clear();
        MainList_Upcoming_Booking.clear();
        onGoing_beens.clear();
        MainList_OnGoing_Booking.clear();
        pastBooking_beens.clear();
//        MainList_Past_Booking.clear();

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("notification")!=null)
            {
                notificationType = this.getIntent().getStringExtra("notification");
            }
            else
            {
                notificationType = "";
            }
        }
        else
        {
            notificationType = "";
        }

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);
        tv_Title = findViewById(R.id.title_textview);

        tv_Title.setText(getResources().getString(R.string.my_shipment));

        ll_OnGoing = findViewById(R.id.my_booking_layout_on_going);
        ll_UpComming = findViewById(R.id.my_booking_layout_up_comming);
        ll_PastBooking = findViewById(R.id.my_booking_layout_past_booking);

        tv_onGoing = findViewById(R.id.tv_onGoing);
        tv_upComing = findViewById(R.id.tv_upComing);
        tv_pastBooking = findViewById(R.id.tv_pastBooking);

        view_up_comming = findViewById(R.id.view_up_comming);
        view_on_going = findViewById(R.id.view_on_going);
        view_past_booking = findViewById(R.id.view_past_booking);

        cv_taxi = findViewById(R.id.cv_taxi);
        cv_transport = findViewById(R.id.cv_transport);


        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);

        ll_OnGoing.setOnClickListener(activity);
        ll_UpComming.setOnClickListener(activity);
        ll_PastBooking.setOnClickListener(activity);

        cv_taxi.setOnClickListener(activity);
        cv_transport.setOnClickListener(activity);

        if (Global.isNetworkconn(activity))
        {
            dialogClass.showDialog();
            callUpComingApi();
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(activity);
            internetDialog.showDialog(activity.getResources().getString(R.string.please_check_internet_connection),
                    activity.getResources().getString(R.string.no_internet_connection));
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.my_booking_layout_on_going:
                call_OnGoing();
                break;

            case R.id.my_booking_layout_up_comming:
                call_UpComming();
                break;

            case R.id.my_booking_layout_past_booking:
                call_PastBooking();
                break;

            case R.id.cv_taxi:
                selectService = "taxi";
                call_service_type();
                break;

            case R.id.cv_transport:
                selectService = "delivery";
                call_service_type();
                break;
        }
    }

    private void call_OnGoing()
    {
        Log.e(TAG,"call_OnGoing()");
        preSelectedTab = selectedTab;
        selectedTab = 1;
        ll_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ll_UpComming.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ll_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        tv_onGoing.setTextColor(getResources().getColor(R.color.colorButton));
        tv_upComing.setTextColor(getResources().getColor(R.color.colorTextGray));
        tv_pastBooking.setTextColor(getResources().getColor(R.color.colorTextGray));

        view_past_booking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_on_going.setBackgroundColor(getResources().getColor(R.color.colorButton));
        view_up_comming.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        load_OnGoing();
    }

    private void call_UpComming()
    {
        Log.e(TAG,"call_UpComming()");
        preSelectedTab = selectedTab;
        selectedTab = 2;
        ll_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        //ll_UpComming.setBackgroundColor(getResources().getColor(R.color.colorButton));
        ll_UpComming.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ll_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        tv_onGoing.setTextColor(getResources().getColor(R.color.colorTextGray));
        tv_upComing.setTextColor(getResources().getColor(R.color.colorButton));
        tv_pastBooking.setTextColor(getResources().getColor(R.color.colorTextGray));

        view_past_booking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_on_going.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_up_comming.setBackgroundColor(getResources().getColor(R.color.colorButton));

        load_UpComming();
    }

    private void call_PastBooking()
    {
        Log.e(TAG,"call_PastBooking()");
        preSelectedTab = selectedTab;
        selectedTab = 0;

        ll_PastBooking.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ll_UpComming.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ll_OnGoing.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        tv_onGoing.setTextColor(getResources().getColor(R.color.colorTextGray));
        tv_upComing.setTextColor(getResources().getColor(R.color.colorTextGray));
        tv_pastBooking.setTextColor(getResources().getColor(R.color.colorButton));

        view_past_booking.setBackgroundColor(getResources().getColor(R.color.colorButton));
        view_on_going.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        view_up_comming.setBackgroundColor(getResources().getColor(R.color.colorWhite));


        load_PastBooking();
    }

    private void load_OnGoing()
    {
        onGoing_fragment = new OnGoing_Fragment();

        Log.e("call","load_OnGoing()");
        if (preSelectedTab==selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, onGoing_fragment, "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else if (preSelectedTab<selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come,R.anim.gla_there_gone,R.anim.gla_back_come,R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, onGoing_fragment, "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come,R.anim.gla_back_gone,R.anim.gla_there_come,R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, onGoing_fragment, "OnGoing");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
    }

    private void load_UpComming()
    {
        upComming_fragment = new UpComming_Fragment();

        Log.e(TAG,"load_UpComming()");
        if (preSelectedTab==selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, upComming_fragment, "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else if (preSelectedTab<selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come,R.anim.gla_there_gone,R.anim.gla_back_come,R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, upComming_fragment, "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come,R.anim.gla_back_gone,R.anim.gla_there_come,R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, upComming_fragment, "UpComming");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
    }

    private void load_PastBooking()
    {
        pastBooking_fragment = new PastBooking_Fragment();

        Log.e(TAG,"load_PastBooking()");
        if (preSelectedTab==selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.replace(R.id.container_my_booking, pastBooking_fragment, "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else if (preSelectedTab<selectedTab)
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_there_come,R.anim.gla_there_gone,R.anim.gla_back_come,R.anim.gla_back_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, pastBooking_fragment, "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
        else
        {
            FragmentManager fragmentManager_items = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction_items = fragmentManager_items.beginTransaction();
            fragmentTransaction_items.setCustomAnimations(R.anim.gla_back_come,R.anim.gla_back_gone,R.anim.gla_there_come,R.anim.gla_there_gone);
            fragmentTransaction_items.replace(R.id.container_my_booking, pastBooking_fragment, "PastBooking");
            fragmentTransaction_items.commitAllowingStateLoss();
        }
    }

    private void callUpComingApi()
    {
        Log.e(TAG,"callUpComingApi()");

        String url = WebServiceAPI.API_UPCOMING_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e(TAG,"callUpComingApi() url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callUpComingApi() responseCode= " + responseCode);
                    Log.e(TAG,"callUpComingApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                upComming_beens.clear();
                                MainList_Upcoming_Booking.clear();

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);
                                            List<Parcel_Been> parcel_beens = new ArrayList<Parcel_Been>();

                                            if (jsonObject!=null)
                                            {
                                                /////DistanceFare, Discount, Trash, CardId, ByDriverAmount, Notes
                                                String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="",PickupTime="", DropTime=""
                                                        ,TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="", NightFareApplicable="",NightFare="",TripFare="", DistanceFare=""
                                                        ,WaitingTime="", WaitingTimeCost="",TollFee="",BookingCharge="", Tax="", PromoCode="", Discount="", SubTotal="",GrandTotal=""
                                                        ,Status="", Trash="", Reason="",PaymentType="", CardId="", ByDriverAmount="", AdminAmount="", CompanyAmount="", PickupLat="",PickupLng=""
                                                        ,DropOffLat="",DropOffLon="", BookingType="", PassengerName="",PassengerContact="", Notes="", FlightNumber="",  NoOfPassenger=""
                                                        , NoOfLuggage="", Model="", DriverName="", CarDetails="",RequestFor="taxi";

                                                String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="", CarDetails_Company="",CarDetails_Color=""
                                                        ,CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="", CarDetails_VehicleInsuranceCertificate=""
                                                        ,CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="", CarDetails_VehicleImage=""
                                                        ,CarDetails_Description="", HistoryType="",LoadingUnloadingTime = "",
                                                        LoadingUnloadingCharge = "", ShareUrl = "",ParcelWeight="",
                                                        ArrivedTime="", DriverMobileNo="",SpecialExtraCharge="",PreviousDue="",CancellationFee="",ReferenceId="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("CompanyId"))
                                                {
                                                    CompanyId = jsonObject.getString("CompanyId");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("ModelId"))
                                                {
                                                    ModelId = jsonObject.getString("ModelId");
                                                }

                                                if (jsonObject.has("DriverId"))
                                                {
                                                    DriverId = jsonObject.getString("DriverId");
                                                }

                                                if (jsonObject.has("CreatedDate"))
                                                {
                                                    CreatedDate = jsonObject.getString("CreatedDate");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("PickupDateTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupDateTime");
                                                }

                                                if (jsonObject.has("DropTime"))
                                                {
                                                    DropTime = jsonObject.getString("DropTime");
                                                }

                                                if (jsonObject.has("TripDuration"))
                                                {
                                                    TripDuration = jsonObject.getString("TripDuration");
                                                }

                                                if (jsonObject.has("TripDistance"))
                                                {
                                                    TripDistance = jsonObject.getString("TripDistance");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = jsonObject.getString("DropoffLocation");
                                                }

                                                if (jsonObject.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                }

                                                if (jsonObject.has("NightFare"))
                                                {
                                                    NightFare = jsonObject.getString("NightFare");
                                                }

                                                if (jsonObject.has("TripFare"))
                                                {
                                                    TripFare = jsonObject.getString("TripFare");
                                                }

                                                if (jsonObject.has("DistanceFare"))
                                                {
                                                    DistanceFare = jsonObject.getString("DistanceFare");
                                                }

                                                if (jsonObject.has("WaitingTime"))
                                                {
                                                    WaitingTime = jsonObject.getString("WaitingTime");
                                                }

                                                if (jsonObject.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                }

                                                if (jsonObject.has("TollFee"))
                                                {
                                                    TollFee = jsonObject.getString("TollFee");
                                                }

                                                if (jsonObject.has("BookingCharge"))
                                                {
                                                    BookingCharge = jsonObject.getString("BookingCharge");
                                                }

                                                if (jsonObject.has("Tax"))
                                                {
                                                    Tax = jsonObject.getString("Tax");
                                                }

                                                if (jsonObject.has("PromoCode"))
                                                {
                                                    PromoCode = jsonObject.getString("PromoCode");
                                                }

                                                if (jsonObject.has("Discount"))
                                                {
                                                    Discount = jsonObject.getString("Discount");
                                                }

                                                if (jsonObject.has("SubTotal"))
                                                {
                                                    SubTotal = jsonObject.getString("SubTotal");
                                                }

                                                if (jsonObject.has("GrandTotal"))
                                                {
                                                    GrandTotal = jsonObject.getString("GrandTotal");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Trash"))
                                                {
                                                    Trash = jsonObject.getString("Trash");
                                                }

                                                if (jsonObject.has("Reason"))
                                                {
                                                    Reason = jsonObject.getString("Reason");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("CardId"))
                                                {
                                                    CardId = jsonObject.getString("CardId");
                                                }

                                                if (jsonObject.has("ByDriverAmount"))
                                                {
                                                    ByDriverAmount = jsonObject.getString("ByDriverAmount");
                                                }

                                                if (jsonObject.has("AdminAmount"))
                                                {
                                                    AdminAmount = jsonObject.getString("AdminAmount");
                                                }

                                                if (jsonObject.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = jsonObject.getString("CompanyAmount");
                                                }

                                                if (jsonObject.has("PickupLat"))
                                                {
                                                    PickupLat = jsonObject.getString("PickupLat");
                                                }

                                                if (jsonObject.has("PickupLng"))
                                                {
                                                    PickupLng = jsonObject.getString("PickupLng");
                                                }

                                                if (jsonObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = jsonObject.getString("DropOffLat");
                                                }

                                                if (jsonObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = jsonObject.getString("DropOffLon");
                                                }

                                                if (jsonObject.has("BookingType"))
                                                {
                                                    BookingType = jsonObject.getString("BookingType");
                                                }

                                                if (jsonObject.has("PassengerName"))
                                                {
                                                    PassengerName = jsonObject.getString("PassengerName");
                                                }

                                                if (jsonObject.has("PassengerContact"))
                                                {
                                                    PassengerContact = jsonObject.getString("PassengerContact");
                                                }

                                                if (jsonObject.has("Notes"))
                                                {
                                                    Notes = jsonObject.getString("Notes");
                                                }

                                                if (jsonObject.has("FlightNumber"))
                                                {
                                                    FlightNumber = jsonObject.getString("FlightNumber");
                                                }

                                                if (jsonObject.has("NoOfPassenger"))
                                                {
                                                    NoOfPassenger = jsonObject.getString("NoOfPassenger");
                                                }

                                                if (jsonObject.has("NoOfLuggage"))
                                                {
                                                    NoOfLuggage = jsonObject.getString("NoOfLuggage");
                                                }

                                                if (jsonObject.has("Model"))
                                                {
                                                    Model = jsonObject.getString("Model");
                                                }

                                                if (jsonObject.has("DriverName"))
                                                {
                                                    DriverName = jsonObject.getString("DriverName");
                                                }

                                                if (jsonObject.has("RequestFor"))
                                                {
                                                    RequestFor = jsonObject.getString("RequestFor");
                                                }
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    HistoryType = jsonObject.getString("HistoryType");
                                                }
                                                if (jsonObject.has("LoadingUnloadingCharge"))
                                                {
                                                    LoadingUnloadingCharge = jsonObject.getString("LoadingUnloadingCharge");
                                                }
                                                if (jsonObject.has("LoadingUnloadingTime"))
                                                {
                                                    LoadingUnloadingTime = jsonObject.getString("LoadingUnloadingTime");
                                                }
                                                if (jsonObject.has("ShareUrl"))
                                                {
                                                    ShareUrl = jsonObject.getString("ShareUrl");
                                                }
                                                if (jsonObject.has("ParcelWeight"))
                                                {
                                                    ParcelWeight = jsonObject.getString("ParcelWeight");
                                                }
                                                if (jsonObject.has("ArrivedTime"))
                                                {
                                                    ArrivedTime = jsonObject.getString("ArrivedTime");
                                                }
                                                if (jsonObject.has("DriverMobileNo"))
                                                {
                                                    DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                }
                                                if (jsonObject.has("SpecialExtraCharge"))
                                                {
                                                    SpecialExtraCharge = jsonObject.getString("SpecialExtraCharge");
                                                }
                                                if (jsonObject.has("PreviousDue"))
                                                {
                                                    PreviousDue = jsonObject.getString("PreviousDue");
                                                }
                                                if (jsonObject.has("CancellationFee"))
                                                {
                                                    CancellationFee = jsonObject.getString("CancellationFee");
                                                }
                                                if (jsonObject.has("ReferenceId"))
                                                {
                                                    ReferenceId = jsonObject.getString("ReferenceId");
                                                }

                                                if (jsonObject.has("Parcel"))
                                                {
                                                    JSONObject Parcel = jsonObject.getJSONObject("Parcel");

                                                    if (Parcel!=null)
                                                    {
                                                        String Parcel_Id = "",Parcel_Name = "",Parcel_Height = "",Parcel_Width = "";
                                                        String Parcel_Weight= "",Parcel_Image= "",Parcel_Status= "",Parcel_CreatedDate= "";

                                                        if (Parcel.has("Id"))
                                                        {
                                                            Parcel_Id = Parcel.getString("Id");
                                                        }

                                                        if (Parcel.has("Name"))
                                                        {
                                                            Parcel_Name = Parcel.getString("Name");
                                                        }

                                                        if (Parcel.has("Height"))
                                                        {
                                                            Parcel_Height = Parcel.getString("Height");
                                                        }

                                                        if (Parcel.has("Width"))
                                                        {
                                                            Parcel_Width = Parcel.getString("Width");
                                                        }

                                                        if (Parcel.has("Weight"))
                                                        {
                                                            Parcel_Weight = Parcel.getString("Weight");
                                                        }

                                                        if (Parcel.has("Image"))
                                                        {
                                                            Parcel_Image = Parcel.getString("Image");
                                                        }

                                                        if (Parcel.has("Status"))
                                                        {
                                                            Parcel_Status = Parcel.getString("Status");
                                                        }

                                                        if (Parcel.has("CreatedDate"))
                                                        {
                                                            Parcel_CreatedDate = Parcel.getString("CreatedDate");
                                                        }

                                                        parcel_beens.add(new Parcel_Been(Parcel_Id,Parcel_Name,Parcel_Height,Parcel_Width,Parcel_Weight,Parcel_Image,Parcel_Status,Parcel_CreatedDate));
                                                    }
                                                }

                                                if (jsonObject.has("CarDetails"))
                                                {
                                                    CarDetails = jsonObject.getString("CarDetails");

                                                    JSONObject CarDetails1 = jsonObject.getJSONObject("CarDetails");

                                                    if (CarDetails1!=null)
                                                    {
                                                        if (CarDetails1.has("Id"))
                                                        {
                                                            CarDetails_Id = CarDetails1.getString("Id");
                                                        }

                                                        if (CarDetails1.has("CompanyId"))
                                                        {
                                                            CarDetails_CompanyId = CarDetails1.getString("CompanyId");
                                                        }

                                                        if (CarDetails1.has("VehicleModel"))
                                                        {
                                                            CarDetails_VehicleModel = CarDetails1.getString("VehicleModel");
                                                        }

                                                        if (CarDetails1.has("Company"))
                                                        {
                                                            CarDetails_Company = CarDetails1.getString("Company");
                                                        }

                                                        if (CarDetails1.has("Color"))
                                                        {
                                                            CarDetails_Color = CarDetails1.getString("Color");
                                                        }

                                                        if (CarDetails1.has("VehicleRegistrationNo"))
                                                        {
                                                            CarDetails_VehicleRegistrationNo = CarDetails1.getString("VehicleRegistrationNo");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificate"))
                                                        {
                                                            CarDetails_RegistrationCertificate = CarDetails1.getString("RegistrationCertificate");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificate"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificate = CarDetails1.getString("VehicleInsuranceCertificate");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificateExpire"))
                                                        {
                                                            CarDetails_RegistrationCertificateExpire = CarDetails1.getString("RegistrationCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificateExpire = CarDetails1.getString("VehicleInsuranceCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleImage"))
                                                        {
                                                            CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails1.getString("VehicleImage");
                                                        }

                                                        if (CarDetails1.has("Description"))
                                                        {
                                                            CarDetails_Description = CarDetails1.getString("Description");
                                                        }
                                                    }
                                                }

                                                MainList_Upcoming_Booking.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime,LoadingUnloadingCharge,ShareUrl,ParcelWeight,ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));

                                                upComming_beens.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime,LoadingUnloadingCharge,ShareUrl,ParcelWeight,ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));
                                            }
                                            else
                                            {
                                                Log.e(TAG,"callUpComingApi() jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"callUpComingApi() history null or lenth 0");
                                    }

                                    Log.e(TAG,"callUpComingApi() upComming_beens.size() = "+upComming_beens.size());

                                    dialogClass.hideDialog();
                                }
                                else
                                {
                                    Log.e(TAG,"callUpComingApi() history not found");
                                    dialogClass.hideDialog();

//                                    call_UpComming();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callUpComingApi() status false");
                                dialogClass.hideDialog();

//                                call_UpComming();
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callUpComingApi() status not found");
                            dialogClass.hideDialog();

//                            call_UpComming();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callUpComingApi() json null");
                        dialogClass.hideDialog();

//                        call_UpComming();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callUpComingApi() Exception "+e.toString());
                    dialogClass.hideDialog();

//                    call_UpComming();
                }
                finally
                {
                    Log.e(TAG,"callUpComingApi() finally");
                    callOngoingApi();
                    //call_UpComming();


                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callOngoingApi()
    {
        Log.e(TAG,"callOngoingApi()");

        String url = WebServiceAPI.API_ONGOING_BOOKING_HISTORY + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity);

        Log.e(TAG,"callOngoingApi() url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callOngoingApi() responseCode= " + responseCode);
                    Log.e(TAG,"callOngoingApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {

                                onGoing_beens.clear();
                                MainList_OnGoing_Booking.clear();

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);
                                            List<Parcel_Been> parcel_beens = new ArrayList<Parcel_Been>();

                                            if (jsonObject!=null)
                                            {
                                                /////DistanceFare, Discount, Trash, CardId, ByDriverAmount, Notes
                                                String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="",PickupTime="", DropTime=""
                                                        ,TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="", NightFareApplicable="",NightFare="",TripFare="", DistanceFare=""
                                                        ,WaitingTime="", WaitingTimeCost="",TollFee="",BookingCharge="", Tax="", PromoCode="", Discount="", SubTotal="",GrandTotal=""
                                                        ,Status="", Trash="", Reason="",PaymentType="", CardId="", ByDriverAmount="", AdminAmount="", CompanyAmount="", PickupLat="",PickupLng=""
                                                        ,DropOffLat="",DropOffLon="", BookingType="", PassengerName="",PassengerContact="", Notes="", FlightNumber="",  NoOfPassenger=""
                                                        , NoOfLuggage="", Model="", DriverName="", CarDetails="",RequestFor="taxi";

                                                String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="", CarDetails_Company="",CarDetails_Color=""
                                                        ,CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="", CarDetails_VehicleInsuranceCertificate=""
                                                        ,CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="", CarDetails_VehicleImage=""
                                                        ,CarDetails_Description="", HistoryType="",
                                                        LoadingUnloadingTime="",LoadingUnloadingCharge="", ShareUrl="",ParcelWeight="",
                                                        ArrivedTime="",DriverMobileNo="",SpecialExtraCharge="",PreviousDue="",CancellationFee="",ReferenceId="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("CompanyId"))
                                                {
                                                    CompanyId = jsonObject.getString("CompanyId");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("ModelId"))
                                                {
                                                    ModelId = jsonObject.getString("ModelId");
                                                }

                                                if (jsonObject.has("DriverId"))
                                                {
                                                    DriverId = jsonObject.getString("DriverId");
                                                }

                                                if (jsonObject.has("CreatedDate"))
                                                {
                                                    CreatedDate = jsonObject.getString("CreatedDate");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("PickupTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupTime");
                                                }

                                                if (jsonObject.has("DropTime"))
                                                {
                                                    DropTime = jsonObject.getString("DropTime");
                                                }

                                                if (jsonObject.has("TripDuration"))
                                                {
                                                    TripDuration = jsonObject.getString("TripDuration");
                                                }

                                                if (jsonObject.has("TripDistance"))
                                                {
                                                    TripDistance = jsonObject.getString("TripDistance");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = jsonObject.getString("DropoffLocation");
                                                }

                                                if (jsonObject.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                }

                                                if (jsonObject.has("NightFare"))
                                                {
                                                    NightFare = jsonObject.getString("NightFare");
                                                }

                                                if (jsonObject.has("TripFare"))
                                                {
                                                    TripFare = jsonObject.getString("TripFare");
                                                }

                                                if (jsonObject.has("DistanceFare"))
                                                {
                                                    DistanceFare = jsonObject.getString("DistanceFare");
                                                }

                                                if (jsonObject.has("WaitingTime"))
                                                {
                                                    WaitingTime = jsonObject.getString("WaitingTime");
                                                }

                                                if (jsonObject.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                }

                                                if (jsonObject.has("TollFee"))
                                                {
                                                    TollFee = jsonObject.getString("TollFee");
                                                }

                                                if (jsonObject.has("BookingCharge"))
                                                {
                                                    BookingCharge = jsonObject.getString("BookingCharge");
                                                }

                                                if (jsonObject.has("Tax"))
                                                {
                                                    Tax = jsonObject.getString("Tax");
                                                }

                                                if (jsonObject.has("PromoCode"))
                                                {
                                                    PromoCode = jsonObject.getString("PromoCode");
                                                }

                                                if (jsonObject.has("Discount"))
                                                {
                                                    Discount = jsonObject.getString("Discount");
                                                }

                                                if (jsonObject.has("SubTotal"))
                                                {
                                                    SubTotal = jsonObject.getString("SubTotal");
                                                }

                                                if (jsonObject.has("GrandTotal"))
                                                {
                                                    GrandTotal = jsonObject.getString("GrandTotal");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Trash"))
                                                {
                                                    Trash = jsonObject.getString("Trash");
                                                }

                                                if (jsonObject.has("Reason"))
                                                {
                                                    Reason = jsonObject.getString("Reason");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("CardId"))
                                                {
                                                    CardId = jsonObject.getString("CardId");
                                                }

                                                if (jsonObject.has("ByDriverAmount"))
                                                {
                                                    ByDriverAmount = jsonObject.getString("ByDriverAmount");
                                                }

                                                if (jsonObject.has("AdminAmount"))
                                                {
                                                    AdminAmount = jsonObject.getString("AdminAmount");
                                                }

                                                if (jsonObject.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = jsonObject.getString("CompanyAmount");
                                                }

                                                if (jsonObject.has("PickupLat"))
                                                {
                                                    PickupLat = jsonObject.getString("PickupLat");
                                                }

                                                if (jsonObject.has("PickupLng"))
                                                {
                                                    PickupLng = jsonObject.getString("PickupLng");
                                                }

                                                if (jsonObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = jsonObject.getString("DropOffLat");
                                                }

                                                if (jsonObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = jsonObject.getString("DropOffLon");
                                                }

                                                if (jsonObject.has("BookingType"))
                                                {
                                                    BookingType = jsonObject.getString("BookingType");
                                                }

                                                if (jsonObject.has("PassengerName"))
                                                {
                                                    PassengerName = jsonObject.getString("PassengerName");
                                                }

                                                if (jsonObject.has("PassengerContact"))
                                                {
                                                    PassengerContact = jsonObject.getString("PassengerContact");
                                                }

                                                if (jsonObject.has("Notes"))
                                                {
                                                    Notes = jsonObject.getString("Notes");
                                                }

                                                if (jsonObject.has("FlightNumber"))
                                                {
                                                    FlightNumber = jsonObject.getString("FlightNumber");
                                                }

                                                if (jsonObject.has("NoOfPassenger"))
                                                {
                                                    NoOfPassenger = jsonObject.getString("NoOfPassenger");
                                                }

                                                if (jsonObject.has("NoOfLuggage"))
                                                {
                                                    NoOfLuggage = jsonObject.getString("NoOfLuggage");
                                                }

                                                if (jsonObject.has("Model"))
                                                {
                                                    Model = jsonObject.getString("Model");
                                                }

                                                if (jsonObject.has("DriverName"))
                                                {
                                                    DriverName = jsonObject.getString("DriverName");
                                                }

                                                if (jsonObject.has("RequestFor"))
                                                {
                                                    RequestFor = jsonObject.getString("RequestFor");
                                                }
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    HistoryType = jsonObject.getString("HistoryType");
                                                }
                                                if (jsonObject.has("LoadingUnloadingTime"))
                                                {
                                                    LoadingUnloadingTime = jsonObject.getString("LoadingUnloadingTime");
                                                }
                                                if (jsonObject.has("LoadingUnloadingCharge"))
                                                {
                                                    LoadingUnloadingCharge = jsonObject.getString("LoadingUnloadingCharge");
                                                }
                                                if (jsonObject.has("ShareUrl"))
                                                {
                                                    ShareUrl = jsonObject.getString("ShareUrl");
                                                }
                                                if (jsonObject.has("ParcelWeight"))
                                                {
                                                    ParcelWeight = jsonObject.getString("ParcelWeight");
                                                }
                                                if (jsonObject.has("ArrivedTime"))
                                                {
                                                    ArrivedTime = jsonObject.getString("ArrivedTime");
                                                }
                                                if (jsonObject.has("DriverMobileNo"))
                                                {
                                                    DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                }
                                                if (jsonObject.has("SpecialExtraCharge"))
                                                {
                                                    SpecialExtraCharge = jsonObject.getString("SpecialExtraCharge");
                                                }
                                                if (jsonObject.has("PreviousDue"))
                                                {
                                                    PreviousDue = jsonObject.getString("PreviousDue");
                                                }
                                                if (jsonObject.has("CancellationFee"))
                                                {
                                                    CancellationFee = jsonObject.getString("CancellationFee");
                                                }
                                                if (jsonObject.has("ReferenceId"))
                                                {
                                                    ReferenceId = jsonObject.getString("ReferenceId");
                                                }

                                                if (jsonObject.has("Parcel"))
                                                {
                                                    JSONObject Parcel = jsonObject.getJSONObject("Parcel");

                                                    if (Parcel!=null)
                                                    {
                                                        String Parcel_Id = "",Parcel_Name = "",Parcel_Height = "",Parcel_Width = "";
                                                        String Parcel_Weight= "",Parcel_Image= "",Parcel_Status= "",Parcel_CreatedDate= "";

                                                        if (Parcel.has("Id"))
                                                        {
                                                            Parcel_Id = Parcel.getString("Id");
                                                        }

                                                        if (Parcel.has("Name"))
                                                        {
                                                            Parcel_Name = Parcel.getString("Name");
                                                        }

                                                        if (Parcel.has("Height"))
                                                        {
                                                            Parcel_Height = Parcel.getString("Height");
                                                        }

                                                        if (Parcel.has("Width"))
                                                        {
                                                            Parcel_Width = Parcel.getString("Width");
                                                        }

                                                        if (Parcel.has("Weight"))
                                                        {
                                                            Parcel_Weight = Parcel.getString("Weight");
                                                        }

                                                        if (Parcel.has("Image"))
                                                        {
                                                            Parcel_Image = Parcel.getString("Image");
                                                        }

                                                        if (Parcel.has("Status"))
                                                        {
                                                            Parcel_Status = Parcel.getString("Status");
                                                        }

                                                        if (Parcel.has("CreatedDate"))
                                                        {
                                                            Parcel_CreatedDate = Parcel.getString("CreatedDate");
                                                        }

                                                        parcel_beens.add(new Parcel_Been(Parcel_Id,Parcel_Name,Parcel_Height,Parcel_Width,Parcel_Weight,Parcel_Image,Parcel_Status,Parcel_CreatedDate));
                                                    }
                                                }

                                                if (jsonObject.has("CarDetails"))
                                                {
                                                    CarDetails = jsonObject.getString("CarDetails");

                                                    JSONObject CarDetails1 = jsonObject.getJSONObject("CarDetails");

                                                    if (CarDetails1!=null)
                                                    {
                                                        if (CarDetails1.has("Id"))
                                                        {
                                                            CarDetails_Id = CarDetails1.getString("Id");
                                                        }

                                                        if (CarDetails1.has("CompanyId"))
                                                        {
                                                            CarDetails_CompanyId = CarDetails1.getString("CompanyId");
                                                        }

                                                        if (CarDetails1.has("VehicleModel"))
                                                        {
                                                            CarDetails_VehicleModel = CarDetails1.getString("VehicleModel");
                                                        }

                                                        if (CarDetails1.has("Company"))
                                                        {
                                                            CarDetails_Company = CarDetails1.getString("Company");
                                                        }

                                                        if (CarDetails1.has("Color"))
                                                        {
                                                            CarDetails_Color = CarDetails1.getString("Color");
                                                        }

                                                        if (CarDetails1.has("VehicleRegistrationNo"))
                                                        {
                                                            CarDetails_VehicleRegistrationNo = CarDetails1.getString("VehicleRegistrationNo");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificate"))
                                                        {
                                                            CarDetails_RegistrationCertificate = CarDetails1.getString("RegistrationCertificate");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificate"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificate = CarDetails1.getString("VehicleInsuranceCertificate");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificateExpire"))
                                                        {
                                                            CarDetails_RegistrationCertificateExpire = CarDetails1.getString("RegistrationCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificateExpire = CarDetails1.getString("VehicleInsuranceCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleImage"))
                                                        {
                                                            CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails1.getString("VehicleImage");
                                                        }

                                                        if (CarDetails1.has("Description"))
                                                        {
                                                            CarDetails_Description = CarDetails1.getString("Description");
                                                        }
                                                    }
                                                }

                                                MainList_OnGoing_Booking.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime,LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));

                                                onGoing_beens.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime,LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));
                                            }
                                            else
                                            {
                                                Log.e(TAG,"callOngoingApi() jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"callOngoingApi() history null or lenth 0");
                                    }

                                    Log.e(TAG,"callOngoingApi() onGoing_beens.size() = "+onGoing_beens.size());

                                    dialogClass.hideDialog();


//                                    callPastApi();
                                }
                                else
                                {
                                    Log.e(TAG,"callOngoingApi() history not found");
                                    dialogClass.hideDialog();

//                                    call_UpComming();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callOngoingApi() status false");
                                dialogClass.hideDialog();

//                                call_UpComming();
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callOngoingApi() status not found");
                            dialogClass.hideDialog();

//                            call_UpComming();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callOngoingApi() json null");
                        dialogClass.hideDialog();

//                        call_UpComming();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callOngoingApi() Exception "+e.toString());
                    dialogClass.hideDialog();

//                    call_UpComming();
                }
                finally
                {
                    Log.e(TAG,"callUpComingApi() finally");
                    callPastApi();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callPastApi()
    {
        Log.e(TAG,"callPastApi()");

        String url = WebServiceAPI.API_PAST_BOOKING_HISTORY +
                SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,activity) + "/" + pastPageIndex;// + "/" + selectService

        Log.e(TAG,"callPastApi() url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"callPastApi() responseCode= " + responseCode);
                    Log.e(TAG,"callPastApi() Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                pastBooking_beens.clear();
//                                MainList_Past_Booking.clear();

                                if (json.has("history"))
                                {
                                    JSONArray history = json.getJSONArray("history");

                                    if (history!=null && history.length()>0)
                                    {
                                        for (int i=0; i<history.length(); i++)
                                        {
                                            JSONObject jsonObject = history.getJSONObject(i);
                                            List<Parcel_Been> parcel_beens = new ArrayList<Parcel_Been>();

                                            if (jsonObject!=null)
                                            {
                                                /////DistanceFare, Discount, Trash, CardId, ByDriverAmount, Notes
                                                String Id="",CompanyId="",PassengerId="",ModelId="",DriverId="",CreatedDate="",TransactionId="",PaymentStatus="",PickupTime="", DropTime=""
                                                        ,TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="", NightFareApplicable="",NightFare="",TripFare="", DistanceFare=""
                                                        ,WaitingTime="", WaitingTimeCost="",TollFee="",BookingCharge="", Tax="", PromoCode="", Discount="", SubTotal="",GrandTotal=""
                                                        ,Status="", Trash="", Reason="",PaymentType="", CardId="", ByDriverAmount="", AdminAmount="", CompanyAmount="", PickupLat="",PickupLng=""
                                                        ,DropOffLat="",DropOffLon="", BookingType="", PassengerName="",PassengerContact="", Notes="", FlightNumber="",  NoOfPassenger=""
                                                        , NoOfLuggage="", Model="", DriverName="", CarDetails="",RequestFor="taxi";

                                                String CarDetails_Id="",CarDetails_CompanyId="",CarDetails_DriverId="",CarDetails_VehicleModel="", CarDetails_Company="",CarDetails_Color=""
                                                        ,CarDetails_VehicleRegistrationNo="",CarDetails_RegistrationCertificate="", CarDetails_VehicleInsuranceCertificate=""
                                                        ,CarDetails_RegistrationCertificateExpire="",CarDetails_VehicleInsuranceCertificateExpire="", CarDetails_VehicleImage=""
                                                        ,CarDetails_Description="", HistoryType="",
                                                        LoadingUnloadingTime="", LoadingUnloadingCharge="",ShareUrl="",ParcelWeight="",
                                                        ArrivedTime="",DriverMobileNo="",SpecialExtraCharge="",PreviousDue="",CancellationFee="",ReferenceId="";

                                                if (jsonObject.has("Id"))
                                                {
                                                    Id = jsonObject.getString("Id");
                                                }

                                                if (jsonObject.has("CompanyId"))
                                                {
                                                    CompanyId = jsonObject.getString("CompanyId");
                                                }

                                                if (jsonObject.has("PassengerId"))
                                                {
                                                    PassengerId = jsonObject.getString("PassengerId");
                                                }

                                                if (jsonObject.has("ModelId"))
                                                {
                                                    ModelId = jsonObject.getString("ModelId");
                                                }

                                                if (jsonObject.has("DriverId"))
                                                {
                                                    DriverId = jsonObject.getString("DriverId");
                                                }

                                                if (jsonObject.has("CreatedDate"))
                                                {
                                                    CreatedDate = jsonObject.getString("CreatedDate");
                                                }

                                                if (jsonObject.has("TransactionId"))
                                                {
                                                    TransactionId = jsonObject.getString("TransactionId");
                                                }

                                                if (jsonObject.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = jsonObject.getString("PaymentStatus");
                                                }

                                                if (jsonObject.has("PickupTime"))
                                                {
                                                    PickupTime = jsonObject.getString("PickupTime");
                                                }

                                                if (jsonObject.has("DropTime"))
                                                {
                                                    DropTime = jsonObject.getString("DropTime");
                                                }

                                                if (jsonObject.has("TripDuration"))
                                                {
                                                    TripDuration = jsonObject.getString("TripDuration");
                                                }

                                                if (jsonObject.has("TripDistance"))
                                                {
                                                    TripDistance = jsonObject.getString("TripDistance");
                                                }

                                                if (jsonObject.has("PickupLocation"))
                                                {
                                                    PickupLocation = jsonObject.getString("PickupLocation");
                                                }

                                                if (jsonObject.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = jsonObject.getString("DropoffLocation");
                                                }

                                                if (jsonObject.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = jsonObject.getString("NightFareApplicable");
                                                }

                                                if (jsonObject.has("NightFare"))
                                                {
                                                    NightFare = jsonObject.getString("NightFare");
                                                }

                                                if (jsonObject.has("TripFare"))
                                                {
                                                    TripFare = jsonObject.getString("TripFare");
                                                }

                                                if (jsonObject.has("DistanceFare"))
                                                {
                                                    DistanceFare = jsonObject.getString("DistanceFare");
                                                }

                                                if (jsonObject.has("WaitingTime"))
                                                {
                                                    WaitingTime = jsonObject.getString("WaitingTime");
                                                }

                                                if (jsonObject.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = jsonObject.getString("WaitingTimeCost");
                                                }

                                                if (jsonObject.has("TollFee"))
                                                {
                                                    TollFee = jsonObject.getString("TollFee");
                                                }

                                                if (jsonObject.has("BookingCharge"))
                                                {
                                                    BookingCharge = jsonObject.getString("BookingCharge");
                                                }

                                                if (jsonObject.has("Tax"))
                                                {
                                                    Tax = jsonObject.getString("Tax");
                                                }

                                                if (jsonObject.has("PromoCode"))
                                                {
                                                    PromoCode = jsonObject.getString("PromoCode");
                                                }

                                                if (jsonObject.has("Discount"))
                                                {
                                                    Discount = jsonObject.getString("Discount");
                                                }

                                                if (jsonObject.has("SubTotal"))
                                                {
                                                    SubTotal = jsonObject.getString("SubTotal");
                                                }

                                                if (jsonObject.has("GrandTotal"))
                                                {
                                                    GrandTotal = jsonObject.getString("GrandTotal");
                                                }

                                                if (jsonObject.has("Status"))
                                                {
                                                    Status = jsonObject.getString("Status");
                                                }

                                                if (jsonObject.has("Trash"))
                                                {
                                                    Trash = jsonObject.getString("Trash");
                                                }

                                                if (jsonObject.has("Reason"))
                                                {
                                                    Reason = jsonObject.getString("Reason");
                                                }

                                                if (jsonObject.has("PaymentType"))
                                                {
                                                    PaymentType = jsonObject.getString("PaymentType");
                                                }

                                                if (jsonObject.has("CardId"))
                                                {
                                                    CardId = jsonObject.getString("CardId");
                                                }

                                                if (jsonObject.has("ByDriverAmount"))
                                                {
                                                    ByDriverAmount = jsonObject.getString("ByDriverAmount");
                                                }

                                                if (jsonObject.has("AdminAmount"))
                                                {
                                                    AdminAmount = jsonObject.getString("AdminAmount");
                                                }

                                                if (jsonObject.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = jsonObject.getString("CompanyAmount");
                                                }

                                                if (jsonObject.has("PickupLat"))
                                                {
                                                    PickupLat = jsonObject.getString("PickupLat");
                                                }

                                                if (jsonObject.has("PickupLng"))
                                                {
                                                    PickupLng = jsonObject.getString("PickupLng");
                                                }

                                                if (jsonObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = jsonObject.getString("DropOffLat");
                                                }

                                                if (jsonObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = jsonObject.getString("DropOffLon");
                                                }

                                                if (jsonObject.has("BookingType"))
                                                {
                                                    BookingType = jsonObject.getString("BookingType");
                                                }

                                                if (jsonObject.has("PassengerName"))
                                                {
                                                    PassengerName = jsonObject.getString("PassengerName");
                                                }

                                                if (jsonObject.has("PassengerContact"))
                                                {
                                                    PassengerContact = jsonObject.getString("PassengerContact");
                                                }

                                                if (jsonObject.has("Notes"))
                                                {
                                                    Notes = jsonObject.getString("Notes");
                                                }

                                                if (jsonObject.has("FlightNumber"))
                                                {
                                                    FlightNumber = jsonObject.getString("FlightNumber");
                                                }

                                                if (jsonObject.has("NoOfPassenger"))
                                                {
                                                    NoOfPassenger = jsonObject.getString("NoOfPassenger");
                                                }

                                                if (jsonObject.has("NoOfLuggage"))
                                                {
                                                    NoOfLuggage = jsonObject.getString("NoOfLuggage");
                                                }

                                                if (jsonObject.has("Model"))
                                                {
                                                    Model = jsonObject.getString("Model");
                                                }

                                                if (jsonObject.has("DriverName"))
                                                {
                                                    DriverName = jsonObject.getString("DriverName");
                                                }

                                                if (jsonObject.has("RequestFor"))
                                                {
                                                    RequestFor = jsonObject.getString("RequestFor");
                                                }
                                                if (jsonObject.has("HistoryType"))
                                                {
                                                    HistoryType = jsonObject.getString("HistoryType");
                                                }
                                                if (jsonObject.has("LoadingUnloadingTime"))
                                                {
                                                    LoadingUnloadingTime = jsonObject.getString("LoadingUnloadingTime");
                                                }
                                                if (jsonObject.has("LoadingUnloadingCharge"))
                                                {
                                                    LoadingUnloadingCharge = jsonObject.getString("LoadingUnloadingCharge");
                                                }
                                                if (jsonObject.has("ShareUrl"))
                                                {
                                                    ShareUrl = jsonObject.getString("ShareUrl");
                                                }
                                                if (jsonObject.has("ParcelWeight"))
                                                {
                                                    ParcelWeight = jsonObject.getString("ParcelWeight");
                                                }
                                                if (jsonObject.has("ArrivedTime"))
                                                {
                                                    ArrivedTime = jsonObject.getString("ArrivedTime");
                                                }
                                                if (jsonObject.has("DriverMobileNo"))
                                                {
                                                    DriverMobileNo = jsonObject.getString("DriverMobileNo");
                                                }
                                                if (jsonObject.has("SpecialExtraCharge"))
                                                {
                                                    SpecialExtraCharge = jsonObject.getString("SpecialExtraCharge");
                                                }
                                                if (jsonObject.has("PreviousDue"))
                                                {
                                                    PreviousDue = jsonObject.getString("PreviousDue");
                                                }
                                                if (jsonObject.has("CancellationFee"))
                                                {
                                                    CancellationFee = jsonObject.getString("CancellationFee");
                                                }
                                                if (jsonObject.has("ReferenceId"))
                                                {
                                                    ReferenceId = jsonObject.getString("ReferenceId");
                                                }

                                                if (jsonObject.has("Parcel"))
                                                {
                                                    JSONObject Parcel = jsonObject.getJSONObject("Parcel");

                                                    if (Parcel!=null)
                                                    {
                                                        String Parcel_Id = "",Parcel_Name = "",Parcel_Height = "",Parcel_Width = "";
                                                        String Parcel_Weight= "",Parcel_Image= "",Parcel_Status= "",Parcel_CreatedDate= "";

                                                        if (Parcel.has("Id"))
                                                        {
                                                            Parcel_Id = Parcel.getString("Id");
                                                        }

                                                        if (Parcel.has("Name"))
                                                        {
                                                            Parcel_Name = Parcel.getString("Name");
                                                        }

                                                        if (Parcel.has("Height"))
                                                        {
                                                            Parcel_Height = Parcel.getString("Height");
                                                        }

                                                        if (Parcel.has("Width"))
                                                        {
                                                            Parcel_Width = Parcel.getString("Width");
                                                        }

                                                        if (Parcel.has("Weight"))
                                                        {
                                                            Parcel_Weight = Parcel.getString("Weight");
                                                        }

                                                        if (Parcel.has("Image"))
                                                        {
                                                            Parcel_Image = Parcel.getString("Image");
                                                        }

                                                        if (Parcel.has("Status"))
                                                        {
                                                            Parcel_Status = Parcel.getString("Status");
                                                        }

                                                        if (Parcel.has("CreatedDate"))
                                                        {
                                                            Parcel_CreatedDate = Parcel.getString("CreatedDate");
                                                        }

                                                        parcel_beens.add(new Parcel_Been(Parcel_Id,Parcel_Name,Parcel_Height,Parcel_Width,Parcel_Weight,Parcel_Image,Parcel_Status,Parcel_CreatedDate));
                                                    }
                                                }

                                                if (jsonObject.has("CarDetails"))
                                                {
                                                    CarDetails = jsonObject.getString("CarDetails");

                                                    JSONObject CarDetails1 = jsonObject.getJSONObject("CarDetails");

                                                    if (CarDetails1!=null)
                                                    {
                                                        if (CarDetails1.has("Id"))
                                                        {
                                                            CarDetails_Id = CarDetails1.getString("Id");
                                                        }

                                                        if (CarDetails1.has("CompanyId"))
                                                        {
                                                            CarDetails_CompanyId = CarDetails1.getString("CompanyId");
                                                        }

                                                        if (CarDetails1.has("VehicleModel"))
                                                        {
                                                            CarDetails_VehicleModel = CarDetails1.getString("VehicleModel");
                                                        }

                                                        if (CarDetails1.has("Company"))
                                                        {
                                                            CarDetails_Company = CarDetails1.getString("Company");
                                                        }

                                                        if (CarDetails1.has("Color"))
                                                        {
                                                            CarDetails_Color = CarDetails1.getString("Color");
                                                        }

                                                        if (CarDetails1.has("VehicleRegistrationNo"))
                                                        {
                                                            CarDetails_VehicleRegistrationNo = CarDetails1.getString("VehicleRegistrationNo");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificate"))
                                                        {
                                                            CarDetails_RegistrationCertificate = CarDetails1.getString("RegistrationCertificate");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificate"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificate = CarDetails1.getString("VehicleInsuranceCertificate");
                                                        }

                                                        if (CarDetails1.has("RegistrationCertificateExpire"))
                                                        {
                                                            CarDetails_RegistrationCertificateExpire = CarDetails1.getString("RegistrationCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            CarDetails_VehicleInsuranceCertificateExpire = CarDetails1.getString("VehicleInsuranceCertificateExpire");
                                                        }

                                                        if (CarDetails1.has("VehicleImage"))
                                                        {
                                                            CarDetails_VehicleImage = WebServiceAPI.BASE_URL_IMAGE + CarDetails1.getString("VehicleImage");
                                                        }

                                                        if (CarDetails1.has("Description"))
                                                        {
                                                            CarDetails_Description = CarDetails1.getString("Description");
                                                        }
                                                    }
                                                }

//                                                MainList_Past_Booking.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
//                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
//                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
//                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
//                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
//                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
//                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
//                                                        CarDetails_Description, HistoryType, BookingType,RequestFor));

                                                pastBooking_beens.add(new PastBooking_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,
                                                        PickupTime, DropTime, TripDuration, TripDistance, PickupLocation, DropoffLocation, NightFareApplicable, NightFare,
                                                        TripFare, WaitingTime, WaitingTimeCost, TollFee, BookingCharge, Tax, PromoCode, Discount, SubTotal, GrandTotal,
                                                        Status, Reason, PaymentType, AdminAmount, CompanyAmount, PickupLat, PickupLng, DropOffLat, DropOffLon, Model,
                                                        DriverName, CarDetails_Id, CarDetails_CompanyId, CarDetails_DriverId, CarDetails_VehicleModel, CarDetails_Company,
                                                        CarDetails_Color, CarDetails_VehicleRegistrationNo, CarDetails_RegistrationCertificate, CarDetails_VehicleInsuranceCertificate,
                                                        CarDetails_RegistrationCertificateExpire, CarDetails_VehicleInsuranceCertificateExpire, CarDetails_VehicleImage,
                                                        CarDetails_Description, HistoryType, BookingType,RequestFor,parcel_beens,
                                                        LoadingUnloadingTime, LoadingUnloadingCharge,ShareUrl,ParcelWeight,
                                                        ArrivedTime,DriverMobileNo,DistanceFare,SpecialExtraCharge,PreviousDue,CancellationFee,ReferenceId));
                                            }
                                            else
                                            {
                                                Log.e(TAG,"callPastApi() jsonObject null position = "+i);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"callPastApi() history null or lenth 0");
                                    }

                                    Log.e(TAG,"callPastApi() pastBooking_beens.size() = "+pastBooking_beens.size());

                                    dialogClass.hideDialog();

//                                    call_UpComming();
                                }
                                else
                                {
                                    Log.e(TAG,"callPastApi() history not found");
                                    dialogClass.hideDialog();

//                                    call_UpComming();
                                }
                            }
                            else
                            {
                                Log.e(TAG,"callPastApi() status false");
                                dialogClass.hideDialog();

//                                call_UpComming();
                            }
                        }
                        else
                        {
                            Log.e(TAG,"callPastApi() status not found");
                            dialogClass.hideDialog();

//                            call_UpComming();
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callPastApi() json null");
                        dialogClass.hideDialog();

//                        call_UpComming();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"callPastApi() Exception "+e.toString());
                    dialogClass.hideDialog();

//                    call_UpComming();
                }
                finally
                {
                    Log.e(TAG,"callPastApi() finally");
                    //call_PastBooking();
                    if(notificationType != null && !notificationType.equalsIgnoreCase("")) {
                        if(notificationType.equalsIgnoreCase("AcceptBooking")) {
                            call_UpComming();
                        } else if(notificationType.equalsIgnoreCase("OnTheWay")) {
                            call_OnGoing();
                        }
                        else if(notificationType.equalsIgnoreCase("AdvanceBooking"))
                        {call_UpComming();}
                    } else {
                        call_PastBooking();
                    }
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void call_service_type()
    {
        upComming_beens.clear();
        onGoing_beens.clear();
//        pastBooking_beens.clear();

        Log.e(TAG,"call_service_type()");
        Log.e(TAG,"call_service_type() selectService:- " + selectService);
        Log.e(TAG,"call_service_type() MainList_Upcoming_Booking.size():- " + MainList_Upcoming_Booking.size());
        Log.e(TAG,"call_service_type() MainList_OnGoing_Booking.size():- " + MainList_OnGoing_Booking.size());
//        Log.e(TAG,"call_service_type() MainList_Past_Booking.size():- " + MainList_Past_Booking.size());

        for (int i=0; i<MainList_Upcoming_Booking.size(); i++)
        {
            if (selectService.equalsIgnoreCase("taxi"))
            {
                if (MainList_Upcoming_Booking.get(i).getRequestFor().equalsIgnoreCase("taxi"))
                {
                    upComming_beens.add(MainList_Upcoming_Booking.get(i));
                }
            }
            else
            {
                if (MainList_Upcoming_Booking.get(i).getRequestFor().equalsIgnoreCase("delivery"))
                {
                    upComming_beens.add(MainList_Upcoming_Booking.get(i));
                }
            }

        }

        for (int i=0; i<MainList_OnGoing_Booking.size(); i++)
        {
            if (selectService.equalsIgnoreCase("taxi"))
            {
                if (MainList_OnGoing_Booking.get(i).getRequestFor().equalsIgnoreCase("taxi"))
                {
                    onGoing_beens.add(MainList_OnGoing_Booking.get(i));
                }
            }
            else
            {
                if (MainList_OnGoing_Booking.get(i).getRequestFor().equalsIgnoreCase("delivery"))
                {
                    onGoing_beens.add(MainList_OnGoing_Booking.get(i));
                }
            }

        }

//        for (int i=0; i<MainList_Past_Booking.size(); i++)
//        {
//            if (selectService.equalsIgnoreCase("taxi"))
//            {
//                if (MainList_Past_Booking.get(i).getRequestFor().equalsIgnoreCase("taxi"))
//                {
//                    pastBooking_beens.add(MainList_Past_Booking.get(i));
//                }
//            }
//            else
//            {
//                if (MainList_Past_Booking.get(i).getRequestFor().equalsIgnoreCase("delivery"))
//                {
//                    pastBooking_beens.add(MainList_Past_Booking.get(i));
//                }
//            }
//
//        }

        Log.e(TAG,"call_service_type() onGoing_beens.size():- " + onGoing_beens.size());
        Log.e(TAG,"call_service_type() upComming_beens.size():- " + upComming_beens.size());
//        Log.e(TAG,"call_service_type() pastBooking_beens.size():- " + pastBooking_beens.size());

        if (upComming_fragment != null)
        {
            upComming_fragment.mAdapter.notifyDataSetChanged();
            if (upComming_beens.size() < 1)
            {
                upComming_fragment.recyclerView.setVisibility(View.GONE);
                upComming_fragment.tv_NoDataFound.setVisibility(View.VISIBLE);
            }
            else
            {
                upComming_fragment.recyclerView.setVisibility(View.VISIBLE);
                upComming_fragment.tv_NoDataFound.setVisibility(View.GONE);
            }
        }
        if (onGoing_fragment != null)
        {
            onGoing_fragment.mAdapter.notifyDataSetChanged();
            if (onGoing_beens.size() < 1)
            {
                onGoing_fragment.recyclerView.setVisibility(View.GONE);
                onGoing_fragment.tv_NoDataFound.setVisibility(View.VISIBLE);
            }
            else
            {
                onGoing_fragment.recyclerView.setVisibility(View.VISIBLE);
                onGoing_fragment.tv_NoDataFound.setVisibility(View.GONE);
            }
        }
//        if (pastBooking_fragment != null)
//        {
//            pastBooking_fragment.mAdapter.notifyDataSetChanged();
//            if (pastBooking_beens.size() < 1)
//            {
//                pastBooking_fragment.recyclerView.setVisibility(View.GONE);
//                pastBooking_fragment.tv_NoDataFound.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                pastBooking_fragment.recyclerView.setVisibility(View.VISIBLE);
//                pastBooking_fragment.tv_NoDataFound.setVisibility(View.GONE);
//            }
//        }

        priviousListSize = 0;
        pastPageIndex = 1;
        PastBooking_Fragment.callPastApi();
    }

    @Override
    protected void onResume()
    {
        Log.e(TAG,"onResume()");

        super.onResume();
        TruckzShipperApplication.setCurrentActivity(activity);

    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");

        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

}
