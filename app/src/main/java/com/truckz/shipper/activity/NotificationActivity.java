package com.truckz.shipper.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.truckz.shipper.R;
import com.truckz.shipper.adapter.NotificationAdapter;
import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.been.NotificationBeen;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.Constants;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CTextViewLight;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static io.fabric.sdk.android.services.concurrency.AsyncTask.init;

public class NotificationActivity extends AppCompatActivity {

    public NotificationActivity activity;
    private RecyclerView recyclerView;
    private Button btn_clear;
    private CTextViewBold title_textview;
    private AQuery aQuery;
    private DialogClass dialogClass;

    private String TAG = "NotificationActivity";

    private List<NotificationBeen> list;

    private ImageView iv_call;
    private LinearLayout back_layout;
    private CTextViewLight txt_no_data_found;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        activity = NotificationActivity.this;

        init();
    }

    private void init() {

        recyclerView = findViewById(R.id.recyclerView);
        btn_clear = findViewById(R.id.btn_clear);
        title_textview  = findViewById(R.id.title_textview);
        txt_no_data_found = findViewById(R.id.txt_no_data_found);

        iv_call = findViewById(R.id.iv_call);
        back_layout = findViewById(R.id.back_layout);
        iv_call.setVisibility(View.INVISIBLE);

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dialogClass = new DialogClass(NotificationActivity.this,0);
        aQuery = new AQuery(NotificationActivity.this);

        title_textview.setText(getString(R.string.activity_notification));

        list = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        callNotification(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, NotificationActivity.this));

        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callClearNotification(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID, NotificationActivity.this));
            }
        });

    }

    private void callNotification(String passangerId) {

        dialogClass.showDialog();

        String url = WebServiceAPI.API_NOTIFICATION_LIST + passangerId;

        Log.e("call", "url = " + url);
        Log.e("call", "param = passangerId" + passangerId);

        aQuery.ajax(url.trim(), JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                //super.callback(url, object, status);

                try {

                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();

                    if(json.has("status")) {

                        if(json.has("notifications")) {

                            JSONArray jsonArray = json.getJSONArray("notifications");

                            GsonBuilder builder =  new GsonBuilder();
                            Gson gson = builder.create();

                            for(int i = 0 ; i < jsonArray.length(); i++)
                            {
                                NotificationBeen notificationBeen = gson.fromJson(jsonArray.get(i).toString(), NotificationBeen.class);
                                list.add(notificationBeen);
                            }

                            NotificationAdapter adapter = new NotificationAdapter(NotificationActivity.this, list);

                            recyclerView.setAdapter(adapter);

                            if(list.size() == 0)
                            {
                                txt_no_data_found.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                txt_no_data_found.setVisibility(View.GONE);
                            }

                        }
                        else
                        {
                            dialogClass.hideDialog();
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(NotificationActivity.this);
                            errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                            txt_no_data_found.setVisibility(View.VISIBLE);
                        }

                    }
                    else
                    {
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(NotificationActivity.this);
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception : "+e.getMessage());
                }

            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callClearNotification(String passangerId)
    {
        dialogClass.showDialog();

        String url = WebServiceAPI.API_REMOVE_NOTIFICATIONS + passangerId;

        Log.e("call", "url = " + url);
        Log.e("call", "param = passangerId" + passangerId);

        aQuery.ajax(url.trim(), JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                try {

                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);
                    dialogClass.hideDialog();

                    if(json.has("status")) {

                        dialogClass.hideDialog();
                        if(json.getBoolean("status")) {

                            onBackPressed();

                        } else {
                            dialogClass.hideDialog();
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(NotificationActivity.this);
                            errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                        }

                    } else {
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(NotificationActivity.this);
                        errorDialogClass.showDialog(getResources().getString(R.string.something_went_wrong),getResources().getString(R.string.error_message));
                    }


                } catch (Exception e) {
                    Log.e(TAG, "Exception : "+e.getMessage());
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        TruckzShipperApplication.setCurrentActivity(activity);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
