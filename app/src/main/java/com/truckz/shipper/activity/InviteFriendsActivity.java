package com.truckz.shipper.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.truckz.shipper.application.TruckzShipperApplication;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.roundedimage.RoundedTransformationBuilder;
import com.truckz.shipper.sms.SimInfo;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.MySnackBar;
import com.truckz.shipper.R;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class InviteFriendsActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "InviteFriendsActivity";
    public static InviteFriendsActivity activity;
    private LinearLayout ll_Back, main_layout;
    private ImageView iv_Back, iv_call;
    private CTextViewBold tv_Title, tv_more_option;

    private ImageView iv_prof_inviteDrive;

    private RelativeLayout ll_faceBook;
    private Transformation mTransformation;

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    private MySnackBar snackBar;
    private int PICK_CONTACT = 105;

    //for permission
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    private String referalCode = "";
    private String referalTotal = "0";
    private TextView tv_ReferalCode, tv_ReferalTotal;

    private String invitationMessag = "";
    private String name = "";
    private String finalMessage = "";
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        activity = InviteFriendsActivity.this;
        invitationMessag = " "+getResources().getString(R.string.has_invite_you_to_become_a)+" ";
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        snackBar = new MySnackBar(activity);

        name = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_FULL_NAME, activity);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(getResources().getColor(R.color.colorThemeRed))
                .borderWidthDp(2)
                .oval(true)
                .build();

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE, activity).equalsIgnoreCase("")) {
            referalCode = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_CODE, activity);
        }

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL, activity) != null && !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL, activity).equalsIgnoreCase("")) {
            referalTotal = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_REFERAL_TOTAL, activity);
        }

//        finalMessage = name + invitationMessag + getResources().getString(R.string.click_here) +
//                inviteCodeMessage + iniviteCode + "\n" + getResources().getString(R.string.app_link_two);

        finalMessage = name + invitationMessag  + getString(R.string.app_name) + "."
                + getResources().getString(R.string.click_here);

        Log.e("call", "finalMessage = " + finalMessage);

        init();
    }

    public void takePermission() {
        if (ActivityCompat.checkSelfPermission(InviteFriendsActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(InviteFriendsActivity.this, Manifest.permission.READ_CONTACTS)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(InviteFriendsActivity.this);
                builder.setTitle(getResources().getString(R.string.storage_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_storage_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_CONTACTS, true);
            editor.commit();
        } else if (ActivityCompat.checkSelfPermission(InviteFriendsActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(InviteFriendsActivity.this, Manifest.permission.SEND_SMS)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(InviteFriendsActivity.this);
                builder.setTitle(getResources().getString(R.string.send_sms));
                builder.setMessage(getResources().getString(R.string.this_app_need_to_send_sms_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.SEND_SMS}, 100);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.SEND_SMS}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.SEND_SMS, true);
            editor.commit();
        } else if (ActivityCompat.checkSelfPermission(InviteFriendsActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(InviteFriendsActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                Log.e("call", " permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(InviteFriendsActivity.this);
                builder.setTitle(getResources().getString(R.string.read_phone_state_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_to_read_phone_state_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 33333333");
                        ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call", " permision 44444444444");
                    }
                });
                builder.show();
            } else {
                Log.e("call", " permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(InviteFriendsActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
            }
            Log.e("call", " permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_PHONE_STATE, true);
            editor.commit();
        } else {
            pickAContactNumber();
        }
    }

    private void init() {

        ll_Back = findViewById(R.id.back_layout);
        iv_Back = findViewById(R.id.back_imageview);
        iv_call = findViewById(R.id.iv_call);


        tv_Title = findViewById(R.id.title_textview);
        tv_more_option = findViewById(R.id.tv_more_option);

        tv_Title.setText("Invite Friends"/*activity.getResources().getString(R.string.invite_friends)*/);

        tv_ReferalCode = findViewById(R.id.tv_ReferalCode);
        tv_ReferalTotal = findViewById(R.id.tv_ReferalTotal);

        tv_ReferalTotal.setText(getResources().getString(R.string.currency) + Double.parseDouble(referalTotal));
        tv_ReferalCode.setText(referalCode);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        ll_faceBook = (RelativeLayout) findViewById(R.id.ll_faceBook);

        iv_prof_inviteDrive = (ImageView) findViewById(R.id.iv_prof_inviteDrive);

        if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, activity) != null &&
                !SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, activity).equalsIgnoreCase(""))
        {
//            Picasso.with(activity)
//                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, activity))
//                    .fit()
//                    .transform(mTransformation)
//                    .into(iv_prof_inviteDrive);

            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_IMAGE, activity))
                    .into(iv_prof_inviteDrive, new Callback()
                    {
                        @Override
                        public void onSuccess()
                        {
//                            progBar_Image.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError()
                        {
                            iv_prof_inviteDrive.setImageResource(R.mipmap.man);
//                            progBar_Image.setVisibility(View.GONE);
                        }
                    });

        }
        else
        {
            iv_prof_inviteDrive.setImageResource(R.mipmap.man);
        }

        ll_faceBook.setOnClickListener(activity);
        tv_more_option.setOnClickListener(activity);
        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);
        iv_call.setOnClickListener(activity);
    }

    //Todo when button is clicked
    public void pickAContactNumber() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor phone = getContentResolver().query(contactData, null, null, null, null);
                if (phone.moveToFirst()) {
                    String name = phone.getString(phone.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String number = "";
                    ContentResolver cr = getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, "DISPLAY_NAME = '" + name + "'", null, null);
                    if (cursor.moveToFirst()) {
                        String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        //
                        //  Get all phone numbers.
                        //
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        first:
                        while (phones.moveToNext()) {
                            number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            break first;
                        }
                        phones.close();
                    }
                    cursor.close();

                    Log.e("call", "contactNumberName=" + number);

                    number = number.replace(" ", "");
                    // Todo something when contact number selected
                    Log.e("call", "contactNumberName=" + name);
                    Log.e("call", "contactNumberName=" + number);
                    OpenSMS(number, finalMessage);
                } else {
                    Log.e("call", "phone.moveToFirst() false");
                }
            } else {
                Log.e("call", "resultCode not ok");
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.back_imageview:
                onBackPressed();
                break;

            case R.id.iv_call:
                MainActivity.call_sos(activity);
                break;

            case R.id.ll_faceBook:
                if (Global.isNetworkconn(activity))
                {
                    Open_FaceBook();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.please_check_internet_connection), activity.getResources().getString(R.string.no_internet_connection));
                }
                break;

            case R.id.tv_more_option:
                if (Global.isNetworkconn(activity))
                {
                    open_more_option();
                }
                else
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog(activity.getResources().getString(R.string.please_check_internet_connection), activity.getResources().getString(R.string.no_internet_connection));
                }
                break;

//            case R.id.ll_twitter:
//                if (Global.isNetworkconn(activity)) {
//                    Open_Twitter();
//                } else {
//                    InternetDialog internetDialog = new InternetDialog(activity);
//                    internetDialog.showDialog("Please check your internet connection!","No internet connection");
//                }
//                break;
//
//            case R.id.ll_email:
//                if (Global.isNetworkconn(activity)) {
//                    Open_Email();
//                } else {
//                    InternetDialog internetDialog = new InternetDialog(activity);
//                    internetDialog.showDialog("Please check your internet connection!","No internet connection");
//                }
//                break;
//
//            case R.id.ll_whatsApp:
//                if (Global.isNetworkconn(activity)) {
//                    Open_Whatsapp();
//                } else {
//                    InternetDialog internetDialog = new InternetDialog(activity);
//                    internetDialog.showDialog("Please check your internet connection!","No internet connection");
//                }
//                break;
//
//            case R.id.ll_SMS:
////                InternetDialog internetDialog = new InternetDialog(activity);
////                internetDialog.showDialog("This feature is upcomming!");
//                takePermission();
//                break;
        }
    }

    private void open_more_option()
    {
        Log.e(TAG,"open_more_option()");

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, finalMessage.toString());
        startActivity(Intent.createChooser(shareIntent, "Share Via"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private void Open_FaceBook() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.facebook.katana");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, finalMessage);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            snackBar.showSnackBar(main_layout, getResources().getString(R.string.facebook_is_not_install));
        }
    }

    private void Open_Twitter() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.twitter.android");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, finalMessage);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            snackBar.showSnackBar(main_layout, getResources().getString(R.string.twitter_is_not_install));
        }
    }

    private void Open_Email() {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, activity));
            intent.putExtra(Intent.EXTRA_SUBJECT, "PickNGo App Passenger");
            intent.putExtra(Intent.EXTRA_TEXT, finalMessage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        } catch (android.content.ActivityNotFoundException e) {
            Log.e("kkkkkkkk", "kkkkkkkkkkkk " + e.toString());
            snackBar.showSnackBar(main_layout, getResources().getString(R.string.email_send_error));
        }
    }

    private void Open_Whatsapp() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, finalMessage);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            snackBar.showSnackBar(main_layout, getResources().getString(R.string.whats_app_is_not_install));
        }
    }

    public static List<SimInfo> getSIMInfo(Context context) {
        List<SimInfo> simInfoList = new ArrayList<>();
        Uri URI_TELEPHONY = Uri.parse("content://telephony/siminfo/");
        Cursor c = context.getContentResolver().query(URI_TELEPHONY, null, null, null, null);

        if (c != null && c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    int id = c.getInt(c.getColumnIndex("_id"));
                    int slot = c.getInt(c.getColumnIndex("slot"));
                    String display_name = c.getString(c.getColumnIndex("display_name"));
                    String icc_id = c.getString(c.getColumnIndex("icc_id"));
                    SimInfo simInfo = new SimInfo(id, display_name, icc_id, slot);
                    Log.d("apipas_sim_info", simInfo.toString());
                    simInfoList.add(simInfo);
                } while (c.moveToNext());
            }
        }
        c.close();

        return simInfoList;
    }

    private void OpenSMS(String phoneNo, String messageszz) {

        final String phone = phoneNo.trim().replace(" ", "");
        /*String message = name+" has invited you to become a PickNGo Passenger.\n" +
                "click here goo.gl/uqxHTs"+
                "\nYour invite code is: " + iniviteCode + "\n"+
                "https://www.facebook.com/PickNGoSrilanka/";*/

        Log.e("call", "phone number = " + phone);
        Log.e("call", "message = " + messageszz);
        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_a_valid_phone_number), Toast.LENGTH_SHORT).show();
        } else {

            final SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(messageszz);
            for (final String msg : messages) {

                final PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                final PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);

                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);
                    }
                }, 1000);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        TruckzShipperApplication.setCurrentActivity(activity);

        sentStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = getResources().getString(R.string.unknown_error);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = getResources().getString(R.string.message_sent_succefully);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s = getResources().getString(R.string.generic_failer_error);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        s = getResources().getString(R.string.error_no_service_availble);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        s = getResources().getString(R.string.error_null_pdu);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        s = getResources().getString(R.string.error_radio_is_off);
                        break;
                    default:
                        break;
                }
                snackBar.showSnackBar(main_layout, s + "");

            }
        };
        deliveredStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = getResources().getString(R.string.message_not_delivered);
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = getResources().getString(R.string.message_delivered_successfully);
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                snackBar.showSnackBar(main_layout, s + "");
            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);
    }
}
