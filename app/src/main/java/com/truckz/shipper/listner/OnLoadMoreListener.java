package com.truckz.shipper.listner;

public interface OnLoadMoreListener {
    void onLoadMore();
}