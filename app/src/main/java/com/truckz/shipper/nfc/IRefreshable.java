package com.truckz.shipper.nfc;

/**
 * Interface for updatebale content
 *
 * @author Millau Julien
 *
 */
public interface IRefreshable {

	/**
	 * Method used to update content
	 */
	void update();

}
