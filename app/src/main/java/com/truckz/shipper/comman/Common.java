package com.truckz.shipper.comman;

import com.github.nkzawa.socketio.client.Socket;

public class Common{

    //FOR SOCKET
    public static Socket socket;

    String Fullname="",Email="",Password="",MobileNo="",Gender="",Image="",DeviceType="",Token="";
    String Lat="",Lng="",Status="",CreatedDate="",Id="";

    //FOR USER PREFERENCE
    public static String PREFRENCE_USER = "USER_DETAIL";
    public static String USER_PREFERENCE_KEY_FULL_NAME = "fullName";
    public static String USER_PREFERENCE_KEY_EMAIL = "email";
    public static String USER_PREFERENCE_KEY_PASSWORD = "password";
    public static String USER_PREFERENCE_KEY_PHONE_NUMBER = "phoneNumber";
    public static String USER_PREFERENCE_KEY_GENDER = "gender";
    public static String USER_PREFERENCE_KEY_DATE_OF_BIRTH = "dateOfBirth";
    public static String USER_PREFERENCE_KEY_IMAGE = "image";
    public static String USER_PREFERENCE_KEY_DEVICE_TYPE = "deviceType";
    public static String USER_PREFERENCE_KEY_TOKEN = "token";
    public static String USER_PREFERENCE_KEY_LATITUDE = "latitude";
    public static String USER_PREFERENCE_KEY_LONGITUDE = "longitude";
    public static String USER_PREFERENCE_KEY_STATUS = "status";
    public static String USER_PREFERENCE_KEY_CREATED_DATE = "createdDate";
    public static String USER_PREFERENCE_KEY_ID = "id";
    public static String USER_PREFERENCE_KEY_WALLET_BALLENCE = "walletBalance";
    public static String USER_PREFERENCE_KEY_ADDRESS = "address";
    public static String USER_PREFERENCE_KEY_CITY = "city";
    public static String USER_PREFERENCE_KEY_CAR_CLASS = "carClass";
    public static String USER_PREFERENCE_KEY_CARD_LIST = "cardList";
    public static String USER_PREFERENCE_KEY_QR_CODE = "QRCode";
    public static String USER_PREFERENCE_KEY_VERIFY_USER = "VerifyUser";
    public static String USER_PREFERENCE_KEY_TICK_PAY_SPLASH = "TickpaySplash";
    public static String USER_PREFERENCE_KEY_COMPANY_NAME = "CompanyName";
    public static String USER_PREFERENCE_KEY_ABN = "ABN";
    public static String USER_PREFERENCE_KEY_BANK_NAME = "BankName";
    public static String USER_PREFERENCE_KEY_BANK_ACCOUNT_NUMBER = "BankAccountNumber";
    public static String USER_PREFERENCE_KEY_BSB = "BSB";
    public static String USER_PREFERENCE_KEY_DESCRIPTION = "Description";
    public static String USER_PREFERENCE_KEY_LICENCE_IMAGE = "LicenceImage";
    public static String USER_PREFERENCE_KEY_REFERAL_CODE = "referalCode";
    public static String USER_PREFERENCE_KEY_REFERAL_TOTAL = "referalTotal";
    public static String USER_PREFERENCE_KEY_RATTING = "ratting";
    public static String USER_PREFERENCE_KEY_OTP = "OTP";
    public static String PREFRENCE_NOTIFICATION = "notificationType";
    public static String USER_ACCOUNT_BLOCK = "userBlock";


    public static String USER_PREFERENCE_KEY_BOOKING_DETAIL = "bookingDetail";
    public static String USER_PREFERENCE_KEY_TRIP_FLAG = "tripFlag";
    public static String USER_PREFERENCE_KEY_DRIVER_ID = "driverId";
    public static String USER_PREFERENCE_KEY_DRIVER_NAME = "driverName";
    public static String CREATED_PASSCODE = "CreatePasscode";
    public static String IS_PASSCODE_REQUIRED = "isPasscodeRequired";

    public static String USER_PREFERENCE_KEY_SELECTED_CAR = "selectedCar";

    public static String USER_PREFERENCE_KEY_FAVORITE_TYPE = "favoriteType";

    public static String USER_PREFERENCE_KEY_FAVORITE_PICKUP_LOCATION = "pickupLocation";
    public static String USER_PREFERENCE_KEY_FAVORITE_PICKUP_LAT = "pickupLat";
    public static String USER_PREFERENCE_KEY_FAVORITE_PICKUP_LONG = "pickupLong";

    public static String USER_PREFERENCE_KEY_FAVORITE_DROPOFF_LOCATION = "dropoffLocation";
    public static String USER_PREFERENCE_KEY_FAVORITE_DROPOFF_LAT = "dropoffLat";
    public static String USER_PREFERENCE_KEY_FAVORITE_DROPOFF_LONG = "dropoffLong";

    public static String USER_PREFERENCE_KEY_PRE_LAT = "preLat";
    public static String USER_PREFERENCE_KEY_PRE_LONG = "preLong";
    public static String USER_PREFERENCE_KEY_NEW_LAT = "newLat";
    public static String USER_PREFERENCE_KEY_NEW_LONG = "newLong";

    public static String USER_DEVICE_ID = "1234567890";

    public static String INTENT_CAPACITY  = "intent_capacity";

}
