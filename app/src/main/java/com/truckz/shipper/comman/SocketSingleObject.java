package com.truckz.shipper.comman;

import android.content.Context;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import java.net.URISyntaxException;


public class SocketSingleObject
{

    public static SocketSingleObject instance;
//    TRUCKZ Rider Link
    private static final String SERVER_ADDRESS = "https://truckzapp.in:8080/";

//   Cab Ride Link
//    private static final String SERVER_ADDRESS = "http://52.66.86.25:8080/";

    private Socket mSocket;
    private Context context;

    public SocketSingleObject(Context context) {
        this.context = context;
        this.mSocket = getServerSocket();
    }

    public static SocketSingleObject get(Context context){
        if(instance == null){
            instance = getSync(context);
        }
        instance.context = context;
        return instance;
    }

    private static synchronized SocketSingleObject getSync(Context context) {
        if(instance == null){
            instance = new SocketSingleObject(context);
        }
        return instance;
    }

    public Socket getSocket(){
        return this.mSocket;
    }

    public Socket getServerSocket() {
        try
        {
//                IO.Options opts = new IO.Options();
//                opts.forceNew = true;
//                opts.reconnection = true;
            mSocket = IO.socket(SERVER_ADDRESS);//, opts
            return mSocket;
        }
        catch (URISyntaxException e)
        {
            throw new RuntimeException(e);
        }
    }
}
