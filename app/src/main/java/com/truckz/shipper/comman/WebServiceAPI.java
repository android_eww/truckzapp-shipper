package com.truckz.shipper.comman;

public interface WebServiceAPI {

    //TRUCKZ RIDE Base Url
    String BASE_URL = "https://truckzapp.in/web/Passenger_Api/";
    //String BASE_URL = "https://truckzapp.in/web/Passenger_Api2/";
    String BASE_URL_IMAGE  = "https://truckzapp.in/web/";
    public static String HEADER_KEY = "key";
    public static String HEADER_VALUE = "TruckZapp123*";

//    CAB Ride
//    String BASE_URL = "http://52.66.86.25/web/Passenger_Api/";
//    String BASE_URL_IMAGE  = "http://52.66.86.25/web/";
//    public static String HEADER_KEY = "key";
//    public static String HEADER_VALUE = "TicktocApp123*";

    //ALL API.............
    public static String API_CHECK_VERSION = BASE_URL + "Init/";

    public static String API_REGISTER = BASE_URL + "Register";
    public static String API_REGISTER_OTP = BASE_URL + "OtpForRegister";
    public static String API_GET_CLASS_TYPE = BASE_URL + "GetCarClass";
    public static String API_LOGIN = BASE_URL + "Login";
    public static String API_SOCIAL_LOGIN = BASE_URL + "SocialLogin";
    public static String API_FORGOT_PASSWORD = BASE_URL + "ForgotPassword";

    public static String API_BOOKING_REQUEST = BASE_URL + "SubmitBookingRequest";
    public static String API_ADVANCE_BOOKING = BASE_URL + "AdvancedBooking";
    public static String API_BOOKING_HISTORY = BASE_URL + "BookingHistory/";
    public static String API_UPCOMING_BOOKING_HISTORY = BASE_URL + "UpcomingBooking/";
    public static String API_ONGOING_BOOKING_HISTORY = BASE_URL + "OngoingBooking/";
    public static String API_PAST_BOOKING_HISTORY = BASE_URL + "PastBooking/";
    public static String API_SHOW_ALL_DRIVER = BASE_URL + "Driver/";
    public static String API_CHANGE_PASSWORD = BASE_URL + "ChangePassword";
    public static String API_UPDATE_PROFILE = BASE_URL + "UpdateProfile";
    public static String API_GET_ESTIMATED_FARE = BASE_URL + "GetEstimateFare";
    public static String API_CURRENT_BOOCING = BASE_URL + "CurrentBooking/";
    public static String API_ADD_CARD = BASE_URL + "AddNewCard";
    public static String API_ADD_MONEY = BASE_URL + "AddMoney";
    public static String API_GET_ALL_CARDS = BASE_URL + "Cards/";
    public static String API_GET_TRANSACTION_HISTORY = BASE_URL + "TransactionHistory/";
    public static String API_SEND_MONEY = BASE_URL + "SendMoney";
    public static String API_QR_CODE_DETAIL = BASE_URL + "QRCodeDetails";
    public static String API_REMOVE_CARD = BASE_URL + "RemoveCard/";
    public static String API_ADD_ADDRESS = BASE_URL + "AddAddress";
    public static String API_GET_ADDRESS = BASE_URL + "GetAddress/";
    public static String API_REMOVE_ADDRESS = BASE_URL + "RemoveAddress/";
    public static String API_VERIFY_USER = BASE_URL + "VarifyUser";
    public static String API_GIVE_RATTING = BASE_URL + "ReviewRating";
    public static String API_TRANSFER_TO_BANK = BASE_URL + "TransferToBank";
    public static String API_UPDATE_BANK_ACCOUNT_DETAIL = BASE_URL + "UpdateBankAccountDetails";
    public static String API_PACKAGE = BASE_URL + "Packages";
    public static String API_BOOK_PACKAGE = BASE_URL + "BookPackage";
    public static String API_BOOK_PACKAGE_HISTORY = BASE_URL + "PackageBookingHistory/";
    public static String API_BOOK_MISS_RIDE = BASE_URL + "BookingMissRequest";
    public static String API_TRACK_RUNNING_TRIP = BASE_URL + "TrackRunningTrip/";
    public static String API_GET_TRANSPORT_SERVICE = BASE_URL + "ParcelAndLabour";
    public static String API_GET_ESTIMATED_FARE_FOR_DELIVERY = BASE_URL + "GetEstimateFareForDeliveryService";
    public static String API_CHECK_PROMOCODE = BASE_URL + "PromoCodeCheck";
    public static String API_NOTIFICATION_LIST = BASE_URL + "NotificationList/";
    public static String API_REMOVE_NOTIFICATIONS = BASE_URL + "RemoveNotifications/";
    public static String API_GET_RECIPE = BASE_URL + "EmailReceipt";
    public static String API_PAY_PREVIOUSDUE = BASE_URL + "PreviousDuePayment";


    //ALL PARAMETER............
    public static String PARAM_EMAIL = "Email";
    public static String PARAM_MOBILE_NUMBER = "MobileNo";
    public static String PARAM_PASSWORD = "Password";
    public static String PARAM_OLD_PASSWORD = "OldPassword";
    public static String PARAM_GENDER = "Gender";
    public static String PARAM_FIRST_NAME = "Firstname";
    public static String PARAM_LAST_NAME = "Lastname";
    public static String PARAM_DEVICE_TYPE = "DeviceType";
    public static String PARAM_TOKEN = "Token";
    public static String PARAM_LAT = "Lat";
    public static String PARAM_LONG = "Lng";
    public static String PARAM_IMAGE = "Image";
    public static String PARAM_PASSPORT = "Passport";
    public static String PARAM_REFERRAL_CODE = "ReferralCode";
    public static String PARAM_DATE_OF_BIRTH = "DOB";
    public static String PARAM_USER_NAME = "Username";
    public static String PARAM_PASSENGER_ID = "PassengerId";
    public static String PARAM_MODEL_ID = "ModelId";
    public static String PARAM_FULL_NAME = "Fullname";
    public static String PARAM_ADDRESS = "Address";
    public static String PARAM_CITY = "City";
    public static String PARAM_PICKUP_LOCATION = "PickupLocation";
    public static String PARAM_DROPOFF_LOCATION = "DropoffLocation";
    public static String PARAM_PICKUP_LAT = "PickupLat";
    public static String PARAM_PICKUP_LONG = "PickupLng";
    public static String PARAM_DROPOFF_LAT = "DropOffLat";
    public static String PARAM_DROPOFF_LONG = "DropOffLon";
    public static String PARAM_PASSENGER_TYPE = "PassengerType"; //(myself,other)
    public static String PARAM_PASSENGER_NAME = "PassengerName"; //
    public static String PARAM_PASSENGER_CONTACT = "PassengerContact";
    public static String PARAM_PICKUP_DATE_TIME = "PickupDateTime";
    public static String PARAM_FLIGHT_NUMBER = "FlightNumber";
    public static String PARAM_IDS = "Ids";
    public static String PARAM_PICKUP_LONG_ = "PickupLong";
    public static String PARAM_PROMO_CODE = "PromoCode";
    public static String PARAM_NOTES = "Notes";
    public static String PARAM_CARD_NUMBER = "CardNo";
    public static String PARAM_CARD_CVV = "Cvv";
    public static String PARAM_ALIAS = "Alias";
    public static String PARAM_CARD_EXPIRY = "Expiry";
    public static String PARAM_AMOUNT = "Amount";
    public static String PARAM_CARD_ID = "CardId";
    public static String PARAM_SENDER_ID= "SenderId";
    public static String PARAM_QR_CODE = "QRCode";
    public static String PARAM_PAYMENT_TYPE = "PaymentType";
    public static String PARAM_NAME = "Name";
    public static String PARAM_ABN = "ABN";
    public static String PARAM_CARD_CVV_CAP = "CVV";
    public static String PARAM_TYPE = "Type";
    public static String PARAM_COMPANY_NAME = "CompanyName";
    public static String PARAM_INVOICE_TYPE = "InvoiceType";
    public static String PARAM_INVOICE_TYPE_SMS = "SMS";
    public static String PARAM_INVOICE_TYPE_EMAIL = "Email";
    public static String PARAM_CUSTOMER_NAME = "CustomerName";
    public static String PARAM_BOOKING_ID= "BookingId";
    public static String PARAM_RATTING = "Rating";
    public static String PARAM_COMMENT = "Comment";
    public static String PARAM_BOOKING_TYPE= "BookingType";
    public static String BOOK_NOW = "BookNow";
    public static String BOOK_LATER = "BookLater";
    public static String PARAM_HOLDER_NAME = "HolderName";
    public static String PARAM_BANK_NAME = "BankName";
    public static String PARAM_BSB = "BSB";
    public static String PARAM_ACCOUNT_NO = "AccountNo";
    public static String PARAM_ACCOUNT_HOLDER_NAME = "AccountHolderName";
    public static String PARAM_BANK_ACCOUNT_NUMBER = "BankAccountNo";
    public static String PARAM_DESCRIPTION = "Description";

    public static String PARAM_PACKAGE_ID = "PackageId";
    public static String PARAM_PICKUP_DATE = "PickupDate";
    public static String PARAM_PICKUP_TIME = "PickupTime";
    public static String PARAM_SPECIAL = "Special";
    public static String PARAM_REQUEST_FOR = "RequestFor";
    public static String PARAM_PARCEL_ID = "ParcelId";
    public static String PARAM_LABOUR = "Labour";

    public static String PARAM_SOCIAL_ID = "SocialId";
    public static String PARAM_SOCIAL_TYPE = "SocialType";

    //new add by sovaran
    public static String PARAM_RECEIVER_NAME = "ReceiverName";
    public static String PARAM_RECEIVER_CONTACT_NO = "ReceiverContactNo";

    String PARAM_PARCEL_WEIGHT = "ParcelWeight";
    String PARAM_RECEIVER_EMAIL = "ReceiverEmail";
    String PARAM_SENDER_ADDRESS = "SenderEmail";

    String API_PREVIOUS_DUE =BASE_URL+"PreviousDue";
    String API_PREVIOUS_DUE_PARAM_ID = "UserId";
    String API_PREVIOUS_DUE_PARAM_UTYPE = "UserType";

    String API_PAYMENT_PAYTM = BASE_URL+"PaytmPayment";
    String PAYMENT_PAYTM_BID = "BookingId";
    String PAYMENT_PAYTM_BTYPE = "BookingType";
    String PAYMENT_PAYTM_STATUS = "PaytmPaymentStatus";
    String PAYMENT_PAYTM_TXID = "TransactionId";
    String PAYMENT_PAYTM_REFID = "ReferenceId";
    String PAYMENT_PAYTM_PAYMENTFOR = "PaymentFor";
    String PAYMENT_PAYTM_AMOUNT = "Amount";
    //
    //(BookNow or BookLater)
    //(failed or success)
}
