package com.truckz.shipper.comman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class SessionSave
{

    static SharedPreferences prefs;
    //Store data's into sharedPreference
    public static void saveUserSession(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Common.PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    // Get Data's from SharedPreferences
    public static String getUserSession(String key, Context context) {
        prefs = context.getSharedPreferences(Common.PREFRENCE_USER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void clearUserSession(Context context)
    {
        Editor editor = context.getSharedPreferences(Common.PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }
}
