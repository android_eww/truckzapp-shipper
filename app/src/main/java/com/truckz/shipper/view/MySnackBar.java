package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;


public class MySnackBar {

    private Context context;
    private Snackbar snackbar;

    public MySnackBar(Context context)
    {
        this.context=context;
    }

    //for linear layout
    public void showSnackBar(LinearLayout ll_RootView, String message)
    {
        snackbar = Snackbar
                .make(ll_RootView, message, Snackbar.LENGTH_LONG)
                .setAction(context.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        snackbar.setActionTextColor(context.getResources().getColor(R.color.colorWhite));
        View sbView = snackbar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        params.setMargins(20,0,20,0);
        sbView.setLayoutParams(params);
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
        textView.setTextSize(12f);
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorThemeRed));
        snackbar.show();
    }

    //for linear layout
    public void showSnackBar(LinearLayout ll_RootView, String message, int flag)
    {
        snackbar = Snackbar
                .make(ll_RootView, message, Snackbar.LENGTH_LONG)
                .setAction(context.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        snackbar.setActionTextColor(context.getResources().getColor(R.color.colorWhite));

        View sbView = snackbar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        params.setMargins(20,0,20,0);
        sbView.setLayoutParams(params);
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorThemeRed));
        snackbar.show();
    }
}
