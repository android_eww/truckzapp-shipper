package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextViewMedium extends TextView {

    public CTextViewMedium(Context context)
    {
        super(context);
        setMediumText();
    }

    public CTextViewMedium(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        setMediumText();
    }

    public CTextViewMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setMediumText();
    }

    public CTextViewMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setMediumText();
    }

    public void setMediumText()
    {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium_0.ttf");
        this.setTypeface(face);
    }
}
