package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextViewBold extends TextView {

    public CTextViewBold(Context context)
    {
        super(context);
        setSemiBoldText();
    }

    public CTextViewBold(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        setSemiBoldText();
    }

    public CTextViewBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setSemiBoldText();
    }

    public CTextViewBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setSemiBoldText();
    }

    public void setSemiBoldText()
    {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Bold_0.ttf");
        this.setTypeface(face);
    }
}
