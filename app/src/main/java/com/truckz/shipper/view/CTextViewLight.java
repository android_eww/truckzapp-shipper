package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextViewLight extends TextView {

    public CTextViewLight(Context context)
    {
        super(context);
        setLightText();
    }

    public CTextViewLight(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        setLightText();
    }

    public CTextViewLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setLightText();
    }

    public CTextViewLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setLightText();
    }

    public void setLightText()
    {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Light_0.ttf");
        this.setTypeface(face);
    }
}
