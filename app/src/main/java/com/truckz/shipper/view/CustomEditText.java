package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText
{

    public CustomEditText(Context context)
    {
        super(context);
        setLightText();
    }

    public CustomEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setLightText();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setLightText();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setLightText();
    }

    private void setLightText()
    {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Light_0.ttf");
        this.setTypeface(face);
    }

}
