package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextViewBlack extends TextView {

    public CTextViewBlack(Context context)
    {
        super(context);
        setBlackText();
    }

    public CTextViewBlack(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        setBlackText();
    }

    public CTextViewBlack(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setBlackText();
    }

    public CTextViewBlack(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setBlackText();
    }

    private void setBlackText() {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Black_0.ttf");
        this.setTypeface(face);
    }
}
