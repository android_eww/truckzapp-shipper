package com.truckz.shipper.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextViewThin extends TextView {

    public CTextViewThin(Context context)
    {
        super(context);
        setThinText();
    }

    public CTextViewThin(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        setThinText();
    }

    public CTextViewThin(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setThinText();
    }

    public CTextViewThin(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setThinText();
    }

    public void setThinText()
    {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto-Thin_0.ttf");
        this.setTypeface(face);
    }
}
