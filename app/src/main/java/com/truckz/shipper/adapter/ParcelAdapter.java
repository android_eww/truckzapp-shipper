package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.BookLaterActivity;
import com.truckz.shipper.been.ParcelBeen;

import java.util.List;

public class ParcelAdapter extends RecyclerView.Adapter<ParcelAdapter.ViewHolder> {

    private String TAG = "ParcelAdapter";
    private Context context;
    private List<ParcelBeen> parcelList;
    public int parcelIndex = 0;

    public ParcelAdapter(Context context, List<ParcelBeen> parcelList)
    {
        this.context = context;
        this.parcelList = parcelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_parcel_add_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position)
    {
        parcelIndex = position + 1;

        if (position == 0)
        {
            holder.llCheckBox.setVisibility(View.VISIBLE);
            holder.tvLable.setVisibility(View.VISIBLE);
            holder.cbParcel.setVisibility(View.VISIBLE);
            holder.ivParcelRemove.setVisibility(View.GONE);
        }
        else
        {
            holder.llCheckBox.setVisibility(View.VISIBLE);
            holder.tvLable.setVisibility(View.INVISIBLE);
            holder.cbParcel.setVisibility(View.GONE);
            holder.ivParcelRemove.setVisibility(View.VISIBLE);
        }

        if (parcelList.get(position).isChecked())
        {
            holder.cbParcel.setChecked(true);
        }
        else
        {
            holder.cbParcel.setChecked(false);
        }

        holder.cbParcel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                parcelList.get(position).setChecked(holder.cbParcel.isChecked());

                if (holder.cbParcel.isChecked())
                {
                    ((BookLaterActivity)context).ivAddParcel.setVisibility(View.VISIBLE);
                }
                else
                {
                    ((BookLaterActivity)context).ivAddParcel.setVisibility(View.GONE);
                    parcelList.subList(1, parcelList.size()).clear();
                    notifyDataSetChanged();
                }
            }
        });

        holder.ivParcelRemove.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                parcelList.remove(position);
                notifyDataSetChanged();
            }
        });


        double Total = 110 * parcelIndex;

        ((BookLaterActivity)context).tv_EstimateFare.setText(context.getString(R.string.currency) +
                String.format("%.2f", Total));

        holder.tvParcelIndex.setText(context.getResources().getString(R.string.parcel)+" - " + parcelIndex);
        holder.tvFare.setText(context.getResources().getString(R.string.currency) +
                parcelList.get(position).getParcelName());
    }

    @Override
    public int getItemCount()
    {
        return parcelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvParcelIndex, tvLable, tvFare;
        LinearLayout llCheckBox;
        CheckBox cbParcel;
        ImageView ivParcelRemove;

        ViewHolder(View view)
        {
            super(view);

            tvParcelIndex = view.findViewById(R.id.tvParcelIndex);
            tvLable = view.findViewById(R.id.tvLable);
            tvFare = view.findViewById(R.id.tvFare);
            llCheckBox = view.findViewById(R.id.llCheckBox);
            cbParcel = view.findViewById(R.id.cbParcel);
            ivParcelRemove = view.findViewById(R.id.ivParcelRemove);
        }
    }
}
