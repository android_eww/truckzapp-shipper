package com.truckz.shipper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.Wallet_Add_Cards_Activity;
import com.truckz.shipper.activity.Wallet_Balance_TopUp_Activity;
import com.truckz.shipper.activity.Wallet_Balance_TransferToBank_Activity;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CTextViewMedium;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;


public class CreditCardList_Adapter extends RecyclerView.Adapter<CreditCardList_Adapter.MyViewHolder> {

    private Context mContext;
    private List<CreditCard_List_Been> albumList;
    private DialogClass dialogClass;
    private AQuery aQuery;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CTextViewLight tv_Year,tv_Month;
        public CTextViewMedium tv_cardNo; //tv_cardType,
        public ImageView iv_creditCard, iv_delete;
        LinearLayout ll_row_card, ll_DeleteCard; //ll_addCard

        public MyViewHolder(View view) {
            super(view);

//            tv_cardType = (TextView) view.findViewById(R.id.tv_cardType);
            tv_cardNo = view.findViewById(R.id.tv_cardNo);
            tv_Year = view.findViewById(R.id.tv_Year);
            tv_Month = view.findViewById(R.id.tv_Month);
            iv_creditCard = view.findViewById(R.id.iv_creditCard);
            iv_delete = view.findViewById(R.id.iv_delete);
            ll_row_card = view.findViewById(R.id.ll_row_card);
//            ll_addCard = view.findViewById(R.id.ll_addCard);
            ll_DeleteCard = view.findViewById(R.id.delete_card_layout);
        }
    }

    public CreditCardList_Adapter(Context mContext, List<CreditCard_List_Been> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        dialogClass = new DialogClass(mContext,0);
        aQuery = new AQuery(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_credit_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final CreditCard_List_Been album = albumList.get(position);
//        holder.tv_cardType.setText(album.getCardName());
        holder.tv_cardNo.setText(album.getCardNum_());

        if (albumList.get(position).getCardExpiry() != null && !albumList.get(position).getCardExpiry().equalsIgnoreCase(""))
        {
            String date = albumList.get(position).getCardExpiry();
            String date1[] = date.split("/");
            if (date1.length>1)
            {
                holder.tv_Month.setText(date1[0]);
                holder.tv_Year.setText(date1[1]);
            }
        }
        else
        {
            holder.tv_Month.setVisibility(View.INVISIBLE);
            holder.tv_Year.setVisibility(View.INVISIBLE);
        }

//        if (position==albumList.size()-1)
//        {
//            holder.ll_addCard.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            holder.ll_addCard.setVisibility(View.GONE);
//        }

        if (albumList.get(position).getCardType()!=null && !albumList.get(position).getCardType().equalsIgnoreCase(""))
        {
            if (albumList.get(position).getCardType().equalsIgnoreCase("visa"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("discover"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_discover);
//                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_yellow);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("amex"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("diners"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_dinner);
//                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_gray);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("jcb"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_jcbs);
//                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_gray);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("mastercard"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_master);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else if (albumList.get(position).getCardType().equalsIgnoreCase("maestro"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
//                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_gray);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
            else
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
                holder.ll_row_card.setBackgroundResource(R.drawable.cab_card_blue);
            }
        }

//        if (album.getCardType().equalsIgnoreCase("mastercard"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
//        }
//        else if (album.getCardType().equalsIgnoreCase("visa"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.cab_card_blue);
//        }
//        else if (album.getCardType().equalsIgnoreCase("amex"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.ic_card_icon_american);
//        }
//        else if (album.getCardType().equalsIgnoreCase("diners"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.ic_card_icon_dinner);
//        }
//        else if (album.getCardType().equalsIgnoreCase("discover"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.cab_card_yellow);
//        }
//        else if (album.getCardType().equalsIgnoreCase("jcb"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.ic_card_icon_jcb);
//        }
//        else if (album.getCardType().equalsIgnoreCase("other"))
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.card_dummy);
//        }
//        else
//        {
//            holder.iv_creditCard.setImageResource(R.drawable.card_dummy);
//        }
//
//        if (position%2==0)
//        {
//            holder.ll_row_card.setBackgroundColor(ContextCompat.getColor(mContext,R.color.darkorange));
//        }
//        else
//        {
//            holder.ll_row_card.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorcardVisa));
//        }

//        holder.ll_addCard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Wallet_Add_Cards_Activity.resumeFlag = 0;
//                Intent i =  new Intent(mContext, Add_Card_In_List_Activity.class);
//                mContext.startActivity(i);
//            }
//        });

        holder.ll_row_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Wallet_Add_Cards_Activity.from!=null && Wallet_Add_Cards_Activity.from.equalsIgnoreCase("Wallet_Balance_TopUp_Activity"))
                {
                    Wallet_Balance_TopUp_Activity.cardId = album.getId();
                    Wallet_Balance_TopUp_Activity.cardNumber = album.getCardNum_();
                    Wallet_Balance_TopUp_Activity.cardType = album.getCardType();
                    ((Activity)mContext).finish();
                }
                else if (Wallet_Add_Cards_Activity.from!=null && Wallet_Add_Cards_Activity.from.equalsIgnoreCase("Wallet_Balance_TransferToBank_Activity"))
                {
                    Wallet_Balance_TransferToBank_Activity.cardId= album.getId();
                    Wallet_Balance_TransferToBank_Activity.cardNumber= album.getCardNum_();
                    Wallet_Balance_TransferToBank_Activity.cardType = album.getCardType();
                    ((Activity)mContext).finish();
                }
                else if(Wallet_Add_Cards_Activity.from!=null && Wallet_Add_Cards_Activity.from.equalsIgnoreCase("due"))
                {
                    Intent intent = new Intent();
                    intent.putExtra("Card", albumList.get(position));
                    ((Activity)mContext).setResult(((Activity)mContext).RESULT_OK, intent);
                    ((Activity)mContext).finish();
                }


            }
        });

        holder.ll_DeleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Global.isNetworkconn(mContext))
                {
                    deleteCard(position);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                    errorDialogClass.showDialog(mContext.getResources().getString(R.string.please_check_your_internet_connection),mContext.getResources().getString(R.string.no_internet_connection));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public void deleteCard(final int position)
    {
        try
        {
            dialogClass.showDialog();
            String url = WebServiceAPI.API_REMOVE_CARD + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,mContext) + "/" + albumList.get(position).getId();

            Log.e("call", "url = " + url);

            aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("responseCode", " = " + responseCode);
                        Log.e("Response", " = " + json);

                        if (json!=null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    albumList.remove(position);
                                    saveCardList(position);
                                    dialogClass.hideDialog();
                                    String message = mContext.getResources().getString(R.string.card_deleted_successfully);
                                    if (json.has("message"))
                                    {
                                        message = json.getString("message");
                                    }
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                    errorDialogClass.showDialog(message,mContext.getResources().getString(R.string.success_message));

                                    notifyDataSetChanged();
                                }
                                else
                                {
                                    String message = mContext.getResources().getString(R.string.something_went_wrong);
                                    if (json.has("message"))
                                    {
                                        message = json.getString("message");
                                    }
                                    dialogClass.hideDialog();
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                    errorDialogClass.showDialog(message,mContext.getResources().getString(R.string.error_message));
                                }
                            }
                            else
                            {
                                String message = mContext.getResources().getString(R.string.something_went_wrong);
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                errorDialogClass.showDialog(message,mContext.getResources().getString(R.string.error_message));
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Log.e("Exception","Exception "+e.toString());
                        dialogClass.hideDialog();
                    }
                }

            }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card albumList = "+e.getMessage());
        }
    }

    public void saveCardList(int position)
    {
        try
        {
            Log.e("call","111111111111111");
            String strCardList = SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,mContext);

            if (strCardList!=null && !strCardList.equalsIgnoreCase(""))
            {
                Log.e("call","222222222222222");
                JSONObject json = new JSONObject(strCardList);

                if (json!=null)
                {
                    Log.e("call","33333333333333");
                    if (json.has("cards"))
                    {
                        Log.e("call","6666666666666666");
                        JSONArray cards = json.getJSONArray("cards");


                        if (cards!=null && (cards.length()-1)>=position)
                        {
                            cards.remove(position);

                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("cards",cards);

                            SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_CARD_LIST,jsonObject.toString(),mContext);
                        }
                        else
                        {
                            Log.e("call","no cards available");
                        }
                    }
                    else
                    {
                        Log.e("call","no cards found");
                    }
                }
                else
                {
                    Log.e("call","json null");
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card albumList = "+e.getMessage());
        }
    }
}