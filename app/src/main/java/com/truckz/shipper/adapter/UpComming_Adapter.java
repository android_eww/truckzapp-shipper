package com.truckz.shipper.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.R;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CTextViewMedium;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class UpComming_Adapter extends ExpandableRecyclerView.Adapter<UpComming_Adapter.ViewHolder> {

    Context context;
    int POSITION;
    String TAG = "UpComming_Adapter";
    private AQuery aQuery;
    private DialogClass dialogClass;
    public String driverPhone = "";


    public UpComming_Adapter(LinearLayoutManager lm, Context context) {
        super(lm);
        this.context = context;
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_DropoffLocation, ll_Date, ll_CancelRequest;
        public CTextViewLight tv_DropoffLocation, tv_Date, tv_BookingId, tv_DriverName;
        public CTextViewMedium tv_CancelRequest;
        public ImageView ivCall;

        //content
        private LinearLayout ll_PickupLocation, ll_PickupTime, ll_DropoffTime, ll_VehicleType, ll_ParcelType, ll_DistanceTravelled, ll_TripFare;
        private LinearLayout ll_NightFare, ll_TollFee, ll_WaitingCost, ll_WaitingTime, ll_BookingCharge, ll_Tax, ll_Discount,
                llLoadTime, llLoadCharge, ll_PaymentType, ll_Total, ll_vehicleNo, ll_Goodweight;
        private CTextViewLight tv_PickupLocation, tv_PickupTime, tv_DropoffTime, tv_VehicleType, tv_ParcelType, tv_DistanceTravelled, tv_TripFare, tv_NightFare, tv_TollFee;
        private CTextViewLight tv_WaitingCost, tv_WaitingTime, tv_BookingCharge, tv_Tax, tv_Discount,
                tvLoadTime, tvLoadCharge, tv_PaymentType, tv_Total, vup_lblDistance, tv_vehicleNo, tv_Goodweight;

        private ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v.findViewById(R.id.row);

            //header

            ll_DropoffLocation = expandableItem.findViewById(R.id.ll_DropoffLocation);
            ll_Date = expandableItem.findViewById(R.id.ll_Date);
            ll_CancelRequest = expandableItem.findViewById(R.id.ll_CancelRequest);

            tv_DropoffLocation = expandableItem.findViewById(R.id.tv_DropoffLocation);
            tv_Date = expandableItem.findViewById(R.id.tv_Date);
            tv_BookingId = expandableItem.findViewById(R.id.tv_BookingId);
            tv_DriverName = expandableItem.findViewById(R.id.tv_DriverName);
            tv_CancelRequest = expandableItem.findViewById(R.id.tv_CancelRequest);
            ivCall = expandableItem.findViewById(R.id.ivCall);

            //content

            ll_PickupLocation = expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_PickupTime = expandableItem.findViewById(R.id.ll_PickupTime);
            ll_DropoffTime = expandableItem.findViewById(R.id.ll_DropoffTime);
            ll_VehicleType = expandableItem.findViewById(R.id.ll_VehicleType);
            ll_ParcelType = expandableItem.findViewById(R.id.ll_ParcelType);
            ll_DistanceTravelled = expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TripFare = expandableItem.findViewById(R.id.ll_TripFare);
            ll_NightFare = expandableItem.findViewById(R.id.ll_NightFare);
            ll_TollFee = expandableItem.findViewById(R.id.ll_TollFee);
            ll_WaitingCost = expandableItem.findViewById(R.id.ll_WaitingCost);
            ll_WaitingTime = expandableItem.findViewById(R.id.ll_WaitingTime);
            ll_BookingCharge = expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_Tax = expandableItem.findViewById(R.id.ll_Tax);
            ll_Discount = expandableItem.findViewById(R.id.ll_Discount);
            llLoadTime = expandableItem.findViewById(R.id.llLoadTime);
            llLoadCharge = expandableItem.findViewById(R.id.llLoadCharge);
            ll_PaymentType = expandableItem.findViewById(R.id.ll_PaymentType);
            ll_Total = expandableItem.findViewById(R.id.ll_Total);
            ll_vehicleNo = expandableItem.findViewById(R.id.ll_vehicleNo);
            ll_Goodweight = expandableItem.findViewById(R.id.ll_Goodweight);

            tv_PickupLocation = expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupTime = expandableItem.findViewById(R.id.tv_PickupTime);
            tv_DropoffTime = expandableItem.findViewById(R.id.tv_DropoffTime);
            tv_VehicleType = expandableItem.findViewById(R.id.tv_VehicleType);
            tv_ParcelType = expandableItem.findViewById(R.id.tv_ParcelType);
            tv_DistanceTravelled = expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TripFare = expandableItem.findViewById(R.id.tv_TripFare);
            tv_NightFare = expandableItem.findViewById(R.id.tv_NightFare);
            tv_TollFee = expandableItem.findViewById(R.id.tv_TollFee);
            tv_WaitingCost = expandableItem.findViewById(R.id.tv_WaitingCost);
            tv_WaitingTime = expandableItem.findViewById(R.id.tv_WaitingTime);
            tv_BookingCharge = expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_Tax = expandableItem.findViewById(R.id.tv_Tax);
            tv_Discount = expandableItem.findViewById(R.id.tv_Discount);
            tvLoadTime = expandableItem.findViewById(R.id.tvLoadTime);
            tvLoadCharge = expandableItem.findViewById(R.id.tvLoadCharge);
            tv_PaymentType = expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Total = expandableItem.findViewById(R.id.tv_Total);
            vup_lblDistance = expandableItem.findViewById(R.id.vup_lblDistance);
            tv_vehicleNo = expandableItem.findViewById(R.id.tv_vehicleNo);
            tv_Goodweight = expandableItem.findViewById(R.id.tv_Goodweight);
            vup_lblDistance.setText(context.getString(R.string.distance));
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_up_comming, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        if (MyBookingActivity.upComming_beens.get(position).getDropoffLocation() != null && !MyBookingActivity.upComming_beens.get(position).getDropoffLocation().equalsIgnoreCase("")) {
            holder.ll_DropoffLocation.setVisibility(View.VISIBLE);
            holder.tv_DropoffLocation.setText(MyBookingActivity.upComming_beens.get(position).getDropoffLocation());
        } else {
            holder.ll_DropoffLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getDriverName() != null && !MyBookingActivity.upComming_beens.get(position).getDriverName().equalsIgnoreCase("") && !MyBookingActivity.upComming_beens.get(position).getDriverName().equalsIgnoreCase("null")) {
            holder.tv_DriverName.setVisibility(View.VISIBLE);
            holder.tv_DriverName.setText(MyBookingActivity.upComming_beens.get(position).getDriverName());
        } else {
            holder.tv_DriverName.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getId() != null && !MyBookingActivity.upComming_beens.get(position).getId().equalsIgnoreCase("")) {
            holder.tv_BookingId.setVisibility(View.VISIBLE);

            if (MyBookingActivity.upComming_beens.get(position).getBookingType() != null
                    && !MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("")) {

                if (MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Now")) {

                    holder.tv_BookingId.setText("N" + MyBookingActivity.upComming_beens.get(position).getId());
                } else if (MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Later")) {

                    holder.tv_BookingId.setText("L" + MyBookingActivity.upComming_beens.get(position).getId());
                }
            }

        } else {
            holder.tv_BookingId.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getCreatedDate() != null && !MyBookingActivity.upComming_beens.get(position).getCreatedDate().equalsIgnoreCase("")) {
            holder.ll_Date.setVisibility(View.VISIBLE);
            holder.tv_Date.setText(MyBookingActivity.upComming_beens.get(position).getCreatedDate());
        } else {
            holder.ll_Date.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getPickupLocation() != null && !MyBookingActivity.upComming_beens.get(position).getPickupLocation().equalsIgnoreCase("")) {
            holder.ll_PickupLocation.setVisibility(View.VISIBLE);
            holder.tv_PickupLocation.setText(MyBookingActivity.upComming_beens.get(position).getPickupLocation());
        } else {
            holder.ll_PickupLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getPickupTime() != null &&
                !MyBookingActivity.upComming_beens.get(position).getPickupTime().equalsIgnoreCase("") &&
                !MyBookingActivity.upComming_beens.get(position).getPickupTime().equalsIgnoreCase("null")) {
            holder.ll_PickupTime.setVisibility(View.VISIBLE);
            holder.tv_PickupTime.setText(MyBookingActivity.upComming_beens.get(position).getPickupTime());
        } else {
            holder.ll_PickupTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getCarDetails_VehicleRegistrationNo() != null && !MyBookingActivity.upComming_beens.get(position).getCarDetails_VehicleRegistrationNo().equalsIgnoreCase("")) {
            holder.tv_vehicleNo.setVisibility(View.VISIBLE);
            holder.tv_vehicleNo.setText(MyBookingActivity.upComming_beens.get(position).getCarDetails_VehicleRegistrationNo());
            holder.ll_vehicleNo.setVisibility(View.VISIBLE);
        } else {
            holder.ll_vehicleNo.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getParcelWeight() != null && !MyBookingActivity.upComming_beens.get(position).getParcelWeight().equalsIgnoreCase("")) {
            holder.ll_Goodweight.setVisibility(View.VISIBLE);
            holder.tv_Goodweight.setText(MyBookingActivity.upComming_beens.get(position).getParcelWeight() + " Kg");
        } else {
            holder.ll_Goodweight.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getDropTime() != null && !MyBookingActivity.upComming_beens.get(position).getDropTime().equalsIgnoreCase("")) {
            holder.ll_DropoffTime.setVisibility(View.VISIBLE);
            String dateString = getDate(Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getDropTime() + "000"), "dd-MM-yyyy hh:mm a");
            holder.tv_DropoffTime.setText(dateString);
        } else {
            holder.ll_DropoffTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getModel() != null && !MyBookingActivity.upComming_beens.get(position).getModel().equalsIgnoreCase("")) {
            holder.ll_VehicleType.setVisibility(View.VISIBLE);
            holder.tv_VehicleType.setText(MyBookingActivity.upComming_beens.get(position).getModel());
        } else {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getParcel_beens() != null) {
            Parcel_Been Parcel = MyBookingActivity.upComming_beens.get(position).getParcel_beens().get(0);

            if (Parcel != null && Parcel.getName() != null && !Parcel.getName().trim().equalsIgnoreCase("")) {
                holder.ll_ParcelType.setVisibility(View.VISIBLE);
                holder.tv_ParcelType.setText(Parcel.getName().trim());
            } else {
                holder.ll_ParcelType.setVisibility(View.GONE);
            }
        } else {
            holder.ll_ParcelType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTripDistance() != null && !MyBookingActivity.upComming_beens.get(position).getTripDistance().equalsIgnoreCase("")) {
            holder.ll_DistanceTravelled.setVisibility(View.VISIBLE);
            String distance = String.format("%.1f", Float.parseFloat(MyBookingActivity.upComming_beens.get(position).getTripDistance()));
            holder.tv_DistanceTravelled.setText(distance + " " + context.getResources().getString(R.string.km));
        } else {
            holder.ll_DistanceTravelled.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTripFare() != null && !MyBookingActivity.upComming_beens.get(position).getTripFare().equalsIgnoreCase("")) {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(context.getString(R.string.currency) +
                    MyBookingActivity.upComming_beens.get(position).getTripFare());
        } else {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getNightFare() != null && !MyBookingActivity.upComming_beens.get(position).getNightFare().equalsIgnoreCase("")) {
            holder.ll_NightFare.setVisibility(View.GONE);
            holder.tv_NightFare.setText(MyBookingActivity.upComming_beens.get(position).getNightFare());
        } else {
            holder.ll_NightFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTollFee() != null && !MyBookingActivity.upComming_beens.get(position).getTollFee().equalsIgnoreCase("")) {
            holder.ll_TollFee.setVisibility(View.GONE);
            holder.tv_TollFee.setText(MyBookingActivity.upComming_beens.get(position).getTollFee());
        } else {
            holder.ll_TollFee.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost() != null && !MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost().equalsIgnoreCase("")) {
            holder.ll_WaitingCost.setVisibility(View.GONE);
            holder.tv_WaitingCost.setText(MyBookingActivity.upComming_beens.get(position).getWaitingTimeCost());
        } else {
            holder.ll_WaitingCost.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getWaitingTime() != null && !MyBookingActivity.upComming_beens.get(position).getWaitingTime().equalsIgnoreCase("")) {
            holder.ll_WaitingTime.setVisibility(View.GONE);
            holder.tv_WaitingTime.setText(MyBookingActivity.upComming_beens.get(position).getWaitingTime());
        } else {
            holder.ll_WaitingTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getBookingCharge() != null && !MyBookingActivity.upComming_beens.get(position).getBookingCharge().equalsIgnoreCase("")) {
            holder.ll_BookingCharge.setVisibility(View.VISIBLE);
            holder.tv_BookingCharge.setText(context.getString(R.string.currency) +
                    MyBookingActivity.upComming_beens.get(position).getBookingCharge());
        } else {
            holder.ll_BookingCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getTax() != null && !MyBookingActivity.upComming_beens.get(position).getTax().equalsIgnoreCase("")) {
            holder.ll_Tax.setVisibility(View.VISIBLE);
            holder.tv_Tax.setText(context.getString(R.string.currency) +
                    MyBookingActivity.upComming_beens.get(position).getTax());
        } else {
            holder.ll_Tax.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingCharge() != null &&
                !MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingCharge().equalsIgnoreCase("")) {
            holder.llLoadCharge.setVisibility(View.VISIBLE);
            holder.tvLoadCharge.setText(context.getString(R.string.currency) +
                    MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingCharge());
        } else {
            holder.llLoadCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingTime() != null &&
                !MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingTime().equalsIgnoreCase("")) {
            holder.llLoadTime.setVisibility(View.VISIBLE);
            holder.tvLoadTime.setText(MyBookingActivity.upComming_beens.get(position).getLoadingUnloadingTime() +
                    context.getString(R.string.min));
        } else {
            holder.llLoadTime.setVisibility(View.GONE);
        }

//        if (MyBookingActivity.upComming_beens.get(position).getDiscount()!=null && !MyBookingActivity.upComming_beens.get(position).getDiscount().equalsIgnoreCase(""))
//        {
//            holder.tv_Discount.setText(MyBookingActivity.upComming_beens.get(position).getDiscount());
//        }
//        else
//        {
//            holder.ll_Discount.setVisibility(View.GONE);
//        }
        holder.ll_Discount.setVisibility(View.GONE);

        if (MyBookingActivity.upComming_beens.get(position).getPaymentType() != null && !MyBookingActivity.upComming_beens.get(position).getPaymentType().equalsIgnoreCase("")) {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(MyBookingActivity.upComming_beens.get(position).getPaymentType());
        } else {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getGrandTotal() != null && !MyBookingActivity.upComming_beens.get(position).getGrandTotal().equalsIgnoreCase("")) {
            holder.ll_Total.setVisibility(View.VISIBLE);
            holder.tv_Total.setText(context.getString(R.string.currency) +
                    MyBookingActivity.upComming_beens.get(position).getGrandTotal());
        } else {
            holder.ll_Total.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getModel() != null && !MyBookingActivity.upComming_beens.get(position).getModel().equalsIgnoreCase("")) {
            holder.ll_VehicleType.setVisibility(View.VISIBLE);
            holder.tv_VehicleType.setText(MyBookingActivity.upComming_beens.get(position).getModel());
        } else {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.upComming_beens.get(position).getArrivedTime() != null &&
                !MyBookingActivity.upComming_beens.get(position).getArrivedTime().equalsIgnoreCase("") &&
                !MyBookingActivity.upComming_beens.get(position).getArrivedTime().equalsIgnoreCase("null")) {
            holder.ll_CancelRequest.setVisibility(View.GONE);
        } else {
            holder.ll_CancelRequest.setVisibility(View.VISIBLE);
        }

        holder.tv_CancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tripFlag = 0;

//                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context)!=null && SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context).equalsIgnoreCase("2"))
//                {
//                    InternetDialog internetDialog =new InternetDialog(context);
//                    internetDialog.showDialog(context.getResources().getString(R.string.please_first_complete_your_trip),context.getResources().getString(R.string.info_message));
//                }
//                else
//                {
//                    openConfimDailog(position);
//                }

                openConfimDailog(position);

            }
        });

        if (MyBookingActivity.upComming_beens.get(position).getStatus() != null &&
                MyBookingActivity.upComming_beens.get(position).getStatus().equalsIgnoreCase("accepted")) {
            holder.ivCall.setVisibility(View.VISIBLE);
        } else {
            holder.ivCall.setVisibility(View.GONE);
        }

        holder.ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyBookingActivity.upComming_beens.get(position).getDriverMobileNo() != null &&
                        !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("") &&
                        !MyBookingActivity.upComming_beens.get(position).getDriverMobileNo().equalsIgnoreCase("null")) {
                    driverPhone = MyBookingActivity.upComming_beens.get(position).getDriverMobileNo();
                } else {
                    driverPhone = "";
                }

                callToDriver();
            }
        });
    }


    public String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return MyBookingActivity.upComming_beens.size();
    }

    public void CancelBookingRequest(int position) {
        try {
            Log.e(TAG, "CancelBookingRequest");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e(TAG, "socket connected");
                Log.e(TAG, "BookingId = " + MyBookingActivity.upComming_beens.get(position).getId());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", MyBookingActivity.upComming_beens.get(position).getId());
                Common.socket.emit("CancelTripByPassenger", jsonObject);
                Log.e("call", "CancelTripByPassenger = " + jsonObject.toString());

                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", context);

                showDialog();
            } else {
                Log.e(TAG, "CancelBookingRequest Socket is not Connected");
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception = " + e.getMessage());
        }
    }

    public void cancel_AdvanceBookingRequest(int position) {
        try {
            Log.e(TAG, "cancelAdvanceBookingRequest");
            if (Common.socket != null && Common.socket.connected()) {
                Log.e(TAG, "socket connected");
                Log.e(TAG, "BookingId = " + MyBookingActivity.upComming_beens.get(position).getId());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BookingId", MyBookingActivity.upComming_beens.get(position).getId());
                Common.socket.emit("AdvancedBookingCancelTripByPassenger", jsonObject);
                Log.e(TAG, "AdvancedBookingCancelTripByPassenger = " + jsonObject.toString());


                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG, "0", context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_SELECTED_CAR, "-1", context);
                SessionSave.saveUserSession(Common.USER_PREFERENCE_KEY_BOOKING_DETAIL, "", context);

                showDialog();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception = " + e.getMessage());
        }
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_Ok = dialog.findViewById(R.id.Ok);
        TextView tv_Message = dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(context.getResources().getString(R.string.your_request_canceled_successfully));

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                MainActivity.activity.selectPickup();
                ((Activity) context).finish();
                ((Activity) context).overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                MainActivity.activity.selectPickup();
                ((Activity) context).finish();
                ((Activity) context).overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    private void openConfimDailog(final int position) {
        Log.e(TAG, "openConfimDailog()");
        final Dialog dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_confirm);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tvTitle, tvMessage, tvYes, tvNo;

        tvTitle = dialog.findViewById(R.id.tvTitle);
        tvMessage = dialog.findViewById(R.id.tvMessage);
        tvYes = dialog.findViewById(R.id.tvYes);
        tvNo = dialog.findViewById(R.id.tvNo);

        tvTitle.setText(context.getString(R.string.confirm_message));
        tvMessage.setText(context.getString(R.string.are_you_sure_you_want_to_cancel_trip));

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Global.isNetworkconn(context)) {
                    if (MyBookingActivity.upComming_beens.get(position).getBookingType() != null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Later")) {
                        cancel_AdvanceBookingRequest(position);
                    } else if (MyBookingActivity.upComming_beens.get(position).getBookingType() != null && MyBookingActivity.upComming_beens.get(position).getBookingType().equalsIgnoreCase("Book Now")) {
                        CancelBookingRequest(position);
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog(context.getResources().getString(R.string.please_check_internet_connection), context.getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog.show();
    }

    public void callToDriver() {
        Log.e(TAG, "callToDriver() driverPhone:- " + driverPhone);

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+driverPhone));
        context.startActivity(intent);


        /*TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        InternetDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.sim_card_not_availble),
                        context.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.sim_state_network_lock),
                        context.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.sim_state_pin_required),
                        context.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.sim_state_puk_required),
                        context.getResources().getString(R.string.error_message));
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + driverPhone));
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
                context.startActivity(intent);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.sim_state_unknown),
                        context.getResources().getString(R.string.error_message));
                break;
        }*/
    }
}
