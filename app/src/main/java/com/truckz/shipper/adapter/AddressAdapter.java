package com.truckz.shipper.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.been.Address_Been;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.TaxiUtil;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.ErrorDialogClass;
import com.truckz.shipper.other.Global;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.daimajia.swipe.SwipeLayout;
import com.truckz.shipper.other.InternetDialog;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;
import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private ArrayList<Address_Been> arrayList;
    private Context context;
    private DialogClass dialogClass;
    private AQuery aQuery;

    public AddressAdapter(ArrayList<Address_Been> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        dialogClass = new DialogClass(context,0);
        aQuery = new AQuery(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        viewHolder.tv_Address.setText(arrayList.get(i).getAddress());

        if (arrayList.get(i).getType().equalsIgnoreCase("Home"))
        {
            viewHolder.iv_Image.setImageResource(R.drawable.cab_fav_home);
        }
        else if (arrayList.get(i).getType().equalsIgnoreCase("Office"))
        {
            viewHolder.iv_Image.setImageResource(R.drawable.cab_fav_office);
        }
        else if (arrayList.get(i).getType().equalsIgnoreCase("Airport"))
        {
            viewHolder.iv_Image.setImageResource(R.drawable.cab_fav_airport);
        }
        else
        {
            viewHolder.iv_Image.setImageResource(R.drawable.cab_fav_other);
        }
//        else if (arrayList.get(i).getType().equalsIgnoreCase("Others"))
//        {
//            viewHolder.iv_Image.setImageResource(R.drawable.cab_fav_other);
//        }

        viewHolder.tv_title.setText(arrayList.get(i).getType());

        viewHolder.sample1.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.sample1.addDrag(SwipeLayout.DragEdge.Right, viewHolder.sample1.findViewById(R.id.bottom_wrapper));

        viewHolder.sample1.getSurfaceView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("HotelsBookingAdapter", "longClick on surface");
                return true;
            }
        });

        viewHolder.tv_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.sample1.close();
                Log.d("HotelsBookingAdapter", "longClick on tv_booknow");
                if (Global.isNetworkconn(context))
                {
                    call_RemoveAddress(i);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                    errorDialogClass.showDialog(context.getResources().getString(R.string.please_check_your_internet_connection),context.getResources().getString(R.string.no_internet_connection));
                }

            }
        });

        viewHolder.ll_Delete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                viewHolder.sample1.close();
                Log.d("HotelsBookingAdapter", "longClick on tv_booknow");
                if (Global.isNetworkconn(context))
                {
                    call_RemoveAddress(i);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                    errorDialogClass.showDialog(context.getResources().getString(R.string.please_check_your_internet_connection),context.getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        viewHolder.ll_Address.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context)!=null &&
                        SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_TRIP_FLAG,context).equalsIgnoreCase("0"))
                {
                    MainActivity.activity.startNew();
                    MainActivity.from = "FavoriteActivity";
                    MainActivity.favorite = 1;
                    MainActivity.favorite_address = arrayList.get(i).getAddress();
                    TaxiUtil.dropoff_Lat = arrayList.get(i).getLat();
                    TaxiUtil.dropoff_Long = arrayList.get(i).getLng();
                    TaxiUtil.dropoff_Address = arrayList.get(i).getAddress();
                    ((Activity)context).finish();
                }
                else
                {
                    InternetDialog internetDialog =new InternetDialog(context);
                    internetDialog.showDialog("Please complete your Current trip.",
                            context.getResources().getString(R.string.info_message));
                }
            }
        });

        viewHolder.iv_Image_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, viewHolder.iv_Image_delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.option_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_delete:
                                //handle menu1 click
                                viewHolder.sample1.close();
                                Log.d("HotelsBookingAdapter", "longClick on tv_booknow");
                                if (Global.isNetworkconn(context))
                                {
                                    call_RemoveAddress(i);
                                }
                                else
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(context.getResources().getString(R.string.please_check_your_internet_connection),context.getResources().getString(R.string.no_internet_connection));
                                }
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_Address,tv_Delete, tv_title;
        private ImageView iv_Image, iv_Image_delete;
        private LinearLayout ll_Address, ll_Delete;
        private SwipeLayout sample1;


        public ViewHolder(View view) {
            super(view);

            tv_Address = (TextView)view.findViewById(R.id.tv_Address);
            iv_Image = (ImageView)view.findViewById(R.id.iv_Image);
            ll_Address = (LinearLayout)view.findViewById(R.id.ll_Address);

            tv_Delete = (TextView)view.findViewById(R.id.tv_Delete);
            ll_Delete = (LinearLayout) view.findViewById(R.id.bottom_wrapper);
            sample1 = (SwipeLayout) view.findViewById(R.id.sample1);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            iv_Image_delete = view.findViewById(R.id.iv_Image_delete);
        }
    }

    public void call_RemoveAddress(final int position)
    {
        try
        {
            dialogClass.showDialog();
            String url = WebServiceAPI.API_REMOVE_ADDRESS + SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_ID,context) + "/" + arrayList.get(position).getId();

            Log.e("call", "url = " + url);

            aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("responseCode", " = " + responseCode);
                        Log.e("Response", " = " + json);

                        if (json!=null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    arrayList.remove(position);
                                    dialogClass.hideDialog();
                                    String message = context.getResources().getString(R.string.address_deleted_successfully);
                                    if (json.has("message"))
                                    {
                                        message = json.getString("message");
                                    }
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(message,context.getResources().getString(R.string.success_message));
                                    notifyDataSetChanged();
                                }
                                else
                                {
                                    String message = context.getResources().getString(R.string.something_went_wrong);
                                    if (json.has("message"))
                                    {
                                        message = json.getString("message");
                                    }
                                    dialogClass.hideDialog();
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(message,context.getResources().getString(R.string.error_message));
                                }
                            }
                            else
                            {
                                String message = context.getResources().getString(R.string.something_went_wrong);
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                errorDialogClass.showDialog(message,context.getResources().getString(R.string.error_message));
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Log.e("Exception","Exception "+e.toString());
                        dialogClass.hideDialog();
                    }
                }

            }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
        catch (Exception e)
        {
            Log.e("call","Exception in getting card list = "+e.getMessage());
        }
    }
}
