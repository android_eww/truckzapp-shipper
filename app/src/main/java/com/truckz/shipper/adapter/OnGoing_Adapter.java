package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.MyReceiptsActivity;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.other.DialogClass;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CTextViewMedium;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OnGoing_Adapter extends ExpandableRecyclerView.Adapter<OnGoing_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "PastBooking_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public OnGoing_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_DropoffLocation, ll_Date;
        public CTextViewLight tv_DropoffLocation, tv_Date, tv_BookingId, tv_DriverName;
        public View view_driverName;

        //content
        private LinearLayout ll_PickupLocation, ll_PickupTime, ll_DropoffTime, ll_VehicleType, ll_ParcelType, ll_DistanceTravelled, ll_TripFare;
        private LinearLayout ll_NightFare, ll_TollFee, ll_WaitingCost, ll_WaitingTime, ll_BookingCharge, ll_Tax, ll_Discount,
                llLoadTime, llLoadCharge, ll_PaymentType, ll_Total, ll_vehicleNo, ll_Goodweight;
        private CTextViewLight tv_PickupLocation, tv_PickupTime, tv_DropoffTime, tv_VehicleType, tv_ParcelType, tv_DistanceTravelled, tv_TripFare, tv_NightFare, tv_TollFee;
        private CTextViewLight tv_WaitingCost, tv_WaitingTime, tv_BookingCharge, tv_Tax, tv_Discount, tv_Goodweight, tv_vehicleNo,
                tvLoadTime, tvLoadCharge, tv_PaymentType, tv_Total;
        public CTextViewMedium tv_TrackYourTrip;

        private ExpandableItem expandableItem;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v.findViewById(R.id.row);

            //header

            ll_DropoffLocation = expandableItem.findViewById(R.id.ll_DropoffLocation);
            ll_Date = expandableItem.findViewById(R.id.ll_Date);

            tv_DropoffLocation = expandableItem.findViewById(R.id.tv_DropoffLocation);
            tv_Date = expandableItem.findViewById(R.id.tv_Date);
            tv_BookingId = expandableItem.findViewById(R.id.tv_BookingId);
            tv_DriverName = expandableItem.findViewById(R.id.tv_DriverName);
            view_driverName = (View) expandableItem.findViewById(R.id.view_driverName);

            ll_vehicleNo = expandableItem.findViewById(R.id.ll_vehicleNo);
            ll_Goodweight = expandableItem.findViewById(R.id.ll_Goodweight);


            //content

            ll_PickupLocation = expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_PickupTime = expandableItem.findViewById(R.id.ll_PickupTime);
            ll_DropoffTime = expandableItem.findViewById(R.id.ll_DropoffTime);
            ll_VehicleType = expandableItem.findViewById(R.id.ll_VehicleType);
            ll_ParcelType = expandableItem.findViewById(R.id.ll_ParcelType);
            ll_DistanceTravelled = expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TripFare = expandableItem.findViewById(R.id.ll_TripFare);
            ll_NightFare = expandableItem.findViewById(R.id.ll_NightFare);
            ll_TollFee = expandableItem.findViewById(R.id.ll_TollFee);
            ll_WaitingCost = expandableItem.findViewById(R.id.ll_WaitingCost);
            ll_WaitingTime = expandableItem.findViewById(R.id.ll_WaitingTime);
            ll_BookingCharge = expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_Tax = expandableItem.findViewById(R.id.ll_Tax);
            ll_Discount = expandableItem.findViewById(R.id.ll_Discount);
            llLoadTime = expandableItem.findViewById(R.id.llLoadTime);
            llLoadCharge = expandableItem.findViewById(R.id.llLoadCharge);
            ll_PaymentType = expandableItem.findViewById(R.id.ll_PaymentType);
            ll_Total = expandableItem.findViewById(R.id.ll_Total);

            tv_PickupLocation = expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupTime = expandableItem.findViewById(R.id.tv_PickupTime);
            tv_DropoffTime = expandableItem.findViewById(R.id.tv_DropoffTime);
            tv_VehicleType = expandableItem.findViewById(R.id.tv_VehicleType);
            tv_ParcelType = expandableItem.findViewById(R.id.tv_ParcelType);
            tv_DistanceTravelled = expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TripFare = expandableItem.findViewById(R.id.tv_TripFare);
            tv_NightFare = expandableItem.findViewById(R.id.tv_NightFare);
            tv_TollFee = expandableItem.findViewById(R.id.tv_TollFee);
            tv_WaitingCost = expandableItem.findViewById(R.id.tv_WaitingCost);
            tv_WaitingTime = expandableItem.findViewById(R.id.tv_WaitingTime);
            tv_BookingCharge = expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_Tax = expandableItem.findViewById(R.id.tv_Tax);
            tv_Discount = expandableItem.findViewById(R.id.tv_Discount);
            tvLoadTime = expandableItem.findViewById(R.id.tvLoadTime);
            tvLoadCharge = expandableItem.findViewById(R.id.tvLoadCharge);
            tv_PaymentType = expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Total = expandableItem.findViewById(R.id.tv_Total);
            tv_TrackYourTrip = expandableItem.findViewById(R.id.tv_TrackYourTrip);

            tv_Goodweight = expandableItem.findViewById(R.id.tv_Goodweight);
            tv_vehicleNo = expandableItem.findViewById(R.id.tv_vehicleNo);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_on_going, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/


        if (MyBookingActivity.onGoing_beens.get(position).getDropoffLocation() != null && !MyBookingActivity.onGoing_beens.get(position).getDropoffLocation().equalsIgnoreCase("")) {
            holder.ll_DropoffLocation.setVisibility(View.VISIBLE);
            holder.tv_DropoffLocation.setText(MyBookingActivity.onGoing_beens.get(position).getDropoffLocation());
        } else {
            holder.ll_DropoffLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDriverName() != null && !MyBookingActivity.onGoing_beens.get(position).getDriverName().equalsIgnoreCase("")) {
            holder.tv_DriverName.setVisibility(View.VISIBLE);
            holder.view_driverName.setVisibility(View.VISIBLE);
            holder.tv_DriverName.setText(MyBookingActivity.onGoing_beens.get(position).getDriverName());
        } else {
            holder.tv_DriverName.setVisibility(View.GONE);
            holder.view_driverName.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getCarDetails_VehicleRegistrationNo() != null && !MyBookingActivity.onGoing_beens.get(position).getCarDetails_VehicleRegistrationNo().equalsIgnoreCase("")) {
            holder.tv_vehicleNo.setVisibility(View.VISIBLE);
            holder.tv_vehicleNo.setText(MyBookingActivity.onGoing_beens.get(position).getCarDetails_VehicleRegistrationNo());
        } else {
            holder.ll_vehicleNo.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getParcelWeight() != null && !MyBookingActivity.onGoing_beens.get(position).getParcelWeight().equalsIgnoreCase("")) {
            holder.ll_Goodweight.setVisibility(View.VISIBLE);
            holder.tv_Goodweight.setText(MyBookingActivity.onGoing_beens.get(position).getParcelWeight() + " " + context.getString(R.string.kg));
        } else {
            holder.ll_Goodweight.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getId() != null && !MyBookingActivity.onGoing_beens.get(position).getId().equalsIgnoreCase("")) {
            holder.tv_BookingId.setVisibility(View.VISIBLE);

            if (MyBookingActivity.onGoing_beens.get(position).getBookingType() != null
                    && !MyBookingActivity.onGoing_beens.get(position).getBookingType().equalsIgnoreCase("")) {

                if (MyBookingActivity.onGoing_beens.get(position).getBookingType().equalsIgnoreCase("Book Now")) {

                    holder.tv_BookingId.setText("N" + MyBookingActivity.onGoing_beens.get(position).getId());
                } else if (MyBookingActivity.onGoing_beens.get(position).getBookingType().equalsIgnoreCase("Book Later")) {

                    holder.tv_BookingId.setText("L" + MyBookingActivity.onGoing_beens.get(position).getId());
                }
            }
        } else {
            holder.tv_BookingId.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getCreatedDate() != null && !MyBookingActivity.onGoing_beens.get(position).getCreatedDate().equalsIgnoreCase("")) {
            holder.ll_Date.setVisibility(View.VISIBLE);
            holder.tv_Date.setText(MyBookingActivity.onGoing_beens.get(position).getCreatedDate());
        } else {
            holder.ll_Date.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getPickupLocation() != null && !MyBookingActivity.onGoing_beens.get(position).getPickupLocation().equalsIgnoreCase("")) {
            holder.ll_PickupLocation.setVisibility(View.VISIBLE);
            holder.tv_PickupLocation.setText(MyBookingActivity.onGoing_beens.get(position).getPickupLocation());
        } else {
            holder.ll_PickupLocation.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getArrivedTime() != null && !MyBookingActivity.onGoing_beens.get(position).getArrivedTime().equalsIgnoreCase("")) {
            holder.ll_PickupTime.setVisibility(View.VISIBLE);
            String dateString = getDate(Long.parseLong(MyBookingActivity.onGoing_beens.get(position).getArrivedTime() + "000"), "dd-MM-yyyy hh:mm a");
            holder.tv_PickupTime.setText(dateString);
        } else {
            holder.ll_PickupTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDropTime() != null && !MyBookingActivity.onGoing_beens.get(position).getDropTime().equalsIgnoreCase("")) {
            holder.ll_DropoffTime.setVisibility(View.VISIBLE);
            String dateString = getDate(Long.parseLong(MyBookingActivity.onGoing_beens.get(position).getDropTime() + "000"), "dd-MM-yyyy hh:mm a");
            holder.tv_DropoffTime.setText(dateString);
        } else {
            holder.ll_DropoffTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getModel() != null && !MyBookingActivity.onGoing_beens.get(position).getModel().equalsIgnoreCase("")) {
            holder.ll_VehicleType.setVisibility(View.VISIBLE);
            holder.tv_VehicleType.setText(MyBookingActivity.onGoing_beens.get(position).getModel());
        } else {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getParcel_beens() != null) {
            Parcel_Been Parcel = MyBookingActivity.onGoing_beens.get(position).getParcel_beens().get(0);

            if (Parcel != null && Parcel.getName() != null && !Parcel.getName().trim().equalsIgnoreCase("")) {
                holder.ll_ParcelType.setVisibility(View.VISIBLE);
                holder.tv_ParcelType.setText(Parcel.getName().trim());
            } else {
                holder.ll_ParcelType.setVisibility(View.GONE);
            }
        } else {
            holder.ll_ParcelType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTripDistance() != null && !MyBookingActivity.onGoing_beens.get(position).getTripDistance().equalsIgnoreCase("")) {
            holder.ll_DistanceTravelled.setVisibility(View.VISIBLE);
            String distance = String.format("%.1f", Float.parseFloat(MyBookingActivity.onGoing_beens.get(position).getTripDistance()));
            holder.tv_DistanceTravelled.setText(distance + " " + context.getResources().getString(R.string.km));
        } else {
            holder.ll_DistanceTravelled.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTripFare() != null && !MyBookingActivity.onGoing_beens.get(position).getTripFare().equalsIgnoreCase("")) {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getTripFare());
        } else {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getNightFare() != null && !MyBookingActivity.onGoing_beens.get(position).getNightFare().equalsIgnoreCase("")) {
            holder.ll_NightFare.setVisibility(View.GONE);
            holder.tv_NightFare.setText(MyBookingActivity.onGoing_beens.get(position).getNightFare());
        } else {
            holder.ll_NightFare.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTollFee() != null && !MyBookingActivity.onGoing_beens.get(position).getTollFee().equalsIgnoreCase("")) {
            holder.ll_TollFee.setVisibility(View.GONE);
            holder.tv_TollFee.setText(MyBookingActivity.onGoing_beens.get(position).getTollFee());
        } else {
            holder.ll_TollFee.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost() != null && !MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost().equalsIgnoreCase("")) {
            holder.ll_WaitingCost.setVisibility(View.GONE);
            holder.tv_WaitingCost.setText(MyBookingActivity.onGoing_beens.get(position).getWaitingTimeCost());
        } else {
            holder.ll_WaitingCost.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getWaitingTime() != null && !MyBookingActivity.onGoing_beens.get(position).getWaitingTime().equalsIgnoreCase("")) {
            holder.ll_WaitingTime.setVisibility(View.GONE);
//            holder.tv_WaitingTime.setText(MyBookingActivity.onGoing_beens.get(position).getWaitingTime());
            long totalSecs = Long.parseLong(MyBookingActivity.onGoing_beens.get(position).getWaitingTime());
            long hours = totalSecs / 3600;
            long minutes = (totalSecs % 3600) / 60;
            long seconds = totalSecs % 60;

            String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            holder.tv_WaitingTime.setText(timeString);

        } else {
            holder.ll_WaitingTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getBookingCharge() != null && !MyBookingActivity.onGoing_beens.get(position).getBookingCharge().equalsIgnoreCase("")) {
            holder.ll_BookingCharge.setVisibility(View.VISIBLE);
            holder.tv_BookingCharge.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getBookingCharge());
        } else {
            holder.ll_BookingCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getTax() != null && !MyBookingActivity.onGoing_beens.get(position).getTax().equalsIgnoreCase("")) {
            holder.ll_Tax.setVisibility(View.VISIBLE);
            holder.tv_Tax.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getTax());
        } else {
            holder.ll_Tax.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getDiscount() != null && !MyBookingActivity.onGoing_beens.get(position).getDiscount().equalsIgnoreCase("")) {
            holder.ll_Discount.setVisibility(View.VISIBLE);
            holder.tv_Discount.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getDiscount());
        } else {
            holder.ll_Discount.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingCharge() != null &&
                !MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingCharge().equalsIgnoreCase("")) {
            holder.llLoadCharge.setVisibility(View.VISIBLE);
            holder.tvLoadCharge.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingCharge());
        } else {
            holder.llLoadCharge.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingTime() != null &&
                !MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingTime().equalsIgnoreCase("")) {
            holder.llLoadTime.setVisibility(View.VISIBLE);
            holder.tvLoadTime.setText(MyBookingActivity.onGoing_beens.get(position).getLoadingUnloadingTime() +
                    context.getString(R.string.min));
        } else {
            holder.llLoadTime.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getPaymentType() != null && !MyBookingActivity.onGoing_beens.get(position).getPaymentType().equalsIgnoreCase("")) {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(MyBookingActivity.onGoing_beens.get(position).getPaymentType());
        } else {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (MyBookingActivity.onGoing_beens.get(position).getGrandTotal() != null && !MyBookingActivity.onGoing_beens.get(position).getGrandTotal().equalsIgnoreCase("")) {
            holder.ll_Total.setVisibility(View.VISIBLE);
            holder.tv_Total.setText(context.getString(R.string.currency) +
                    MyBookingActivity.onGoing_beens.get(position).getGrandTotal());
        } else {
            holder.ll_Total.setVisibility(View.GONE);
        }

        holder.tv_TrackYourTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.activity != null) {
                    ((MyBookingActivity) context).onBackPressed();
                    MainActivity.activity.ClearWholeData(MyBookingActivity.onGoing_beens.get(position).getId());
                }
            }
        });

    }

    public String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return MyBookingActivity.onGoing_beens.size();
    }

}
