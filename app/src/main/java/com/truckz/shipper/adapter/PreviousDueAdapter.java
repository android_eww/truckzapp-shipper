package com.truckz.shipper.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.Add_Card_In_List_Activity;
import com.truckz.shipper.activity.Wallet_Add_Cards_Activity;
import com.truckz.shipper.been.CreditCard_List_Been;
import com.truckz.shipper.been.previousDue.History;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.view.CustomSpinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PreviousDueAdapter extends RecyclerView.Adapter
{
    Context context;
    List<History> list;
    onpayCliick onpayCliick;

    public PreviousDueAdapter(Context context, List<History> history,onpayCliick onpayCliick)
    {
        this.context = context;
        list = history;
        this.onpayCliick = onpayCliick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_previousdue,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            History item = list.get(position);
            if(item.getBookingType() != null && !item.getBookingType().equalsIgnoreCase(""))
            {holder1.tv_BookingId.setText(item.getBookingType().toUpperCase().charAt(4)+item.getBookingId());}
            if(item.getPickupLocation() != null && !item.getPickupLocation().equalsIgnoreCase(""))
            {holder1.tv_PickUpLoc.setText(item.getPickupLocation());}
            if(item.getDropoffLocation() != null && !item.getDropoffLocation().equalsIgnoreCase(""))
            {holder1.tv_DropOffLoc.setText(item.getDropoffLocation());}
            if(item.getAmount() != null && !item.getAmount().equalsIgnoreCase(""))
            {holder1.tv_PreviousDue.setText(context.getResources().getString(R.string.currency)+item.getAmount());}


            if(item.getChargeType().equalsIgnoreCase("PaymentFailedPaytm"))
            {
                holder1.tv_title.setText("Reason");
                holder1.tv_CancelBy.setText("Paytm Payment Failed");
            }
            else
            {
                holder1.tv_title.setText("Cancel By");
                holder1.tv_CancelBy.setText("Shipper");
            }

            holder1.ll_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayCliick.onPayButton(list.get(position),position);
                }
            });
        }
    }



    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_BookingId,tv_PickUpLoc,tv_DropOffLoc,tv_PreviousDue,tv_CancelBy,tv_title;
        LinearLayout ll_pay;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            tv_CancelBy = itemView.findViewById(R.id.tv_CancelBy);
            tv_PreviousDue = itemView.findViewById(R.id.tv_PreviousDue);
            tv_DropOffLoc = itemView.findViewById(R.id.tv_DropOffLoc);
            tv_PickUpLoc = itemView.findViewById(R.id.tv_PickUpLoc);
            tv_BookingId = itemView.findViewById(R.id.tv_BookingId);
            tv_title = itemView.findViewById(R.id.tv_title);
            ll_pay = itemView.findViewById(R.id.ll_pay);
        }
    }



    public interface onpayCliick
    {
        void onPayButton(History history,int position);
    }
}
