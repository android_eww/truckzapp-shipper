package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.CustomPackageActivity;
import com.truckz.shipper.R;
import com.truckz.shipper.been.Package_Been;

import java.util.List;


public class Hours_Adapter extends RecyclerView.Adapter<Hours_Adapter.MyViewHolder> {

    private Context context;
    private List<Package_Been> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_Time,tv_Km,tv_NoteOne,tv_NoteTwo,tv_Amount;
        private LinearLayout ll_row;

        public MyViewHolder(View view) {
            super(view);

            tv_Time = (TextView) view.findViewById(R.id.time);
            tv_Km = (TextView) view.findViewById(R.id.km);
            tv_NoteOne = (TextView) view.findViewById(R.id.note_one);
            tv_NoteTwo = (TextView) view.findViewById(R.id.note_two);
            tv_Amount = (TextView) view.findViewById(R.id.amount);

            ll_row = (LinearLayout) view.findViewById(R.id.ll_row);
        }
    }

    public Hours_Adapter(Context mContext, List<Package_Been> albumList) {
        this.context = mContext;
        this.list = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hours, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.tv_Time.setText(list.get(position).getTime());
        holder.tv_Km.setText(list.get(position).getKM()+" "+context.getResources().getString(R.string.km));
        holder.tv_Amount.setText(String.format("%.1f",Double.parseDouble(list.get(position).getKM())));

        String notes[] = list.get(position).getNotes().split(":");

        if (notes.length==2)
        {
            holder.tv_NoteOne.setText(notes[0]);
            holder.tv_NoteTwo.setText(notes[1]);
        }
        else
        {
            holder.tv_NoteOne.setText("");
            holder.tv_NoteTwo.setText("");
        }

        holder.ll_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomPackageActivity.itemPos = position;
                ((CustomPackageActivity)context).setCurrentAndSelectedTime();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}