package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.truckz.shipper.been.Model_Been;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.CustomPackageActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 2/28/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyView> {

private List<Model_Been> list;
private Context context;

public class MyView extends RecyclerView.ViewHolder {

    public ImageView carImage;
    private LinearLayout ll_row;

    public MyView(View view) {
        super(view);

        carImage = (ImageView) view.findViewById(R.id.car_image);
        ll_row = (LinearLayout) view.findViewById(R.id.cardview);

    }
}


    public RecyclerViewAdapter(Context context,List<Model_Been> horizontalList) {
        this.context=context;
        this.list = horizontalList;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_item, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        Picasso.with(context).load(list.get(position).getModelImage()).into(holder.carImage);

        if (list.get(position).getIsSelected())
        {
            holder.ll_row.setBackgroundResource(R.drawable.circle_red_border);
        }
        else
        {
            holder.ll_row.setBackgroundResource(R.drawable.circle_white_border);
        }

        Log.e("call","notifi data set change");

        holder.ll_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomPackageActivity.selectedCarPosition = position;

                for (int i=0; i<list.size(); i++)
                {
                    if (position==i)
                    {
                        list.set(i,new Model_Been(list.get(i).getModelId(),list.get(i).getModelName(),list.get(i).getModelImage(),true,list.get(i).getPackage()));
                    }
                    else
                    {
                        list.set(i,new Model_Been(list.get(i).getModelId(),list.get(i).getModelName(),list.get(i).getModelImage(),false,list.get(i).getPackage()));
                    }
                }

                for (int i=0; i<list.size(); i++)
                {
                    Log.e("call","isSelected = "+list.get(i).getIsSelected()+" ,pos = "+i);
                }

                ((CustomPackageActivity)context).selectFragment(0);
                Log.e("call","car clicked position = "+position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}