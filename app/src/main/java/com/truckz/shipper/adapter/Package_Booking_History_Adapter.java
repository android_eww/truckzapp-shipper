package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.CustomPackageHistoryActivity;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.R;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Package_Booking_History_Adapter extends ExpandableRecyclerView.Adapter<Package_Booking_History_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "PackageBook_History";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public Package_Booking_History_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_PickupLocation, ll_Date;
        public TextView tv_PickupLocation, tv_PickupDate, tv_PickupTime, tv_BookingId, tv_PackageName;
        public View view_packageName;

        //content
        private LinearLayout ll_PackageStatus,ll_PaymentType,ll_PaymentStatus,ll_PackageType,ll_Distance,ll_Amount,ll_Description;
        private TextView tv_PackageStatus,tv_PaymentType,tv_PaymentStatus,tv_PackageType,tv_Distance,tv_Amount,tv_Description;

        private ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            //header

            ll_PickupLocation = (LinearLayout) expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_Date = (LinearLayout) expandableItem.findViewById(R.id.ll_Date);

            tv_PickupLocation = (TextView) expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupDate = (TextView) expandableItem.findViewById(R.id.tv_PickupDate);
            tv_PickupTime = (TextView) expandableItem.findViewById(R.id.tv_PickupTime);
            tv_BookingId = (TextView) expandableItem.findViewById(R.id.tv_BookingId);
            tv_PackageName = (TextView) expandableItem.findViewById(R.id.tv_PackageName);
            view_packageName = (View) expandableItem.findViewById(R.id.view_packageName);


            //content
            ll_PackageStatus = (LinearLayout) expandableItem.findViewById(R.id.ll_PackageStatus);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_PaymentStatus = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentStatus);
            ll_PackageType = (LinearLayout) expandableItem.findViewById(R.id.ll_PackageType);
            ll_Distance = (LinearLayout) expandableItem.findViewById(R.id.ll_Distance);
            ll_Amount = (LinearLayout) expandableItem.findViewById(R.id.ll_Amount);
            ll_Description = (LinearLayout) expandableItem.findViewById(R.id.ll_Description);

            tv_PackageStatus = (TextView) expandableItem.findViewById(R.id.tv_PackageStatus);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_PaymentStatus = (TextView) expandableItem.findViewById(R.id.tv_PaymentStatus);
            tv_PackageType = (TextView) expandableItem.findViewById(R.id.tv_PackageType);
            tv_Distance = (TextView) expandableItem.findViewById(R.id.tv_Distance);
            tv_Amount = (TextView) expandableItem.findViewById(R.id.tv_Amount);
            tv_Description = (TextView) expandableItem.findViewById(R.id.tv_Description);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_package_booking_history, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        Log.e("call","tv_PackageName = " + CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsName());

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsName()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsName().equalsIgnoreCase(""))
        {
            Log.e("call","tv_PackageName" + CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsName());
            holder.tv_PackageName.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsName());
        }
        else
        {
            Log.e("call","tv_PackageName = Null" );
            holder.tv_PackageName.setVisibility(View.GONE);
            holder.view_packageName.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getId()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getId().equalsIgnoreCase(""))
        {
            holder.tv_BookingId.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getId());
        }
        else
        {
            holder.tv_BookingId.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupLocation()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.tv_PickupLocation.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_PickupLocation.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupDate()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupDate().equalsIgnoreCase(""))
        {
            if (CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupTime()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupTime().equalsIgnoreCase(""))
            {
                holder.tv_PickupDate.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupDate());
                holder.tv_PickupTime.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getPickupTime());
            }

        }
        else
        {
            holder.ll_Date.setVisibility(View.GONE);
        }


        //content

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getStatus()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getStatus().equalsIgnoreCase(""))
        {
            holder.tv_PackageStatus.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getStatus());
        }
        else
        {
            holder.ll_PackageStatus.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentType()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.tv_PaymentType.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentStatus()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentStatus().equalsIgnoreCase(""))
        {
            holder.tv_PaymentStatus.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getPaymentStatus());
        }
        else
        {
            holder.ll_PaymentStatus.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsType()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsType().equalsIgnoreCase(""))
        {
            if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsTime()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsTime().equalsIgnoreCase(""))
            {
                holder.tv_PackageType.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsTime() + " " + CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsType());
            }
        }
        else
        {
            holder.ll_PackageType.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsKM()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsKM().equalsIgnoreCase(""))
        {
            holder.tv_Distance.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsKM() + " "+context.getResources().getString(R.string.km));
        }
        else
        {
            holder.ll_Distance.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsAmount()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsAmount().equalsIgnoreCase(""))
        {
            holder.tv_Amount.setText("LKR " + CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsAmount());
        }
        else
        {
            holder.ll_Amount.setVisibility(View.GONE);
        }

        if (CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsNotes()!=null && !CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsNotes().equalsIgnoreCase(""))
        {
            holder.tv_Description.setText(CustomPackageHistoryActivity.Package_History_Been.get(position).getDetailsNotes());
        }
        else
        {
            holder.ll_Description.setVisibility(View.GONE);
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return CustomPackageHistoryActivity.Package_History_Been.size();
    }

}
