package com.truckz.shipper.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.other.DialogClass;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.MyReceiptsActivity;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CTextViewMedium;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MyReceipt_Adapter extends ExpandableRecyclerView.Adapter<MyReceipt_Adapter.ViewHolder> {

    Context context;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;


    public MyReceipt_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_dropOffLoc, ll_pickupLoc;
        public CTextViewLight tv_PassengerName, tv_drop_off_location, tv_date_time_Created, tv_pickup_location;
        public CTextViewMedium tv_GetReceipt;

        //content
        private LinearLayout ll_row_item_pastJob, ll_VehicleType, ll_DistanceTravelled, ll_TollFee, ll_DiscountApplied, ll_ChargedCard, ll_FareTotal, llLoadTime, llLoadCharge;
        public CTextViewLight tv_VehicleType, tv_DistanceTravelled, tv_TollFee, tv_DiscountApplied, tv_ChargedCard, tv_FareTotal, tvLoadTime, tvLoadCharge;
        private ExpandableItem expandableItem;

        private LinearLayout llPickUpTime, llDropOffTime, llGoodType, llGoodWeight, llTripFare, llBookingChage, llTax;
        private CTextViewLight tvPickUpTime, tvDropOffTime, tvGoodType, tvGoodWeight, tvTripFare, tvBookingChage, tvTax;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v.findViewById(R.id.row);

            tv_drop_off_location = expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickup_location = expandableItem.findViewById(R.id.tv_pickup_location);
            tv_date_time_Created = expandableItem.findViewById(R.id.tv_date_time_Created);
            tv_PassengerName = expandableItem.findViewById(R.id.tv_PassengerName);
            tv_GetReceipt = expandableItem.findViewById(R.id.tv_GetReceipt);

            ll_dropOffLoc = expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_pickupLoc = expandableItem.findViewById(R.id.ll_pickupLoc);

            tv_VehicleType = expandableItem.findViewById(R.id.tv_VehicleType);
            tv_DistanceTravelled = expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TollFee = expandableItem.findViewById(R.id.tv_TollFee);
            tv_DiscountApplied = expandableItem.findViewById(R.id.tv_DiscountApplied);
            tvLoadTime = expandableItem.findViewById(R.id.tvLoadTime);
            tvLoadCharge = expandableItem.findViewById(R.id.tvLoadCharge);
            tv_ChargedCard = expandableItem.findViewById(R.id.tv_ChargedCard);
            tv_FareTotal = expandableItem.findViewById(R.id.tv_FareTotal);

            tvPickUpTime = expandableItem.findViewById(R.id.tv_pickupTime);
            tvDropOffTime = expandableItem.findViewById(R.id.tvDropoffTime);
            tvGoodType = expandableItem.findViewById(R.id.tvGoodsType);
            tvGoodWeight = expandableItem.findViewById(R.id.tvGoodsWeight);
            tvTripFare = expandableItem.findViewById(R.id.tvTripFare);
            tvBookingChage = expandableItem.findViewById(R.id.tvBookingCharge);
            tvTax = expandableItem.findViewById(R.id.tvTax);


            ll_row_item_pastJob = expandableItem.findViewById(R.id.ll_row_item_pastJob);

            ll_VehicleType = expandableItem.findViewById(R.id.ll_VehicleType);
            ll_DistanceTravelled = expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TollFee = expandableItem.findViewById(R.id.ll_TollFee);
            ll_DiscountApplied = expandableItem.findViewById(R.id.ll_DiscountApplied);
            llLoadTime = expandableItem.findViewById(R.id.llLoadTime);
            llLoadCharge = expandableItem.findViewById(R.id.llLoadCharge);
            ll_ChargedCard = expandableItem.findViewById(R.id.ll_ChargedCard);
            ll_FareTotal = expandableItem.findViewById(R.id.ll_FareTotal);

            llPickUpTime = expandableItem.findViewById(R.id.ll_pickUpTime);
            llDropOffTime = expandableItem.findViewById(R.id.llDropoff_time);
            llGoodType = expandableItem.findViewById(R.id.llGoodsType);
            llGoodWeight = expandableItem.findViewById(R.id.llgoodsWeight);
            llTripFare = expandableItem.findViewById(R.id.llTripFare);
            llBookingChage = expandableItem.findViewById(R.id.ll_BookingCharge);
            llTax = expandableItem.findViewById(R.id.llTax);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_my_receipt, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        if (MyReceiptsActivity.list.get(position).getDropoffLocation() != null && !MyReceiptsActivity.list.get(position).getDropoffLocation().equalsIgnoreCase("")) {
            holder.tv_drop_off_location.setText(MyReceiptsActivity.list.get(position).getDropoffLocation().trim());
        } else {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getDriverName() != null && !MyReceiptsActivity.list.get(position).getDriverName().equalsIgnoreCase("")) {
            holder.tv_PassengerName.setText(MyReceiptsActivity.list.get(position).getDriverName().trim());
        } else {
            holder.tv_PassengerName.setVisibility(View.INVISIBLE);
        }

        if (MyReceiptsActivity.list.get(position).getPickupLocation() != null && !MyReceiptsActivity.list.get(position).getPickupLocation().equalsIgnoreCase("")) {
            holder.tv_pickup_location.setText(MyReceiptsActivity.list.get(position).getPickupLocation().trim());
        } else {
            holder.ll_pickupLoc.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getCreatedDate() != null && !MyReceiptsActivity.list.get(position).getCreatedDate().equalsIgnoreCase("")) {
            holder.tv_date_time_Created.setText(MyReceiptsActivity.list.get(position).getCreatedDate().trim());
        } else {
            holder.tv_date_time_Created.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getTollFee() != null && !MyReceiptsActivity.list.get(position).getTollFee().equalsIgnoreCase("")) {
            holder.tv_TollFee.setText((context.getResources().getString(R.string.currency) +
                    Double.parseDouble(MyReceiptsActivity.list.get(position).getTollFee())).trim());
        } else {
            holder.ll_TollFee.setVisibility(View.VISIBLE);
            holder.tv_TollFee.setText(context.getResources().getString(R.string.currency) + "0.00");
        }

        //pickup
        //dropoff
        //vehicle type
        //distance
        //toll
        //fare total
        //discount
        //charged card

        //llGoodType, llGoodWeight, llTripFare, llBookingChage, llTax;

        if(MyReceiptsActivity.list.get(position).getParcelType() != null && !MyReceiptsActivity.list.get(position).getParcelType().equalsIgnoreCase("")){
            holder.tvGoodType.setText(MyReceiptsActivity.list.get(position).getParcelType());
        } else {
            holder.llGoodType.setVisibility(View.GONE);
        }


        if(MyReceiptsActivity.list.get(position).getPickupTime() != null && !MyReceiptsActivity.list.get(position).getPickupTime().equalsIgnoreCase("")) {
             //getDate(Long.parseLong((MyReceiptsActivity.list.get(position).getPickupTime()+"000"), "dd-MM-yyyy hh:mm a");
            String dateString = getDate(Long.parseLong(MyReceiptsActivity.list.get(position).getPickupTime()+"000"), "dd-MM-yyyy hh:mm a");
            holder.tvPickUpTime.setText(dateString.trim());
        } else {
            holder.llPickUpTime.setVisibility(View.GONE);
        }


        if(MyReceiptsActivity.list.get(position).getDropTime() != null && !MyReceiptsActivity.list.get(position).getPickupTime().equalsIgnoreCase("")) {
            String dateString = getDate(Long.parseLong(MyReceiptsActivity.list.get(position).getDropTime()+"000"), "dd-MM-yyyy hh:mm a");
            holder.tvDropOffTime.setText(dateString.trim());
        } else {
            holder.llDropOffTime.setVisibility(View.GONE);
        }

        //if(MyReceiptsActivity.list.get(position).getP)

        if(MyReceiptsActivity.list.get(position).getParcelWeight() != null && !MyReceiptsActivity.list.get(position).getParcelWeight().equalsIgnoreCase("")) {

            holder.tvGoodWeight.setText((MyReceiptsActivity.list.get(position).getParcelWeight() + " Kg").trim());

        } else {
            holder.llGoodWeight.setVisibility(View.GONE);
        }

        if(MyReceiptsActivity.list.get(position).getTripFare() != null && !MyReceiptsActivity.list.get(position).getTripFare().equalsIgnoreCase("")) {

            holder.tvTripFare.setText(" "+(context.getResources().getString(R.string.currency) + " " + MyReceiptsActivity.list.get(position).getTripFare()).trim());

        } else {
            holder.llTripFare.setVisibility(View.GONE);
        }

        if(MyReceiptsActivity.list.get(position).getBookingCharge() !=  null && MyReceiptsActivity.list.get(position).getBookingCharge().equalsIgnoreCase("")) {
            holder.tvBookingChage.setText((context.getResources().getString(R.string.currency) + " " + MyReceiptsActivity.list.get(position).getBookingCharge()).trim());
        } else {
            holder.llBookingChage.setVisibility(View.GONE);
        }

        if(MyReceiptsActivity.list.get(position).getTax() != null && MyReceiptsActivity.list.get(position).getTax().equalsIgnoreCase("")) {
            holder.tvTax.setText((context.getResources().getString(R.string.currency) + " " + MyReceiptsActivity.list.get(position).getTax()).trim());
        } else {
            holder.llTax.setVisibility(View.GONE);
        }


        if (MyReceiptsActivity.list.get(position).getModel() != null && !MyReceiptsActivity.list.get(position).getModel().equalsIgnoreCase("")) {
            holder.tv_VehicleType.setText((MyReceiptsActivity.list.get(position).getModel()).trim());
        } else {
            holder.ll_VehicleType.setVisibility(View.GONE);
        }

        Log.e("call", "distance = " + MyReceiptsActivity.list.get(position).getTripDistance());

        if (MyReceiptsActivity.list.get(position).getTripDistance() != null && !MyReceiptsActivity.list.get(position).getTripDistance().equalsIgnoreCase("")) {
            String distance = String.format("%.2f", Float.parseFloat(MyReceiptsActivity.list.get(position).getTripDistance()));
            holder.tv_DistanceTravelled.setText((distance + " " + context.getResources().getString(R.string.km)).trim());
            //holder.tv_DistanceTravelled.setText(MyReceiptsActivity.list.get(position).getTripDistance());
        } else {
            holder.ll_DistanceTravelled.setVisibility(View.VISIBLE);
            holder.tv_DistanceTravelled.setText("0" + " " + context.getResources().getString(R.string.km));
        }


        Log.e("call", "tv_FareTotal = " + MyReceiptsActivity.list.get(position).getGrandTotal());

        if (MyReceiptsActivity.list.get(position).getGrandTotal() != null && !MyReceiptsActivity.list.get(position).getGrandTotal().equalsIgnoreCase("")) {
            holder.tv_FareTotal.setText((context.getResources().getString(R.string.currency) +
                    Double.parseDouble(MyReceiptsActivity.list.get(position).getGrandTotal())).trim());
        } else {
            holder.ll_FareTotal.setVisibility(View.VISIBLE);
            holder.tv_FareTotal.setText(context.getResources().getString(R.string.currency) + "0.00");
        }

        Log.e("call", "tv_DiscountApplied = " + MyReceiptsActivity.list.get(position).getDiscount());

        if (MyReceiptsActivity.list.get(position).getDiscount() != null &&
                !MyReceiptsActivity.list.get(position).getDiscount().equalsIgnoreCase("")) {
            holder.tv_DiscountApplied.setText((context.getResources().getString(R.string.currency) +
                    Double.parseDouble(MyReceiptsActivity.list.get(position).getDiscount())).trim());
        } else {
            holder.ll_DiscountApplied.setVisibility(View.VISIBLE);
            holder.tv_DiscountApplied.setText(context.getResources().getString(R.string.currency) + "0.00");
        }

        if (MyReceiptsActivity.list.get(position).getLoadingUnloadingCharge() != null &&
                !MyReceiptsActivity.list.get(position).getLoadingUnloadingCharge().equalsIgnoreCase("")) {
            holder.llLoadCharge.setVisibility(View.VISIBLE);
            holder.tvLoadCharge.setText(" "+(context.getString(R.string.currency) +
                    MyReceiptsActivity.list.get(position).getLoadingUnloadingCharge()).trim());
        } else {
            holder.llLoadCharge.setVisibility(View.GONE);
        }

        if (MyReceiptsActivity.list.get(position).getLoadingUnloadingTime() != null &&
                !MyReceiptsActivity.list.get(position).getLoadingUnloadingTime().equalsIgnoreCase("")) {
            holder.llLoadTime.setVisibility(View.VISIBLE);
            holder.tvLoadTime.setText(" "+(MyReceiptsActivity.list.get(position).getLoadingUnloadingTime() +
                    context.getString(R.string.min)).trim());
        } else {
            holder.llLoadTime.setVisibility(View.GONE);
        }

        Log.e("call", "tv_ChargedCard = " + MyReceiptsActivity.list.get(position).getPaymentType());
        if (MyReceiptsActivity.list.get(position).getPaymentType() != null && !MyReceiptsActivity.list.get(position).getPaymentType().equalsIgnoreCase("")) {
            holder.tv_ChargedCard.setText((MyReceiptsActivity.list.get(position).getPaymentType()).trim());
        } else {
            holder.ll_ChargedCard.setVisibility(View.GONE);
        }

        holder.tv_GetReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("call", "tv_GetReceipt clicked");

                sendEmail(position, MyReceiptsActivity.list.get(position).getShreUrl());

               /* if (Global.isNetworkconn(context)) {
                    Log.e("call", "getShreUrl = " + position + MyReceiptsActivity.list.get(position).getShreUrl());
                    if (MyReceiptsActivity.list.get(position).getShreUrl() != null && !MyReceiptsActivity.list.get(position).getShreUrl().equalsIgnoreCase("")) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.please_download_from_below_link) + " \n\n " + MyReceiptsActivity.list.get(position).getShreUrl());
                        context.startActivity(Intent.createChooser(shareIntent, context.getResources().getString(R.string.share_via)));
                    } else {
                        InternetDialog internetDialog = new InternetDialog(context);
                        internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong), context.getResources().getString(R.string.error_message));
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog(context.getResources().getString(R.string.please_check_internet_connection), context.getResources().getString(R.string.no_internet_connection));
                }*/
            }
        });
    }

    public void sendEmail(int position,String text) {
        if(Global.isNetworkconn(context))
        {
            if (MyReceiptsActivity.list.get(position).getShreUrl() != null &&
                    !MyReceiptsActivity.list.get(position).getShreUrl().equalsIgnoreCase("")) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                //String[] recipients={"mailto@gmail.com"};
                //intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT,"My Receipts");
                intent.putExtra(Intent.EXTRA_TEXT,context.getResources().getString(R.string.please_download_from_below_link) + " \n\n " +
                        text);
                //intent.putExtra(Intent.EXTRA_CC,"mailcc@gmail.com");
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                context.startActivity(Intent.createChooser(intent, "Send mail"));

            }
            else
                {
                InternetDialog internetDialog = new InternetDialog(context);
                internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong), context.getResources().getString(R.string.error_message));
            }
        }
        else
        {
            InternetDialog internetDialog = new InternetDialog(context);
            internetDialog.showDialog(context.getResources().getString(R.string.please_check_internet_connection), context.getResources().getString(R.string.no_internet_connection));
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return MyReceiptsActivity.list.size();
    }

}
