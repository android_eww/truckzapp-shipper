package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.BookLaterActivity;
import com.squareup.picasso.Picasso;

public class TransportServiceAdapter extends RecyclerView.Adapter<TransportServiceAdapter.ViewHolder> {

    private Context context;

    public TransportServiceAdapter(Context context) {
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_transport_service, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        viewHolder.tv_Name.setText(BookLaterActivity.transportService_beens.get(position).getName()+"");

        if (BookLaterActivity.transportService_beens.get(position).getImage()!=null && !BookLaterActivity.transportService_beens.get(position).getImage().equalsIgnoreCase(""))
        {
            viewHolder.iv_Image.setVisibility(View.VISIBLE);
            Picasso.with(context).load(BookLaterActivity.transportService_beens.get(position).getImage()).into(viewHolder.iv_Image);
        }
        else
        {
            viewHolder.iv_Image.setVisibility(View.INVISIBLE);
        }

        Log.e("call","pos = "+position+" = "+BookLaterActivity.transportService_beens.get(position).isSelected());

        if (BookLaterActivity.transportService_beens.get(position).isSelected())
        {
            viewHolder.ll_Row.setBackgroundColor(context.getResources().getColor(R.color.colorBlack));
            viewHolder.tv_Name.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }
        else
        {
            viewHolder.ll_Row.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            viewHolder.tv_Name.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }

        viewHolder.iv_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position!=(BookLaterActivity.transportService_beens.size()-1))
                {
                    for (int i=0; i<BookLaterActivity.transportService_beens.size(); i++)
                    {
                        if (position==i)
                        {
                            if (BookLaterActivity.transportService_beens.get(position).isSelected())
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(false);
                            }
                            else
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(true);
                            }
                        }
                        else
                        {
                            BookLaterActivity.transportService_beens.get(i).setSelected(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            }
        });

        viewHolder.ll_Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position!=(BookLaterActivity.transportService_beens.size()-1))
                {
                    for (int i=0; i<BookLaterActivity.transportService_beens.size(); i++)
                    {
                        if (position==i)
                        {
                            if (BookLaterActivity.transportService_beens.get(position).isSelected())
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(false);
                            }
                            else
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(true);
                            }
                        }
                        else
                        {
                            BookLaterActivity.transportService_beens.get(i).setSelected(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            }
        });

        viewHolder.ll_RowMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position!=(BookLaterActivity.transportService_beens.size()-1))
                {
                    for (int i=0; i<BookLaterActivity.transportService_beens.size(); i++)
                    {
                        if (position==i)
                        {
                            if (BookLaterActivity.transportService_beens.get(position).isSelected())
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(false);
                            }
                            else
                            {
                                BookLaterActivity.transportService_beens.get(position).setSelected(true);
                            }
                        }
                        else
                        {
                            BookLaterActivity.transportService_beens.get(i).setSelected(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return BookLaterActivity.transportService_beens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_Name;
        private ImageView iv_Image;
        private LinearLayout ll_Row,ll_RowMain;


        public ViewHolder(View view) {
            super(view);

            tv_Name = (TextView)view.findViewById(R.id.tv_Name);
            iv_Image = (ImageView)view.findViewById(R.id.iv_Image);
            ll_Row = (LinearLayout)view.findViewById(R.id.ll_Row);
            ll_RowMain = (LinearLayout)view.findViewById(R.id.ll_RowMain);
        }
    }
}
