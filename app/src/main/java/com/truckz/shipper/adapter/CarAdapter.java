package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.other.DialogClass;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CTextViewLight;

import java.text.DecimalFormat;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

//    private List<CarType_Been> carType_beens;
    private Context context;
    private DialogClass dialogClass;
    private AQuery aQuery;

    public CarAdapter(Context context) {//List<CarType_Been> carType_beens,
//        this.carType_beens = carType_beens;
        this.context = context;
        dialogClass = new DialogClass(context,0);
        aQuery = new AQuery(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_car, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        if (MainActivity.carType_beens.get(position).getAvai() > 0)
        {
            viewHolder.tv_Time.setText(MainActivity.getEstimateFare_beens.get(position).getDuration()+" min");
            if(MainActivity.getEstimateFare_beens.get(position).getTotal() != null && !MainActivity.getEstimateFare_beens.get(position).getTotal().equalsIgnoreCase("")
            && !MainActivity.getEstimateFare_beens.get(position).getTotal().equalsIgnoreCase("null"))
            {viewHolder.tv_Price.setText(context.getResources().getString(R.string.currency)+" "+commaFormat(Double.parseDouble(MainActivity.getEstimateFare_beens.get(position).getTotal())));}
            viewHolder.tvCapacity.setText(MainActivity.carType_beens.get(position).getCapacity() + " " +
                    context.getResources().getString(R.string.ton));
        }
        else
        {
//            viewHolder.tv_Time.setText("0 "+context.getResources().getString(R.string.min));
            viewHolder.tv_Time.setText("No Truck");
            viewHolder.tv_Price.setText(context.getResources().getString(R.string.currency)+" 0");
            //viewHolder.tvCapacity.setText("0 "+ context.getResources().getString(R.string.ton));
            viewHolder.tvCapacity.setText(MainActivity.carType_beens.get(position).getCapacity() + " " +
                    context.getResources().getString(R.string.ton));
        }

        viewHolder.tv_Name.setText(MainActivity.carType_beens.get(position).getName());

        if (MainActivity.carType_beens.get(position).getModelSizeImage()!=null &&
                !MainActivity.carType_beens.get(position).getModelSizeImage().equalsIgnoreCase(""))
        {
            Log.e("TAG","displayImage() iv_CarTwo:- " + MainActivity.carType_beens.get(position).getModelSizeImage());
            Picasso.with(context)
                    .load(MainActivity.carType_beens.get(position).getModelSizeImage())
                    .into(viewHolder.iv_Image);
        }
        else
        {
            viewHolder.iv_Image.setVisibility(View.INVISIBLE);
        }

        if (MainActivity.carType_beens.get(position).getSelectedCar()==-1)
        {
//            viewHolder.ll_Image.setBackgroundResource(R.drawable.circle_white_border);
            Picasso.with(context)
                    .load(MainActivity.carType_beens.get(position).getModelSizeImage())
                    .into(viewHolder.iv_Image);
        }
        else
        {
//            viewHolder.ll_Image.setBackgroundResource(R.drawable.circle_red_border);
            Picasso.with(context)
                    .load(MainActivity.carType_beens.get(position).getImage())
                    .into(viewHolder.iv_Image);
        }

        viewHolder.ll_Car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i=0; i<MainActivity.carType_beens.size(); i++)
                {
                    if (i==position)
                    {
                        MainActivity.carType_beens.get(position).setSelectedCar(position);
                    }
                    else
                    {
                        MainActivity.carType_beens.get(i).setSelectedCar(-1);
                    }
                }
                ((MainActivity)context).carClicked();
                notifyDataSetChanged();
            }
        });

        viewHolder.ll_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i=0; i<MainActivity.carType_beens.size(); i++)
                {
                    if (i == position)
                    {
                        MainActivity.carType_beens.get(position).setSelectedCar(position);
                    }
                    else
                    {
                        MainActivity.carType_beens.get(i).setSelectedCar(-1);
                    }
                }
                ((MainActivity)context).carClicked();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainActivity.carType_beens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout ll_Car,ll_row;
        private LinearLayout ll_Image;
        private ImageView iv_Image;
        private CTextViewBold tv_Available, tv_Time, tvCapacity, tv_Price, tv_Name;


        public ViewHolder(View view) {
            super(view);

            ll_Car = view.findViewById(R.id.ll_Car);
            ll_row = view.findViewById(R.id.ll_row);
            ll_Image= view.findViewById(R.id.ll_Image);

            iv_Image = view.findViewById(R.id.iv_Image);
            tv_Available = view.findViewById(R.id.tv_Available);
            tvCapacity = view.findViewById(R.id.tvCapacity);
            tv_Time = view.findViewById(R.id.tv_Time);
            tv_Price = view.findViewById(R.id.tv_Price);
            tv_Name = view.findViewById(R.id.tv_Name);
        }
    }

    public String commaFormat(Double x)
    {
        return new DecimalFormat("#,###.0").format(x);
    }
}
