package com.truckz.shipper.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.truckz.shipper.activity.MainActivity;
import com.truckz.shipper.activity.MyBookingActivity;
import com.truckz.shipper.R;
import com.truckz.shipper.activity.MyReceiptsActivity;
import com.truckz.shipper.been.Parcel_Been;
import com.truckz.shipper.been.PastBooking_Been;
import com.truckz.shipper.been.previousDue.History;
import com.truckz.shipper.comman.Common;
import com.truckz.shipper.comman.SessionSave;
import com.truckz.shipper.comman.TaxiUtil;
import com.truckz.shipper.comman.WebServiceAPI;
import com.truckz.shipper.fragment.PastBooking_Fragment;
import com.truckz.shipper.listner.OnLoadMoreListener;
import com.truckz.shipper.other.DialogClass;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.truckz.shipper.other.Global;
import com.truckz.shipper.other.InternetDialog;
import com.truckz.shipper.view.CTextViewLight;
import com.truckz.shipper.view.CTextViewMedium;
import com.truckz.shipper.view.CustomEditText;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PastBooking_Adapter extends ExpandableRecyclerView.Adapter<ExpandableRecyclerView.ViewHolder> {
    private Context context;
    int POSITION;
    String TAG = "PastBooking_Req";
    private AQuery aQuery;
    private DialogClass dialogClass;

    private LinearLayoutManager linearLayoutManager;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 2;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    onpayCliick onpayCliick;


    public PastBooking_Adapter(LinearLayoutManager layoutManager, ExpandableRecyclerView recyclerView, Context context, onpayCliick onpayCliick) {
        super(layoutManager);
        this.context = context;
        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 0);
        this.onpayCliick = onpayCliick;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                Log.e(TAG, "call totalItemCount:- " + totalItemCount);
                Log.e(TAG, "call lastVisibleItem:- " + lastVisibleItem);
                Log.e(TAG, "call isLoading:- " + isLoading);

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (MyBookingActivity.pastBooking_beens.size() > 0 &&
                            MyBookingActivity.pastBooking_beens.get(MyBookingActivity.pastBooking_beens.size() - 1) != null &&
                            onLoadMoreListener != null) {
                        if (MyBookingActivity.pastBooking_beens.size() < 10) {
                            isLoading = false;
                        } else {
                            onLoadMoreListener.onLoadMore();
                            isLoading = true;
                        }
                    }
                } else {
                    isLoading = false;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return MyBookingActivity.pastBooking_beens.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public ExpandableRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        this.context = parent.getContext();
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View v = inflater.inflate(R.layout.row_past_booking, parent, false);
//
//        ViewHolder vh = new ViewHolder(v);

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_past_booking, parent, false);
            Log.e(TAG, "VIEW_TYPE_ITEM data is load");
            return new MyViewHolder(view);

        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            Log.e(TAG, "VIEW_TYPE_LOADING Please wait data load");
            return new LoadingViewHolder(view);
        }


        return null;
    }

    @Override
    public void onBindViewHolder(final ExpandableRecyclerView.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);
        POSITION = position;

        if (viewHolder instanceof MyViewHolder) {
            final MyViewHolder holder = (MyViewHolder) viewHolder;

            if (MyBookingActivity.pastBooking_beens.get(position).getDropoffLocation() != null && !MyBookingActivity.pastBooking_beens.get(position).getDropoffLocation().equalsIgnoreCase("")) {
                holder.ll_DropoffLocation.setVisibility(View.VISIBLE);
                holder.tv_DropoffLocation.setText(MyBookingActivity.pastBooking_beens.get(position).getDropoffLocation());
            } else {
                holder.ll_DropoffLocation.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getDriverName() != null && !MyBookingActivity.pastBooking_beens.get(position).getDriverName().equalsIgnoreCase("") && !MyBookingActivity.pastBooking_beens.get(position).getDriverName().equalsIgnoreCase("null") && !MyBookingActivity.pastBooking_beens.get(position).getDriverName().equalsIgnoreCase("NULL")) {
                holder.tv_DriverName.setVisibility(View.VISIBLE);
                holder.view_driverName.setVisibility(View.VISIBLE);
                holder.tv_DriverName.setText(MyBookingActivity.pastBooking_beens.get(position).getDriverName());
            } else {
                holder.tv_DriverName.setVisibility(View.GONE);
                holder.view_driverName.setVisibility(View.GONE);

            }

            if (MyBookingActivity.pastBooking_beens.get(position).getId() != null && !MyBookingActivity.pastBooking_beens.get(position).getId().equalsIgnoreCase("")) {
                holder.tv_BookingId.setVisibility(View.VISIBLE);
                if (MyBookingActivity.pastBooking_beens.get(position).getBookingType() != null && !MyBookingActivity.pastBooking_beens.get(position).getBookingType().equalsIgnoreCase("")) {

                    if (MyBookingActivity.pastBooking_beens.get(position).getBookingType().equalsIgnoreCase("Book Now")) {

                        holder.tv_BookingId.setText("N" + MyBookingActivity.pastBooking_beens.get(position).getId());
                    } else if (MyBookingActivity.pastBooking_beens.get(position).getBookingType().equalsIgnoreCase("Book Later")) {

                        holder.tv_BookingId.setText("L" + MyBookingActivity.pastBooking_beens.get(position).getId());
                    }
                }
            } else {
                holder.tv_BookingId.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getCreatedDate() != null && !MyBookingActivity.pastBooking_beens.get(position).getCreatedDate().equalsIgnoreCase("")) {
                holder.ll_Date.setVisibility(View.VISIBLE);
                holder.tv_Date.setText(MyBookingActivity.pastBooking_beens.get(position).getCreatedDate());
            } else {
                holder.ll_Date.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getPickupLocation() != null && !MyBookingActivity.pastBooking_beens.get(position).getPickupLocation().equalsIgnoreCase("")) {
                holder.ll_PickupLocation.setVisibility(View.VISIBLE);
                holder.tv_PickupLocation.setText(MyBookingActivity.pastBooking_beens.get(position).getPickupLocation());
            } else {
                holder.ll_PickupLocation.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getArrivedTime() != null && !MyBookingActivity.pastBooking_beens.get(position).getArrivedTime().equalsIgnoreCase("")) {
                holder.ll_PickupTime.setVisibility(View.VISIBLE);
                String dateString = getDate(Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getArrivedTime() + "000"), "dd-MMM-yyyy hh:mm a"); // dd-MM-yyyy HH:mm a and HH/mm  yyyy/MM/dd
                holder.tv_PickupTime.setText(dateString);
            } else {
                holder.ll_PickupTime.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getDropTime() != null && !MyBookingActivity.pastBooking_beens.get(position).getDropTime().equalsIgnoreCase("")) {
                holder.ll_DropoffTime.setVisibility(View.VISIBLE);
                String dateString = getDate(Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getDropTime() + "000"), "dd-MMM-yyyy hh:mm a");
                holder.tv_DropoffTime.setText(dateString);
            } else {
                holder.ll_DropoffTime.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getModel() != null && !MyBookingActivity.pastBooking_beens.get(position).getModel().equalsIgnoreCase("")) {
                holder.ll_VehicleType.setVisibility(View.VISIBLE);
                holder.tv_VehicleType.setText(MyBookingActivity.pastBooking_beens.get(position).getModel());
            } else {
                holder.ll_VehicleType.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getParcel_beens() != null) {
                Parcel_Been Parcel = MyBookingActivity.pastBooking_beens.get(position).getParcel_beens().get(0);

                if (Parcel != null && Parcel.getName() != null && !Parcel.getName().trim().equalsIgnoreCase("")) {
                    holder.ll_ParcelType.setVisibility(View.VISIBLE);
                    holder.tv_ParcelType.setText(Parcel.getName().trim());
                } else {
                    holder.ll_ParcelType.setVisibility(View.GONE);
                }
            } else {
                holder.ll_ParcelType.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getParcelWeight() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getParcelWeight().equalsIgnoreCase("") &&
                    !MyBookingActivity.pastBooking_beens.get(position).getParcelWeight().equalsIgnoreCase("null")) {
                holder.ll_ParcelWeight.setVisibility(View.VISIBLE);
                holder.tv_ParcelWeight.setText(MyBookingActivity.pastBooking_beens.get(position).getParcelWeight() + " " +
                        context.getString(R.string.kg));
            } else {
                holder.ll_ParcelWeight.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getTripDistance() != null && !MyBookingActivity.pastBooking_beens.get(position).getTripDistance().equalsIgnoreCase("")) {
                holder.ll_DistanceTravelled.setVisibility(View.VISIBLE);
                String distance = String.format("%.1f", Float.parseFloat(MyBookingActivity.pastBooking_beens.get(position).getTripDistance()));
                holder.tv_DistanceTravelled.setText(distance + " " + context.getResources().getString(R.string.km));
            } else {
                holder.ll_DistanceTravelled.setVisibility(View.GONE);
                holder.tv_DistanceTravelled.setText(0.0 + " " + context.getResources().getString(R.string.km));
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getStatus() != null && !MyBookingActivity.pastBooking_beens.get(position).getStatus().equalsIgnoreCase("")) {
                holder.ll_Trip_Status.setVisibility(View.VISIBLE);
                holder.tv_Trip_Status.setText(WordUtils.capitalize(MyBookingActivity.pastBooking_beens.get(position).getStatus()));
                if (MyBookingActivity.pastBooking_beens.get(position).getStatus().equalsIgnoreCase("completed")) {
                    holder.ll_DistanceTravelled.setVisibility(View.VISIBLE);
                } else {
                    holder.ll_DistanceTravelled.setVisibility(View.GONE);
                }
            } else {
                holder.ll_Trip_Status.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getTripFare() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getTripFare().equalsIgnoreCase("") &&
                    !MyBookingActivity.pastBooking_beens.get(position).getTripFare().equalsIgnoreCase("null") &&
                    MyBookingActivity.pastBooking_beens.get(position).getDistanceFare() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getDistanceFare().equalsIgnoreCase("") &&
                    !MyBookingActivity.pastBooking_beens.get(position).getDistanceFare().equalsIgnoreCase("null")) {
                holder.ll_TripFare.setVisibility(View.VISIBLE);

                Double tripCharges = Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getDistanceFare()) +
                        Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getTripFare());

                if(MyBookingActivity.pastBooking_beens.get(position).getBookingCharge() != null &&
                        !MyBookingActivity.pastBooking_beens.get(position).getBookingCharge().equalsIgnoreCase("") &&
                        !MyBookingActivity.pastBooking_beens.get(position).getBookingCharge().equalsIgnoreCase("null"))
                {
                    tripCharges = tripCharges + Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getBookingCharge());
                }
                if (MyBookingActivity.pastBooking_beens.get(position).getNightFare() != null &&
                        !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("")
                        && !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("null")
                        && !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("0"))
                {
                    tripCharges = tripCharges + Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getNightFare());
                }

                //String.format("%.1f", tripCharges)
                holder.tv_TripFare.setText(context.getString(R.string.currency) + commaFormat(tripCharges));
            } else {
                holder.ll_TripFare.setVisibility(View.GONE);
            }

            if(MyBookingActivity.pastBooking_beens.get(position).getCancellationFee() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getCancellationFee().equalsIgnoreCase("")
            && !MyBookingActivity.pastBooking_beens.get(position).getCancellationFee().equalsIgnoreCase("null"))
            {
                holder.ll_cancelFee.setVisibility(View.VISIBLE);

                holder.tv_CancelFee.setText(context.getString(R.string.currency) + String.format("%.1f",Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getCancellationFee())));
            }
            else {
                holder.ll_cancelFee.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getNightFare() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("")
            && !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("null")
                    && !MyBookingActivity.pastBooking_beens.get(position).getNightFare().equalsIgnoreCase("0")) {
                //holder.ll_NightFare.setVisibility(View.VISIBLE);
                holder.tv_NightFare.setText(context.getString(R.string.currency) + MyBookingActivity.pastBooking_beens.get(position).getNightFare());
            } else {
                holder.ll_NightFare.setVisibility(View.GONE);
            }

            if(MyBookingActivity.pastBooking_beens.get(position).getSpecialExtraCharge() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getSpecialExtraCharge().equalsIgnoreCase("")
                    && !MyBookingActivity.pastBooking_beens.get(position).getSpecialExtraCharge().equalsIgnoreCase("null")
            && !MyBookingActivity.pastBooking_beens.get(position).getSpecialExtraCharge().equalsIgnoreCase("0"))
            {
                holder.ll_surCharge.setVisibility(View.VISIBLE);
                holder.tv_Surcharge.setText(context.getString(R.string.currency) + commaFormat(Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getSpecialExtraCharge())));
            }
            else
            {
                holder.ll_surCharge.setVisibility(View.GONE);
            }

            if(MyBookingActivity.pastBooking_beens.get(position).getSubTotal() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getSubTotal().equalsIgnoreCase("")
                    && !MyBookingActivity.pastBooking_beens.get(position).getSubTotal().equalsIgnoreCase("null"))
            {
                holder.tv_SubTotal.setText(context.getString(R.string.currency) + commaFormat(Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getSubTotal())));
            }
            else
            {holder.ll_subTotal.setVisibility(View.GONE);}

            if (MyBookingActivity.pastBooking_beens.get(position).getTollFee() != null && !MyBookingActivity.pastBooking_beens.get(position).getTollFee().equalsIgnoreCase("")) {
                holder.ll_TollFee.setVisibility(View.GONE);
                holder.tv_TollFee.setText(MyBookingActivity.pastBooking_beens.get(position).getTollFee());
            } else {
                holder.ll_TollFee.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getWaitingTimeCost() != null && !MyBookingActivity.pastBooking_beens.get(position).getWaitingTimeCost().equalsIgnoreCase("")) {
                holder.ll_WaitingCost.setVisibility(View.GONE);
                holder.tv_WaitingCost.setText(MyBookingActivity.pastBooking_beens.get(position).getWaitingTimeCost());
            } else {
                holder.ll_WaitingCost.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getWaitingTime() != null && !MyBookingActivity.pastBooking_beens.get(position).getWaitingTime().equalsIgnoreCase("")) {
                holder.ll_WaitingTime.setVisibility(View.GONE);
//            holder.tv_WaitingTime.setText(MyBookingActivity.pastBooking_beens.get(position).getWaitingTime());
                long totalSecs = Long.parseLong(MyBookingActivity.pastBooking_beens.get(position).getWaitingTime());
                long hours = totalSecs / 3600;
                long minutes = (totalSecs % 3600) / 60;
                long seconds = totalSecs % 60;

                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
                holder.tv_WaitingTime.setText(timeString);
            } else {
                holder.ll_WaitingTime.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getBookingCharge() != null && !MyBookingActivity.pastBooking_beens.get(position).getBookingCharge().equalsIgnoreCase("")) {
                holder.ll_BookingCharge.setVisibility(View.GONE);
                holder.tv_BookingCharge.setText(context.getString(R.string.currency) +
                        MyBookingActivity.pastBooking_beens.get(position).getBookingCharge());
            } else {
                holder.ll_BookingCharge.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getTax() != null
                    && !MyBookingActivity.pastBooking_beens.get(position).getTax().equalsIgnoreCase("")
                    && !MyBookingActivity.pastBooking_beens.get(position).getTax().equalsIgnoreCase("null")) {
                holder.ll_Tax.setVisibility(View.VISIBLE);
                holder.tv_Tax.setText(context.getString(R.string.currency) +
                        String.format("%.1f",Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getTax())));
            } else {
                holder.ll_Tax.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getDiscount() != null && !MyBookingActivity.pastBooking_beens.get(position).getDiscount().equalsIgnoreCase("")
            && !MyBookingActivity.pastBooking_beens.get(position).getDiscount().equalsIgnoreCase("0")) {
                holder.ll_Discount.setVisibility(View.VISIBLE);
                holder.tv_Discount.setText(context.getString(R.string.currency) +
                        MyBookingActivity.pastBooking_beens.get(position).getDiscount());
            } else {
                holder.ll_Discount.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingCharge() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingCharge().equalsIgnoreCase("")
            && !MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingCharge().equalsIgnoreCase("null")) {
                holder.llLoadCharge.setVisibility(View.VISIBLE);
                holder.tvLoadCharge.setText(context.getString(R.string.currency) +String.format("%.1f",Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingCharge())));
            } else {
                holder.llLoadCharge.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingTime() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingTime().equalsIgnoreCase("")) {
                holder.llLoadTime.setVisibility(View.VISIBLE);
                holder.tvLoadTime.setText(MyBookingActivity.pastBooking_beens.get(position).getLoadingUnloadingTime() +
                        context.getString(R.string.min));
            } else {
                holder.llLoadTime.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getPaymentType() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getPaymentType().equalsIgnoreCase("")) {
                holder.ll_PaymentType.setVisibility(View.VISIBLE);
                if(MyBookingActivity.pastBooking_beens.get(position).getPaymentType().equalsIgnoreCase("paytm"))
                {
                    holder.tv_PaymentType.setText("Paytm");
                }
                else
                {holder.tv_PaymentType.setText(MyBookingActivity.pastBooking_beens.get(position).getPaymentType());}
            } else {
                holder.ll_PaymentType.setVisibility(View.GONE);
            }

            if(MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus().equalsIgnoreCase("")
            && !MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus().equalsIgnoreCase("null"))
            {
                holder.tv_PaymentStatus.setText(MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus());
            }
            else {
                holder.ll_PaymentStatus.setVisibility(View.GONE);
            }

            double prviousDue = 0;
            if(MyBookingActivity.pastBooking_beens.get(position).getPreviousDue() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getPreviousDue().equalsIgnoreCase("")
                    && !MyBookingActivity.pastBooking_beens.get(position).getPreviousDue().equalsIgnoreCase("null")
            && !MyBookingActivity.pastBooking_beens.get(position).getPreviousDue().equalsIgnoreCase("0"))
            {
                prviousDue = Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getPreviousDue());
                holder.tv_previousDue.setText(context.getString(R.string.currency)+prviousDue);
                holder.ll_previousDue.setVisibility(View.VISIBLE);
            }
            else
            {holder.ll_previousDue.setVisibility(View.GONE);}

            if (MyBookingActivity.pastBooking_beens.get(position).getGrandTotal() != null && !MyBookingActivity.pastBooking_beens.get(position).getGrandTotal().equalsIgnoreCase("")) {
                holder.ll_Total.setVisibility(View.VISIBLE);

                double ttt = Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getGrandTotal()) /*+ prviousDue*/;//change date 29-7-19

                //holder.tv_Total.setText(context.getString(R.string.currency) + String.format("%.1f",ttt));
                holder.tv_Total.setText(context.getString(R.string.currency) + commaFormat(ttt));
            } else {
                holder.ll_Total.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getCarDetails_VehicleRegistrationNo() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getCarDetails_VehicleRegistrationNo().equalsIgnoreCase("")) {
                holder.ll_vehicleno.setVisibility(View.VISIBLE);
                holder.tv_vehicleno.setText(MyBookingActivity.pastBooking_beens.get(position).getCarDetails_VehicleRegistrationNo());
            } else {
                holder.ll_vehicleno.setVisibility(View.GONE);
            }

            if (MyBookingActivity.pastBooking_beens.get(position).getStatus() != null &&
                    MyBookingActivity.pastBooking_beens.get(position).getStatus().equalsIgnoreCase("completed")) {
                holder.tv_GetReceipt.setVisibility(View.VISIBLE);
                //holder.ll_PaymentType.setVisibility(View.VISIBLE);
            } else {
                holder.tv_GetReceipt.setVisibility(View.GONE);
                //holder.ll_PaymentType.setVisibility(View.GONE);
            }

            if(MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus() != null &&
                    !MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus().equalsIgnoreCase("") &&
                    !MyBookingActivity.pastBooking_beens.get(position).getPaymentStatus().equalsIgnoreCase("success"))
            {
                if(MyBookingActivity.pastBooking_beens.get(position).getPreviousDue() != null && !MyBookingActivity.pastBooking_beens.get(position).getPreviousDue().equalsIgnoreCase("")
                && !MyBookingActivity.pastBooking_beens.get(position).getPreviousDue().equalsIgnoreCase("null") && Double.parseDouble(MyBookingActivity.pastBooking_beens.get(position).getPreviousDue()) > 0)
                {
                    holder.tv_GetReceipt.setVisibility(View.GONE);
                    holder.ll_pay.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.ll_pay.setVisibility(View.GONE);
                }
            }
            else
            {
                holder.ll_pay.setVisibility(View.GONE);
            }

            holder.ll_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayCliick.onPayButton(MyBookingActivity.pastBooking_beens.get(position),position);
                }
            });

            holder.tv_GetReceipt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "tv_GetReceipt onClick()");

//                    sendEmail(position);
                    openDialogForEmail(position);
                }
            });

        } else if (viewHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;

            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return MyBookingActivity.pastBooking_beens.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        public ProgressBar progressBar;

        private ExpandableItem expandableItem;

        public LoadingViewHolder(View view) {
            super(view);

            layout = view;
            expandableItem = (ExpandableItem) view.findViewById(R.id.row);

            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    public class MyViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        //header
        private LinearLayout ll_DropoffLocation, ll_Date,ll_subTotal;
        public CTextViewLight tv_DropoffLocation, tv_Date, tv_BookingId, tv_DriverName;
        public View view_driverName;

        //content
        private LinearLayout ll_PickupLocation, ll_PickupTime, ll_DropoffTime, ll_VehicleType, ll_ParcelType, ll_DistanceTravelled, ll_TripFare,ll_surCharge,ll_cancelFee;
        private LinearLayout ll_NightFare, ll_TollFee, ll_WaitingCost, ll_WaitingTime, ll_BookingCharge, ll_Tax, ll_Discount,ll_previousDue,
                llLoadTime, llLoadCharge, ll_PaymentType, ll_Total, ll_Trip_Status, ll_ParcelWeight, ll_vehicleno,ll_PaymentStatus,ll_pay;
        private CTextViewLight tv_PickupLocation, tv_PickupTime, tv_DropoffTime, tv_VehicleType, tv_ParcelType, tv_DistanceTravelled, tv_TripFare, tv_NightFare, tv_TollFee, tv_Trip_Status,tv_previousDue;
        private CTextViewLight tv_WaitingCost, tv_WaitingTime, tv_BookingCharge, tv_Tax, tv_Discount,
                tvLoadTime, tvLoadCharge, tv_PaymentType, tv_Total, tv_ParcelWeight, tv_vehicleno,tv_Surcharge,tv_SubTotal,tv_CancelFee,tv_PaymentStatus;
        public CTextViewMedium tv_GetReceipt;

        private ExpandableItem expandableItem;

        public MyViewHolder(View view) {
            super(view);

            layout = view;
            expandableItem = (ExpandableItem) view.findViewById(R.id.row);

            //header

            ll_DropoffLocation = expandableItem.findViewById(R.id.ll_DropoffLocation);
            ll_Date = expandableItem.findViewById(R.id.ll_Date);
            ll_PaymentStatus = expandableItem.findViewById(R.id.ll_PaymentStatus);
            ll_pay = expandableItem.findViewById(R.id.ll_pay);

            tv_DropoffLocation = expandableItem.findViewById(R.id.tv_DropoffLocation);
            tv_Date = expandableItem.findViewById(R.id.tv_Date);
            tv_BookingId = expandableItem.findViewById(R.id.tv_BookingId);
            tv_DriverName = expandableItem.findViewById(R.id.tv_DriverName);
            view_driverName = expandableItem.findViewById(R.id.view_driverName);
            tv_GetReceipt = expandableItem.findViewById(R.id.tv_GetReceipt);
            tv_Surcharge = expandableItem.findViewById(R.id.tv_Surcharge);
            ll_surCharge = expandableItem.findViewById(R.id.ll_surCharge);
            tv_SubTotal = expandableItem.findViewById(R.id.tv_SubTotal);
            tv_previousDue = expandableItem.findViewById(R.id.tv_previousDue);
            ll_previousDue = expandableItem.findViewById(R.id.ll_previousDue);
            tv_CancelFee = expandableItem.findViewById(R.id.tv_CancelFee);
            ll_cancelFee = expandableItem.findViewById(R.id.ll_cancelFee);
            ll_subTotal = expandableItem.findViewById(R.id.ll_subTotal);
            tv_PaymentStatus = expandableItem.findViewById(R.id.tv_PaymentStatus);


            //content

            ll_PickupLocation = expandableItem.findViewById(R.id.ll_PickupLocation);
            ll_PickupTime = expandableItem.findViewById(R.id.ll_PickupTime);
            ll_DropoffTime = expandableItem.findViewById(R.id.ll_DropoffTime);
            ll_VehicleType = expandableItem.findViewById(R.id.ll_VehicleType);
            ll_ParcelType = expandableItem.findViewById(R.id.ll_ParcelType);
            ll_DistanceTravelled = expandableItem.findViewById(R.id.ll_DistanceTravelled);
            ll_TripFare = expandableItem.findViewById(R.id.ll_TripFare);
            ll_NightFare = expandableItem.findViewById(R.id.ll_NightFare);
            ll_TollFee = expandableItem.findViewById(R.id.ll_TollFee);
            ll_WaitingCost = expandableItem.findViewById(R.id.ll_WaitingCost);
            ll_WaitingTime = expandableItem.findViewById(R.id.ll_WaitingTime);
            ll_BookingCharge = expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_BookingCharge.setVisibility(View.GONE);
            ll_Tax = expandableItem.findViewById(R.id.ll_Tax);
            ll_Discount = expandableItem.findViewById(R.id.ll_Discount);
            llLoadTime = expandableItem.findViewById(R.id.llLoadTime);
            llLoadCharge = expandableItem.findViewById(R.id.llLoadCharge);
            ll_PaymentType = expandableItem.findViewById(R.id.ll_PaymentType);
            ll_Total = expandableItem.findViewById(R.id.ll_Total);
            ll_Trip_Status = expandableItem.findViewById(R.id.ll_Trip_Status);
            ll_ParcelWeight = expandableItem.findViewById(R.id.ll_ParcelWeight);
            ll_vehicleno = expandableItem.findViewById(R.id.ll_vehicleNo);

            tv_PickupLocation = expandableItem.findViewById(R.id.tv_PickupLocation);
            tv_PickupTime = expandableItem.findViewById(R.id.tv_PickupTime);
            tv_DropoffTime = expandableItem.findViewById(R.id.tv_DropoffTime);
            tv_VehicleType = expandableItem.findViewById(R.id.tv_VehicleType);
            tv_ParcelType = expandableItem.findViewById(R.id.tv_ParcelType);
            tv_DistanceTravelled = expandableItem.findViewById(R.id.tv_DistanceTravelled);
            tv_TripFare = expandableItem.findViewById(R.id.tv_TripFare);
            tv_NightFare = expandableItem.findViewById(R.id.tv_NightFare);
            tv_TollFee = expandableItem.findViewById(R.id.tv_TollFee);
            tv_WaitingCost = expandableItem.findViewById(R.id.tv_WaitingCost);
            tv_WaitingTime = expandableItem.findViewById(R.id.tv_WaitingTime);
            tv_BookingCharge = expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_Tax = expandableItem.findViewById(R.id.tv_Tax);
            tv_Discount = expandableItem.findViewById(R.id.tv_Discount);
            tvLoadTime = expandableItem.findViewById(R.id.tvLoadTime);
            tvLoadCharge = expandableItem.findViewById(R.id.tvLoadCharge);
            tv_PaymentType = expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Total = expandableItem.findViewById(R.id.tv_Total);
            tv_Trip_Status = expandableItem.findViewById(R.id.tv_Trip_Status);
            tv_ParcelWeight = expandableItem.findViewById(R.id.tv_ParcelWeight);
            tv_vehicleno = expandableItem.findViewById(R.id.tv_vehicleNo);
        }
    }

    private void openDialogForEmail(final int position) {
        Log.e(TAG, "openDialogForEmail() position:- " + position);

        final Dialog dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_favorite_option_other);

        LinearLayout dialog_ok_layout;
        ImageView dialog_close, ivClearEmail;
        final CustomEditText etAddress;

        dialog_ok_layout = dialog.findViewById(R.id.dialog_ok_layout);
        dialog_close = dialog.findViewById(R.id.dialog_close);
        ivClearEmail = dialog.findViewById(R.id.ivClearEmail);
        ivClearEmail.setVisibility(View.VISIBLE);
        etAddress = dialog.findViewById(R.id.etAddressTitle);

        etAddress.setText(SessionSave.getUserSession(Common.USER_PREFERENCE_KEY_EMAIL, context));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ivClearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAddress.setText("");
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Global.isNetworkconn(context)) {
                    if (!TextUtils.isEmpty(etAddress.getText().toString().trim())) {
                        if (isValidEmail(etAddress.getText().toString().trim())) {
                            dialog.dismiss();
                            callSendRecipe(etAddress.getText().toString().trim(), position);
                        } else {
                            etAddress.setError("Please enter valid email");
                        }
                    } else {
                        etAddress.setError("Please enter email");
                    }
                } else {
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog(context.getResources().getString(R.string.please_check_internet_connection),
                            context.getResources().getString(R.string.no_internet_connection));
                }
            }
        });

        dialog.show();
    }

    private boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callSendRecipe(final String etAddress, final int position) {
        Log.e(TAG, "callSendRecipe() position:- " + position);
        Log.e(TAG, "callSendRecipe() etAddress:- " + etAddress);

        String url = WebServiceAPI.API_GET_RECIPE;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_BOOKING_ID, MyBookingActivity.pastBooking_beens.get(position).getId());
        params.put(WebServiceAPI.PARAM_BOOKING_TYPE, MyBookingActivity.pastBooking_beens.get(position).getBookingType());
        params.put(WebServiceAPI.PARAM_SENDER_ADDRESS, etAddress.trim());

        Log.e(TAG, "callSendRecipe() url = " + url);
        Log.e(TAG, "callSendRecipe() params = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "callSendRecipe() responseCode = " + responseCode);
                    Log.e(TAG, "callSendRecipe() Response  = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                String message = "Email send successfully.";
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(context);
                                internetDialog.showDialog(message, context.getResources().getString(R.string.success_message));
                            } else {
                                String message = context.getResources().getString(R.string.something_went_wrong);
                                if (json.has("message")) {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                InternetDialog internetDialog = new InternetDialog(context);
                                internetDialog.showDialog(message, context.getResources().getString(R.string.error_message));
                            }
                        } else {
                            dialogClass.hideDialog();
                            InternetDialog internetDialog = new InternetDialog(context);
                            internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong),
                                    context.getResources().getString(R.string.error_message));
                        }
                    } else {
                        dialogClass.hideDialog();
                        InternetDialog internetDialog = new InternetDialog(context);
                        internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong),
                                context.getResources().getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "callSendRecipe() Exception " + e.toString());
                    dialogClass.hideDialog();
                    InternetDialog internetDialog = new InternetDialog(context);
                    internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong),
                            context.getResources().getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

//    public void sendEmail(int position)
//    {
//        if(Global.isNetworkconn(context))
//        {
//            if (MyBookingActivity.pastBooking_beens.get(position).getShareUrl() != null &&
//                    !MyBookingActivity.pastBooking_beens.get(position).getShareUrl().equalsIgnoreCase("") &&
//                    !MyBookingActivity.pastBooking_beens.get(position).getShareUrl().equalsIgnoreCase("null"))
//            {
//                Intent intent=new Intent(Intent.ACTION_SEND);
//                //String[] recipients={"mailto@gmail.com"};
//                //intent.putExtra(Intent.EXTRA_EMAIL, recipients);
//                intent.putExtra(Intent.EXTRA_SUBJECT,"My Receipts");
//                intent.putExtra(Intent.EXTRA_TEXT,context.getResources().getString(R.string.please_download_from_below_link) + " \n\n " +
//                        MyBookingActivity.pastBooking_beens.get(position).getShareUrl());
//                //intent.putExtra(Intent.EXTRA_CC,"mailcc@gmail.com");
//                intent.setType("text/html");
//                intent.setPackage("com.google.android.gm");
//                context.startActivity(Intent.createChooser(intent, "Send mail"));
//
//            }
//            else
//            {
//                InternetDialog internetDialog = new InternetDialog(context);
//                internetDialog.showDialog(context.getResources().getString(R.string.something_went_wrong),
//                        context.getResources().getString(R.string.error_message));
//            }
//        }
//        else
//        {
//            InternetDialog internetDialog = new InternetDialog(context);
//            internetDialog.showDialog(context.getResources().getString(R.string.please_check_internet_connection),
//                    context.getResources().getString(R.string.no_internet_connection));
//        }
//    }

    public String commaFormat(Double x)
    {
        return new DecimalFormat("#,###.0").format(x);
    }

    public interface onpayCliick
    {
        void onPayButton(PastBooking_Been history, int position);
    }
}
