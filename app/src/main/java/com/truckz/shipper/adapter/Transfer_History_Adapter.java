package com.truckz.shipper.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;
import com.truckz.shipper.been.Transfer_History_Been;
import java.util.List;

public class Transfer_History_Adapter extends RecyclerView.Adapter<Transfer_History_Adapter.MyViewHolder> {

    private Context mContext;
    private List<Transfer_History_Been> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_historyName, tv_historyDateTime, tv_historyMoney,tv_Status,tv_TransactionFailPending;
        private LinearLayout rl_row_card;

        public MyViewHolder(View view) {
            super(view);

            tv_historyName = (TextView) view.findViewById(R.id.tv_historyName);
            tv_historyDateTime = (TextView) view.findViewById(R.id.tv_historyDateTime);
            tv_historyMoney = (TextView) view.findViewById(R.id.tv_historyMoney);
            tv_Status = (TextView) view.findViewById(R.id.tv_status);
            tv_TransactionFailPending = (TextView) view.findViewById(R.id.tv_TransactionFailPending);
            rl_row_card = (LinearLayout) view.findViewById(R.id.rl_row_card);
        }
    }

    public Transfer_History_Adapter(Context mContext, List<Transfer_History_Been> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transfer_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        holder.tv_historyName.setText(list.get(position).getDescription());
        holder.tv_historyDateTime.setText(list.get(position).getUpdatedDate());

        if (list.get(position).getAmount()!=null && !list.get(position).getAmount().equalsIgnoreCase(""))
        {
            float total = Float.parseFloat(list.get(position).getAmount());
            holder.tv_historyMoney.setText(list.get(position).getType()+mContext.getResources().getString(R.string.currency)+String.format("%.2f",total));
        }
        else
        {
            holder.tv_historyMoney.setText("");
        }


        if (list.get(position).getType()!=null && list.get(position).getType().equalsIgnoreCase("+"))
        {
            holder.tv_Status.setVisibility(View.GONE);
            if (list.get(position).getStatus().equalsIgnoreCase("completed") || list.get(position).getStatus().equalsIgnoreCase(""))
            {
                holder.tv_TransactionFailPending.setVisibility(View.GONE);
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorGreen));
            }
            else
            {
                holder.tv_TransactionFailPending.setVisibility(View.VISIBLE);
                holder.tv_TransactionFailPending.setText(mContext.getResources().getString(R.string.transaction_failed));
                holder.tv_TransactionFailPending.setTextColor(ContextCompat.getColor(mContext, R.color.colorThemeRed));
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorThemeRed));
            }
        }
        else
        {
            if (list.get(position).getStatus().equalsIgnoreCase("completed") || list.get(position).getStatus().equalsIgnoreCase(""))
            {
                holder.tv_TransactionFailPending.setVisibility(View.GONE);
                holder.tv_Status.setVisibility(View.GONE);
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            }
            else if (list.get(position).getStatus().equalsIgnoreCase("pending"))
            {
                holder.tv_TransactionFailPending.setVisibility(View.VISIBLE);
                holder.tv_TransactionFailPending.setText(mContext.getResources().getString(R.string.transaction_pending));
                holder.tv_Status.setVisibility(View.GONE);
                holder.tv_Status.setText(list.get(position).getStatus());
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorThemeRed));
            }
            else
            {
                holder.tv_TransactionFailPending.setVisibility(View.VISIBLE);
                holder.tv_TransactionFailPending.setText(mContext.getResources().getString(R.string.transaction_failed));
                holder.tv_Status.setVisibility(View.GONE);
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorThemeRed));
                holder.tv_TransactionFailPending.setTextColor(ContextCompat.getColor(mContext, R.color.colorThemeRed));
            }
        }

        holder.rl_row_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}