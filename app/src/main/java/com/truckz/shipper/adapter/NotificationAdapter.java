package com.truckz.shipper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.truckz.shipper.R;
import com.truckz.shipper.activity.NotificationActivity;
import com.truckz.shipper.been.NotificationBeen;
import com.truckz.shipper.view.CTextViewBold;
import com.truckz.shipper.view.CTextViewLight;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    public Context context;
    public List<NotificationBeen> list;

    public NotificationAdapter(Context context, List<NotificationBeen> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.notification_list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //2019-05-09 09:39:03
//        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
//
//        String dateString = "";
//
//        try {
//            Date date = simpleDateFormat.parse(list.get(position).getCreatedDate());
//            dateString = simpleDateFormat1.format(date);
//        } catch (Exception e) {
//
//        }

        holder.txt_date.setText(list.get(position).getCreatedDate());
        holder.txt_title.setText(list.get(position).getPushTitle());
        holder.txt_des.setText(list.get(position).getPushMessage());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CTextViewBold txt_title;
        public CTextViewLight txt_des, txt_date;


        public ViewHolder(View itemView) {
            super(itemView);

            txt_title = itemView.findViewById(R.id.txt_title);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_des = itemView.findViewById(R.id.txt_des);
        }
    }
}
