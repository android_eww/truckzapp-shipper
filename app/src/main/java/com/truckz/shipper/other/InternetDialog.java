package com.truckz.shipper.other;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truckz.shipper.R;

public class InternetDialog {

    private Context context;
    private Dialog dialog;
    private ImageView ivLoder;
    dialogPositiveClick dialogPositiveClick;

    public void setListener(dialogPositiveClick dialogPositiveClick)
    {
        this.dialogPositiveClick = dialogPositiveClick;
    }

    public InternetDialog(Context context)
    {
        this.context = context;
    }

    public void showDialog(String message, String title)
    {
        showPopup(message,title,true,true);
    }

    public void showDialog(String message, String title,boolean isCancelable,boolean isVisivle)
    {
        showPopup(message,title,isCancelable,isVisivle);
    }

    //******************************** showPopup() *****************************************************************************************
    private void showPopup(String message,String title,boolean isCancelable,boolean isVisivle) {

        dialog = new Dialog(context,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(isCancelable);
        dialog.setContentView(R.layout.dialog_my_class);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        tv_Message.setText(message);
        tv_title.setText(title);

        if(!isVisivle)
        {
            iv_close.setVisibility(View.GONE);
        }

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dialogPositiveClick != null)
                {
                    dialogPositiveClick.onOkClick();
                }
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialogPositiveClick != null)
                {
                    dialogPositiveClick.onOkClick();
                }
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

//                if (MainActivity.ringTone!=null)
//                {
//                    MainActivity.ringTone.stop();
//                }
            }
        });

        dialog.show();

    }//End...

    public boolean isShowing()
    {
        boolean flag = false;

        if (dialog!=null && dialog.isShowing())
        {
            flag = true;
        }

        return flag;
    }

    public void hideDialog()
    {
        if (dialog!=null && dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    public interface dialogPositiveClick
    {
        void onOkClick();
    }
}
