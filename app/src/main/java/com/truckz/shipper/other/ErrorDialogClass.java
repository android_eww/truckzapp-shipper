package com.truckz.shipper.other;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truckz.shipper.R;


public class ErrorDialogClass {
    private Dialog dialog;
    TextView tv_Ok, tv_Message, tv_Title;
    LinearLayout ll_Ok;
    ImageView iv_close;

    public ErrorDialogClass(Context context)
    {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);
    }

    public void showDialog(String message, String title)
    {
        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if (message!=null && !message.equalsIgnoreCase(""))
        {
            tv_Message.setText(message);
        }
        else
        {
            tv_Message.setVisibility(View.GONE);
        }

        if (title!=null && !title.equalsIgnoreCase(""))
        {
            tv_Title.setText(title);
        }
        else
        {
            tv_Title.setVisibility(View.GONE);
        }
        tv_Title.setVisibility(View.GONE);
        dialog.show();
    }

    public void HideErrorDialog()
    {
        dialog.hide();
    }}
