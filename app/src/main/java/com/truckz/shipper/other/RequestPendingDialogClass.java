package com.truckz.shipper.other;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.truckz.shipper.R;
import com.wang.avi.AVLoadingIndicatorView;

public class RequestPendingDialogClass {

    private Context context;
    private int style=0;
    private Dialog dialog;
//    private ImageView ivLoder;

    public RequestPendingDialogClass(Context context, int style)
    {
        this.context = context;
        this.style = style;
    }

    public void showDialog()
    {
        showPopup();
    }

    public void hideDialog()
    {
        if (dialog!=null)
        {
            if (dialog.isShowing())
            {
                Log.e("call","Dialog class dialog.dismiss()");
                dialog.dismiss();
            }
            else
            {
                Log.e("call","Dialog class isShowing = false");
            }
        }
        else
        {
            Log.e("call","dialog = null");
        }
    }

    //******************************** showPopup() *****************************************************************************************
    private void showPopup() {

        AVLoadingIndicatorView avi;

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.request_pending_loader, null);

//        avi = (AVLoadingIndicatorView)view.findViewById(R.id.avi);

        dialog = new Dialog(context,0);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(view);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.TOP;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

    }//End...
}
